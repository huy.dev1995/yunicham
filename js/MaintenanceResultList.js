/*//////追加クリック////////////////////////////////////////////////////////*/

function buttonTsuika() {
  localStorage.setItem('key1', '0')
  // window.open("MaintenanceResultConfirm.html", "", "width=1200,height=650");
  window.location.href = "MaintenanceResultConfirm.html";
}

/*//////データの取得(初期処理)///////////////////////////////////////////////*/

var dataMaintenanceResultList = []

function getData() {

  var dataset_maintenanceResultList = []
  var lineMaintenanceResultList = JSON.parse(MaintenanceResultList);
  dataset_maintenanceResultList.push(lineMaintenanceResultList);
  for (var i = 0; i < lineMaintenanceResultList.length; i++) {
    Object.values(dataset_maintenanceResultList[0][i]);
    dataMaintenanceResultList.push(Object.values(dataset_maintenanceResultList[0][i]));
  }
}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////メインテーブル作成(初期処理)/////////////////////////////////////////*/

function makeMainTable() {

  var tBody = document.getElementById("myTbody");

  for (var count = 0; count < dataMaintenanceResultList.length; count++) {
    var row = tBody.insertRow(count);

    var c1 = row.insertCell(0);
    var c2 = row.insertCell(1);
    var c3 = row.insertCell(2);
    var c4 = row.insertCell(3);
    var c5 = row.insertCell(4);
    var c6 = row.insertCell(5);

    c1.style.cssText = "width: 20%; border-color:lightgray; text-align: center; border-top-style:none; height: 50px;";
    c2.style.cssText = "width: 36%; border-color:lightgray; text-align: left; border-top-style:none; padding-left: 15px; height: 50px;";
    c3.style.cssText = "width: 11%; border-color:lightgray; text-align: right; border-top-style:none; padding-right: 15px;";
    c4.style.cssText = "width: 11%; border-color:lightgray; text-align: right; border-top-style:none; padding-right: 15px;";
    c5.style.cssText = "width: 11%; border-color:lightgray; text-align: right; border-top-style:none; padding-right: 15px;";
    c6.style.cssText = "width: 11%; border-color:lightgray; text-align: right; border-top-style:none; padding-right: 15px;";

    c1.innerHTML = dataMaintenanceResultList[count][0];
    c2.innerHTML = dataMaintenanceResultList[count][1];
    c3.innerHTML = dataMaintenanceResultList[count][2];
    c4.innerHTML = dataMaintenanceResultList[count][3];
    c5.innerHTML = dataMaintenanceResultList[count][4];
    c6.innerHTML = dataMaintenanceResultList[count][5];
  }
}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////詳細クリック/////////////////////////////////////////////////////////*/
var gouki = "";
var jisshi = "";
var joutaiSL = "";
var joutaiTL = "";
var joutaiGM = "";
var joutaiShouhin = "";
/*///////////////////////////////////////////////////////////////////////////*/

$(document).ready(function () {
  var dateControl1 = document.querySelector('input[id="date1"]');
  var dateControl2 = document.querySelector('input[id="date2"]');
  ///////////////////////////////////
  var getToday = new Date();
  var dd = String(getToday.getDate()).padStart(2, '0');
  var mm = String(getToday.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = getToday.getFullYear();
  ///////////////////////////////////
  getToday = yyyy + '-' + mm + '-' + dd;
  dateControl1.value = getToday;
  dateControl2.value = getToday;

  getData();
  makeMainTable();
  $('#myTbody tr').addClass('item')
  ///////////////////////////////////
  $("#denpyoKensakuInputArea").slideToggle();
  $(".btn_minus_s").css("display", "none");
  $(".btn_plus_s").css("display", "table-row");
  // 
  //メインテーブルのクリック時の処理
  $("#myTbody tr").click(function () {
    // クリックした行の情報取得		
    gouki = $(this).children('td')[0].innerText;
    jisshi = $(this).children('td')[1].innerText;
    joutaiSL = $(this).children('td')[2].innerText;
    joutaiTL = $(this).children('td')[3].innerText;
    joutaiGM = $(this).children('td')[4].innerText;
    joutaiShouhin = $(this).children('td')[5].innerText;

    // 選択されている他の行があれば選択解除
    $(this).siblings().removeClass("selected");
    // クリックされた行を選択
    $(this).addClass("selected");
    // Set item  save in localstorage 
    var index0 = $(this).children()[0].textContent;
    var index1 = $(this).children()[1].textContent;
    var index2 = $(this).children()[2].textContent;
    var index3 = $(this).children()[3].textContent;
    var index4 = $(this).children()[4].textContent;
    var index5 = $(this).children()[5].textContent;

    localStorage.setItem('key1', '1');

    localStorage.setItem('index0', index0);
    localStorage.setItem('index1', index1);

    localStorage.setItem('index2', index2);
    localStorage.setItem('index3', index3);
    localStorage.setItem('index4', index4);

    localStorage.setItem('index5', index5);
    localStorage.setItem('keyHozenkekka', '1');

    window.location.href = "MaintenanceResultConfirm.html";
    // window.open("MaintenanceResultConfirm.html", "", "width=1200,height=650");

  });

});
/////////////////////////////////////検索ボックス///////////////////////////////////////////////
var count = 0;

function shosaiClick() {
  count++

  if (count % 2 == 0) {
    $("#denpyoKensakuInputArea").slideToggle(1);
    $(".btn_minus_s").css("display", "none");
    $(".btn_plus_s").css("display", "table-row");
  } else {
    $("#denpyoKensakuInputArea").slideToggle();
    $(".btn_minus_s").css("display", "table-row");
    $(".btn_plus_s").css("display", "none");
  }

}

// When click button sort
function sortTable(n) {
  var tBody, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  tBody = document.getElementById("myTbody");
  switching = true;
  //Set the sorting direction to ascending:
  dir = "asc";
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = tBody.rows;
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 0; i < (rows.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /*check if the two rows should switch place,
      based on the direction, asc or desc:*/
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      //Each time a switch is done, increase this count by 1:
      switchcount++;
    } else {
      /*If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again.*/
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}

// When click button sort(number)
function sortTableNumber(n) {
  var tBody, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  tBody = document.getElementById("myTbody");
  switching = true;
  //Set the sorting direction to ascending:
  dir = "asc";
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = tBody.rows;
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 0; i < (rows.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /*check if the two rows should switch place,
      based on the direction, asc or desc:*/
      if (dir == "asc") {
        if (parseInt(x.innerHTML) > parseInt(y.innerHTML)) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      } else if (dir == "desc") {
        if (parseInt(x.innerHTML) < parseInt(y.innerHTML)) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      //Each time a switch is done, increase this count by 1:
      switchcount++;
    } else {
      /*If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again.*/
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}