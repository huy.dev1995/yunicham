//****************************************************************************************
/////////////////////////////// soi code edit //////////////////////////////////////////
// Get sceen Change item When button Item registration of Stop project no.1 to no.10
//No.1
function aitemu_teishikoumoku_1() {
    aitemu_btn_counter = 1;

    $("#myModal").css('display', 'block');

}
//No.2
function aitemu_teishikoumoku_2() {
    aitemu_btn_counter = 2;

    $("#myModal").css('display', 'block');

}
//No.3
function aitemu_teishikoumoku_3() {
    aitemu_btn_counter = 3;

    $("#myModal").css('display', 'block');

}
//No.4
function aitemu_teishikoumoku_4() {
    aitemu_btn_counter = 4;

    $("#myModal").css('display', 'block');

}
//No.5
function aitemu_teishikoumoku_5() {
    aitemu_btn_counter = 5;

    $("#myModal").css('display', 'block');

}
//No.6
function aitemu_teishikoumoku_6() {
    aitemu_btn_counter = 6;

    $("#myModal").css('display', 'block');

}
//No.7  
function aitemu_teishikoumoku_7() {
    aitemu_btn_counter = 7;

    $("#myModal").css('display', 'block');

}
//No.8  
function aitemu_teishikoumoku_8() {
    aitemu_btn_counter = 8;

    $("#myModal").css('display', 'block');

}
//No.9 
function aitemu_teishikoumoku_9() {
    aitemu_btn_counter = 9;

    $("#myModal").css('display', 'block');

}
//No.10
function aitemu_teishikoumoku_10() {
    aitemu_btn_counter = 10;

    $("#myModal").css('display', 'block');

}
//****************************************************************************************         
// Click row 1 of sceen Change item
function aitemu_toroku1() {

    $("#myModal").css('display', 'none');
    $("#aitemu_row_1_div3_" + aitemu_btn_counter).css("display", "inline");
    $("#aitemu_row_2_div3_" + aitemu_btn_counter).css("display", "none");
    $("#mitouroku_div3_" + aitemu_btn_counter).css("display", "none");
}
// Click row 2 of sceen Change item
function aitemu_toroku2() {

    $("#myModal").css('display', 'none');
    $("#aitemu_row_1_div3_" + aitemu_btn_counter).css("display", "none");
    $("#aitemu_row_2_div3_" + aitemu_btn_counter).css("display", "inline");
    $("#mitouroku_div3_" + aitemu_btn_counter).css("display", "none");
}
//****************************************************************************************
// Get screen split setting
function bunwarisettei_button1() {

    var Modal_bunwarisettei = document.getElementById("myModal_bunwarisettei");
    Modal_bunwarisettei.style.display = "block";

}
// When on click button setting
function buttonAdd_bunwarisettei() {

    $("#teishikoumokuNo2").hide();
    $("#teishikoumokuNo3").hide();
    $("#teishikoumokuNo4").hide();
    $("#teishikoumokuNo5").hide();
    $("#teishikoumokuNo6").hide();
    $("#teishikoumokuNo7").hide();
    $("#teishikoumokuNo8").hide();
    $("#teishikoumokuNo9").hide();
    $("#teishikoumokuNo10").hide();

    var bunwarisuu_number = $("#inputBox_bunwarisettei0").val();

    for (var i = 0; i < bunwarisuu_number; i++) {
        var idTeishiKoumoku = "teishikoumokuNo" + (i + 1);
        $('#' + idTeishiKoumoku).show();
    }

    //---------------------------------------------
    $("#myModal_bunwarisettei").hide();
    //---------------------------------------------
    timeSumBunwari();
}
//****************************************************************************************
// Get the button add to that opens the modal 1 to 10
var tsuika_btn1_counter = 0;
function tsuika_teshikomoku1() {

    tsuika_btn1_counter++;
    document.getElementById("No").value = 1;
    document.getElementById("buttonEdit_chokotei").style.visibility = "hidden";
    document.getElementById("buttonDelete_chokotei").style.visibility = "hidden";
    document.getElementById("buttonAdd_chokotei").style.display = "block";
    document.getElementById("buttonCancel_chokotei").style.display = "block";

    document.getElementById("buttonEdit_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonDelete_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonAdd_choukiteishi").style.display = "block";
    document.getElementById("buttonCancel_choukiteishi").style.display = "block";

    var time = $("#miwariate_jikan_atai_1").text();

    var temp_miwariate = time.split(':');
    var temp_miwariate_seconds = (+temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    if (temp_miwariate_seconds >= 300) {
        // 長期停止
        $("#myModal_choukiteishi").css('display', 'block');
        resetModalChoukiteishi();
    } else {
        // チョコ停
        $("#myModal_chokoteishi").css('display', 'block');
        resetMoalChokotei();

    }
}
var tsuika_btn2_counter = 0;
function tsuika_teshikomoku2() {
    tsuika_btn2_counter++;

    document.getElementById("No").value = 2;

    document.getElementById("buttonEdit_chokotei").style.visibility = "hidden";
    document.getElementById("buttonDelete_chokotei").style.visibility = "hidden";
    document.getElementById("buttonAdd_chokotei").style.display = "block";
    document.getElementById("buttonCancel_chokotei").style.display = "block";

    document.getElementById("buttonEdit_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonDelete_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonAdd_choukiteishi").style.display = "block";
    document.getElementById("buttonCancel_choukiteishi").style.display = "block";

    var time = $("#miwariate_jikan_atai_2").text();

    var temp_miwariate = time.split(':');
    var temp_miwariate_seconds = (+temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    if (temp_miwariate_seconds >= 300) {
        // 長期停止
        $("#myModal_choukiteishi").css('display', 'block');
        resetModalChoukiteishi();
    } else {
        // チョコ停
        $("#myModal_chokoteishi").css('display', 'block');
        resetMoalChokotei();
    }

}
var tsuika_btn3_counter = 0;
function tsuika_teshikomoku3() {
    tsuika_btn3_counter++;
    document.getElementById("No").value = 3;
    document.getElementById("buttonEdit_chokotei").style.visibility = "hidden";
    document.getElementById("buttonDelete_chokotei").style.visibility = "hidden";
    document.getElementById("buttonAdd_chokotei").style.display = "block";
    document.getElementById("buttonCancel_chokotei").style.display = "block";

    document.getElementById("buttonEdit_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonDelete_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonAdd_choukiteishi").style.display = "block";
    document.getElementById("buttonCancel_choukiteishi").style.display = "block";

    var time = $("#miwariate_jikan_atai_3").text();

    var temp_miwariate = time.split(':');
    var temp_miwariate_seconds = (+temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    if (temp_miwariate_seconds >= 300) {
        // 長期停止
        $("#myModal_choukiteishi").css('display', 'block');
        resetModalChoukiteishi();
    } else {
        // チョコ停
        $("#myModal_chokoteishi").css('display', 'block');
        resetMoalChokotei();
    }

}
var tsuika_btn4_counter = 0;
function tsuika_teshikomoku4() {
    tsuika_btn4_counter++;

    document.getElementById("No").value = 4;
    document.getElementById("buttonEdit_chokotei").style.visibility = "hidden";
    document.getElementById("buttonDelete_chokotei").style.visibility = "hidden";
    document.getElementById("buttonAdd_chokotei").style.display = "block";
    document.getElementById("buttonCancel_chokotei").style.display = "block";

    document.getElementById("buttonEdit_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonDelete_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonAdd_choukiteishi").style.display = "block";
    document.getElementById("buttonCancel_choukiteishi").style.display = "block";

    var time = $("#miwariate_jikan_atai_4").text();

    var temp_miwariate = time.split(':');
    var temp_miwariate_seconds = (+temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    if (temp_miwariate_seconds >= 300) {
        // 長期停止
        $("#myModal_choukiteishi").css('display', 'block');
        resetModalChoukiteishi();
    } else {
        // チョコ停
        $("#myModal_chokoteishi").css('display', 'block');
        resetMoalChokotei();
    }
}
var tsuika_btn5_counter = 0;
function tsuika_teshikomoku5() {
    tsuika_btn5_counter++;

    document.getElementById("No").value = 5;
    document.getElementById("buttonEdit_chokotei").style.visibility = "hidden";
    document.getElementById("buttonDelete_chokotei").style.visibility = "hidden";
    document.getElementById("buttonAdd_chokotei").style.display = "block";
    document.getElementById("buttonCancel_chokotei").style.display = "block";
    document.getElementById("buttonEdit_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonDelete_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonAdd_choukiteishi").style.display = "block";
    document.getElementById("buttonCancel_choukiteishi").style.display = "block";

    var time = $("#miwariate_jikan_atai_5").text();

    var temp_miwariate = time.split(':');
    var temp_miwariate_seconds = (+temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    if (temp_miwariate_seconds >= 300) {
        // 長期停止
        $("#myModal_choukiteishi").css('display', 'block');
        resetModalChoukiteishi();
    } else {
        // チョコ停
        $("#myModal_chokoteishi").css('display', 'block');
        resetMoalChokotei();
    }
}
var tsuika_btn6_counter = 0;
function tsuika_teshikomoku6() {
    tsuika_btn6_counter++;

    document.getElementById("No").value = 6;

    document.getElementById("buttonEdit_chokotei").style.visibility = "hidden";
    document.getElementById("buttonDelete_chokotei").style.visibility = "hidden";
    document.getElementById("buttonAdd_chokotei").style.display = "block";
    document.getElementById("buttonCancel_chokotei").style.display = "block";

    document.getElementById("buttonEdit_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonDelete_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonAdd_choukiteishi").style.display = "block";
    document.getElementById("buttonCancel_choukiteishi").style.display = "block";

    var time = $("#miwariate_jikan_atai_6").text();

    var temp_miwariate = time.split(':');
    var temp_miwariate_seconds = (+temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    if (temp_miwariate_seconds >= 300) {
        // 長期停止
        $("#myModal_choukiteishi").css('display', 'block');
        resetModalChoukiteishi();
    } else {
        // チョコ停
        $("#myModal_chokoteishi").css('display', 'block');
        resetMoalChokotei();
    }

}
var tsuika_btn7_counter = 0;
function tsuika_teshikomoku7() {
    tsuika_btn7_counter++;
    resetModalChoukiteishi();
    document.getElementById("No").value = 7;

    document.getElementById("buttonEdit_chokotei").style.visibility = "hidden";
    document.getElementById("buttonDelete_chokotei").style.visibility = "hidden";
    document.getElementById("buttonAdd_chokotei").style.display = "block";
    document.getElementById("buttonCancel_chokotei").style.display = "block";

    document.getElementById("buttonEdit_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonDelete_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonAdd_choukiteishi").style.display = "block";
    document.getElementById("buttonCancel_choukiteishi").style.display = "block";

    var time = $("#miwariate_jikan_atai_7").text();

    var temp_miwariate = time.split(':');
    var temp_miwariate_seconds = (+temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    if (temp_miwariate_seconds >= 300) {
        // 長期停止
        $("#myModal_choukiteishi").css('display', 'block');
        resetModalChoukiteishi();
    } else {
        // チョコ停
        $("#myModal_chokoteishi").css('display', 'block');
        resetMoalChokotei();
    }

}
var tsuika_btn8_counter = 0;
function tsuika_teshikomoku8() {
    tsuika_btn8_counter++;

    document.getElementById("No").value = 8;

    document.getElementById("buttonEdit_chokotei").style.visibility = "hidden";
    document.getElementById("buttonDelete_chokotei").style.visibility = "hidden";
    document.getElementById("buttonAdd_chokotei").style.display = "block";
    document.getElementById("buttonCancel_chokotei").style.display = "block";

    document.getElementById("buttonEdit_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonDelete_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonAdd_choukiteishi").style.display = "block";
    document.getElementById("buttonCancel_choukiteishi").style.display = "block";

    var time = $("#miwariate_jikan_atai_8").text();

    var temp_miwariate = time.split(':');
    var temp_miwariate_seconds = (+temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    if (temp_miwariate_seconds >= 300) {
        // 長期停止
        $("#myModal_choukiteishi").css('display', 'block');
        resetModalChoukiteishi();
    } else {
        // チョコ停
        $("#myModal_chokoteishi").css('display', 'block');
        resetMoalChokotei();
    }
}
var tsuika_btn9_counter = 0;
function tsuika_teshikomoku9() {
    tsuika_btn9_counter++;

    document.getElementById("No").value = 9;
    document.getElementById("buttonEdit_chokotei").style.visibility = "hidden";
    document.getElementById("buttonDelete_chokotei").style.visibility = "hidden";
    document.getElementById("buttonAdd_chokotei").style.display = "block";
    document.getElementById("buttonCancel_chokotei").style.display = "block";

    document.getElementById("buttonEdit_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonDelete_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonAdd_choukiteishi").style.display = "block";
    document.getElementById("buttonCancel_choukiteishi").style.display = "block";

    var time = $("#miwariate_jikan_atai_9").text();

    var temp_miwariate = time.split(':');
    var temp_miwariate_seconds = (+temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    if (temp_miwariate_seconds >= 300) {
        // 長期停止
        $("#myModal_choukiteishi").css('display', 'block');
        resetModalChoukiteishi();
    } else {
        // チョコ停
        $("#myModal_chokoteishi").css('display', 'block');
        resetMoalChokotei();
    }

}
var tsuika_btn10_counter = 0;
function tsuika_teshikomoku10() {
    tsuika_btn10_counter++;

    document.getElementById("No").value = 10;
    document.getElementById("buttonEdit_chokotei").style.visibility = "hidden";
    document.getElementById("buttonDelete_chokotei").style.visibility = "hidden";
    document.getElementById("buttonAdd_chokotei").style.display = "block";
    document.getElementById("buttonCancel_chokotei").style.display = "block";

    document.getElementById("buttonEdit_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonDelete_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonAdd_choukiteishi").style.display = "block";
    document.getElementById("buttonCancel_choukiteishi").style.display = "block";

    var time = $("#miwariate_jikan_atai_10").text();

    var temp_miwariate = time.split(':');
    var temp_miwariate_seconds = (+temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    if (temp_miwariate_seconds >= 300) {
        // 長期停止
        $("#myModal_choukiteishi").css('display', 'block');
        resetModalChoukiteishi();
    } else {
        // チョコ停
        $("#myModal_chokoteishi").css('display', 'block');
        resetMoalChokotei();
    }

}
// Reset modal choukiteushi
function resetModalChoukiteishi() {
    document.getElementById("inputBox_choukiteishi1").value = "";
    document.getElementById("inputBox_choukiteishi2").value = "";
    document.getElementById("inputBox_choukiteishi3").value = "";
}
// Reset modal chokotei
function resetMoalChokotei() {
    document.getElementById("inputBox_chokotei1").value = "";
    document.getElementById("inputBox_chokotei2").value = "";
    document.getElementById("inputBox_chokotei3").value = "";
}
//****************************************************************************************
// When click button add choukiteishi 
var rows_choukiteishi = 0;
function buttonAdd_choukiteishi() {
    rows_choukiteishi++;
    var No = document.getElementById("No").value;
    var taisakushubetsu_choukiteishi = $("#select_choukiteishi3").val();
    var taisakureberu_choukiteishi = $("#select_choukiteishi2").val();
    var youinshubetsu_choukiteishi = $("#select_choukiteishi1").val();
    var youin1_choukiteishi = $("#inputBox_choukiteishi1").val();
    var youin2_choukiteishi = $("#inputBox_choukiteishi2").val();
    var taisaku_choukiteishi = $("#inputBox_choukiteishi3").val();

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var markup = "<tr>" + "<input type='hidden' id='rowA" + rows_choukiteishi + "' value='" + rows_choukiteishi + "'>" +
        "<td id='rowBB" + No + rows_choukiteishi + "'style='text-align: center'>" + taisakushubetsu_choukiteishi + "</td><td style='text-align: center' id='rowCC" + No + rows_choukiteishi + "'>" + taisakureberu_choukiteishi + "</td><td id='rowDD" + No + rows_choukiteishi + "' style='text-align: center'>" + youinshubetsu_choukiteishi + "</td><td id='rowEE" + No + rows_choukiteishi + "'>" + youin1_choukiteishi + "</td><td id='rowFF" + No + rows_choukiteishi + "'>" + youin2_choukiteishi + "</td><td id='rowGG" + No + rows_choukiteishi + "'>" + taisaku_choukiteishi + "<input type='hidden' name='youintaisaku_choukiteishi' value='2'></td></tr>";
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    //***************************************************
    //-----------------------1---------------------------
    if (tsuika_btn1_counter > 0) {
        $(".tableBody_teishikomoku_No1").append(markup);
        tsuika_btn1_counter = 0;

    }
    //-----------------------1---------------------------
    //***************************************************
    //------------------------2--------------------------
    if (tsuika_btn2_counter > 0) {
        $(".tableBody_teishikomoku_No2").append(markup);
        tsuika_btn2_counter = 0;

    }
    //------------------------2--------------------------
    //***************************************************
    //------------------------3--------------------------
    if (tsuika_btn3_counter > 0) {
        $(".tableBody_teishikomoku_No3").append(markup);
        tsuika_btn3_counter = 0;

    }
    //------------------------3--------------------------
    //***************************************************
    //------------------------4--------------------------
    if (tsuika_btn4_counter > 0) {
        $(".tableBody_teishikomoku_No4").append(markup);
        tsuika_btn4_counter = 0;

    }
    //------------------------4--------------------------
    //***************************************************
    //------------------------5--------------------------
    if (tsuika_btn5_counter > 0) {
        $(".tableBody_teishikomoku_No5").append(markup);
        tsuika_btn5_counter = 0;

    }
    //------------------------5--------------------------
    //***************************************************
    //------------------------6--------------------------
    if (tsuika_btn6_counter > 0) {
        $(".tableBody_teishikomoku_No6").append(markup);
        tsuika_btn6_counter = 0;

    }
    //------------------------6--------------------------
    //***************************************************
    //------------------------7--------------------------
    if (tsuika_btn7_counter > 0) {
        $(".tableBody_teishikomoku_No7").append(markup);
        tsuika_btn7_counter = 0;

    }
    //------------------------7--------------------------
    //***************************************************
    //------------------------8--------------------------
    if (tsuika_btn8_counter > 0) {
        $(".tableBody_teishikomoku_No8").append(markup);
        tsuika_btn8_counter = 0;

    }
    //------------------------8--------------------------
    //***************************************************
    //------------------------9--------------------------
    if (tsuika_btn9_counter > 0) {
        $(".tableBody_teishikomoku_No9").append(markup);
        tsuika_btn9_counter = 0;

    }
    //------------------------9--------------------------
    //***************************************************
    //------------------------10-------------------------
    if (tsuika_btn10_counter > 0) {
        $(".tableBody_teishikomoku_No10").append(markup);
        tsuika_btn10_counter = 0;

    }
    //------------------------10-------------------------
    //***************************************************

    $('input[name=youintaisaku_choukiteishi]').addClass("choukiteishi");

    $("#myModal_choukiteishi").hide();


    // -----------------------------------teishikomoku_No1--------------------------------------
    $("#teishikomoku_No1").on("click", function (e) {
        $("#tableEventShifter").focus();
    });

    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No1 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 1;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_1 = document.getElementById("miwariate_jikan_atai_1").textContent;

            var arai1 = miwariate_jikan_atai_1.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            } else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("select_choukiteishi1").value = "";
                document.getElementById("select_choukiteishi2").value = "";
                document.getElementById("select_choukiteishi3").value = "";

                document.getElementById("inputBox_choukiteishi1").value = "";
                document.getElementById("inputBox_choukiteishi2").value = "";
                document.getElementById("inputBox_choukiteishi3").value = "";

                var select1_1 = document.getElementById("rowDD1" + rowValue).textContent;
                var select2_1 = document.getElementById("rowCC1" + rowValue).textContent;
                var select3_1 = document.getElementById("rowBB1" + rowValue).textContent;

                var inpt1_1 = document.getElementById("rowEE1" + rowValue).textContent;
                var inpt2_1 = document.getElementById("rowFF1" + rowValue).textContent;
                var inpt3_1 = document.getElementById("rowGG1" + rowValue).textContent;

                $("#select_choukiteishi1 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1_1;
                }).prop('selected', true);

                $("#select_choukiteishi2 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2_1;
                }).prop('selected', true);

                $("#select_choukiteishi3 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3_1;
                }).prop('selected', true);

                document.getElementById("inputBox_choukiteishi1").value = inpt1_1
                document.getElementById("inputBox_choukiteishi2").value = inpt2_1
                document.getElementById("inputBox_choukiteishi3").value = inpt3_1

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }

            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end
        }


    });
    // -----------------------------------teishikomoku_No1--------------------------------------


    // -----------------------------------teishikomoku_No2--------------------------------------
    $("#teishikomoku_No2").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No2 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 2;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_2 = document.getElementById("miwariate_jikan_atai_2").textContent;

            var arai1 = miwariate_jikan_atai_2.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            } else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("select_choukiteishi1").value = "";
                document.getElementById("select_choukiteishi2").value = "";
                document.getElementById("select_choukiteishi3").value = "";

                document.getElementById("inputBox_choukiteishi1").value = "";
                document.getElementById("inputBox_choukiteishi2").value = "";
                document.getElementById("inputBox_choukiteishi3").value = "";

                var select1_2 = document.getElementById("rowDD2" + rowValue).textContent;
                var select2_2 = document.getElementById("rowCC2" + rowValue).textContent;
                var select3_2 = document.getElementById("rowBB2" + rowValue).textContent;

                var inpt1_2 = document.getElementById("rowEE2" + rowValue).textContent;
                var inpt2_2 = document.getElementById("rowFF2" + rowValue).textContent;
                var inpt3_3 = document.getElementById("rowGG2" + rowValue).textContent;

                $("#select_choukiteishi1 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1_2;
                }).prop('selected', true);

                $("#select_choukiteishi2 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2_2;
                }).prop('selected', true);

                $("#select_choukiteishi3 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3_2;
                }).prop('selected', true);

                document.getElementById("inputBox_choukiteishi1").value = inpt1_2;
                document.getElementById("inputBox_choukiteishi2").value = inpt2_2;
                document.getElementById("inputBox_choukiteishi3").value = inpt3_3;
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }

            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end
        }

    });
    // -----------------------------------teishikomoku_No2--------------------------------------

    // -----------------------------------teishikomoku_No3--------------------------------------
    $("#teishikomoku_No3").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No3 tbody tr").on("click", function (e) {


        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 3;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_3 = document.getElementById("miwariate_jikan_atai_3").textContent;

            var arai1 = miwariate_jikan_atai_3.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            } else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("select_choukiteishi1").value = "";
                document.getElementById("select_choukiteishi2").value = "";
                document.getElementById("select_choukiteishi3").value = "";

                document.getElementById("inputBox_choukiteishi1").value = "";
                document.getElementById("inputBox_choukiteishi2").value = "";
                document.getElementById("inputBox_choukiteishi3").value = "";

                var select1_3 = document.getElementById("rowDD3" + rowValue).textContent;
                var select2_3 = document.getElementById("rowCC3" + rowValue).textContent;
                var select3_3 = document.getElementById("rowBB3" + rowValue).textContent;

                var inpt1_3 = document.getElementById("rowEE3" + rowValue).textContent;
                var inpt2_3 = document.getElementById("rowFF3" + rowValue).textContent;
                var inpt3_3 = document.getElementById("rowGG3" + rowValue).textContent;

                $("#select_choukiteishi1 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1_3;
                }).prop('selected', true);

                $("#select_choukiteishi2 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2_3;
                }).prop('selected', true);

                $("#select_choukiteishi3 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3_3;
                }).prop('selected', true);

                document.getElementById("inputBox_choukiteishi1").value = inpt1_3;
                document.getElementById("inputBox_choukiteishi2").value = inpt2_3;
                document.getElementById("inputBox_choukiteishi3").value = inpt3_3;

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }

            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }


    });

    // -----------------------------------teishikomoku_No3--------------------------------------

    // -----------------------------------teishikomoku_No4--------------------------------------
    $("#teishikomoku_No4").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No4 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 4;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_4 = document.getElementById("miwariate_jikan_atai_4").textContent;

            var arai1 = miwariate_jikan_atai_4.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }
            else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("select_choukiteishi1").value = "";
                document.getElementById("select_choukiteishi2").value = "";
                document.getElementById("select_choukiteishi3").value = "";

                document.getElementById("inputBox_choukiteishi1").value = "";
                document.getElementById("inputBox_choukiteishi2").value = "";
                document.getElementById("inputBox_choukiteishi3").value = "";

                var select1_4 = document.getElementById("rowDD4" + rowValue).textContent;
                var select2_4 = document.getElementById("rowCC4" + rowValue).textContent;
                var select3_4 = document.getElementById("rowBB4" + rowValue).textContent;

                var inpt1_4 = document.getElementById("rowEE4" + rowValue).textContent;
                var inpt2_4 = document.getElementById("rowFF4" + rowValue).textContent;
                var inpt3_4 = document.getElementById("rowGG4" + rowValue).textContent;

                $("#select_choukiteishi1 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1_4;
                }).prop('selected', true);

                $("#select_choukiteishi2 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2_4;
                }).prop('selected', true);

                $("#select_choukiteishi3 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3_4;
                }).prop('selected', true);

                // document.getElementById("select1_chokotei").value = select1;
                // document.getElementById("select2_chokotei").value = select2;
                // document.getElementById("select3_chokotei").value = select3;
                document.getElementById("inputBox_choukiteishi1").value = inpt1_4;
                document.getElementById("inputBox_choukiteishi2").value = inpt2_4;
                document.getElementById("inputBox_choukiteishi3").value = inpt3_4;

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }


            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end
        }

    });
    // -----------------------------------teishikomoku_No4--------------------------------------

    // -----------------------------------teishikomoku_No5--------------------------------------
    $("#teishikomoku_No5").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No5 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 5;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_5 = document.getElementById("miwariate_jikan_atai_5").textContent;

            var arai1 = miwariate_jikan_atai_5.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }
            else {

                $("#myModal_choukiteishi").css('display', 'block');
                document.getElementById("select_choukiteishi1").value = "";
                document.getElementById("select_choukiteishi2").value = "";
                document.getElementById("select_choukiteishi3").value = "";

                document.getElementById("inputBox_choukiteishi1").value = "";
                document.getElementById("inputBox_choukiteishi2").value = "";
                document.getElementById("inputBox_choukiteishi3").value = "";

                var select1 = document.getElementById("rowDD5" + rowValue).textContent;
                var select2 = document.getElementById("rowCC5" + rowValue).textContent;
                var select3 = document.getElementById("rowBB5" + rowValue).textContent;

                var inpt1 = document.getElementById("rowEE5" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFF5" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGG5" + rowValue).textContent;

                $("#select_choukiteishi1 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select_choukiteishi2 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select_choukiteishi3 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);

                document.getElementById("inputBox_choukiteishi1").value = inpt1;
                document.getElementById("inputBox_choukiteishi2").value = inpt2;
                document.getElementById("inputBox_choukiteishi3").value = inpt3;
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }

            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }

    });
    // -----------------------------------teishikomoku_No5--------------------------------------

    // -----------------------------------teishikomoku_No6--------------------------------------
    $("#teishikomoku_No6").on("click", function (e) {
        $("#tableEventShifter").focus();
    });

    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No6 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 6;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_6 = document.getElementById("miwariate_jikan_atai_6").textContent;

            var arai1 = miwariate_jikan_atai_6.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            } else {

                $("#myModal_choukiteishi").css('display', 'block');
                document.getElementById("select_choukiteishi1").value = "";
                document.getElementById("select_choukiteishi2").value = "";
                document.getElementById("select_choukiteishi3").value = "";

                document.getElementById("inputBox_choukiteishi1").value = "";
                document.getElementById("inputBox_choukiteishi2").value = "";
                document.getElementById("inputBox_choukiteishi3").value = "";

                var select1 = document.getElementById("rowDD6" + rowValue).textContent;
                var select2 = document.getElementById("rowCC6" + rowValue).textContent;
                var select3 = document.getElementById("rowBB6" + rowValue).textContent;

                var inpt1 = document.getElementById("rowEE6" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFF6" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGG6" + rowValue).textContent;

                $("#select_choukiteishi1 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select_choukiteishi2 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select_choukiteishi3 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);
                document.getElementById("inputBox_choukiteishi1").value = inpt1;
                document.getElementById("inputBox_choukiteishi2").value = inpt2;
                document.getElementById("inputBox_choukiteishi3").value = inpt3;
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }

            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }


    });
    // -----------------------------------teishikomoku_No6--------------------------------------

    // -----------------------------------teishikomoku_No7--------------------------------------
    $("#teishikomoku_No7").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No7 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 7;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {


            var miwariate_jikan_atai_7 = document.getElementById("miwariate_jikan_atai_7").textContent;

            var arai1 = miwariate_jikan_atai_7.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            } else {

                $("#myModal_choukiteishi").css('display', 'block');
                document.getElementById("select_choukiteishi1").value = "";
                document.getElementById("select_choukiteishi2").value = "";
                document.getElementById("select_choukiteishi3").value = "";

                document.getElementById("inputBox_choukiteishi1").value = "";
                document.getElementById("inputBox_choukiteishi2").value = "";
                document.getElementById("inputBox_choukiteishi3").value = "";

                var select1 = document.getElementById("rowDD7" + rowValue).textContent;
                var select2 = document.getElementById("rowCC7" + rowValue).textContent;
                var select3 = document.getElementById("rowBB7" + rowValue).textContent;

                var inpt1 = document.getElementById("rowEE7" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFF7" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGG7" + rowValue).textContent;

                $("#select_choukiteishi1 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select_choukiteishi2 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select_choukiteishi3 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);


                document.getElementById("inputBox_choukiteishi1").value = inpt1;
                document.getElementById("inputBox_choukiteishi2").value = inpt2;
                document.getElementById("inputBox_choukiteishi3").value = inpt3;
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }


            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }

    });
    // -----------------------------------teishikomoku_No7--------------------------------------

    // -----------------------------------teishikomoku_No8--------------------------------------
    $("#teishikomoku_No8").on("click", function (e) {
        $("#tableEventShifter").focus();
    });

    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No8 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 8;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {


            var miwariate_jikan_atai_8 = document.getElementById("miwariate_jikan_atai_8").textContent;

            var arai1 = miwariate_jikan_atai_8.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            } else {

                $("#myModal_choukiteishi").css('display', 'block');
                document.getElementById("select_choukiteishi1").value = "";
                document.getElementById("select_choukiteishi2").value = "";
                document.getElementById("select_choukiteishi3").value = "";

                document.getElementById("inputBox_choukiteishi1").value = "";
                document.getElementById("inputBox_choukiteishi2").value = "";
                document.getElementById("inputBox_choukiteishi3").value = "";

                var select1 = document.getElementById("rowDD8" + rowValue).textContent;
                var select2 = document.getElementById("rowCC8" + rowValue).textContent;
                var select3 = document.getElementById("rowBB8" + rowValue).textContent;

                var inpt1 = document.getElementById("rowEE8" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFF8" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGG8" + rowValue).textContent;

                $("#select_choukiteishi1 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select_choukiteishi2 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select_choukiteishi3 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);

                document.getElementById("inputBox_choukiteishi1").value = inpt1;
                document.getElementById("inputBox_choukiteishi2").value = inpt2;
                document.getElementById("inputBox_choukiteishi3").value = inpt3;
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }


            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end
        }

    });
    // -----------------------------------teishikomoku_No8--------------------------------------

    // -----------------------------------teishikomoku_No9--------------------------------------
    $("#teishikomoku_No9").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No9 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 9;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {


            var miwariate_jikan_atai_9 = document.getElementById("miwariate_jikan_atai_9").textContent;

            var arai1 = miwariate_jikan_atai_9.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            } else {

                $("#myModal_choukiteishi").css('display', 'block');
                document.getElementById("select_choukiteishi1").value = "";
                document.getElementById("select_choukiteishi2").value = "";
                document.getElementById("select_choukiteishi3").value = "";

                document.getElementById("inputBox_choukiteishi1").value = "";
                document.getElementById("inputBox_choukiteishi2").value = "";
                document.getElementById("inputBox_choukiteishi3").value = "";

                var select1 = document.getElementById("rowDD9" + rowValue).textContent;
                var select2 = document.getElementById("rowCC9" + rowValue).textContent;
                var select3 = document.getElementById("rowBB9" + rowValue).textContent;

                var inpt1 = document.getElementById("rowEE9" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFF9" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGG9" + rowValue).textContent;

                $("#select_choukiteishi1 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select_choukiteishi2 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select_choukiteishi3 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);


                document.getElementById("inputBox_choukiteishi1").value = inpt1;
                document.getElementById("inputBox_choukiteishi2").value = inpt2;
                document.getElementById("inputBox_choukiteishi3").value = inpt3;
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }

            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }

    });
    // -----------------------------------teishikomoku_No9--------------------------------------

    // -----------------------------------teishikomoku_No10--------------------------------------
    $("#teishikomoku_No10").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No10 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 10;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {


            var miwariate_jikan_atai_10 = document.getElementById("miwariate_jikan_atai_10").textContent;

            var arai1 = miwariate_jikan_atai_10.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            } else {

                $("#myModal_choukiteishi").css('display', 'block');
                document.getElementById("select_choukiteishi1").value = "";
                document.getElementById("select_choukiteishi2").value = "";
                document.getElementById("select_choukiteishi3").value = "";

                document.getElementById("inputBox_choukiteishi1").value = "";
                document.getElementById("inputBox_choukiteishi2").value = "";
                document.getElementById("inputBox_choukiteishi3").value = "";

                var select1 = document.getElementById("rowDD10" + rowValue).textContent;
                var select2 = document.getElementById("rowCC10" + rowValue).textContent;
                var select3 = document.getElementById("rowBB10" + rowValue).textContent;

                var inpt1 = document.getElementById("rowEE10" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFF10" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGG10" + rowValue).textContent;

                $("#select_choukiteishi1 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select_choukiteishi2 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select_choukiteishi3 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);


                document.getElementById("inputBox_choukiteishi1").value = inpt1;
                document.getElementById("inputBox_choukiteishi2").value = inpt2;
                document.getElementById("inputBox_choukiteishi3").value = inpt3;
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }

            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }

    });
    // -----------------------------------teishikomoku_No10--------------------------------------

}
// When click button delete choukiteishi
function buttonDelete_choukiteishi() {


    $("#teishikomoku_No1 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }

    });

    $("#teishikomoku_No2 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    });

    $("#teishikomoku_No3 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    });

    $("#teishikomoku_No4 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    });

    $("#teishikomoku_No5 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    });

    $("#teishikomoku_No6 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    });


    $("#teishikomoku_No7 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    });


    $("#teishikomoku_No8 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    });

    $("#teishikomoku_No9 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    })


    $("#teishikomoku_No10 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    });


    //---------------------------------------------
    $("#myModal_choukiteishi").hide();
    //---------------------------------------------

}
// When click button edit choukiteishi
function buttonEdit_choukiteishi() {
    var no = document.getElementById("No").value;
    var indexe = document.getElementById("rowA").value;
    var aa = document.getElementById("inputBox_choukiteishi1").value;
    var bb = document.getElementById("inputBox_choukiteishi2").value;
    var cc = document.getElementById("select_choukiteishi1").value;
    var dd = document.getElementById("inputBox_choukiteishi3").value;
    var ee = document.getElementById("select_choukiteishi2").value;
    var ff = document.getElementById("select_choukiteishi3").value;
    // No.1
    if (no == "1") {
        $("#rowAA1" + indexe).text(indexe);
        $("#rowBB1" + indexe).text(ff);
        $("#rowCC1" + indexe).text(ee);
        $("#rowDD1" + indexe).text(cc);
        $("#rowEE1" + indexe).text(aa);
        $("#rowFF1" + indexe).text(bb);

        document.getElementById('rowGG1' + indexe).innerHTML = dd + "<input type='hidden' name='youintaisaku_choukiteishi' value='2'>";

        $("#myModal_choukiteishi").css('display', 'none');

    }

    //No.2
    if (no == "2") {
        $("#rowAA2" + indexe).text(indexe);
        $("#rowBB2" + indexe).text(ff);
        $("#rowCC2" + indexe).text(ee);
        $("#rowDD2" + indexe).text(cc);
        $("#rowEE2" + indexe).text(aa);
        $("#rowFF2" + indexe).text(bb);

        document.getElementById('rowGG2' + indexe).innerHTML = dd + "<input type='hidden' name='youintaisaku_choukiteishi' value='2'>";

        $("#myModal_choukiteishi").css('display', 'none');

    }
    // No.3
    if (no == "3") {

        $("#rowAA3" + indexe).text(indexe);
        $("#rowBB3" + indexe).text(ff);
        $("#rowCC3" + indexe).text(ee);
        $("#rowDD3" + indexe).text(cc);
        $("#rowEE3" + indexe).text(aa);
        $("#rowFF3" + indexe).text(bb);

        document.getElementById('rowGG3' + indexe).innerHTML = dd + "<input type='hidden' name='youintaisaku_choukiteishi' value='2'>";

        $("#myModal_choukiteishi").css('display', 'none');

    }
    // No.4
    if (no == "4") {

        $("#rowAA4" + indexe).text(indexe);
        $("#rowBB4" + indexe).text(ff);
        $("#rowCC4" + indexe).text(ee);
        $("#rowDD4" + indexe).text(cc);
        $("#rowEE4" + indexe).text(aa);
        $("#rowFF4" + indexe).text(bb);

        document.getElementById('rowGG4' + indexe).innerHTML = dd + "<input type='hidden' name='youintaisaku_choukiteishi' value='2'>";

        $("#myModal_choukiteishi").css('display', 'none');

    }

    //No.5
    if (no == "5") {

        $("#rowAA5" + indexe).text(indexe);
        $("#rowBB5" + indexe).text(ff);
        $("#rowCC5" + indexe).text(ee);
        $("#rowDD5" + indexe).text(cc);
        $("#rowEE5" + indexe).text(aa);
        $("#rowFF5" + indexe).text(bb);

        document.getElementById('rowGG5' + indexe).innerHTML = dd + "<input type='hidden' name='youintaisaku_choukiteishi' value='2'>";

        $("#myModal_choukiteishi").css('display', 'none');

    }
    // No.6
    if (no == "6") {

        $("#rowAA6" + indexe).text(indexe);
        $("#rowBB6" + indexe).text(ff);
        $("#rowCC6" + indexe).text(ee);
        $("#rowDD6" + indexe).text(cc);
        $("#rowEE6" + indexe).text(aa);
        $("#rowFF6" + indexe).text(bb);

        document.getElementById('rowGG6' + indexe).innerHTML = dd + "<input type='hidden' name='youintaisaku_choukiteishi' value='2'>";

        $("#myModal_choukiteishi").css('display', 'none');

    }
    // No.7
    if (no == "7") {

        $("#rowAA7" + indexe).text(indexe);
        $("#rowBB7" + indexe).text(ff);
        $("#rowCC7" + indexe).text(ee);
        $("#rowDD7" + indexe).text(cc);
        $("#rowEE7" + indexe).text(aa);
        $("#rowFF7" + indexe).text(bb);

        document.getElementById('rowGG7' + indexe).innerHTML = dd + "<input type='hidden' name='youintaisaku_choukiteishi' value='2'>";

        $("#myModal_choukiteishi").css('display', 'none');

    }
    // No.8
    if (no == "8") {

        $("#rowAA8" + indexe).text(indexe);
        $("#rowBB8" + indexe).text(ff);
        $("#rowCC8" + indexe).text(ee);
        $("#rowDD8" + indexe).text(cc);
        $("#rowEE8" + indexe).text(aa);
        $("#rowFF8" + indexe).text(bb);

        document.getElementById('rowGG8' + indexe).innerHTML = dd + "<input type='hidden' name='youintaisaku_choukiteishi' value='2'>";

        $("#myModal_choukiteishi").css('display', 'none');

    }
    // No.9
    if (no == "9") {

        $("#rowAA9" + indexe).text(indexe);
        $("#rowBB9" + indexe).text(ff);
        $("#rowCC9" + indexe).text(ee);
        $("#rowDD9" + indexe).text(cc);
        $("#rowEE9" + indexe).text(aa);
        $("#rowFF9" + indexe).text(bb);

        document.getElementById('rowGG9' + indexe).innerHTML = dd + "<input type='hidden' name='youintaisaku_choukiteishi' value='2'>";

        $("#myModal_choukiteishi").css('display', 'none');

    }
    //No.10
    if (no == "10") {

        $("#rowAA10" + indexe).text(indexe);
        $("#rowBB10" + indexe).text(ff);
        $("#rowCC10" + indexe).text(ee);
        $("#rowDD10" + indexe).text(cc);
        $("#rowEE10" + indexe).text(aa);
        $("#rowFF10" + indexe).text(bb);

        document.getElementById('rowGG10' + indexe).innerHTML = dd + "<input type='hidden' name='youintaisaku_choukiteishi' value='2'>";

        $("#myModal_choukiteishi").css('display', 'none');

    }
}
// When click button cancel chokiteishi
function buttonCancel_choukiteishi() {
    $("#myModal_choukiteishi").hide();
}
//****************************************************************************************
// When click button add chokotei
var rows_chokotei = 0;
function buttonAdd_chokotei() {
    rows_chokotei++;
    var No = document.getElementById("No").value;
    //get the value from チョコ停 modal
    var taisakushubetsu_chokotei = $("#select1_chokotei").val();
    var taisakureberu_chokotei = $("#select2_chokotei").val();
    var youinshubetsu_chokotei = $("#select3_chokotei").val();
    var youin1_chokotei = $("#inputBox_chokotei1").val();
    var youin2_chokotei = $("#inputBox_chokotei2").val();
    var taisaku_chokotei = $("#inputBox_chokotei3").val();

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var markup = "<tr>" + "<input type='hidden' id='rowA" + rows_chokotei + "' value='" + rows_chokotei + "'>" +
        "<td id='rowBS" + No + rows_chokotei + "'style='text-align: center'>" + taisakushubetsu_chokotei + "</td><td style='text-align: center' id='rowCS" + No + rows_chokotei + "'>" + taisakureberu_chokotei + "</td><td id='rowDS" + No + rows_chokotei + "' style='text-align: center'>" + youinshubetsu_chokotei + "</td><td id='rowES" + No + rows_chokotei + "'>" + youin1_chokotei + "</td><td id='rowFS" + No + rows_chokotei + "'>" + youin2_chokotei + "</td><td id='rowGS" + No + rows_chokotei + "'>" + taisaku_chokotei + "<input type='hidden' name='youintaisaku_chokotei' value='2'></td></tr>";
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //***************************************************
    //-----------------------1---------------------------
    if (tsuika_btn1_counter > 0) {
        $(".tableBody_teishikomoku_No1").append(markup);
        tsuika_btn1_counter = 0;

    }
    //-----------------------1---------------------------
    //***************************************************
    //------------------------2--------------------------
    if (tsuika_btn2_counter > 0) {

        $(".tableBody_teishikomoku_No2").append(markup);
        tsuika_btn2_counter = 0;

    }
    //------------------------2--------------------------
    //***************************************************
    //------------------------3--------------------------
    if (tsuika_btn3_counter > 0) {
        $(".tableBody_teishikomoku_No3").append(markup);
        tsuika_btn3_counter = 0;

    }
    //------------------------3--------------------------
    //***************************************************
    //------------------------4--------------------------
    if (tsuika_btn4_counter > 0) {
        $(".tableBody_teishikomoku_No4").append(markup);
        tsuika_btn4_counter = 0;

    }
    //------------------------4--------------------------
    //***************************************************
    //------------------------5--------------------------
    if (tsuika_btn5_counter > 0) {
        $(".tableBody_teishikomoku_No5").append(markup);
        tsuika_btn5_counter = 0;

    }
    //------------------------5--------------------------
    //***************************************************
    //------------------------6--------------------------
    if (tsuika_btn6_counter > 0) {

        $(".tableBody_teishikomoku_No6").append(markup);
        tsuika_btn6_counter = 0;

    }
    //------------------------6--------------------------
    //***************************************************
    //------------------------7--------------------------
    if (tsuika_btn7_counter > 0) {
        $(".tableBody_teishikomoku_No7").append(markup);
        tsuika_btn7_counter = 0;

    }
    //------------------------7--------------------------
    //***************************************************
    //------------------------8--------------------------
    if (tsuika_btn8_counter > 0) {
        $(".tableBody_teishikomoku_No8").append(markup);
        tsuika_btn8_counter = 0;

    }
    //------------------------8--------------------------
    //***************************************************
    //------------------------9--------------------------
    if (tsuika_btn9_counter > 0) {
        $(".tableBody_teishikomoku_No9").append(markup);
        tsuika_btn9_counter = 0;

    }
    //------------------------9--------------------------
    //***************************************************
    //------------------------10-------------------------
    if (tsuika_btn10_counter > 0) {
        $(".tableBody_teishikomoku_No10").append(markup);
        tsuika_btn10_counter = 0;

    }
    //------------------------10-------------------------
    //***************************************************

    $('input[name=youintaisaku_chokotei]').addClass("chokotei");


    $("#myModal_chokoteishi").css('display', 'none');


    // -----------------------------------teishikomoku_No1--------------------------------------
    $("#teishikomoku_No1").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No1 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 1;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_1 = document.getElementById("miwariate_jikan_atai_1").textContent;

            var arai1 = miwariate_jikan_atai_1.split(":");
            console.log(arai1);
            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');

                document.getElementById("select1_chokotei").value = "";
                document.getElementById("select2_chokotei").value = "";
                document.getElementById("select3_chokotei").value = "";
                document.getElementById("inputBox_chokotei1").value = "";
                document.getElementById("inputBox_chokotei2").value = "";
                document.getElementById("inputBox_chokotei3").value = "";

                var select1 = document.getElementById("rowBS1" + rowValue).textContent;
                var select2 = document.getElementById("rowCS1" + rowValue).textContent;
                var select3 = document.getElementById("rowDS1" + rowValue).textContent;
                var inpt1 = document.getElementById("rowES1" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFS1" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGS1" + rowValue).textContent;

                $("#select1_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select2_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select3_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);
                document.getElementById("inputBox_chokotei1").value = inpt1;
                document.getElementById("inputBox_chokotei2").value = inpt2;
                document.getElementById("inputBox_chokotei3").value = inpt3;

                document.getElementById("buttonEdit_chokotei").style.visibility = "visible";
                document.getElementById("buttonDelete_chokotei").style.visibility = "visible";
                document.getElementById("buttonAdd_chokotei").style.display = "none";
                document.getElementById("buttonCancel_chokotei").style.display = "inline";

            } else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";

            }

            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }

    });
    // -----------------------------------teishikomoku_No1--------------------------------------


    // -----------------------------------teishikomoku_No2--------------------------------------
    $("#teishikomoku_No2").on("click", function (e) {
        $("#tableEventShifter").focus();
    });

    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No2 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;
        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 2;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_2 = document.getElementById("miwariate_jikan_atai_2").textContent;

            var arai1 = miwariate_jikan_atai_2.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');

                document.getElementById("select1_chokotei").value = "";
                document.getElementById("select2_chokotei").value = "";
                document.getElementById("select3_chokotei").value = "";
                document.getElementById("inputBox_chokotei1").value = "";
                document.getElementById("inputBox_chokotei2").value = "";
                document.getElementById("inputBox_chokotei3").value = "";

                var select1 = document.getElementById("rowBS2" + rowValue).textContent;
                var select2 = document.getElementById("rowCS2" + rowValue).textContent;
                var select3 = document.getElementById("rowDS2" + rowValue).textContent;
                var inpt1 = document.getElementById("rowES2" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFS2" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGS2" + rowValue).textContent;

                $("#select1_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select2_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select3_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);
                document.getElementById("inputBox_chokotei1").value = inpt1;
                document.getElementById("inputBox_chokotei2").value = inpt2;
                document.getElementById("inputBox_chokotei3").value = inpt3;

                document.getElementById("buttonEdit_chokotei").style.visibility = "visible";
                document.getElementById("buttonDelete_chokotei").style.visibility = "visible";
                document.getElementById("buttonAdd_chokotei").style.display = "none";
                document.getElementById("buttonCancel_chokotei").style.display = "inline";

            } else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }

            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end


        }

    });
    // -----------------------------------teishikomoku_No2--------------------------------------
    //soi add code teshikomoku
    // -----------------------------------teishikomoku_No3--------------------------------------
    $("#teishikomoku_No3").on("click", function (e) {
        $("#tableEventShifter").focus();
    });
    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No3 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 3;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_3 = document.getElementById("miwariate_jikan_atai_3").textContent;

            var arai1 = miwariate_jikan_atai_3.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("select1_chokotei").value = "";
                document.getElementById("select2_chokotei").value = "";
                document.getElementById("select3_chokotei").value = "";
                document.getElementById("inputBox_chokotei1").value = "";
                document.getElementById("inputBox_chokotei2").value = "";
                document.getElementById("inputBox_chokotei3").value = "";

                var select1 = document.getElementById("rowBS3" + rowValue).textContent;
                var select2 = document.getElementById("rowCS3" + rowValue).textContent;
                var select3 = document.getElementById("rowDS3" + rowValue).textContent;
                var inpt1 = document.getElementById("rowES3" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFS3" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGS3" + rowValue).textContent;
                $("#select1_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select2_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select3_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);

                document.getElementById("inputBox_chokotei1").value = inpt1;
                document.getElementById("inputBox_chokotei2").value = inpt2;
                document.getElementById("inputBox_chokotei3").value = inpt3;

                document.getElementById("buttonEdit_chokotei").style.visibility = "visible";
                document.getElementById("buttonDelete_chokotei").style.visibility = "visible";
                document.getElementById("buttonAdd_chokotei").style.display = "none";
                document.getElementById("buttonCancel_chokotei").style.display = "inline";

            } else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }


            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }

    });
    // -----------------------------------teishikomoku_No3--------------------------------------
    // -----------------------------------teishikomoku_No4--------------------------------------
    $("#teishikomoku_No4").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く) teishikoumokuNo4
    $("#teishikomoku_No4 tbody tr").on("click", function (e) {


        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 4;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {


            var miwariate_jikan_atai_4 = document.getElementById("miwariate_jikan_atai_4").textContent;

            var arai1 = miwariate_jikan_atai_4.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');

                document.getElementById("select1_chokotei").value = "";
                document.getElementById("select2_chokotei").value = "";
                document.getElementById("select3_chokotei").value = "";
                document.getElementById("inputBox_chokotei1").value = "";
                document.getElementById("inputBox_chokotei2").value = "";
                document.getElementById("inputBox_chokotei3").value = "";

                var select1 = document.getElementById("rowBS4" + rowValue).textContent;
                var select2 = document.getElementById("rowCS4" + rowValue).textContent;
                var select3 = document.getElementById("rowDS4" + rowValue).textContent;
                var inpt1 = document.getElementById("rowES4" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFS4" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGS4" + rowValue).textContent;

                $("#select1_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select2_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select3_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);
                document.getElementById("inputBox_chokotei1").value = inpt1;
                document.getElementById("inputBox_chokotei2").value = inpt2;
                document.getElementById("inputBox_chokotei3").value = inpt3;

                document.getElementById("buttonEdit_chokotei").style.visibility = "visible";
                document.getElementById("buttonDelete_chokotei").style.visibility = "visible";
                document.getElementById("buttonAdd_chokotei").style.display = "none";
                document.getElementById("buttonCancel_chokotei").style.display = "inline";

            } else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }


            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }

    });
    // -----------------------------------teishikomoku_No4--------------------------------------/
    // -----------------------------------teishikomoku_No5--------------------------------------
    $("#teishikomoku_No5").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No5 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 5;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_5 = document.getElementById("miwariate_jikan_atai_5").textContent;

            var arai1 = miwariate_jikan_atai_5.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');

                document.getElementById("select1_chokotei").value = "";
                document.getElementById("select2_chokotei").value = "";
                document.getElementById("select3_chokotei").value = "";
                document.getElementById("inputBox_chokotei1").value = "";
                document.getElementById("inputBox_chokotei2").value = "";
                document.getElementById("inputBox_chokotei3").value = "";

                var select1 = document.getElementById("rowBS5" + rowValue).textContent;
                var select2 = document.getElementById("rowCS5" + rowValue).textContent;
                var select3 = document.getElementById("rowDS5" + rowValue).textContent;
                var inpt1 = document.getElementById("rowES5" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFS5" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGS5" + rowValue).textContent;


                $("#select1_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select2_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select3_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);

                document.getElementById("inputBox_chokotei1").value = inpt1;
                document.getElementById("inputBox_chokotei2").value = inpt2;
                document.getElementById("inputBox_chokotei3").value = inpt3;

                document.getElementById("buttonEdit_chokotei").style.visibility = "visible";
                document.getElementById("buttonDelete_chokotei").style.visibility = "visible";
                document.getElementById("buttonAdd_chokotei").style.display = "none";
                document.getElementById("buttonCancel_chokotei").style.display = "inline";

            } else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }


            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end
        }

    });
    // -----------------------------------teishikomoku_No5--------------------------------------
    // -----------------------------------teishikomoku_No6--------------------------------------
    $("#teishikomoku_No6").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No6 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 6;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_6 = document.getElementById("miwariate_jikan_atai_6").textContent;

            var arai1 = miwariate_jikan_atai_6.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("select1_chokotei").value = "";
                document.getElementById("select2_chokotei").value = "";
                document.getElementById("select3_chokotei").value = "";
                document.getElementById("inputBox_chokotei1").value = "";
                document.getElementById("inputBox_chokotei2").value = "";
                document.getElementById("inputBox_chokotei3").value = "";

                var select1 = document.getElementById("rowBS6" + rowValue).textContent;
                var select2 = document.getElementById("rowCS6" + rowValue).textContent;
                var select3 = document.getElementById("rowDS6" + rowValue).textContent;
                var inpt1 = document.getElementById("rowES6" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFS6" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGS6" + rowValue).textContent;

                $("#select1_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select2_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select3_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);
                document.getElementById("inputBox_chokotei1").value = inpt1;
                document.getElementById("inputBox_chokotei2").value = inpt2;
                document.getElementById("inputBox_chokotei3").value = inpt3;

                document.getElementById("buttonEdit_chokotei").style.visibility = "visible";
                document.getElementById("buttonDelete_chokotei").style.visibility = "visible";
                document.getElementById("buttonAdd_chokotei").style.display = "none";
                document.getElementById("buttonCancel_chokotei").style.display = "inline";

            } else {

                $("#myModal_choukiteishi").css('display', 'block');

            }

            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }

    });
    // -----------------------------------teishikomoku_No6--------------------------------------
    // -----------------------------------teishikomoku_No7--------------------------------------
    $("#teishikomoku_No7").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No7 tbody tr").on("click", function (e) {


        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 7;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_7 = document.getElementById("miwariate_jikan_atai_7").textContent;

            var arai1 = miwariate_jikan_atai_7.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("select1_chokotei").value = "";
                document.getElementById("select2_chokotei").value = "";
                document.getElementById("select3_chokotei").value = "";
                document.getElementById("inputBox_chokotei1").value = "";
                document.getElementById("inputBox_chokotei2").value = "";
                document.getElementById("inputBox_chokotei3").value = "";
                var select1 = document.getElementById("rowBS7" + rowValue).textContent;
                var select2 = document.getElementById("rowCS7" + rowValue).textContent;
                var select3 = document.getElementById("rowDS7" + rowValue).textContent;
                var inpt1 = document.getElementById("rowES7" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFS7" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGS7" + rowValue).textContent;
                $("#select1_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select2_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select3_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);

                document.getElementById("inputBox_chokotei1").value = inpt1;
                document.getElementById("inputBox_chokotei2").value = inpt2;
                document.getElementById("inputBox_chokotei3").value = inpt3;

                document.getElementById("buttonEdit_chokotei").style.visibility = "visible";
                document.getElementById("buttonDelete_chokotei").style.visibility = "visible";
                document.getElementById("buttonAdd_chokotei").style.display = "none";
                document.getElementById("buttonCancel_chokotei").style.display = "inline";

            } else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }


            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }

    });
    // -----------------------------------teishikomoku_No7--------------------------------------
    // -----------------------------------teishikomoku_No8--------------------------------------
    $("#teishikomoku_No8").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No8 tbody tr").on("click", function (e) {


        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 8;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_8 = document.getElementById("miwariate_jikan_atai_8").textContent;

            var arai1 = miwariate_jikan_atai_8.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("select1_chokotei").value = "";
                document.getElementById("select2_chokotei").value = "";
                document.getElementById("select3_chokotei").value = "";
                document.getElementById("inputBox_chokotei1").value = "";
                document.getElementById("inputBox_chokotei2").value = "";
                document.getElementById("inputBox_chokotei3").value = "";

                var select1 = document.getElementById("rowBS8" + rowValue).textContent;
                var select2 = document.getElementById("rowCS8" + rowValue).textContent;
                var select3 = document.getElementById("rowDS8" + rowValue).textContent;
                var inpt1 = document.getElementById("rowES8" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFS8" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGS8" + rowValue).textContent;
                $("#select1_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select2_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select3_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);
                document.getElementById("inputBox_chokotei1").value = inpt1;
                document.getElementById("inputBox_chokotei2").value = inpt2;
                document.getElementById("inputBox_chokotei3").value = inpt3;

                document.getElementById("buttonEdit_chokotei").style.visibility = "visible";
                document.getElementById("buttonDelete_chokotei").style.visibility = "visible";
                document.getElementById("buttonAdd_chokotei").style.display = "none";
                document.getElementById("buttonCancel_chokotei").style.display = "inline";

            } else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }


            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }

    });
    // -----------------------------------teishikomoku_No8--------------------------------------
    // -----------------------------------teishikomoku_No9--------------------------------------
    $("#teishikomoku_No9").on("click", function (e) {
        $("#tableEventShifter").focus();
    });

    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No9 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 9;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_9 = document.getElementById("miwariate_jikan_atai_9").textContent;

            var arai1 = miwariate_jikan_atai_9.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("select1_chokotei").value = "";
                document.getElementById("select2_chokotei").value = "";
                document.getElementById("select3_chokotei").value = "";
                document.getElementById("inputBox_chokotei1").value = "";
                document.getElementById("inputBox_chokotei2").value = "";
                document.getElementById("inputBox_chokotei3").value = "";
                var select1 = document.getElementById("rowBS9" + rowValue).textContent;
                var select2 = document.getElementById("rowCS9" + rowValue).textContent;
                var select3 = document.getElementById("rowDS9" + rowValue).textContent;
                var inpt1 = document.getElementById("rowES9" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFS9" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGS9" + rowValue).textContent;
                $("#select1_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select2_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select3_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);
                document.getElementById("inputBox_chokotei1").value = inpt1;
                document.getElementById("inputBox_chokotei2").value = inpt2;
                document.getElementById("inputBox_chokotei3").value = inpt3;

                document.getElementById("buttonEdit_chokotei").style.visibility = "visible";
                document.getElementById("buttonDelete_chokotei").style.visibility = "visible";
                document.getElementById("buttonAdd_chokotei").style.display = "none";
                document.getElementById("buttonCancel_chokotei").style.display = "inline";
            } else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }


            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }

    });
    // -----------------------------------teishikomoku_No9--------------------------------------
    // -----------------------------------teishikomoku_No10--------------------------------------
    $("#teishikomoku_No10").on("click", function (e) {
        $("#tableEventShifter").focus();
    });

    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No10 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 10;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_10 = document.getElementById("miwariate_jikan_atai_10").textContent;

            var arai1 = miwariate_jikan_atai_10.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("select1_chokotei").value = "";
                document.getElementById("select2_chokotei").value = "";
                document.getElementById("select3_chokotei").value = "";
                document.getElementById("inputBox_chokotei1").value = "";
                document.getElementById("inputBox_chokotei2").value = "";
                document.getElementById("inputBox_chokotei3").value = "";

                var select1 = document.getElementById("rowBS10" + rowValue).textContent;
                var select2 = document.getElementById("rowCS10" + rowValue).textContent;
                var select3 = document.getElementById("rowDS10" + rowValue).textContent;
                var inpt1 = document.getElementById("rowES10" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFS10" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGS10" + rowValue).textContent;
                $("#select1_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select2_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select3_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);

                document.getElementById("inputBox_chokotei1").value = inpt1;
                document.getElementById("inputBox_chokotei2").value = inpt2;
                document.getElementById("inputBox_chokotei3").value = inpt3;

                document.getElementById("buttonEdit_chokotei").style.visibility = "visible";
                document.getElementById("buttonDelete_chokotei").style.visibility = "visible";
                document.getElementById("buttonAdd_chokotei").style.display = "none";
                document.getElementById("buttonCancel_chokotei").style.display = "inline";

            } else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("inputBox_choukiteishi1").value = inpt1;
                document.getElementById("inputBox_choukiteishi2").value = inpt2;
                document.getElementById("inputBox_choukiteishi3").value = inpt3;
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }

            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }

    });
    // -----------------------------------teishikomoku_No10--------------------------------------
}
// When click button cancel chokotei
function buttonCancel_chokotei() {
    $("#myModal_chokoteishi").css('display', 'none');
}
// When click button edit chokotei
function buttonEdit_chokotei() {
    var no = document.getElementById("No").value;
    var indexe = document.getElementById("rowA").value;
    var a = document.getElementById("inputBox_chokotei1").value;
    var b = document.getElementById("inputBox_chokotei2").value;
    var c = document.getElementById("select1_chokotei").value;
    var d = document.getElementById("inputBox_chokotei3").value;
    var e = document.getElementById("select2_chokotei").value;
    var f = document.getElementById("select3_chokotei").value;
    // No.1
    if (no == "1") {
        $("#rowAS1" + indexe).text(indexe);
        $("#rowBS1" + indexe).text(f);
        $("#rowCS1" + indexe).text(e);
        $("#rowDS1" + indexe).text(c);
        $("#rowES1" + indexe).text(a);
        $("#rowFS1" + indexe).text(b);
        document.getElementById('rowGS1' + indexe).innerHTML = d + "<input type='hidden' name='youintaisaku_chokotei' value='2'>";

        //---------------------------------------------
        $("#myModal_chokoteishi").css('display', 'none');

    }
    // No.2
    if (no == "2") {
        $("#rowAS2" + indexe).text(indexe);
        $("#rowBS2" + indexe).text(f);
        $("#rowCS2" + indexe).text(e);
        $("#rowDS2" + indexe).text(c);
        $("#rowES2" + indexe).text(a);
        $("#rowFS2" + indexe).text(b);

        document.getElementById('rowGS2' + indexe).innerHTML = d + "<input type='hidden' name='youintaisaku_chokotei' value='2'>";

        //---------------------------------------------
        $("#myModal_chokoteishi").css('display', 'none');

    }
    // No.3
    if (no == "3") {
        $("#rowAS3" + indexe).text(indexe);
        $("#rowBS3" + indexe).text(f);
        $("#rowCS3" + indexe).text(e);
        $("#rowDS3" + indexe).text(c);
        $("#rowES3" + indexe).text(a);
        $("#rowFS3" + indexe).text(b);
        document.getElementById('rowGS3' + indexe).innerHTML = d + "<input type='hidden' name='youintaisaku_chokotei' value='2'>";

        //---------------------------------------------
        $("#myModal_chokoteishi").css('display', 'none');

    }
    // No.4
    if (no == "4") {
        $("#rowAS4" + indexe).text(indexe);
        $("#rowBS4" + indexe).text(f);
        $("#rowCS4" + indexe).text(e);
        $("#rowDS4" + indexe).text(c);
        $("#rowES4" + indexe).text(a);
        $("#rowFS4" + indexe).text(b);

        document.getElementById('rowGS4' + indexe).innerHTML = d + "<input type='hidden' name='youintaisaku_chokotei' value='2'>";

        //---------------------------------------------
        $("#myModal_chokoteishi").css('display', 'none');

    }
    // No.5
    if (no == "5") {
        $("#rowAS5" + indexe).text(indexe);
        $("#rowBS5" + indexe).text(f);
        $("#rowCS5" + indexe).text(e);
        $("#rowDS5" + indexe).text(c);
        $("#rowES5" + indexe).text(a);
        $("#rowFS5" + indexe).text(b);

        document.getElementById('rowGS5' + indexe).innerHTML = d + "<input type='hidden' name='youintaisaku_chokotei' value='2'>";

        //---------------------------------------------
        $("#myModal_chokoteishi").css('display', 'none');

    }
    // No.6
    if (no == "6") {
        $("#rowAS6" + indexe).text(indexe);
        $("#rowBS6" + indexe).text(f);
        $("#rowCS6" + indexe).text(e);
        $("#rowDS6" + indexe).text(c);
        $("#rowES6" + indexe).text(a);
        $("#rowFS6" + indexe).text(b);

        document.getElementById('rowGS6' + indexe).innerHTML = d + "<input type='hidden' name='youintaisaku_chokotei' value='2'>";

        //---------------------------------------------
        $("#myModal_chokoteishi").css('display', 'none');

    }
    // No.7
    if (no == "7") {

        $("#rowAS7" + indexe).text(indexe);
        $("#rowBS7" + indexe).text(f);
        $("#rowCS7" + indexe).text(e);
        $("#rowDS7" + indexe).text(c);
        $("#rowES7" + indexe).text(a);
        $("#rowFS7" + indexe).text(b);

        document.getElementById('rowGS7' + indexe).innerHTML = d + "<input type='hidden' name='youintaisaku_chokotei' value='2'>";

        //---------------------------------------------
        $("#myModal_chokoteishi").css('display', 'none');

    }
    // No.8
    if (no == "8") {

        $("#rowAS8" + indexe).text(indexe);
        $("#rowBS8" + indexe).text(f);
        $("#rowCS8" + indexe).text(e);
        $("#rowDS8" + indexe).text(c);
        $("#rowES8" + indexe).text(a);
        $("#rowFS8" + indexe).text(b);

        document.getElementById('rowGS8' + indexe).innerHTML = d + "<input type='hidden' name='youintaisaku_chokotei' value='2'>";

        //---------------------------------------------
        $("#myModal_chokoteishi").css('display', 'none');

    }
    // No9
    if (no == "9") {

        $("#rowAS9" + indexe).text(indexe);
        $("#rowBS9" + indexe).text(f);
        $("#rowCS9" + indexe).text(e);
        $("#rowDS9" + indexe).text(c);
        $("#rowES9" + indexe).text(a);
        $("#rowFS9" + indexe).text(b);

        document.getElementById('rowGS9' + indexe).innerHTML = d + "<input type='hidden' name='youintaisaku_chokotei' value='2'>";

        //---------------------------------------------
        $("#myModal_chokoteishi").css('display', 'none');

    }
    // No.10
    if (no == "10") {

        $("#rowAS10" + indexe).text(indexe);
        $("#rowBS10" + indexe).text(f);
        $("#rowCS10" + indexe).text(e);
        $("#rowDS10" + indexe).text(c);
        $("#rowES10" + indexe).text(a);
        $("#rowFS10" + indexe).text(b);

        document.getElementById('rowGS10' + indexe).innerHTML = d + "<input type='hidden' name='youintaisaku_chokotei' value='2'>";

        //---------------------------------------------
        $("#myModal_chokoteishi").css('display', 'none');

    }

}
// When click button delete chokotei
function buttonDelete_chokotei() {
    $("#teishikomoku_No1 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }

    })
    //soi add code delete teshikomoku
    $("#teishikomoku_No2 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    })
    $("#teishikomoku_No3 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    })
    $("#teishikomoku_No4 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    })
    $("#teishikomoku_No5 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    })
    $("#teishikomoku_No6 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    })
    $("#teishikomoku_No7 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    })
    $("#teishikomoku_No8 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    })
    $("#teishikomoku_No9 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    })
    $("#teishikomoku_No10 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    })

    //---------------------------------------------
    $("#myModal_chokoteishi").css('display', 'none');
    //---------------------------------------------
}
//****************************************************************************************
//When tojiru_button onclick
function tojiru_button() {
    //---------------------------------------------
    $("#myModal").hide();
    //---------------------------------------------
}
$(document).ready(function () {
    getTime();
});
//soi add code
function getTime() {
    //Get time Stop time
    var length = localStorage.getItem('lengthSBV');
    if (length > 0) {

        $('tr.disabledtr').css('display', 'none');

    }
    var theFirtA;
    var theEndB;

    var c_temp_go = 0;
    for (var i = 0; i < length; i++) {
        
        var a = localStorage.getItem('A' + i);
        var b = localStorage.getItem('B' + i);
        var c = localStorage.getItem('C' + i);

        var c_temp_zen = c;
        // let[hours, minutes, seconds] = c_temp_zen.split(':');
        let [hours, minutes, seconds] = c_temp_zen.split(':');

        seconds_temp = hours * 60 * 60 + minutes * 60 + seconds * 1;

        c_temp_go = c_temp_go + seconds_temp;

        var d = localStorage.getItem('D' + i);
        // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        var html = "<tr>" +
            "<td class='textColor text_align'>" + a + "</td>" +
            "<td class='textColor text_align'>" + b + "</td>" +
            "<td class='textColor text_align'>" + c + "</td>" +
            "<td class='textColor' text-align:='' left=''>" + d + "</td>" +
            "</tr>";

        $('table.table_Div2 > tbody').append(html);

        if (i == 0) {
            theFirtA = a;
        }
        if (i == length - 1) {
            theEndB = b;
        }

    }

    $('#timeView1').text(theFirtA + " ~ " + theEndB);
    $('#dtmhien').css('display', 'none');

    if (length == 0) {
        $('#timeView1').css('display', 'none');
        $("#timeView0").text("7:13:30 ～ 7:21:00(7:30)");
    }

    var inTime2 = theFirtA;
    var outTime2 = theEndB;

    var hoursIn2 = inTime2.split(':', 1);
    var hoursOut2 = outTime2.split(':', 1);
    var minutesArrayIn2 = inTime2.split(':', 2);
    var minutesArrayOut2 = outTime2.split(':', 2);
    var secondArrayIn2 = inTime2.split(':', 3);
    var secondArrayOut2 = outTime2.split(':', 3);

    var minutesIn2 = minutesArrayIn2[1];
    var minutesOut2 = minutesArrayOut2[1];

    var secondIn2 = secondArrayIn2[2];
    var secondOut2 = secondArrayOut2[2];

    var hoursInInt2 = parseInt(hoursIn2);
    var hoursOutInt2 = parseInt(hoursOut2);

    var sumHours2 = "00";
    var sumMinutes2 = "00";
    var sumSecond2 = "00";

    if (secondOut2 >= secondIn2) {
        sumSecond2 = secondOut2 - secondIn2;
    } else {
        minutesOut2 -= 1;
        sumSecond2 = 60 - (secondIn2 - secondOut2);
    }
    if (minutesOut2 >= minutesIn2) {
        sumMinutes2 = minutesOut2 - minutesIn2;
    } else {
        hoursOutInt2 -= 1;
        sumMinutes2 = 60 - (minutesIn2 - minutesOut2);
    }
    if (hoursOutInt2 >= hoursInInt2) {
        sumHours2 = hoursOutInt2 - hoursInInt2;
    } else {
        sumHours2 = 24 - (hoursInInt2 - hoursOutInt2);
    }

    var vHour = ('00' + sumHours2).slice(-2);
    var vMin = ('00' + sumMinutes2).slice(-2);
    var vSec = ('00' + sumSecond2).slice(-2);

    $('#timeView2').text("(" + vHour + ":" + vMin + ":" + vSec + ")");
    // end
    hour_temp = Math.floor(c_temp_go / 3600);
    c_temp_go_temp = c_temp_go - hour_temp * 60 * 60;
    minute_temp = Math.floor(c_temp_go_temp / 60);
    second_temp = Math.floor(c_temp_go_temp % 60);
    if (hour_temp < 10) {
        hour_temp = "0" + hour_temp;
    }

    if (minute_temp < 10) {
        minute_temp = "0" + minute_temp;
    }

    if (second_temp < 10) {
        second_temp = "0" + second_temp;
    }

    $("#miwariate_jikan_atai_1").text(hour_temp + ":" + minute_temp + ":" + second_temp);

    if (c_temp_go >= 300) {

        $("#teishijikan_kubun_1").text("長期停止");

    } else {

        $("#teishijikan_kubun_1").text("チョコ停");

    }

    $("#labelDefault").removeClass("displayDiv");
    $("#labelDefault").addClass("displayDiv");

    $("#labelDefault_goukei").text(hour_temp + ":" + minute_temp + ":" + second_temp);
    $("#labelDefault_goukei_hidden").text("00:00:00");
    $("#labelDefault_miwariate").text(hour_temp + ":" + minute_temp + ":" + second_temp);
}