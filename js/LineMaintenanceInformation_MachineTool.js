/*//////追加クリック////////////////////////////////////////////////////////*/

function clickTsuika() {

    $(".modalMachineTool").css('display', 'block');

    $("#buttonEdit").css('display', 'none');
    $("#buttonDelete").css('display', 'none');
    $("#buttonRegistration").css('display', 'block');
}
function buttonCancel() {
    $(".modalMachineTool").css('display', 'none');
    // .location.reload();
}
function buttonRegistration() {
    var selectKimu = $("#selectKimu").val();
    var selectMachineToolName = $("#selectMachineToolName").val();
    var selectWhereUse = $("#selectWhereUse").val();
    var selectUser = $("#selectUser").val();
    var selectCleaning = $("#selectCleaning").val();

    var inputHiddenTableMT = $("#inputHiddenTableMT").val();

    var convert = parseInt(inputHiddenTableMT);
    convert = convert + 1;
    $("#inputHiddenTableMT").val(convert);

    var htmlMT = "<tr class='item'><td id='MTA" + convert + "' style='width: 100px; border-color: lightgray; padding-left: 20px; border-top-style: none; height: 50px;'>" + selectKimu + "</td><td id='MTB" + convert + "' style='width: 250px; border-color: lightgray; padding-left: 20px; border-top-style: none;'>" + selectMachineToolName + "</td><td id='MTC" + convert + "' style='width: 200px; border-color: lightgray; padding-left: 20px; border-top-style: none;'>" + selectWhereUse + "</td><td id='MTD" + convert + "' style='width: 250px; border-color: lightgray; padding-left: 20px; border-top-style: none;'>" + selectUser + "</td><td  id='MTE" + convert + "' style='width: 150px; border-color: lightgray; padding-left: 20px; border-top-style: none;'>" + selectCleaning + "</td><td id='MTF" + convert + "'style='display:none'>" + convert + "</td></tr>"
    $("#myTbody").append(htmlMT);

    $(".modalMachineTool").css('display', 'none');
    clickCauseTable();
    //location.reload();
    $("#buttonEdit").css('display', 'block');
    $("#buttonDelete").css('display', 'block');
    $("#buttonRegistration").css('display', 'none');

}
function buttonEdit() {
    var selectkimu = $("#selectKimu").val();
    var selectMachineToolName = $("#selectMachineToolName").val();
    var selectWhereUse = $("#selectWhereUse").val();
    var selectUser = $("#selectUser").val();
    var selectCleaning = $("#selectCleaning").val();
    var idhidden = $("#inputHiddenModalMT").val();

    $("#MTA" + idhidden).text(selectkimu);
    $("#MTB" + idhidden).text(selectMachineToolName);
    $("#MTC" + idhidden).text(selectWhereUse);
    $("#MTD" + idhidden).text(selectUser);
    $("#MTE" + idhidden).text(selectCleaning);

    $("#MTF" + idhidden).text(idhidden);
    $(".modalMachineTool").css('display', 'none');
    // location.reload();
}
function buttonDelete() {
    $("#myTbody").find('tr[class="item selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }

    })
    $("#buttonEdit").css('display', 'block');
    $("#buttonDelete").css('display', 'block');
    $("#buttonRegistration").css('display', 'none');
    $(".modalMachineTool").css('display', 'none');
    // location.reload();
}

/*//////データの取得(初期処理)///////////////////////////////////////////////*/

var dataMachineTool = []

function getData() {

    var dataset_machinetool = []

    var lineMachineTool = JSON.parse(MachineTool);

    dataset_machinetool.push(lineMachineTool);

    for (var i = 0; i < lineMachineTool.length; i++) {
        Object.values(dataset_machinetool[0][i]);
        dataMachineTool.push(Object.values(dataset_machinetool[0][i]));
    }

}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////メインテーブル作成(初期処理)/////////////////////////////////////////*/

function makeMainTable() {

    var tBody = document.getElementById("myTbody");

    for (var count = 0; count < dataMachineTool.length; count++) {
        var row = tBody.insertRow(count);

        var c1 = row.insertCell(0);
        var c2 = row.insertCell(1);
        var c3 = row.insertCell(2);
        var c4 = row.insertCell(3);
        var c5 = row.insertCell(4);
        var c6 = row.insertCell(5);

        c1.style.cssText = "width:100px; border-color:lightgray; padding-left: 20px; border-top-style:none; height: 50px;";
        c2.style.cssText = "width:250px; border-color:lightgray; padding-left: 20px; border-top-style:none;";
        c3.style.cssText = "width:200px; border-color:lightgray; padding-left: 20px; border-top-style:none;";
        c4.style.cssText = "width:250px; border-color:lightgray; padding-left: 20px; border-top-style:none;";
        c5.style.cssText = "width:150px; border-color:lightgray; padding-left: 20px; border-top-style:none;";
        c6.style.cssText = "display:none";
        c1.setAttribute("id", "MTA" + count);
        c2.setAttribute("id", "MTB" + count);
        c3.setAttribute("id", "MTC" + count);
        c4.setAttribute("id", "MTD" + count);
        c5.setAttribute("id", "MTE" + count);
        c6.setAttribute("id", "MTF" + count);
        c1.innerHTML = dataMachineTool[count][0];
        c2.innerHTML = dataMachineTool[count][1];
        c3.innerHTML = dataMachineTool[count][2];
        c4.innerHTML = dataMachineTool[count][3];
        c5.innerHTML = dataMachineTool[count][4];
        c6.innerHTML = count;

    }
}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////詳細クリック/////////////////////////////////////////////////////////*/
var Work = "";
var MachineToolName = "";
var WhereUser = "";
var User = "";
var Cleaning1 = "";

/*///////////////////////////////////////////////////////////////////////////*/

$(document).ready(function () {
    getData();
    makeMainTable();
    $('#myTbody tr').attr('class', 'item');
    localStorage.removeItem('index0');
    localStorage.clear();
    clickCauseTable();

});

function clickCauseTable() {
    //メインテーブルのクリック時の処理
    $("#myTbody tr").click(function () {
        // クリックした行の情報取得
        Work = $(this).children('td')[0].innerText;
        MachineToolName = $(this).children('td')[1].innerText;
        WhereUser = $(this).children('td')[2].innerText;
        User = $(this).children('td')[3].innerText;
        Cleaning1 = $(this).children('td')[4].innerText;

        // 選択されている他の行があれば選択解除
        $(this).siblings().removeClass("selected");
        //クリックされた行を選択
        $(this).addClass("selected");

        //Set item  save in localstorage 
        var index0 = $(this).children()[0].textContent;
        var index1 = $(this).children()[1].textContent;
        var index2 = $(this).children()[2].textContent;
        var index3 = $(this).children()[3].textContent;
        var index4 = $(this).children()[4].textContent;
        var index5 = $(this).children()[5].textContent;

        localStorage.setItem('key1', '1');
        localStorage.setItem('index0', index0);
        localStorage.setItem('index1', index1);
        localStorage.setItem('index2', index2);
        localStorage.setItem('index3', index3);
        localStorage.setItem('index4', index4);
        localStorage.setItem('index5', index5);
        // 変更画面
        $(".modalMachineTool").css('display', 'block');
        var Kinmu = localStorage.getItem("index0");
        var Kousaku = localStorage.getItem("index1");
        var Shiyoubashou = localStorage.getItem("index2");
        var shiyousha = localStorage.getItem("index3");
        var Seisoujisshi = localStorage.getItem("index4");
        var hidden = localStorage.getItem("index5")
        // load data
        $("#selectKimu option").each(function () {
            if ($(this).text() == Kinmu) {
                $(this).attr('selected', 'selected');
            }
        });
        $("#selectMachineToolName option").each(function () {
            if ($(this).text() == Kousaku) {
                $(this).attr('selected', 'selected');
            }
        });
        $("#selectWhereUse option").each(function () {
            if ($(this).text() == Shiyoubashou) {
                $(this).attr('selected', 'selected');
            }
        });
        $("#selectUser option").each(function () {
            if ($(this).text() == shiyousha) {
                $(this).attr('selected', 'selected');
            }
        });
        $("#selectCleaning option").each(function () {
            if ($(this).text() == Seisoujisshi) {
                $(this).attr('selected', 'selected');
            }
        });
        $('#inputHiddenModalMT').val(hidden);
        //
        $("#buttonEdit").css('visibility', 'visible');
        $("#buttonDelete").css('visibility', 'visible');
        $("#buttonRegistration").css('display', 'none');

    });
}

