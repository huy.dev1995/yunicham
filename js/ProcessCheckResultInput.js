/*//////データの取得(初期処理)///////////////////////////////////////////////*/

var dataProcessCheckResultInput = []
var dataProcessCheckResultInput1 = []
var dataProcessCheckResultInput2 = []
function getData() {

    var dataset_processcheckresultinput = []
    var lineProcessCheckResultInput = JSON.parse(ProcessCheckResultInput);
    dataset_processcheckresultinput.push(lineProcessCheckResultInput);
    //
    var dataset_processcheckresultinput1 = []
    var lineProcessCheckResultInput1 = JSON.parse(ProcessCheckResultInput1);
    dataset_processcheckresultinput1.push(lineProcessCheckResultInput1);
    //
    var dataset_processcheckresultinput2 = []
    var lineProcessCheckResultInput2 = JSON.parse(ProcessCheckResultInput2);
    dataset_processcheckresultinput2.push(lineProcessCheckResultInput2);
    //
    for (var i = 0; i < lineProcessCheckResultInput.length; i++) {
        Object.values(dataset_processcheckresultinput[0][i]);
        dataProcessCheckResultInput.push(Object.values(dataset_processcheckresultinput[0][i]));
    }
    //
    for (var i = 0; i < lineProcessCheckResultInput1.length; i++) {
        Object.values(dataset_processcheckresultinput1[0][i]);
        dataProcessCheckResultInput1.push(Object.values(dataset_processcheckresultinput1[0][i]));
    }
    //
    for (var i = 0; i < lineProcessCheckResultInput2.length; i++) {
        Object.values(dataset_processcheckresultinput2[0][i]);
        dataProcessCheckResultInput2.push(Object.values(dataset_processcheckresultinput2[0][i]));
    }
}
/*///////////////////////////////////////////////////////////////////////////*/

/*//////メインテーブル作成(初期処理)/////////////////////////////////////////*/

function makeMainTable1() {

    var tBody = document.getElementById("myTbody1");
    var index = 0;
    for (var count = 0; count < dataProcessCheckResultInput.length; count++) {
        var row = tBody.insertRow(count);
        index++;
        var c1 = row.insertCell(0);
        var c2 = row.insertCell(1);
        var c3 = row.insertCell(2);
        var c4 = row.insertCell(3);
        var c5 = row.insertCell(4);
        var c6 = row.insertCell(5);
        var c7 = row.insertCell(6);
        var c8 = row.insertCell(7);
        var c9 = row.insertCell(8);
        var c10 = row.insertCell(9);
        var c11 = row.insertCell(10);
        var c12 = row.insertCell(11);
        var c13 = row.insertCell(12);
        var c14 = row.insertCell(13);
        var c15 = row.insertCell(14);

        c8.id = "c8" + count;

        c9.id = "c9" + count;

        c1.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none; height: 70px;";
        c2.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none;";
        c3.style.cssText = "border-color:lightgray; text-align: center; border-top-style:none;";
        c4.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none;";
        c5.style.cssText = "border-color:lightgray; text-align: center; border-top-style:none;";
        c6.style.cssText = "border-color:lightgray; text-align: center; border-top-style:none;";
        c7.style.cssText = "border-color:lightgray; text-align: center; border-top-style:none;";
        c8.style.cssText = "border-color:lightgray; text-align: center; border-top-style:none;";
        c9.style.cssText = "border-color:lightgray; text-align: center; border-top-style:none;";
        c10.style.cssText = "border-color:lightgray; text-align: center; border-top-style:none;";
        c11.style.cssText = "border-color:lightgray; text-align: center; border-top-style:none;";
        c12.style.cssText = "border-color:lightgray; text-align: center; border-top-style:none;";
        c13.style.cssText = "border-color:lightgray; text-align: center; border-top-style:none;";
        c14.style.cssText = "border-color:lightgray; text-align: center; border-top-style:none;";
        c15.style.cssText = "border-color:lightgray; text-align: center; border-top-style:none;";

        var buttonhtml = "<div class='area'>" +
            "<input  onclick='funcOK" + index + "()' type='radio' name='tab_name" + index + "' frameborder='0' id='tabOK" + index + "' value='1'>" +
            "<label class='tab_class' for='tabOK" + index + "'> OK </label>" +
            "<input onclick='funcNG" + index + "()' type='radio' name='tab_name" + index + "'  id='tabNG" + index + "' value='2'>" +
            "<label class='tab_class' for='tabNG" + index + "'>NG</label>" +
            " </div>";
        c1.innerHTML = dataProcessCheckResultInput[count][0];
        c2.innerHTML = dataProcessCheckResultInput[count][1];
        c3.innerHTML = dataProcessCheckResultInput[count][2];
        c4.innerHTML = buttonhtml;
        c5.innerHTML = "OK";
        c6.innerHTML = "OK";
        c7.innerHTML = "-";
        c8.innerHTML = "";
        c9.innerHTML = "";
        c10.innerHTML = "";
        c11.innerHTML = "";
        c12.innerHTML = "";
        c13.innerHTML = "";
        c14.innerHTML = "";
        c15.innerHTML = "";
        
        // c8.setAttribute('onclick', 'btnRow("row")');
        c8.setAttribute('class', 'selected');
        // c9.setAttribute('onclick', 'btnRow1("row1")');
        // c9.setAttribute('class', 'selected1');

        if (c8.innerHTML == "") {
            c8.style.cssText += "background: gainsboro";
        }
        if (c9.innerHTML == "") {
            c9.style.cssText += "background: gainsboro";
        }
        if (c10.innerHTML == "") {
            c10.style.cssText += "background: gainsboro";
        }
        if (c11.innerHTML == "") {
            c11.style.cssText += "background: gainsboro";
        }
        if (c12.innerHTML == "") {
            c12.style.cssText += "background: gainsboro";
        }
        if (c13.innerHTML == "") {
            c13.style.cssText += "background: gainsboro";
        }
        if (c14.innerHTML == "") {
            c14.style.cssText += "background: gainsboro";
        }
        if (c15.innerHTML == "") {
            c15.style.cssText += "background: gainsboro";
        }

    }

}
function makeMainTable2() {

    var tBody = document.getElementById("myTbody2");
    var index1 = 0;
    for (var count = 0; count < dataProcessCheckResultInput1.length; count++) {
        var row = tBody.insertRow(count);
        index1++;
        var c12 = row.insertCell(0);
        var c22 = row.insertCell(1);
        var c32 = row.insertCell(2);
        var c42 = row.insertCell(3);
        var c52 = row.insertCell(4);
        var c62 = row.insertCell(5);
        var c72 = row.insertCell(6);

        c62.id = "c62" + count;

        c12.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none; height: 70px;";
        c22.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none;";
        c32.style.cssText = "border-color:lightgray; text-align: center; border-top-style:none;";
        c42.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none;";
        c52.style.cssText = "border-color:lightgray; text-align: center; border-top-style:none;";
        c62.style.cssText = "border-color:lightgray; text-align: center; border-top-style:none;";
        c72.style.cssText = "border-color:lightgray; text-align: center; border-top-style:none;";
        var buttonhtml1 = "<div class='area'>" +
            "<input onclick='funcOK2" + index1 + "()' type='radio' name='tab_name1" + index1 + "' frameborder='0' id='tabOK1" + index1 + "' value='1'>" +
            "<label class='tab_class1' for='tabOK1" + index1 + "'> OK </label>" +
            "<input onclick='funcNG2" + index1 + "()' type='radio' name='tab_name1" + index1 + "'  id='tabNG1" + index1 + "' value='2'>" +
            "<label class='tab_class1' for='tabNG1" + index1 + "'>NG</label>" +
            " </div>";
        c12.innerHTML = dataProcessCheckResultInput1[count][0];
        c22.innerHTML = dataProcessCheckResultInput1[count][1];
        c32.innerHTML = dataProcessCheckResultInput1[count][2];
        c42.innerHTML = buttonhtml1;
        c52.innerHTML = "OK";
        c62.innerHTML = "";
        c72.innerHTML = "";

        // c62.setAttribute('onclick', 'btnRow1("row1")');
        c62.setAttribute('class', 'selected');

        if (c52.innerHTML == "") {
            c52.style.cssText += "background: gainsboro";
        }
        if (c62.innerHTML == "") {
            c62.style.cssText += "background: gainsboro";
        }
        if (c72.innerHTML == "") {
            c72.style.cssText += "background: gainsboro";
        }

    }

}

function makeMainTable3() {
    var index2 = 0;
    var tBody = document.getElementById("myTbody3");

    for (var count = 0; count < dataProcessCheckResultInput2.length; count++) {
        index2++;
        var row = tBody.insertRow(count);

        var c13 = row.insertCell(0);
        var c23 = row.insertCell(1);
        var c33 = row.insertCell(2);
        var c43 = row.insertCell(3);
        var c53 = row.insertCell(4);

        c13.style.cssText = " border-color:lightgray; padding-left: 15px; border-top-style:none; height: 70px;";
        c23.style.cssText = " border-color:lightgray; padding-left: 15px; border-top-style:none;";
        c33.style.cssText = " border-color:lightgray; text-align: center; border-top-style:none;";
        c43.style.cssText = " border-color:lightgray; text-align: center; border-top-style:none;";
        c53.style.cssText = " border-color:lightgray; padding-left: 15px; border-top-style:none;";

        c13.innerHTML = dataProcessCheckResultInput2[count][0];
        c23.innerHTML = dataProcessCheckResultInput2[count][1];
        c33.innerHTML = dataProcessCheckResultInput2[count][2];
        c43.innerHTML = dataProcessCheckResultInput2[count][3];

        var buttonhtml = "<div class='area'>" +
            "<input type='radio' name='tab_name2" + index2 + "' frameborder='0' id='tabOK2" + index2 + "' value='1'>" +
            "<label class='tab_class2' for='tabOK2" + index2 + "'> OK </label>" +
            "<input type='radio' name='tab_name2" + index2 + "'  id='tabNG2" + index2 + "' value='2'>" +
            "<label class='tab_class2' for='tabNG2" + index2 + "'>NG</label>" +
            " </div>";
        c53.innerHTML = buttonhtml;

    }

}
//button cancel
var key = localStorage.getItem("keyCancel");

function btnCancel() {

    // if (key == "0") {

    window.location.href = "LineMaintenanceInformation.html";
    // }
    // if (key == "1") {

    //     window.location.href = "InspectionResultList.html";
    // }
}
//button 確認
function btnVerification() {
    window.location.href = "LineMaintenanceInformation.html";
    // if (key == "0") {

    //     window.location.href = "ConfirmationList.html";
    // }
    // if (key == "1") {

    //     window.location.href = "InspectionResultList.html";
    // }
}
/*///////////////////////////////////////////////////////////////////////////*/

/*//////詳細クリック/////////////////////////////////////////////////////////*/
var InspectionPoint = "";
var Item = "";
var Method = "";
var EvaluationCriteria = "";
var Result = "";

$(document).ready(function () {
    getData();
    makeMainTable1();
    makeMainTable2();
    makeMainTable3();
    $('#child1').text("設備情報一覧");
    $('#href1').attr('href', 'LineMaintenanceList.html');
    $('#child2').text("設備情報");
    $('#href2').attr('href', 'LineMaintenanceInformation.html');
    //data from ConfirmationList.html
    if (key == "0") {

    }
    // data from InspectionResultList
    if (key == "1") {

    }
    //最初30分毎表示
    $('.selected').css('background', 'yellow');

    //最初3R表示
    $('.selected1').css('background', 'yellow');


});

//30分毎/////////////////////////////
// var listRow = []
// var indexsl = 0;
// function btnRow(row) {
//     // alert("helloworld");
//     // $(this).addClass("selected");
//     // $("#c90").empty()
//     $('.selected').css('background', 'yellow');
//     // $('.selected1').css('background', 'gainsboro');

//     listRow.push(row)

// }
// function btnRow1(row1){
//     // alert("helloworld");
//     // $(this).addClass("selected");
//     // $('td').removeClass('selected');
//     $("#c80").empty()
//     $('.selected1').css('background','yellow');
//     $('.selected').css('background','gainsboro');

//     listRow.push(row1)

// }

// function funcOK1(){

//     for(var i = 0; i < listRow.length; i++){

//         if(listRow[listRow.length-1] == "row"){
//             $("#c80").empty()
//             $("#c80").append("<p>OK</p>")

//         }else if(listRow[listRow.length-1] == "row1"){
//             $("#c90").empty()
//             $("#c90").append("<p>OK</p>")

//         }

//     }

// }
// function funcNG1(){

//     for(var i = 0; i < listRow.length; i++){

//         if(listRow[listRow.length-1] == "row"){
//             $("#c80").empty()
//             $("#c80").append("<p>NG</p>")

//         }else if(listRow[listRow.length-1] == "row1"){
//             $("#c90").empty()
//             $("#c90").append("<p>NG</p>")

//         }

//     }

// }
function funcOK1() {

    $("#c80").empty()
    $("#c80").append("<p>OK</p>")

}
function funcOK2() {

    $("#c81").empty()
    $("#c81").append("<p>OK</p>")

}
function funcOK3() {

    $("#c82").empty()
    $("#c82").append("<p>OK</p>")

}
function funcOK4() {

    $("#c83").empty()
    $("#c83").append("<p>OK</p>")

}
function funcOK5() {

    $("#c84").empty()
    $("#c84").append("<p>OK</p>")

}
function funcOK6() {

    $("#c85").empty()
    $("#c85").append("<p>OK</p>")

}

function funcNG1() {

    $("#c80").empty()
    $("#c80").append("<p>NG</p>")

}
function funcNG2() {

    $("#c81").empty()
    $("#c81").append("<p>NG</p>")

}
function funcNG3() {

    $("#c82").empty()
    $("#c82").append("<p>NG</p>")

}
function funcNG4() {

    $("#c83").empty()
    $("#c83").append("<p>NG</p>")

}
function funcNG5() {

    $("#c84").empty()
    $("#c84").append("<p>NG</p>")

}
function funcNG6() {

    $("#c85").empty()
    $("#c85").append("<p>NG</p>")

}

///R毎////////////////////////////////////////////////////////////////////
function funcOK21() {

    $("#c620").empty()
    $("#c620").append("<p>OK</p>")

}
function funcOK22() {

    $("#c621").empty()
    $("#c621").append("<p>OK</p>")
}
function funcOK23() {

    $("#c622").empty()
    $("#c622").append("<p>OK</p>")

}
function funcOK24() {

    $("#c623").empty()
    $("#c623").append("<p>OK</p>")

}
function funcOK25() {

    $("#c624").empty()
    $("#c624").append("<p>OK</p>")

}
function funcOK26() {

    $("#c625").empty()
    $("#c625").append("<p>OK</p>")

}
function funcNG21() {

    $("#c620").empty()
    $("#c620").append("<p>NG</p>")

}
function funcNG22() {

    $("#c621").empty()
    $("#c621").append("<p>NG</p>")

}
function funcNG23() {

    $("#c622").empty()
    $("#c622").append("<p>NG</p>")

}
function funcNG24() {

    $("#c623").empty()
    $("#c623").append("<p>NG</p>")

}
function funcNG25() {

    $("#c624").empty()
    $("#c624").append("<p>NG</p>")

}
function funcNG26() {

    $("#c625").empty()
    $("#c625").append("<p>NG</p>")

}