/*//////追加クリック////////////////////////////////////////////////////////*/

function buttonTsuika() {
    localStorage.setItem('key1', '0')
    // window.open("InspectionPlanRegistration.html", "", "width=1200,height=650");
    window.location.href = "InspectionPlanRegistration.html";

}

/*//////データの取得(初期処理)///////////////////////////////////////////////*/

var dataInspectionPlanList = []

function getData() {

    var dataset_inspectionplanlist = []
    var lineInspectionPlanList = JSON.parse(InspectionPlanList);
    dataset_inspectionplanlist.push(lineInspectionPlanList);
    for (var i = 0; i < lineInspectionPlanList.length; i++) {
        Object.values(dataset_inspectionplanlist[0][i]);
        dataInspectionPlanList.push(Object.values(dataset_inspectionplanlist[0][i]));
    }

}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////メインテーブル作成(初期処理)/////////////////////////////////////////*/

function makeMainTable() {

    var tBody = document.getElementById("myTbody");

    for (var count = 0; count < dataInspectionPlanList.length; count++) {
        var row = tBody.insertRow(count);

        var c0 = row.insertCell(0);
        var c1 = row.insertCell(1);
        var c2 = row.insertCell(2);
        var c3 = row.insertCell(3);
        var c4 = row.insertCell(4);
        var c5 = row.insertCell(5);
        var c6 = row.insertCell(6);
        var c7 = row.insertCell(7);
        var c8 = row.insertCell(8);
        c0.style.cssText = "width:8%; border-color:lightgray; text-align: center; border-top-style:none; height: 50px;";
        c1.style.cssText = "width:10%; border-color:lightgray; text-align: center; border-top-style:none; height: 50px;";
        c2.style.cssText = "width:8%; border-color:lightgray; text-align: center; border-top-style:none;";
        c3.style.cssText = "width:5%; border-color:lightgray; text-align: center; border-top-style:none;";
        c4.style.cssText = "width:10%; border-color:lightgray; padding-left: 15px; border-top-style:none;";
        c5.style.cssText = "width:10%; border-color:lightgray; padding-left: 15px; border-top-style:none;";
        c6.style.cssText = "width:8%; border-color:lightgray; padding-left: 15px; border-top-style:none;";
        c7.style.cssText = "width:5%; border-color:lightgray; padding-left: 15px; border-top-style:none;";
        c8.style.cssText = "width:12%; border-color:lightgray; padding-left: 15px; border-top-style:none;";
        
        // c4.style.cssText = "display:none";
        c0.innerHTML = "10号機";
        c1.innerHTML = dataInspectionPlanList[count][0];
        c2.innerHTML = dataInspectionPlanList[count][1];
        c3.innerHTML = dataInspectionPlanList[count][2];
        c4.innerHTML = dataInspectionPlanList[count][3];
        c5.innerHTML = dataInspectionPlanList[count][4];
        c6.innerHTML = dataInspectionPlanList[count][5];
        c7.innerHTML = dataInspectionPlanList[count][6];
        c8.innerHTML = dataInspectionPlanList[count][7];
        // c4.innerHTML = count;

    }
}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////詳細クリック/////////////////////////////////////////////////////////*/
var InspectionType = "";
var ImplementationFrequency = "";
var ImplementationMonth = "";
var ImplementationWeek = "";
var Weekdays = "";
var ImplementationDate = "";
var Dates = "";
var ImplementationTiming = "";
var gouki = "";
/*///////////////////////////////////////////////////////////////////////////*/

$(document).ready(function () {

    getData();
    makeMainTable();
    $('#myTbody tr').addClass('item')
    ///////////////////////////////////
    $("#denpyoKensakuInputArea").slideToggle();
    $(".btn_minus_s").css("display", "none");
    $(".btn_plus_s").css("display", "table-row");
    // 
    //メインテーブルのクリック時の処理
    $("#myTbody tr").click(function () {
        // クリックした行の情報取得
        gouki = $(this).children('td')[0].innerText;
        InspectionType = $(this).children('td')[1].innerText;
        ImplementationFrequency = $(this).children('td')[2].innerText;
        ImplementationMonth = $(this).children('td')[3].innerText;
        ImplementationWeek = $(this).children('td')[4].innerText;
        Weekdays = $(this).children('td')[5].innerText;
        ImplementationDate = $(this).children('td')[6].innerText;
        Dates = $(this).children('td')[7].innerText;
        ImplementationTiming = $(this).children('td')[8].innerText;


        // 選択されている他の行があれば選択解除
        $(this).siblings().removeClass("selected");
        // クリックされた行を選択
        $(this).addClass("selected");
        // Set item  save in localstorage 
        var index00 = $(this).children()[0].textContent;
        var index0 = $(this).children()[1].textContent;
        var index1 = $(this).children()[2].textContent;
        var index2 = $(this).children()[3].textContent;
        var index3 = $(this).children()[4].textContent;
        var index4 = $(this).children()[5].textContent;
        var index5 = $(this).children()[6].textContent;
        var index6 = $(this).children()[7].textContent;
        var index7 = $(this).children()[8].textContent;
        localStorage.setItem('key1', '1');
             
        localStorage.setItem('index0', index0);
        localStorage.setItem('index1', index1);
        
        localStorage.setItem('index7', index2);
        localStorage.setItem('index5', index3);
        localStorage.setItem('index6', index4);

        localStorage.setItem('index3', index5);
        localStorage.setItem('index4', index6);
                
        localStorage.setItem('index2', index7);

        window.location.href = "InspectionPlanRegistration.html";
        // window.open("InspectionPlanRegistration.html", "", "width=1200,height=650");

    });

});
/////////////////////////////////////検索ボックス///////////////////////////////////////////////
var count = 0;

function shosaiClick() {
    count++

    if (count % 2 == 0) {
        $("#denpyoKensakuInputArea").slideToggle(1);
        $(".btn_minus_s").css("display", "none");
        $(".btn_plus_s").css("display", "table-row");
    } else {
        $("#denpyoKensakuInputArea").slideToggle();
        $(".btn_minus_s").css("display", "table-row");
        $(".btn_plus_s").css("display", "none");
    }

}
