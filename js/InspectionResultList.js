////////////////////////////////setDefaultDayIsToday////////////////////////////////
// $(document).ready(function () {
//     var dateControl1 = document.querySelector('input[id="date1"]');
//     var dateControl2 = document.querySelector('input[id="date2"]');
//     var dateControl3 = document.querySelector('input[id="date3"]');
//     var dateControl4 = document.querySelector('input[id="date4"]');
//     ///////////////////////////////////
//     var getToday = new Date();
//     var dd = String(getToday.getDate()).padStart(2, '0');
//     var mm = String(getToday.getMonth() + 1).padStart(2, '0'); //January is 0!
//     var yyyy = getToday.getFullYear();
//     ///////////////////////////////////
//     getToday = yyyy + '-' + mm + '-' + dd;
//     // dateControl1.value = getToday;
//     dateControl2.value = getToday;
//     // dateControl3.value = getToday;
//     dateControl4.value = getToday;
// });
/*//////データの取得(初期処理)///////////////////////////////////////////////*/

var dataInspectionResultList = []
function getData() {

    var dataset_inspectionresultList = []
    var lineInspectionResultList = JSON.parse(InspectionResultList);
    dataset_inspectionresultList.push(lineInspectionResultList);
    for (var i = 0; i < lineInspectionResultList.length; i++) {
        Object.values(dataset_inspectionresultList[0][i]);
        dataInspectionResultList.push(Object.values(dataset_inspectionresultList[0][i]));
    }

}
/*///////////////////////////////////////////////////////////////////////////*/

/*//////メインテーブル作成(初期処理)/////////////////////////////////////////*/

function makeMainTable() {

    var tBody = document.getElementById("myTbody");

    for (var count = 0; count < dataInspectionResultList.length; count++) {
        var row = tBody.insertRow(count);

        var c1 = row.insertCell(0);
        var c2 = row.insertCell(1);
        var c3 = row.insertCell(2);
        var c4 = row.insertCell(3);
        var c5 = row.insertCell(4);
        var c6 = row.insertCell(5);
        var c7 = row.insertCell(6);
        var c8 = row.insertCell(7);
        var c9 = row.insertCell(8);

        c1.style.cssText = " border-color:lightgray; text-align: center; border-top-style:none; height: 50px; ";
        c2.style.cssText = " border-color:lightgray; padding-left: 20px; border-top-style:none; height: 50px; ";
        c3.style.cssText = " border-color:lightgray; padding-left: 20px; border-top-style:none;";
        c4.style.cssText = " border-color:lightgray; padding-left: 20px; border-top-style:none;";
        c5.style.cssText = " border-color:lightgray; text-align: end; border-top-style:none;";
        c6.style.cssText = " border-color:lightgray; text-align: end; border-top-style:none;";
        c7.style.cssText = " border-color:lightgray; text-align: end; border-top-style:none;";
        c8.style.cssText = " border-color:lightgray; text-align: end; border-top-style:none;";
        c9.style.cssText = " border-color:lightgray; padding-left: 35px; border-top-style:none;";

        c1.setAttribute('onclick', 'btnRow()');
        c2.setAttribute('onclick', 'btnRow()');
        c3.setAttribute('onclick', 'btnRow()');
        c4.setAttribute('onclick', 'btnRow()');
        c5.setAttribute('onclick', 'btnRow()');
        c6.setAttribute('onclick', 'btnRow()');
        c7.setAttribute('onclick', 'btnRow()');
        c8.setAttribute('onclick', 'btnRow()');
        c9.setAttribute('onclick', 'btnRow()');

        c1.innerHTML = "10号機";
        c2.innerHTML = dataInspectionResultList[count][0];
        c3.innerHTML = dataInspectionResultList[count][1];
        c4.innerHTML = dataInspectionResultList[count][2];
        c5.innerHTML = dataInspectionResultList[count][3];
        c6.innerHTML = dataInspectionResultList[count][4];
        c7.innerHTML = dataInspectionResultList[count][5];
        c8.innerHTML = dataInspectionResultList[count][6];

        if (dataInspectionResultList[count][1] == "") {
            var button1 = "<button type='button' class='btn myBtn1'style ='width: 100px;' onclick='btnInput()'>入力 </button>";
            c9.innerHTML = button1;
            c1.removeAttribute('onclick', 'btnRow()');
            c2.removeAttribute('onclick', 'btnRow()');
            c3.removeAttribute('onclick', 'btnRow()');
            c4.removeAttribute('onclick', 'btnRow()');
            c6.removeAttribute('onclick', 'btnRow()');
            c7.removeAttribute('onclick', 'btnRow()');
            c8.removeAttribute('onclick', 'btnRow()');
            c9.removeAttribute('onclick', 'btnRow()');

        }

    }
}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////詳細クリック/////////////////////////////////////////////////////////*/
var Plans = "";
var Implementation = "";
var InspectionType = "";
var SL = "";
var TL = "";
var GM = "";
var Quality = "";

$(document).ready(function () {
    getData();
    makeMainTable();
    ///////////////////////////////////
    $("#denpyoKensakuInputArea").slideToggle();
    $(".btn_minus_s").css("display", "none");
    $(".btn_plus_s").css("display", "table-row");
    // 
    $('#myTbody tr').attr('class', 'item');

    $("#myTbody tr").click(function () {
        // クリックした行の情報取得
        Plans = $(this).children('td')[0].innerText;
        Implementation = $(this).children('td')[1].innerText;
        InspectionType = $(this).children('td')[2].innerText;
        SL = $(this).children('td')[3].innerText;
        TL = $(this).children('td')[4].innerText;
        GM = $(this).children('td')[5].innerText;
        Quality = $(this).children('td')[6].innerText;

        // 選択されている他の行があれば選択解除
        $(this).siblings().removeClass("selected");
        // クリックされた行を選択
        $(this).addClass("selected");
        // Set item  save in localstorage 
        var indexCell0 = $(this).children()[0].textContent;
        var indexCell1 = $(this).children()[1].textContent;
        var indexCell2 = $(this).children()[2].textContent;
        var indexCell3 = $(this).children()[3].textContent;
        var indexCell4 = $(this).children()[4].textContent;
        var indexCell5 = $(this).children()[5].textContent;
        var indexCell6 = $(this).children()[6].textContent;
        // localStorage.setItem('key1', '1');
        localStorage.setItem('indexCell0', indexCell1);
        localStorage.setItem('indexCell1', indexCell2);
        localStorage.setItem('indexCell2', indexCell3);
        localStorage.setItem('indexCell3', indexCell4);
        localStorage.setItem('indexCell4', indexCell5);
        localStorage.setItem('indexCell5', indexCell6);

        // window.location.href = "InspectionResultReference.html";

    });

});

function btnRow() {
    localStorage.setItem('keyCancel', '1');
    window.location.href = "InspectionResultReference1.html";
}
function btnInput() {
    localStorage.setItem('key1', '1');
    window.location.href = "InspectionResultInput.html";
    // window.open("InspectionResultInput.html", "", "width=1350,height=1300");
}
/////////////////////////////////////検索ボックス///////////////////////////////////////////////
var count = 0;

function shosaiClick() {
    count++

    if (count % 2 == 0) {
        $("#denpyoKensakuInputArea").slideToggle(1);
        $(".btn_minus_s").css("display", "none");
        $(".btn_plus_s").css("display", "table-row");
    } else {
        $("#denpyoKensakuInputArea").slideToggle();
        $(".btn_minus_s").css("display", "table-row");
        $(".btn_plus_s").css("display", "none");
    }

}
