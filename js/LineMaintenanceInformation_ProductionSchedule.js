// get data file json 
var dataProductionSchedule = []

function getData() {

    var dataset_productionschedule = []

    var lineProductionSchedule = JSON.parse(ProductionSchedule);

    dataset_productionschedule.push(lineProductionSchedule);

    for (var i = 0; i < lineProductionSchedule.length; i++) {
        Object.values(dataset_productionschedule[0][i]);
        dataProductionSchedule.push(Object.values(dataset_productionschedule[0][i]));
    }

}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////メインテーブル作成(初期処理)/////////////////////////////////////////*/

function makeMainTable() {

    var tBody = document.getElementById("myTbody");

    for (var count = 0; count < dataProductionSchedule.length; count++) {
        var row = tBody.insertRow(count);

        var c1 = row.insertCell(0);
        var c2 = row.insertCell(1);
        var c3 = row.insertCell(2);
        var c4 = row.insertCell(3);
        var c5 = row.insertCell(4);
        var c6 = row.insertCell(5);
        var c7 = row.insertCell(6);
        c1.style.cssText = "width:250px; border-color:lightgray;padding-left: 20px; border-top-style:none; height: 50px;";
        c2.style.cssText = "width:250px; border-color:lightgray; padding-left: 20px; border-top-style:none;";
        c3.style.cssText = "width:250px; border-color:lightgray; padding-left: 20px; border-top-style:none;";
        c4.style.cssText = "width:100px; border-color:lightgray; padding-left: 20px; border-top-style:none;";
        c5.style.cssText = "width:100px; border-color:lightgray; padding-left: 20px; border-top-style:none;";
        c6.style.cssText = "width:200px; border-color:lightgray; padding-right: 20px;text-align: end; border-top-style:none;";
        c7.style.cssText = "width:200px; border-color:lightgray; padding-right: 20px; text-align: end; border-top-style:none;";

        c1.innerHTML = dataProductionSchedule[count][0];
        c2.innerHTML = dataProductionSchedule[count][1];
        c3.innerHTML = dataProductionSchedule[count][2];
        c4.innerHTML = dataProductionSchedule[count][3];
        c5.innerHTML = dataProductionSchedule[count][4];
        c6.innerHTML = dataProductionSchedule[count][5];
        c7.innerHTML = dataProductionSchedule[count][6];

    }
}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////詳細クリック/////////////////////////////////////////////////////////*/
// var Work = "";
// var ScheduledItems = "";
// var Practitioner = "";
// var ScheduleContents = "";

/*///////////////////////////////////////////////////////////////////////////*/

$(document).ready(function () {
    getData();
    makeMainTable();
    $('#myTbody tr').addClass('item')
    //メインテーブルのクリック時の処理
    // $("#myTbody tr").click(function () {
    //     // クリックした行の情報取得
    //     Work = $(this).children('td')[0].innerText;
    //     ScheduledItems = $(this).children('td')[1].innerText;
    //     Practitioner = $(this).children('td')[2].innerText;
    //     ScheduleContents = $(this).children('td')[2].innerText;

    //     // 選択されている他の行があれば選択解除
    //     $(this).siblings().removeClass("selected");
    //     // クリックされた行を選択
    //     $(this).addClass("selected");
    //     // // Set item  save in localstorage 
    //     // var index0 = $(this).children()[0].textContent;
    //     // var index1 = $(this).children()[1].textContent;
    //     // var index2 = $(this).children()[2].textContent;
    //     // var index3 = $(this).children()[3].textContent;
    //     // localStorage.setItem('key1', '1');
    //     // localStorage.setItem('index0', index0);
    //     // localStorage.setItem('index1', index1);
    //     // localStorage.setItem('index2', index2);
    //     // localStorage.setItem('index3', index3);
    //     // window.location.href = "InspectionResultInput.html";

    // });

    // $('#myTbody tr').attr('onclick', "myFunction()");

});

// function myFunction() {
//     var myWindow = window.open("InspectionResultInput.html");
//     // var myWindow = window.open("InspectionResultInput.html","", "width=200,height=100");
// }

