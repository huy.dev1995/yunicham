/*//////詳細クリック/////////////////////////////////////////////////////////*/
var Youtei = "";
var Settbi = "";
var Youtai = "";

//////////////// Load page/////////////////////////
$(document).ready(function () {
    ////////////////検索ボックス//////////////////
    $("#denpyoKensakuInputArea").slideToggle();
    $(".btn_minus_s").css("display", "none");
    $(".btn_plus_s").css("display", "table-row");
    // load data from file json
    getData();
    makeMainTable();
    // click row
    clickrow();
    //////////////////add class item in tr////////////////////////
    $('#myTbody tr').addClass('item');
    ////////////////Select time at modal///////////////////////////
    selectTime();
});
function clickrow(){
    $("#myTbody tr").click(function () {
        // クリックした行の情報取得
        Youtei = $(this).children('td')[0].innerText;
        Settbi = $(this).children('td')[1].innerText;
        Youtai = $(this).children('td')[2].innerText;

        // 選択されている他の行があれば選択解除
        $(this).siblings().removeClass("selected");
        // クリックされた行を選択
        $(this).addClass("selected");
        // Set item  save in localstorage 
        var index0 = $(this).children()[0].textContent;
        var index1 = $(this).children()[1].textContent;
        var index2 = $(this).children()[2].textContent;
        
        localStorage.setItem('keyHozen', '1');

        localStorage.setItem('cell01', index0);
        localStorage.setItem('cell02', index1);
        localStorage.setItem('cell03', index2);

        window.location.href = "ConservationPlanReference.html";
    });

}
/////////////////////////////////////検索ボックス///////////////////////////////////////////////
var count = 0;

function shosaiClick() {
    count++

    if (count % 2 == 0) {
        $("#denpyoKensakuInputArea").slideToggle(1);
        $(".btn_minus_s").css("display", "none");
        $(".btn_plus_s").css("display", "table-row");
    } else {
        $("#denpyoKensakuInputArea").slideToggle();
        $(".btn_minus_s").css("display", "table-row");
        $(".btn_plus_s").css("display", "none");
    }

}
/*//////データの取得(初期処理)///////////////////////////////////////////////*/

var dataConservationPlan = []

function getData() {

    var dataset_conservationPlan = []
    var lineConservationPlan = JSON.parse(ConservationPlan);
    dataset_conservationPlan.push(lineConservationPlan);
    for (var i = 0; i < lineConservationPlan.length; i++) {
        Object.values(dataset_conservationPlan[0][i]);
        dataConservationPlan.push(Object.values(dataset_conservationPlan[0][i]));
    }

}
/*///////////////////////////////////////////////////////////////////////////*/

/*//////メインテーブル作成(初期処理)/////////////////////////////////////////*/

function makeMainTable() {

    var tBody = document.getElementById("myTbody");

    for (var count = 0; count < dataConservationPlan.length; count++) {
        var row = tBody.insertRow(count);

        var c1 = row.insertCell(0);
        var c2 = row.insertCell(1);
        var c3 = row.insertCell(2);

        c1.style.cssText = "width:20%; border-color:lightgray; padding-left: 15px; border-top-style:none; height: 50px;";
        c2.style.cssText = "width:10%; border-color:lightgray; text-align: center; border-top-style:none;";
        c3.style.cssText = "width:10%; border-color:lightgray; text-align: center; border-top-style:none;";

        c1.innerHTML = dataConservationPlan[count][0];
        c2.innerHTML = dataConservationPlan[count][1];
        c3.innerHTML = dataConservationPlan[count][2];
        if (c3.innerHTML == "確認済") {
            c3.style.cssText = "width:10%; border-color:lightgray; text-align: center; border-top-style:none;color: lightgray;";
        }
        if (c3.innerHTML == "登録済") {
            c3.style.cssText = "width:10%; border-color:lightgray; text-align: center; border-top-style:none;color: blue;";
        }
        if (c3.innerHTML == "未登録") {
            c3.style.cssText = "width:10%; border-color:lightgray; text-align: center; border-top-style:none;color: red;";
        }


    }
}
/*//////追加クリック////////////////////////////////////////////////////////*/

function clickTsuika() {

    $(".modalConservationPlan").css('display', 'block');

    // $("#input_ActivitySchedule").val('');

    // $("#buttonEdit").css('display', 'none');
    // $("#buttonDelete").css('display', 'none');
    // $("#buttonRegistration").css('display', 'block');

}
/////////////////////////////Modal button/////////////////////////////////////////////////
function buttonCancel() {
    $(".modalConservationPlan").css('display', 'none');
}
function buttonRegistration() {
    var select1 = $("#selectweek option:selected").text();
    var htmladd = "<tr class='item'><td style='width: 20%; border-color: lightgray; padding-left: 15px; border-top-style: none; height: 50px;'>" + select1 + "</td><td style='width: 10%; border-color: lightgray; text-align: center; border-top-style: none;'>10号機</td><td style='width: 10%; border-color: lightgray; text-align: center; border-top-style: none; color: red;'>未登録</td></tr>";
    $("#myTbody").append(htmladd);

    $(".modalConservationPlan").css('display', 'none');
    clickrow();

}
function selectTime() {
    Date.prototype.yyyymmdd = function () {
        var mm = this.getMonth() + 1; // getMonth() is zero-based
        var dd = this.getDate();

        return [this.getFullYear(),
        (mm > 9 ? '' : '0') + mm,
        (dd > 9 ? '' : '0') + dd
        ].join('/');
    };

    var today = new Date();
    var ii;

    //1.  Tim ngay chu nhat cua tuan sau. 
    for (ii = 0; ii < 7; ii++) {
        if (today.getDay() == 0) {
            break;
        }
        today.setDate(today.getDate() + 1);
    }

    var jj;

    var weekCal = [];
    var dayCal = [];

    var nextDay = today;
    for (jj = 0; jj < 4; jj++) {

        dayCal = [];
        var key;
        for (ii = 0; ii < 7; ii++) {

            if (ii == 0) {
                key = nextDay.getFullYear() + '年' + (nextDay.getMonth() + 1) + '月' + '第' + jj + '週';
            }
            dayCal.push(nextDay.yyyymmdd());
            nextDay.setDate(nextDay.getDate() + 1);

        }

        var obj = {
            index: jj,
            key: key,
            value: dayCal,
        }

        weekCal.push(obj)


    }

    // the firts label view
    var labeltime = weekCal;
    $("#lbViewTime").text(labeltime[0].value[0] + '　~　' + labeltime[0].value[6])
    // convert data string to oject
    var indexWeek = JSON.stringify(weekCal);
    localStorage.setItem("indexWeek", indexWeek);

    var html = '<select name="selectweek" id="selectweek"  class="selectSmall selectFontSize inputLeftMargin" style="width:200px" onchange="changeSelectTime(this.value);">';
    for (ii = 0; ii < weekCal.length; ii++) {
        html = html + '<option value=' + weekCal[ii].index + '>' + weekCal[ii].key + '</option>'
    }
    html += '</select>';
    // html += "<label id='lbViewTime'></label>"
    $("#tdBodyTableKensaku").html(html);
}
// change sekect time
function changeSelectTime(value) {
    var getIndex = localStorage.getItem("indexWeek");
    var convertdata = JSON.parse(getIndex);
    var index1 = convertdata[value];
    $('#lbViewTime').text(index1.value[0] + ' ~ ' + index1.value[6]);

}