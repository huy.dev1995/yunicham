/*//////追加クリック////////////////////////////////////////////////////////*/

function clickTsuika() {
    $(".modalInformation").css('display', 'block');
    $("#input_Information").val('');

    $("#buttonEdit").css('display', 'none');
    $("#buttonDelete").css('display', 'none');
    $("#buttonRegistration").css('display', 'block');
}
function buttonCancel() {
    $(".modalInformation").css('display', 'none');
    // location.reload();
}
function buttonRegistration() {

    var selectIF = $("#selectKimu").val();
    var inputIF = $("#input_Information").val();

    var inputHiddenTable = $("#inputHiddenTableIF").val();

    var convert = parseInt(inputHiddenTable);
    convert = convert + 1;
    $("#inputHiddenTableIF").val(convert);

    var htmlIF = "<tr class='item'><td id='IFA" + convert + "'style='width: 150px; border-color: lightgray; padding-left: 20px; border-top-style: none; height: 50px;'>" + selectIF + "</td><td id='IFB" + convert + "' style='width: 500px; border-color: lightgray; padding-left: 20px; border-top-style: none;'>" + inputIF + "</td><td id='IFC" + convert + "'style='width: 150px; border-color: lightgray; padding-left: 20px; border-top-style: none;'>XXXX</td><td id='IFD" + convert + "'style='display:none'>" + convert + "</td></tr>"
    $("#myTbody").append(htmlIF);

    clickCauseTable();
    $(".modalInformation").css('display', 'none');
    $("#buttonEdit").css('display', 'block');
    $("#buttonDelete").css('display', 'block');
    $("#buttonRegistration").css('display', 'none');
    // location.reload();
}
function buttonEdit() {
    var selectkimu = $("#selectKimu").val();
    var input1 = $("#input_Information").val();
    var idhidden = $("#inputHiddenModalIF").val();

    $("#IFA" + idhidden).text(selectkimu);
    $("#IFB" + idhidden).text(input1);
    $("#IFD" + idhidden).text(idhidden);
    $(".modalInformation").css('display', 'none');
    // location.reload();
}
function buttonDelete() {
    $("#myTbody").find('tr[class="item selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }

    })
    $("#buttonEdit").css('display', 'block');
    $("#buttonDelete").css('display', 'block');
    $("#buttonRegistration").css('display', 'none');

    $(".modalInformation").css('display', 'none');
    // location.reload();
}
/*//////データの取得(初期処理)///////////////////////////////////////////////*/

var dataInformation = []

function getData() {

    var dataset_information = []

    var lineInformation = JSON.parse(Information);

    dataset_information.push(lineInformation);

    for (var i = 0; i < lineInformation.length; i++) {
        Object.values(dataset_information[0][i]);
        dataInformation.push(Object.values(dataset_information[0][i]));
    }

}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////メインテーブル作成(初期処理)/////////////////////////////////////////*/

function makeMainTable() {

    var tBody = document.getElementById("myTbody");

    for (var count = 0; count < dataInformation.length; count++) {
        var row = tBody.insertRow(count);

        var c1 = row.insertCell(0);
        var c2 = row.insertCell(1);
        var c3 = row.insertCell(2);
        var c4 = row.insertCell(3);

        c1.style.cssText = "width:150px; border-color:lightgray; padding-left: 20px; border-top-style:none; height: 50px;";
        c2.style.cssText = "width:500px; border-color:lightgray; padding-left: 20px; border-top-style:none;";
        c3.style.cssText = "width:150px; border-color:lightgray; padding-left: 20px; border-top-style:none;";
        c4.style.cssText = "display:none";
        c1.setAttribute("id", "IFA" + count);
        c2.setAttribute("id", "IFB" + count);
        c3.setAttribute("id", "IFC" + count);
        c4.setAttribute("id", "IFD" + count);
        c1.innerHTML = dataInformation[count][0];
        c2.innerHTML = dataInformation[count][1];
        c3.innerHTML = dataInformation[count][2];
        c4.innerHTML = count;


    }
}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////詳細クリック/////////////////////////////////////////////////////////*/
var Work = "";
var ContactItems = "";
var Filler = "";

/*///////////////////////////////////////////////////////////////////////////*/

$(document).ready(function () {
    getData();
    makeMainTable();
    $('#myTbody tr').attr('class', 'item');
    clickCauseTable();

});

function clickCauseTable() {
    $("#myTbody tr").click(function () {
        // クリックした行の情報取得
        Work = $(this).children('td')[0].innerText;
        ContactItems = $(this).children('td')[1].innerText;
        Filler = $(this).children('td')[2].innerText;

        // 選択されている他の行があれば選択解除
        $(this).siblings().removeClass("selected");
        //クリックされた行を選択
        $(this).addClass("selected");

        //Set item  save in localstorage 
        var index0 = $(this).children()[0].textContent;
        var index1 = $(this).children()[1].textContent;
        var index2 = $(this).children()[2].textContent;
        var index3 = $(this).children()[3].textContent;

        localStorage.setItem('key1', '1');
        localStorage.setItem('index0', index0);
        localStorage.setItem('index1', index1);
        localStorage.setItem('index2', index2);
        localStorage.setItem('index3', index3);

        // 変更画面
        $(".modalInformation").css('display', 'block');
        var Kinmu = localStorage.getItem("index0");
        var RenrakuJikou = localStorage.getItem("index1");
        var hidden = localStorage.getItem("index3")
        // load data
        $("#selectKimu option").each(function () {
            if ($(this).text() == Kinmu) {
                $(this).attr('selected', 'selected');
            }
        });
        //
        $('#input_Information').val(RenrakuJikou);

        $('#inputHiddenModalIF').val(hidden);

        $("#buttonEdit").css('visibility', 'visible');
        $("#buttonDelete").css('visibility', 'visible');
        $("#buttonRegistration").css('display', 'none');

    });
}

