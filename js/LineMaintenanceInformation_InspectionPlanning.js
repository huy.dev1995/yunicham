
/*//////データの取得(初期処理)///////////////////////////////////////////////*/

var dataInspectionPlanning = []

function getData() {

    var dataset_inspectionplanning = []

    var lineInspectionPlanning = JSON.parse(InspectionPlanning);

    dataset_inspectionplanning.push(lineInspectionPlanning);

    for (var i = 0; i < lineInspectionPlanning.length; i++) {
        Object.values(dataset_inspectionplanning[0][i]);
        dataInspectionPlanning.push(Object.values(dataset_inspectionplanning[0][i]));
    }

}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////メインテーブル作成(初期処理)/////////////////////////////////////////*/

function makeMainTable() {

    var tBody = document.getElementById("myTbody");

    for (var count = 0; count < dataInspectionPlanning.length; count++) {
        var row = tBody.insertRow(count);

        var c1 = row.insertCell(0);
        var c2 = row.insertCell(1);
        var c3 = row.insertCell(2);
        var c4 = row.insertCell(3);
        c1.style.cssText = "width:150px; border-color:lightgray; text-align:center; border-top-style:none; height: 50px;";
        c2.style.cssText = "width:200px; border-color:lightgray; text-align:center; border-top-style:none;";
        c3.style.cssText = "width:250px; border-color:lightgray; padding-left: 20px; border-top-style:none;";
        c4.style.cssText = "width:600px; border-color:lightgray; padding-left: 20px; border-top-style:none;";

        c1.innerHTML = dataInspectionPlanning[count][0];
        c2.innerHTML = dataInspectionPlanning[count][1];
        c3.innerHTML = dataInspectionPlanning[count][2];
        c4.innerHTML = dataInspectionPlanning[count][3];

        if (c2.innerHTML == '点検') {
            $('#myTbody tr').attr('onclick', "myFunction1()");
        }

    }
}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////詳細クリック/////////////////////////////////////////////////////////*/
var Work = "";
var ScheduledItems = "";
var Practitioner = "";
var ScheduleContents = "";

/*///////////////////////////////////////////////////////////////////////////*/

$(document).ready(function () {
    getData();
    makeMainTable();
    $('#myTbody tr').attr('class', 'item');
    //メインテーブルのクリック時の処理
    $("#myTbody tr").click(function () {
        // クリックした行の情報取得
        Work = $(this).children('td')[0].innerText;
        ScheduledItems = $(this).children('td')[1].innerText;
        Practitioner = $(this).children('td')[2].innerText;
        ScheduleContents = $(this).children('td')[3].innerText;

        // 選択されている他の行があれば選択解除
        $(this).siblings().removeClass("selected");
        // クリックされた行を選択
        $(this).addClass("selected");
        // // Set item  save in localstorage 

        localStorage.setItem('key1', '0');
        localStorage.setItem('Work', Work);
        localStorage.setItem('ScheduledItems', ScheduledItems);
        localStorage.setItem('Practitioner', Practitioner);
        localStorage.setItem('ScheduleContents', ScheduleContents);

        if (ScheduledItems == "保全") {
            window.location.href = "ConservationPlanInput.html";
        }
        if (ScheduleContents == "工程内チェック(OP)") {
            window.location.href = "ProcessCheckResultInput.html";
        }
        if (ScheduleContents == "工程内チェック(後工程)") {
            window.location.href = "ProcessCheckResultInputLater.html";
        }
    });

});

function myFunction1() {
    window.location.href = "InspectionResultInput.html";
    // var myWindow = window.open("InspectionResultInput.html");
    // var myWindow = window.open("InspectionResultInput.html", "", "width=1350,height=1300");
}
function myFunction2() {
    window.location.href = "ConservationPlanInput.html";

}

