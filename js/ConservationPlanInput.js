/*//////データの取得(初期処理)///////////////////////////////////////////////*/

var dataConservationPlanInput = []
function getData() {

    var dataset_conservationplaninput = []
    var lineConservationPlanInput = JSON.parse(ConservationPlanInput);
    dataset_conservationplaninput.push(lineConservationPlanInput);
    for (var i = 0; i < lineConservationPlanInput.length; i++) {
        Object.values(dataset_conservationplaninput[0][i]);
        dataConservationPlanInput.push(Object.values(dataset_conservationplaninput[0][i]));
    }


}
/*///////////////////////////////////////////////////////////////////////////*/

/*//////メインテーブル作成(初期処理)/////////////////////////////////////////*/

function makeMainTable() {

    var tBody = document.getElementById("myTbody");
    var index = 0;
    for (var count = 0; count < dataConservationPlanInput.length; count++) {
        index++;
        var row = tBody.insertRow(count);

        var c1 = row.insertCell(0);
        var c2 = row.insertCell(1);
        var c3 = row.insertCell(2);
        var c4 = row.insertCell(3);
        var c5 = row.insertCell(4);
        var c6 = row.insertCell(5);
        var c7 = row.insertCell(6);
        var c8 = row.insertCell(7);

        c1.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none; height: 50px; ";
        c2.style.cssText = "border-color:lightgray; text-align: center; padding-left: 15px; border-top-style:none;";
        c3.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none;";
        c4.style.cssText = "border-color:lightgray; text-align: center; border-top-style:none;";
        c5.style.cssText = "border-color:lightgray; text-align: end;padding-right: 15px; border-top-style:none;";
        c6.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none;";
        c7.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none;";
        c8.style.cssText = "border-color:lightgray; border-top-style:none;";

        c1.innerHTML = dataConservationPlanInput[count][0];
        c2.innerHTML = dataConservationPlanInput[count][1];
        c3.innerHTML = dataConservationPlanInput[count][2];
        c4.innerHTML = dataConservationPlanInput[count][3];
        c5.innerHTML = dataConservationPlanInput[count][4];
        c6.innerHTML = dataConservationPlanInput[count][6];

        var buttonhtml ="<div class='area'>" +
        "<input type='radio' name='tab_name" + index + "' frameborder='0' id='tabOK" + index + "' value='1'>" +
        "<label class='tab_class' for='tabOK" + index + "'> OK </label>" +
        "<input type='radio' name='tab_name" + index + "'  id='tabReplacementRequired" + index + "' value='2'>" +
        "<label class='tab_class' for='tabReplacementRequired" + index + "'>NG</label>" +
        " </div>";
        var selectbox = "<input type='text' id='inputKomento' name='inputKomento' style='width: 95%;' class='selectDateTime inputLeftMargin'>"
     
        c7.innerHTML = buttonhtml;
        c8.innerHTML = selectbox;

    }

}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////詳細クリック/////////////////////////////////////////////////////////*/
var InspectionPoint = "";
var Item = "";
var Method = "";
var EvaluationCriteria = "";
var Result = "";
var key = localStorage.getItem("key1");
$(document).ready(function () {
    getData();
    makeMainTable();
    // get dat from InspectionResultList.html
    // if (key == "1") {
    //     //get value from loalstrorage set at label
    //     var plans = localStorage.getItem("indexCell0");
    //     $('#idPlans').text(plans);
    //     var inspectionType = localStorage.getItem("indexCell2");
    //     $('#idInspectionType').text(inspectionType);
    //     $('#child1').text("点検結果一覧");
    //     $('#href1').attr('href', 'InspectionResultList.html');
    //     $('#child2').css("display","none");
    //     $('#hiddenA').css("display","none");

    // }
    if (key == "0") {
        //get value from loalstrorage set at label
        // var plans = localStorage.getItem("indexCell0");
        $('#idPlans').text("2020/12/19");
        var inspectionType0 = localStorage.getItem("ScheduleContents");
        $('#idInspectionType').text("2021/2/1 (月) 1直");
        $('#child1').text("設備情報一覧");
        $('#href1').attr('href', 'LineMaintenanceList.html');
        $('#child2').text("設備情報");
        $('#href2').attr('href', 'LineMaintenanceInformation.html');
    }

});
function btnCancel() {

    if (key == "1") {
        window.location.href = "InspectionResultList.html";
    }
    else {
        window.location.href = "LineMaintenanceInformation.html";
    }

}
function btnHold() {
    if (key == "1") {
        window.location.href = "InspectionResultList.html";
    }
    else {
        window.location.href = "LineMaintenanceInformation.html";
    }
}