// ------------------------------------------------------------------
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/date
// https://stackoverflow.com/questions/1531093/how-do-i-get-the-current-date-in-javascript
////////////////////////////////setDefaultDayIsToday////////////////////////////////
// -------------------------------------------------------------------------------//
//////////////////////////////////////theModal//////////////////////////////////////
// ------------------------------------------------------
// https://www.w3schools.com/howto/howto_css_modals.asp
//////////////////////////////////////theModal//////////////////////////////////////
var key = localStorage.getItem("keyHozenkekka");
$(document).ready(function () {
  var key = localStorage.getItem("keyHozenkekka");
  if (key == 0) {
    $('#child1').text("確認対象一覧");
    $('#achild1').attr('href', 'ConfirmationList.html');
    $('#child2').text("保全結果確認");
  }
  if (key == 1) {
    $('.div31').css("display", "none");
    $('#buttonConfirm').css("display", "none");
    $('#child1').text("保全結果一覧");
    $('#achild1').attr('href', 'MaintenanceResultList.html');
    $('#child2').text("保全結果参照");

  }
  getData1();
  makeMainTable1();

  getData21();
  makeMainTable21();

  getData22();
  makeMainTable22();
});

/*///////////////////////////////////////////////////////////////////////////*/

// When click button 戻る
function btnCancel() {
  if (key == 0) {
    window.location.href = "ConfirmationList.html";
  }
  if (key == 1) {
    window.location.href = "MaintenanceResultList.html";
  }
}

// When click button 確認
function buttonConfirm() {
  if (key == 0) {
    window.location.href = "ConfirmationList.html";
  }
  window.location.href = "MaintenanceResultList.html";
}
/*///////////////////////////////////////////////////////////////////////////*/

/*/////////////////////startJSON_Ref1/////////////////////////////////////*/
var dataMaintenanceResultReferenceKekka = []
function getData1() {
  var dataSetMaintenanceResultReferenceKekka = []
  var lineMaintenanceResultReferenceKekka = JSON.parse(MaintenanceResultConfirm);
  dataSetMaintenanceResultReferenceKekka.push(lineMaintenanceResultReferenceKekka);
  for (var i = 0; i < lineMaintenanceResultReferenceKekka.length; i++) {
    Object.values(dataSetMaintenanceResultReferenceKekka[0][i]);
    dataMaintenanceResultReferenceKekka.push(Object.values(dataSetMaintenanceResultReferenceKekka[0][i]));
  }
}
function makeMainTable1() {
  var tBody = document.getElementById("myTbody1");
  for (var count = 0; count < dataMaintenanceResultReferenceKekka.length; count++) {
    var row = tBody.insertRow(count);

    var c1 = row.insertCell(0);
    var c2 = row.insertCell(1);
    var c3 = row.insertCell(2);
    var c4 = row.insertCell(3);
    var c5 = row.insertCell(4);
    var c6 = row.insertCell(5);
    var c7 = row.insertCell(6);
    var c8 = row.insertCell(7);

    c1.style.cssText = "width: 9%; border-color:lightgray; text-align: center; border-top-style:none; height: 38px; min-width: 100px;";
    c2.style.cssText = "width: 22%; border-color:lightgray; text-align: left; border-top-style:none; padding-left: 15px; min-width: 250px;";
    c3.style.cssText = "width: 7%; border-color:lightgray; text-align: center; border-top-style:none; min-width: 50px;";
    c4.style.cssText = "width: 13%; border-color:lightgray; text-align: right; border-top-style:none; padding-right: 15px; min-width: 100px;";
    c5.style.cssText = "width: 15%; border-color:lightgray; text-align: left; border-top-style:none; padding-left: 15px; min-width: 100px;";
    c6.style.cssText = "width: 15%; border-color:lightgray; text-align: left; border-top-style:none; padding-left: 15px; min-width: 100px;";
    c7.style.cssText = "width: 11%; border-color:lightgray; text-align: left; border-top-style:none; padding-left: 15px; min-width: 100px;";
    c8.style.cssText = "width: 8%; border-color:lightgray; text-align: center; border-top-style:none; min-width: 50px;";

    c1.innerHTML = dataMaintenanceResultReferenceKekka[count][0];
    c2.innerHTML = dataMaintenanceResultReferenceKekka[count][1];
    c3.innerHTML = dataMaintenanceResultReferenceKekka[count][2];
    c4.innerHTML = dataMaintenanceResultReferenceKekka[count][3];
    c5.innerHTML = dataMaintenanceResultReferenceKekka[count][4];
    c6.innerHTML = dataMaintenanceResultReferenceKekka[count][5];
    c7.innerHTML = dataMaintenanceResultReferenceKekka[count][6];
    c8.innerHTML = dataMaintenanceResultReferenceKekka[count][7];
  }
}
/*/////////////////////endJSON_Ref1/////////////////////////////////////*/

/*/////////////////////startJSON_Ref21/////////////////////////////////////*/
var dataMaintenanceResultReferenceKakunin1 = []
function getData21() {
  var dataSetMaintenanceResultReferenceKakunin1 = []
  var lineMaintenanceResultReferenceKakunin1 = JSON.parse(MaintenanceResultConfirm1);
  dataSetMaintenanceResultReferenceKakunin1.push(lineMaintenanceResultReferenceKakunin1);
  for (var i = 0; i < lineMaintenanceResultReferenceKakunin1.length; i++) {
    Object.values(dataSetMaintenanceResultReferenceKakunin1[0][i]);
    dataMaintenanceResultReferenceKakunin1.push(Object.values(dataSetMaintenanceResultReferenceKakunin1[0][i]));
  }
}
function makeMainTable21() {
  //var tBody = document.getElementById("myTbody21");
  var myRow1 = document.getElementById("myRow1");
  var myRow2 = document.getElementById("myRow2");
  for (var count = 0; count < dataMaintenanceResultReferenceKakunin1.length; count++) {
    var c11 = myRow1.insertCell(count);
    var c21 = myRow2.insertCell(count);

    c11.style.cssText = "width: 15%; border-color:lightgray; text-align: center; height: 45px; background-color: #D3F4F9;";
    c21.style.cssText = "width: 15%; border-color:lightgray; text-align: center; height: 40px;";

    c11.innerHTML = dataMaintenanceResultReferenceKakunin1[count][0];
    c21.innerHTML = dataMaintenanceResultReferenceKakunin1[count][1];
  }
}
/*/////////////////////endJSON_Ref21/////////////////////////////////////*/

/*/////////////////////startJSON_Ref22/////////////////////////////////////*/
var dataMaintenanceResultReferenceKakunin2 = []
function getData22() {
  var dataSetMaintenanceResultReferenceKakunin2 = []
  var lineMaintenanceResultReferenceKakunin2 = JSON.parse(MaintenanceResultConfirm2);
  dataSetMaintenanceResultReferenceKakunin2.push(lineMaintenanceResultReferenceKakunin2);
  for (var i = 0; i < lineMaintenanceResultReferenceKakunin2.length; i++) {
    Object.values(dataSetMaintenanceResultReferenceKakunin2[0][i]);
    dataMaintenanceResultReferenceKakunin2.push(Object.values(dataSetMaintenanceResultReferenceKakunin2[0][i]));
  }
}
function makeMainTable22() {
  var tBody = document.getElementById("myTbody22");
  for (var count = 0; count < dataMaintenanceResultReferenceKakunin2.length; count++) {
    var row = tBody.insertRow(count);

    var c1 = row.insertCell(0);
    var c2 = row.insertCell(1);
    var c3 = row.insertCell(2);
    var c4 = row.insertCell(3);

    c1.style.cssText = "width: 15%; border-color:lightgray; text-align: left; border-top-style:none; padding-left: 15px; height: 40px; min-width: 150px;";
    c2.style.cssText = "width: 15%; border-color:lightgray; text-align: left; border-top-style:none; padding-left: 15px; min-width: 170px;";
    c3.style.cssText = "width: 15%; border-color:lightgray; text-align: center; border-top-style:none; min-width: 70px;";
    c4.style.cssText = "width: 55%; border-color:lightgray; text-align: left; border-top-style:none; padding-left: 15px; min-width: 100px;";

    c1.innerHTML = dataMaintenanceResultReferenceKakunin2[count][0];
    c2.innerHTML = dataMaintenanceResultReferenceKakunin2[count][1];
    c3.innerHTML = dataMaintenanceResultReferenceKakunin2[count][2];
    c4.innerHTML = dataMaintenanceResultReferenceKakunin2[count][3];
  }
}
/*/////////////////////endJSON_Ref22/////////////////////////////////////*/

// When click button sort
function sortTable(n) {
  var tBody, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  tBody = document.getElementById("myTbody22");
  switching = true;
  //Set the sorting direction to ascending:
  dir = "asc";
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = tBody.rows;
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 0; i < (rows.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /*check if the two rows should switch place,
      based on the direction, asc or desc:*/
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      //Each time a switch is done, increase this count by 1:
      switchcount++;
    } else {
      /*If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again.*/
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}

// When click button sort(number)
function sortTableNumber(n) {
  var tBody, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  tBody = document.getElementById("myTbody22");
  switching = true;
  //Set the sorting direction to ascending:
  dir = "asc";
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = tBody.rows;
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 0; i < (rows.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /*check if the two rows should switch place,
      based on the direction, asc or desc:*/
      if (dir == "asc") {
        if (parseInt(x.innerHTML) > parseInt(y.innerHTML)) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      } else if (dir == "desc") {
        if (parseInt(x.innerHTML) < parseInt(y.innerHTML)) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      //Each time a switch is done, increase this count by 1:
      switchcount++;
    } else {
      /*If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again.*/
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}

// When click button sort(date)
function sortTableDate(n) {
  var tBody, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  tBody = document.getElementById("myTbody22");
  switching = true;
  //Set the sorting direction to ascending:
  dir = "asc";
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = tBody.rows;
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 0; i < (rows.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];

      var datex = x.textContent.split("/");
      var yearx = datex[0];
      var monthx = datex[1];
      var dayx = datex[2];

      var datey = y.textContent.split("/");
      var yeary = datey[0];
      var monthy = datey[1];
      var dayy = datey[2];

      /*check if the two rows should switch place,
      based on the direction, asc or desc:*/
      if (dir == "asc") {
        if (parseInt(yearx) > parseInt(yeary)) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        } if (parseInt(yearx) == parseInt(yeary)) {
          if (parseInt(monthx) > parseInt(monthy)) {
            //if so, mark as a switch and break the loop:
            shouldSwitch = true;
            break;
          } if (parseInt(monthx) == parseInt(monthy)) {
            if (parseInt(dayx) > parseInt(dayy)) {
              //if so, mark as a switch and break the loop:
              shouldSwitch = true;
              break;
            }
          }
        }
      } else if (dir == "desc") {
        if (parseInt(yearx) < parseInt(yeary)) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
        if (parseInt(yearx) == parseInt(yeary)) {
          if (parseInt(monthx) < parseInt(monthy)) {
            //if so, mark as a switch and break the loop:
            shouldSwitch = true;
            break;
          }
          if (parseInt(monthx) == parseInt(monthy)) {
            if (parseInt(dayx) < parseInt(dayy)) {
              //if so, mark as a switch and break the loop:
              shouldSwitch = true;
              break;
            }
          }
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      //Each time a switch is done, increase this count by 1:
      switchcount++;
    } else {
      /*If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again.*/
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}