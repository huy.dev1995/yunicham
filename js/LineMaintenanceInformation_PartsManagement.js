/*//////追加クリック////////////////////////////////////////////////////////*/

function clickTsuika() {

    $(".modalPartsManagement").css('display', 'block');
    $("#input_PartsManagement").val('');

    $("#buttonEdit").css('display', 'none');
    $("#buttonDelete").css('display', 'none');
    $("#buttonRegistration").css('display', 'block');
}
function buttonCancel() {
    $(".modalPartsManagement").css('display', 'none');
    // location.reload();
}
function buttonRegistration() {
    var inputPM = $("#input_PartsManagement").val();

    var inputHiddenTable = $("#inputHiddenTablePM").val();
    var convert = parseInt(inputHiddenTable);
    convert = convert + 1;

    $("#inputHiddenTablePM").val(convert);

    var htmlPM = " <tr class='item'><td id='PMA" + convert + "' style='width: 700px; border-color: lightgray; padding-left: 20px; border-top-style: none; height: 50px;'>" + inputPM + "</td><td id='PMB" + convert + "' style='width: 150px; border-color: lightgray; padding-left: 20px; border-top-style: none;'>XXXX</td><td id='PMC" + convert + "'style='display:none'>" + convert + "</td></tr>"

    $("#myTbody").append(htmlPM);

    $(".modalPartsManagement").css('display', 'none');
    clickCauseTable();
    $("#buttonEdit").css('display', 'block');
    $("#buttonDelete").css('display', 'block');
    $("#buttonRegistration").css('display', 'none');
    // location.reload();
}
function buttonEdit() {
    var inputEditPM = $("#input_PartsManagement").val();
    var idhidden = $("#inputHiddenModalPM").val();

    $("#PMA" + idhidden).text(inputEditPM);
    $("#PMC" + idhidden).text(idhidden);

    $(".modalPartsManagement").css('display', 'none');
    // location.reload();
}
function buttonDelete() {
    $("#myTbody").find('tr[class="item selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }

    })

    $("#buttonEdit").css('display', 'block');
    $("#buttonDelete").css('display', 'block');
    $("#buttonRegistration").css('display', 'none');
    $(".modalPartsManagement").css('display', 'none');
    // location.reload();
}

/*//////データの取得(初期処理)///////////////////////////////////////////////*/

var dataPartsManagement = []

function getData() {

    var dataset_partsmanagement = []

    var linePartsManagement = JSON.parse(PartsManagement);

    dataset_partsmanagement.push(linePartsManagement);

    for (var i = 0; i < linePartsManagement.length; i++) {
        Object.values(dataset_partsmanagement[0][i]);
        dataPartsManagement.push(Object.values(dataset_partsmanagement[0][i]));
    }

}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////メインテーブル作成(初期処理)/////////////////////////////////////////*/

function makeMainTable() {

    var tBody = document.getElementById("myTbody");

    for (var count = 0; count < dataPartsManagement.length; count++) {
        var row = tBody.insertRow(count);

        var c1 = row.insertCell(0);
        var c2 = row.insertCell(1);
        var c3 = row.insertCell(2);

        c1.setAttribute("id", "PMA" + count);
        c2.setAttribute("id", "PMB" + count);
        c3.setAttribute("id", "PMC" + count);

        c1.style.cssText = "width:700px; border-color:lightgray; padding-left: 20px; border-top-style:none; height: 50px;";
        c2.style.cssText = "width:150px; border-color:lightgray; padding-left: 20px; border-top-style:none;";
        c3.style.cssText = "display:none";

        c1.innerHTML = dataPartsManagement[count][0];
        c2.innerHTML = dataPartsManagement[count][1];
        c3.innerHTML = count;
        $('#inputHiddenTablePM').val(count);


    }
}

/*//////詳細クリック/////////////////////////////////////////////////////////*/
var PartsManagementinput = "";
var Filler = "";

//*///////////////////////////////////////////////////////////////////////////*/

$(document).ready(function () {
    getData();
    makeMainTable();
    $('#myTbody tr').attr('class', 'item');
    clickCauseTable();

});
function clickCauseTable() {

$("#myTbody tr").click(function () {
    // クリックした行の情報取得
    PartsManagementinput = $(this).children('td')[0].innerText;
    Filler = $(this).children('td')[1].innerText;

    // 選択されている他の行があれば選択解除
    $(this).siblings().removeClass("selected");
    //クリックされた行を選択
    $(this).addClass("selected");

    //Set item  save in localstorage 
    var index0 = $(this).children()[0].textContent;
    var index1 = $(this).children()[1].textContent;
    var index2 = $(this).children()[2].textContent;
    localStorage.setItem('key1', '1');
    localStorage.setItem('index0', index0);
    localStorage.setItem('index1', index1);
    localStorage.setItem('index2', index2);
    // 変更画面
    $(".modalPartsManagement").css('display', 'block');
    var BuhinKanri = localStorage.getItem("index0");
    var hidden = localStorage.getItem("index2")
    // load data
    $('#input_PartsManagement').val(BuhinKanri);
    $('#inputHiddenModalPM').val(hidden);

    $("#buttonEdit").css('visibility', 'visible');
    $("#buttonDelete").css('visibility', 'visible');
    $("#buttonRegistration").css('display', 'none');

});

}
