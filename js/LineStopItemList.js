////////////////////////////////setDefaultDayIsToday////////////////////////////////
// // ------------------------------------------------------------------
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/date
// https://stackoverflow.com/questions/1531093/how-do-i-get-the-current-date-in-javascript
$(document).ready(function () {

    var x = document.documentURI;
    var m = x.replace("html/LineStopItemList.html", '');

    var plus = m + "Icons" + "/" + "plus.jpg";
    var minus = m + "Icons" + "/" + "minus.jpg";
    // var lookingGlass = m + "Icons" + "/" + "search.jpg";

    document.getElementById("plusIcon").src = plus;
    document.getElementById("minusIcon").src = minus;
    // document.getElementById("lookingGlassIcon").src = lookingGlass;

});
////////////////////////////////setDefaultDayIsToday////////////////////////////////
// -------------------------------------------------------------------------------//

$(document).ready(function () {
    var dateControl1 = document.querySelector('input[id="date1"]');
    var dateControl2 = document.querySelector('input[id="date2"]');

    ///////////////////////////////////
    var getToday = new Date();
    var dd = String(getToday.getDate()).padStart(2, '0');
    var mm = String(getToday.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = getToday.getFullYear();
    ///////////////////////////////////
    getToday = yyyy + '-' + mm + '-' + dd;
    dateControl1.value = getToday;
    dateControl2.value = getToday;
});


/*//////編集クリック////////////////////////////////////////////////////////*/

function buttonEdit() {
    if (lineStopItemNo == null) {
        // 行未選択の場合、画面遷移させない。
        alert("行が選択されていません。");
    } else {
        localStorage.clear();
        localStorage.setItem("LineStopItemNo", lineStopItemNo);

        //awaring LineStopItemList.html pages
        localStorage.setItem("LineStopItemList_html_pages", "true");
        //awaring LineStopItemList.html pages

        // 停止項目登録画面へ遷移
        var x = document.documentURI;
        // alert(x);
        var m = x.replace("LineStopItemList.html", '');
        // alert(m);
        window.location.href = m + "LineStopItemEdit.html";
        // window.location.href = "LineStopItemEdit.html";
    }
}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////セレクト絞り込み/////////////////////////////////////////////////////*/

// 停止項目セレクト1選択時
function changeSelectTeishi1(value) {
    $('#selIjou').prop('disabled', false);

    $('.option').addClass("displayDiv");
    $('.option' + value).removeClass("displayDiv");

    document.getElementById('selTeishi2').selectedIndex = 0;
    document.getElementById('selIjou').selectedIndex = 0;
}

// 停止項目セレクト2選択時
function changeSelectTeishi2(value) {
    $('#selIjou').prop('disabled', false);
}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////データの取得(初期処理)///////////////////////////////////////////////*/

var dataStopItem = []
var dataStopCause = []
var dataLineOperation = []
var dataAbnormalItem = []

function getData() {
    var dataset_stopitem = []
    var dataset_stopcause = []
    var dataset_lineoperation = []
    var dataset_abnormalitem = []

    var lineStopItem = JSON.parse(LineStopItem);


    var lineStopCause = JSON.parse(LineStopCause);


    var lineOperation = JSON.parse(LineOperation);


    var lineAbnormalItem = JSON.parse(LineAbnormalItem);

    dataset_stopitem.push(lineStopItem);
    dataset_stopcause.push(lineStopCause);
    dataset_lineoperation.push(lineOperation);
    dataset_abnormalitem.push(lineAbnormalItem);

    for (var i = 0; i < lineStopItem.length; i++) {
        Object.values(dataset_stopitem[0][i]);
        dataStopItem.push(Object.values(dataset_stopitem[0][i]));
    }

    for (var i = 0; i < lineStopCause.length; i++) {
        Object.values(dataset_stopcause[0][i]);
        dataStopCause.push(Object.values(dataset_stopcause[0][i]));
    }

    for (var i = 0; i < lineOperation.length; i++) {
        Object.values(dataset_lineoperation[0][i]);
        dataLineOperation.push(Object.values(dataset_lineoperation[0][i]));
    }

    for (var i = 0; i < lineAbnormalItem.length; i++) {
        Object.values(dataset_abnormalitem[0][i]);
        dataAbnormalItem.push(Object.values(dataset_abnormalitem[0][i]));
    }
}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////メインテーブル作成(初期処理)/////////////////////////////////////////*/


function makeMainTable() {

    var x = document.documentURI;
    var m = x.replace("html/LineStopItemList.html", '');

    var linkGazou = m + "Icons" + "/" + "pictures.jpg";
    var linkNote = m + "Icons" + "/" + "notes.jpg";


    document.getElementsByClassName("iconGazou").src = linkGazou;
    document.getElementsByClassName("iconNote").src = linkNote;


    var tBody = document.getElementById("myTbody");

    var rowCount = [];
    var rowCountCase = 0;


    for (var i = 0; i < dataStopItem.length; i++) {

        var t = dataStopItem[i][0];

        //***********************************************
        for (var j = 0; j < dataStopCause.length; j++) {

            var h = dataStopCause[j][0];

            if (t == h) {
                rowCountCase++;
            }

        };
        //***********************************************
        rowCount.push(i + ":" + rowCountCase);
        rowCountCase = 0;

    };

    var numberRow = 0;
    for (var i = 0; i < rowCount.length; i++) {
        let [x, y] = rowCount[i].split(":");
        if (y == 0) {
            numberRow++;
        } else if (y == 1) {
            numberRow++;
        } else {
            var a = y - 1;
            numberRow++;
            numberRow = numberRow + a;
        }
    };

    //*****************************************************
    //*                                                   *
    //*                      Tuyo                         *
    //*                                                   *
    //*****************************************************

    var dtmhien = 0;
    for (var count = 0; count < numberRow; count++) {
        var row = tBody.insertRow(count);

        if (dtmhien == rowCount.length) {
            dtmhien = dtmhien - 1;
        }

        //----------------------------------
        let [x, y] = rowCount[dtmhien].split(":");
        //----------------------------------

        if (y == 0) {
            hpmlong = x;
        } else if (y == 1) {
            hpmlong = x;
        } else {
            hpmlong = x;
        }

        var b = dataStopItem.length;

        var c1 = row.insertCell(0);
        var c2 = row.insertCell(1);
        var c3 = row.insertCell(2);
        var c4 = row.insertCell(3);
        var c5 = row.insertCell(4);
        var c6 = row.insertCell(5);
        var c7 = row.insertCell(6);
        var c8 = row.insertCell(7);
        var c9 = row.insertCell(8);
        var c10 = row.insertCell(9);
        var c11 = row.insertCell(10);

        var c12 = row.insertCell(11);
        var c13 = row.insertCell(12);
        var c14 = row.insertCell(13);
        var c15 = row.insertCell(14);
        var c16 = row.insertCell(15);
        var c17 = row.insertCell(16);
        var c18 = row.insertCell(17);
        var c19 = row.insertCell(18);

        //詳細
        var c20 = row.insertCell(19);

        //日付
        c1.style.cssText = "width:5%; border-color:lightgray; padding-left: 10px; border-top-style:none; font-size: 13px; height: 70px;";

        //勤務
        c2.style.cssText = "width:3%; border-color:lightgray; text-align:center; border-top-style:none; font-size: 14px; height: 70px; ";

        //発生 : dataStopItem[][2]
        c3.style.cssText = "width:5%; border-color:lightgray; padding-left: 10px;border-top-style:none; font-size: 14px; height: 70px;";

        //復旧 : dataStopItem[][3]
        c4.style.cssText = "width:5%; border-color:lightgray; padding-left: 10px; border-top-style:none; font-size: 14px; height: 70px;";

        //停止 : dataStopItem[][1]
        c5.style.cssText = "width:5%; border-color:lightgray; padding-left: 10px; border-top-style:none; font-size: 14px; height: 70px;";

        //停止区分 : 停止時間、停止項目より判定(現行の処理のままできると思います)
        c6.style.cssText = "width:5%; border-color:lightgray; text-align:center; border-top-style:none; font-size: 14px; height: 70px;";

        //停止項目 : dataStopItem[][4] - LineStopItem[][5]
        c7.style.cssText = "width:10%; border-color:lightgray; padding-left: 10px; border-top-style:none; font-size: 14px; height: 70px;";

        //異常内容 : dataStopItem[][6]
        c8.style.cssText = "width:7%; border-color:lightgray; padding-left: 10px; border-top-style:none; font-size: 14px";

        //対策種別 : dataStopCause[][2]
        c9.style.cssText = "width:5%; border-color:lightgray; text-align:center; border-top-style:none; font-size: 14px; height: 70px;";

        //対策レベル : dataStopCause[][3]
        c10.style.cssText = "width:5%; border-color:lightgray; text-align:center; border-top-style:none; font-size: 14px; height: 70px;";

        //要因種別 : dataStopCause[][4]
        c11.style.cssText = "width:5%; border-color:lightgray; text-align:center; border-top-style:none; font-size: 14px; height: 70px;";

        //現象 : dataStopCause[][5]
        c12.style.cssText = "width:7%; border-color:lightgray; padding-left: 10px; border-top-style:none; font-size: 14px; height: 70px;";

        //原因 : dataStopCause[][6]
        c13.style.cssText = "width:7%; border-color:lightgray; padding-left: 10px; border-top-style:none; font-size: 14px; height: 70px;";

        //処置・対策 : dataStopCause[][7]
        c14.style.cssText = "width:7%; border-color:lightgray; padding-left: 10px; border-top-style:none; font-size: 14px; height: 70px;";

        //段替え : dataStopItem[][7]、dataStopItem[][8]を使用(現行の処理のままできると思います)
        c15.style.cssText = "width:5%; border-color:lightgray; padding-left: 10px; border-top-style:none; font-size: 14px; height: 70px;";

        //対応者 : XXXXX
        c16.style.cssText = "width:4%; border-color:lightgray; padding-left: 10px; border-top-style:none; font-size: 14px; height: 70px;";

        //画像 : 空欄
        c17.style.cssText = "width:3%; border-color:lightgray; text-align:center; border-top-style:none; font-size: 14px; height: 70px;";

        //メモ : 空欄
        c18.style.cssText = "width:3%; border-color:lightgray; text-align:center; border-top-style:none; font-size: 14px; height: 70px;";

        //分割 : dataStopItem, dataAbnormalItemから有/無の判断(現行の処理のままできると思います。)
        c19.style.cssText = "width:10%; border-color:lightgray; text-align:center; border-top-style:none; font-size: 14px; height: 70px;";

        //詳細
        c20.style.cssText = "width:10%; border-color:lightgray; text-align:center; border-top-style:none; height: 70px;";
        // **********************************************************************************************
        // **********************************************************************************************
        // **********************************************************************************************
        // **********************************************************************************************

        //*********************************************************************************************
        //c1_日付
        c1.innerHTML = '2021/03/29'
            + '<input name="lineStopId" value=' + dataStopItem[hpmlong][0] + ' type="hidden">';

        //勤務
        c2.innerHTML = '1値';

        var hour = dataStopItem[hpmlong][1].substr(0, 2);
        var min = dataStopItem[hpmlong][1].substr(3, 2);
        var sec = dataStopItem[hpmlong][1].substr(6, 2);

        //c3_発生
        c3.innerHTML = dataStopItem[hpmlong][2];

        //c4_復旧
        c4.innerHTML = dataStopItem[hpmlong][3];

        //c5_停止
        if (hour != "00") {
            c5.innerHTML = dataStopItem[hpmlong][1];
        } else {
            c5.innerHTML = min + ":" + sec;
        }

        //c6_停止区分
        if (dataStopItem[hpmlong][4] == "人" || dataStopItem[hpmlong][4] == "その他" || dataStopItem[hpmlong][4] == "不稼働") {
            c6.innerHTML = "不稼働";
        } else {
            if (hour > 0) {
                c6.innerHTML = "長期停止";
            } else if (min >= 5) {
                c6.innerHTML = "長期停止";
            } else {
                c6.innerHTML = "チョコ停";
            }
        }

        //c7_停止項目
        c7.innerHTML = dataStopItem[hpmlong][4] + "-" + dataStopItem[hpmlong][5];

        //c8_異常内容
        c8.innerHTML = dataStopItem[hpmlong][6];


        //c15_段替え
        if (dataStopItem[hpmlong][8] != "") {
            c15.innerHTML = "[段替え前]" + dataStopItem[hpmlong][7] + "</br>[段替え後]" + dataStopItem[hpmlong][8];
        } else {
            c15.innerHTML = "";
        }

        //c16_対応者
        c16.innerHTML = "XXXXX";

        //c17_画像
        // c17.innerHTML = "";
        c17.innerHTML = '<img class="iconGazou" id="iconGazou' + count + '" src="' + linkGazou + '" style="width: 25px; height: 25px; padding-left: 1%; opacity: 33%;" onclick="thumbNailModalOpen(this.id)">';
        // c17.innerHTML = '<img class="iconGazou" id="iconGazou' + count + '" src="' + linkGazou + '" style="width: 25px; height: 25px; padding-left: 1%; opacity: 33%;" onclick="">';

        //c18_メモ
        // c18.innerHTML = "";
        // c18.innerHTML = '<img class="iconNote" id="iconNote' + count + '" src="' + linkNote + '" style="width: 25px; height: 25px; padding-left: 1%; opacity: 33%;" onclick="">';
        c18.innerHTML = '<img class="iconNote" id="iconNote' + count + '" src="' + linkNote + '" style="width: 25px; height: 25px; padding-left: 1%; opacity: 33%;" onclick="memoModalOpen(this.id)">';


        var TagId = [];
        // 紐づけされたタグIDの全取得
        for (var i = 0; i < dataAbnormalItem.length; i++) {
            if (dataStopItem[hpmlong][0] == dataAbnormalItem[i][0]) {
                TagId.push(dataAbnormalItem[i][1]);
            }
        }

        //c19_分割
        c19.innerHTML = "無";
        //********************************************************************
        // タグIDに紐づけされた別の停止項目IDがあれば"有"を表示
        for (var i = 0; i < dataAbnormalItem.length; i++) {
            for (var j = 0; j < TagId.length; j++) {
                if (TagId[j] == dataAbnormalItem[i][1]) {
                    if (dataStopItem[hpmlong][0] != dataAbnormalItem[i][0]) {
                        c19.innerHTML = "有";
                        break;
                        break;
                    }
                }
            }
        }
        //********************************************************************

        c20.innerHTML = '<button type="button" class="btn myBtn1 buttonSizeShousai" id="buttonShousaiID' + count + '" style="text-align: center; margin-right: 5px; margin-left: 5px;" onclick = "clickDetails(this.id)">詳細</button>';

        //*******************************************************************************************

        if (y == 0) {

            //c9_対策種別
            c9.innerHTML = "";

            //c10_対策レベル
            c10.innerHTML = "";

            //c11_要因種別
            c11.innerHTML = "";

            //c12_現象
            c12.innerHTML = "";

            //c13_原因
            c13.innerHTML = "";

            //c14_処置・対策
            c14.innerHTML = "";

        } else if (y == 1) {

            //c9_対策種別
            c9.innerHTML = dataStopCause[hpmlong][2];

            //c10_対策レベル
            c10.innerHTML = dataStopCause[hpmlong][3];

            //c11_要因種別
            c11.innerHTML = dataStopCause[hpmlong][4];

            //c12_現象
            c12.innerHTML = dataStopCause[hpmlong][5];

            //c13_原因
            c13.innerHTML = dataStopCause[hpmlong][6];

            //c14_処置・対策
            c14.innerHTML = dataStopCause[hpmlong][7];

        } else {

            var flag

            hpmlong = x - 1;

            if (flag == true) {

                hpmlong = hpmlong + 1;

            }

            //c9_対策種別
            c9.innerHTML = dataStopCause[hpmlong][2];

            //c10_対策レベル
            c10.innerHTML = dataStopCause[hpmlong][3];

            //c11_要因種別
            c11.innerHTML = dataStopCause[hpmlong][4];

            //c12_現象
            c12.innerHTML = dataStopCause[hpmlong][5];

            //c13_原因
            c13.innerHTML = dataStopCause[hpmlong][6];

            //c14_処置・対策
            c14.innerHTML = dataStopCause[hpmlong][7];

            flag = true;

        }

        //*******************************************************************************************	        

        dtmhien++;
    };

}



/*///////////////////////////////////////////////////////////////////////////*/

/*//////詳細クリック/////////////////////////////////////////////////////////*/

// 選択中の停止項目No
var lineStopItemNo = null;
// 行選択の初期値(ヘッダー部クリック、未選択では詳細表示、編集を不可とする。)
//var lineNo = 99;

//******************************
//*                            *
//******************************

// 日付取得用
var hiduke = "";

//勤務
var kinnmu = "";

//号機
var gouki = "";

// 発生時間
var teishiST = "";

// 復旧時間
var teishiEND = "";

// 停止時間取得用
var teishi = "";

// 停止区分の値取得用
var kubun = "";

// 停止項目の値取得用
var koumoku = "";

// 異常内容・現象の値取得用
var ijou = "";

//対策種別
var taisakushubetsu = "";

//対策レベル
var taisakureberu = "";

//要因種別
var youinshubetsu = "";

//現像
var genzou = "";

//原因
var genin = "";

//処置・対策
var shochi = "";

//段替え
var dangae = "";

//対応者
var taiousha = "";

//画像
var gazou = "";

//メモ
var memo = "";

// 分割取得用
var bunkatsu = "";

//******************************
//*                            *
//******************************

// Get the modal
var modal = document.getElementById("myModal");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// クリック時
function clickDetails(id) {
    // $('#modalRow1').removeClass("hide");
    // $('#modalRow7').removeClass("hide");
    // $('#modalRow3').removeClass("hide");
    // $('#modalRow5').removeClass("hide");
    // $('#modalRow6').removeClass("hide");
    // $('#modalRow9').removeClass("hide");
    // $('#modalRow10').removeClass("hide");
    // $('#modalRow11').removeClass("hide");
    // $('#modalRow12').removeClass("hide");
    // $('#modalRow13').removeClass("hide");
    // $('#modalRow14').removeClass("hide");

    // クリックした行の情報取得
    // lineStopItemNo = $(this).find("input").val();
    // var t = id.substring(0, 15);
    var m = id.replace("buttonShousaiID", '');
    // alert(m);

    lineStopItemNo = $("#myTbody tr")[m].children[0].children[0].value;

    //日付
    hiduke = $("#myTbody tr")[m].children[0].innerText;

    //勤務
    kinnmu = $("#myTbody tr")[m].children[1].innerText;

    //********************************停止時間***********************************
    //発生
    teishiST = $("#myTbody tr")[m].children[2].innerText;

    //復旧
    teishiEND = $("#myTbody tr")[m].children[3].innerText;

    //停止
    teishi = $("#myTbody tr")[m].children[4].innerText;
    //********************************停止時間***********************************

    //区分
    kubun = $("#myTbody tr")[m].children[5].innerText;

    //項目
    koumoku = $("#myTbody tr")[m].children[6].innerText;

    //異常内容
    ijou = $("#myTbody tr")[m].children[7].innerText;

    //対策種別
    taisakushubetsu = $("#myTbody tr")[m].children[8].innerText;

    //対策レベル
    taisakureberu = $("#myTbody tr")[m].children[9].innerText;

    //要因種別
    youinshubetsu = $("#myTbody tr")[m].children[10].innerText;

    //現像
    genzou = $("#myTbody tr")[m].children[11].innerText;

    //原因
    genin = $("#myTbody tr")[m].children[12].innerText;

    //処置・対策
    shochi = $("#myTbody tr")[m].children[13].innerText;

    //段替え
    dangae = $("#myTbody tr")[m].children[14].innerText;

    //対応者
    taiousha = $("#myTbody tr")[m].children[15].innerText;

    //画像
    // gazou = $("#myTbody tr")[m].children[16].innerText;

    //メモ
    // memo = $("#myTbody tr")[m].children[17].innerText;

    //分割
    bunkatsu = $("#myTbody tr")[m].children[18].innerText;



    $('#modalRow1').show();
    $('#modalRow2').show();
    $('#modalRow3').show();
    $('#modalRow4').show();
    $('#modalRow5').show();
    $('#modalRow6').show();
    $('#modalRow7').show();
    $('#modalRow8').show();
    $('#modalRow9').show();
    $('#modalRow10').show();
    $('#modalRow11').show();
    $('#modalRow12').show();
    $('#modalRow13').show();
    $('#modalRow14').show();

    $('#textIjou').show();

    if (lineStopItemNo == null) {
        // 行未選択の場合、画面遷移させない。
        alert("行が選択されていません。");
    } else {

        //日付
        document.getElementById("textDate").innerText = hiduke;
        if (hiduke == "") {

            $('#modalRow1').hide();
        }

        //勤務
        document.getElementById("textKinmu").innerText = kinnmu;
        if (kinnmu == "") {

            $('#modalRow2').hide();
        }

        //号機
        gouki = "10号機"
        document.getElementById("textGouki").innerText = gouki;
        if (gouki == "") {

            $('#modalRow3').hide();
        }

        //停止時間
        var teishijikan = teishiST + "~" + teishiEND + "(" + teishi + ")";
        document.getElementById("textTeishiJikan").innerText = teishijikan;
        if (teishijikan == "") {

            $('#modalRow4').hide();
        }

        //停止区分
        document.getElementById("textKbn").innerText = kubun;
        if (kubun == "") {

            $('#modalRow5').hide();
        }

        //停止項目
        document.getElementById("textKoumoku").innerText = koumoku;
        if (koumoku == "") {

            $('#modalRow6').hide();
        }

        //対策種別
        document.getElementById("textTaisakushubetsu").innerText = taisakushubetsu;
        if (taisakushubetsu == "") {

            $('#modalRow7').hide();
        }

        //対策レベル
        document.getElementById("textTaisakureberu").innerText = taisakureberu;
        if (taisakureberu == "") {

            $('#modalRow8').hide();
        }

        //要因種別
        document.getElementById("textYouinshubetsu").innerText = youinshubetsu;
        if (youinshubetsu == "") {

            $('#modalRow9').hide();
        }

        //現象
        document.getElementById("textGenzou").innerText = genzou;
        if (genzou == "") {

            $('#modalRow10').hide();
        }

        //真因
        document.getElementById("textGenin").innerText = genin;
        if (genin == "") {

            $('#modalRow11').hide();
        }

        //処置・対策
        document.getElementById("textShochi").innerText = shochi;
        if (shochi == "") {

            $('#modalRow12').hide();
        }

        //対応者
        document.getElementById("textTaiousha").innerText = taiousha;
        if (taiousha == "") {

            $('#modalRow13').hide();
        }

        if (koumoku == "不稼働-段替え") {
            // 段替え：要因・対策を非表示、段替え前後のアイテム表示
            // document.getElementById("tableCause").style.display = "none";
            document.getElementById("IjyouNaiyou").innerText = "段替え";
            document.getElementById("textIjou").innerText = "マミーポコ 男26 GL → オヤスミマン 男22";
        } else if (kubun == "不稼働") {
            // 段替え以外の不稼働：要因・対策を非表示、段替え前後のアイテム非表示
            // document.getElementById("tableCause").style.display = "none";
            document.getElementById("textIjou").innerText = "";
        } else {
            // 不稼働以外：要因・対策を表示、異常内容を表示
            // document.getElementById("tableCause").style.display = "block";
            document.getElementById("textIjou").innerText = ijou;
        }

        if (ijou == "") {

            $('#modalRow15').hide();
        }

        //分割
        document.getElementById("textBunkatsu").innerText = bunkatsu;
        if (bunkatsu == "") {

            $('#modalRow6').hide();
        }

        makeOperationTable();
        // makeCauseTable();

        // for (var i = 1; i < 15; i++) {
        // 	if ($('#modalRow' + i).hasClass("hide")) {

        // 		$('#modalRow' + i).hide();

        // 	}
        // };

        // ポップアップ表示
        $("#myModal").css('display', 'block')
        // modal.style.display = "block";
    }
}

// When the user clicks on <span> (x), close the modal

function btnClose() {
    $("#myModal").css('display', 'none')
    // modal.style.display = "none";
}

// span.onclick = function() {
//     modal.style.display = "none";
// }

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        $("#myModal").css('display', 'none')
        // modal.style.display = "none";
    }
}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////ポップアップメニュー内テーブル作成///////////////////////////////////*/

// 停止状況テーブル
function makeOperationTable() {
    var tOperation = document.getElementById("tBodyOperation");

    while (tOperation.rows.length > 0) tOperation.deleteRow(0);

    // タグIDを全取得
    var TagId = [];
    // 紐づけされたタグIDの全取得
    for (var i = 0; i < dataAbnormalItem.length; i++) {
        if (dataAbnormalItem[i][0] == lineStopItemNo) {
            TagId.push(dataAbnormalItem[i][1]);
        }
    }

    for (var count = 0; count < dataLineOperation.length; count++) {
        for (i = 0; i < TagId.length; i++) {
            if (dataLineOperation[count][0] == TagId[i]) {
                var row = tOperation.insertRow(i);

                var c1 = row.insertCell(0);
                var c2 = row.insertCell(1);
                var c3 = row.insertCell(2);
                var c4 = row.insertCell(3);

                c1.style.cssText = "width:8%; border-color:lightgray; padding-left: 15px;";
                c2.style.cssText = "width:8%; border-color:lightgray; padding-left: 15px;";
                c3.style.cssText = "width:8%; border-color:lightgray; padding-left: 15px;";
                c4.style.cssText = "width:76%; border-color:lightgray; padding-left: 15px;";

                c1.innerHTML = dataLineOperation[count][1];
                c2.innerHTML = dataLineOperation[count][2];
                c3.innerHTML = dataLineOperation[count][3];
                c4.innerHTML = dataLineOperation[count][4];
            }
        }
    }
}

// 要因・対策テーブル
function makeCauseTable() {
    var tCause = document.getElementById("tBodyCause");

    while (tCause.rows.length > 0) tCause.deleteRow(0);

    var insert = 0;
    for (var count = 0; count < dataStopCause.length; count++) {
        if (lineStopItemNo == dataStopCause[count][0]) {
            var row = tCause.insertRow(insert);

            var c1 = row.insertCell(0);
            var c2 = row.insertCell(1);
            var c3 = row.insertCell(2);
            var c4 = row.insertCell(3);
            var c5 = row.insertCell(4);
            var c6 = row.insertCell(5);

            c1.style.cssText = "width:7%; border-color:lightgray; text-align:center;";
            c2.style.cssText = "width:7%; border-color:lightgray; text-align:center;";
            c3.style.cssText = "width:7%; border-color:lightgray; text-align:center;";
            c4.style.cssText = "width:22%; border-color:lightgray; text-align:left;";
            c5.style.cssText = "width:22%; border-color:lightgray; text-align:left;";
            c6.style.cssText = "width:25%; border-color:lightgray; text-align:left;";

            c1.innerHTML = dataStopCause[count][2];
            c2.innerHTML = dataStopCause[count][3];
            c3.innerHTML = dataStopCause[count][4];
            c4.innerHTML = dataStopCause[count][5];
            c5.innerHTML = dataStopCause[count][6];
            c6.innerHTML = dataStopCause[count][7];

            insert++;
        }
    }

}

/*///////////////////////////////////////////////////////////////////////////*/

$(document).ready(function () {
    getData();
    makeMainTable();

    // メインテーブルのクリック時の処理
    $("#myTbody tr").click(function (e) {

        // クリックした行の情報取得
        lineStopItemNo = $(this).find("input").val();

        //日付
        hiduke = $(this).children('td')[0].innerText;

        //勤務
        kinnmu = $(this).children('td')[1].innerText;

        //発生
        teishiST = $(this).children('td')[2].innerText;

        //復旧
        teishiEND = $(this).children('td')[3].innerText;

        //停止
        teishi = $(this).children('td')[4].innerText;

        //区分
        kubun = $(this).children('td')[5].innerText;

        //項目
        koumoku = $(this).children('td')[6].innerText;

        //異常内容
        ijou = $(this).children('td')[7].innerText;

        //対策種別
        taisakushubetsu = $(this).children('td')[8].innerText;

        //対策レベル
        taisakureberu = $(this).children('td')[9].innerText;

        //要因種別
        youinshubetsu = $(this).children('td')[10].innerText;

        //現像
        genzou = $(this).children('td')[11].innerText;

        //原因
        genin = $(this).children('td')[12].innerText;

        //処置・対策
        shochi = $(this).children('td')[13].innerText;

        //段替え
        dangae = $(this).children('td')[14].innerText;

        //対応者
        taiousha = $(this).children('td')[15].innerText;

        //画像
        gazou = $(this).children('td')[16].innerText;

        //メモ
        memo = $(this).children('td')[17].innerText;

        //分割
        bunkatsu = $(this).children('td')[18].innerText;

        // 選択されている他の行があれば選択解除
        $(this).siblings().removeClass("selected");
        // クリックされた行を選択
        $(this).addClass("selected");



        var cellNo = e.target.cellIndex;
        if (cellNo == 16 || cellNo == 17 || cellNo == 19 || cellNo == undefined) {

            lineStopItemNo = $(this).find("input").val();

        } else {

            buttonEdit();

        }


    });
});


/*///////////////////////////////////ShouSaiKenSaku(this.id)////////////////////////////////////////*/
$(document).ready(function () {
    $('.ShouSaiRow').hide();
    $('#ShouSaiKenSakuMinus').hide();
    $('#minusIcon').hide();
});
//-----------------------------
function ShouSaiKenSaku(id) {
    // alert(id);

    var status = id.replace("ShouSaiKenSaku", '');

    if (status == "Plus") {

        $('.ShouSaiRow').show();
        $('#ShouSaiKenSakuMinus').show();
        $('#ShouSaiKenSakuPlus').hide();

        //*******************
        $('#plusIcon').hide();
        $('#minusIcon').show();

    } else {

        $('.ShouSaiRow').hide();
        $('#ShouSaiKenSakuPlus').show();
        $('#ShouSaiKenSakuMinus').hide();

        //*******************
        $('#plusIcon').show();
        $('#minusIcon').hide();
    }

    // var hide = document.getElementById("plusIcon");

    // if (hide.classList[0] == "minus") {
    //     $('.ShouSaiRow').hide();
    //     //*******************
    //     $('#plusIcon').show();
    //     $('#minusIcon').hide();
    // }
}

$(document).ready(function () {
    $('#iconGazou0').css("opacity", "100%");
    $('#iconNote0').css("opacity", "100%");

    $("#div_thumbnailImagesView_" + 1).hide();
    $("#div_thumbnailImagesView_" + 1).addClass("hide");
    $(".ichiranNiModoru").hide();

    //*************************************************

    $("#div_memoView_" + 1).hide();
    $("#div_memoView_" + 1).addClass("hide");
    $(".ichiranNiModoruMemoMain").hide();
});


function thumbNailModalOpen(id) {

    //***************************
    // alert(id);
    //***************************
    var x = id.slice(9);
    var gazouID = "iconGazou" + x;


    var thumbNailModal = document.getElementById(gazouID);
    var opacityValue = thumbNailModal.style.opacity;

    if (opacityValue == "1") {
        // alert(id);
        $("#modal_content_thumbnail").css('display', 'block');
    }


}

function memoModalOpen(id) {

    //***************************
    // alert(id);
    //***************************
    var x = id.slice(8);
    var noteID = "iconNote" + x;

    var memoModal = document.getElementById(noteID);

    var opacityValue = memoModal.style.opacity;

    if (opacityValue == "1") {
        // alert(id);
        $("#modal_content_memo").css('display', 'block');
    }

}

function tojiruThumbnail(id) {
    // alert(id);
    $("#modal_content_thumbnail").css('display', 'none');
}


function imagesView(id) {

    // alert(id);
    var x = id.slice(13);

    if (id != "") {
        $("#div_thumbnail_" + 1).hide();
        $("#div_thumbnail_" + 1).addClass("hide");
        // ------------------------------------------------
        $("#div_thumbnailImagesView_" + 1).show();
        $("#div_thumbnailImagesView_" + 1).removeClass("hide");
        $('main.images').addClass("overflow_x");
        ////////ボタン////////
        $(".ichiranNiModoru").show();
        ////////ボタン////////

        $("#div_thumbnailImagesView_" + 1).children().hide();
        $("#imagesReviewDefault" + x).show();

    }
}

function modoruThumbnail(id) {
    // alert(id);

    $("#div_thumbnail_" + 1).show();
    $("#div_thumbnail_" + 1).removeClass("hide");

    $("#div_thumbnailImagesView_" + 1).hide();
    $("#div_thumbnailImagesView_" + 1).addClass("hide");
    $(".ichiranNiModoru").hide();
    $('main.images').removeClass("overflow_x");
}



function tojiruMemoMain(id) {

    // alert(id);
    $("#modal_content_memo").css('display', 'none');
}


function memoView(id) {
    // alert(id);
    if (id != "") {

        $("#div_memo_" + 1).hide();
        $("#div_memo_" + 1).addClass("hide");
        // ------------------------------------------------

        $("#div_memoView_" + 1).show();
        $("#div_memoView_" + 1).removeClass("hide");
        $('main.memo').addClass("overflow_x");

        ////////ボタン////////
        $(".ichiranNiModoruMemoMain").show();
        ////////ボタン////////
    }
}

function memoView1() {
    // alert(id);
    // if (id != "") {

    $("#div_memo_" + 1).hide();
    $("#div_memo_" + 1).addClass("hide");
    // ------------------------------------------------

    $("#div_memoView_" + 1).show();
    $("#memosView1").css("display", "block");
    $("#memosView2").css("display", "none");
    $("#div_memoView_" + 1).removeClass("hide");
    $('main.memo').addClass("overflow_x");

    ////////ボタン////////
    $(".ichiranNiModoruMemoMain").show();
    ////////ボタン////////
    // }
}
function memoView2() {
    // alert(id);
    // if (id != "") {

    $("#div_memo_" + 1).hide();
    $("#div_memo_" + 1).addClass("hide");
    // ------------------------------------------------

    $("#div_memoView_" + 1).show();
    $("#memosView1").css("display", "none");
    $("#memosView2").css("display", "block");
    $("#div_memoView_" + 1).removeClass("hide");
    $('main.memo').addClass("overflow_x");

    ////////ボタン////////
    $(".ichiranNiModoruMemoMain").show();
    ////////ボタン////////
    // }
}

function modoruMemoMain(id) {
    // body...
    $("#div_memo_" + 1).show();
    $("#div_memo_" + 1).removeClass("hide");

    $("#div_memoView_" + 1).hide();
    $("#div_memoView_" + 1).addClass("hide");
    $(".ichiranNiModoruMemoMain").hide();
    $('main.memo').removeClass("overflow_x");
}
/*///////////////////////////////////ShouSaiKenSaku(this.id)////////////////////////////////////////*/