function drawGraph(date, data, data2, draw, select1) {
    $("#lineGraph").empty();
    var rect = d3.select('#lineGraph').node().getBoundingClientRect(),
        width = rect.width / 3 - 50,
        height = rect.height;

    // set the dimensions and margins of the graph
    var margin = { top: 10, right: 130, bottom: 100, left: 100 },
        width = width * 3 - margin.left,
        height = height - margin.top - margin.bottom;


    //Panel title  
    d3.select("#lineGraph")
        .append("h1")
        .attr('class', 'text-center mt-3')
        .text('生産実績（' + select1 + '号機）')

    // append the svg object to the body of the page
    var svg = d3.select("#lineGraph")
        .append("svg")
        .attr("id", "graph-chart")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom - 60)
        .append("g")
        .attr("transform",
            "translate(" + (margin.left / 2) + "," + margin.top + ")");

    var scaleX = d3.scalePoint()
        .domain(date)
        .range([0, width + margin.left]);

    if (select1 == "0") {
        data = [
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
        ];
    }

    var scaleY = d3.scaleLinear()
        .domain([Math.ceil(d3.max(data)), Math.round(d3.min(data))]).nice()
        .range([0, height - margin.bottom]);

    var scaleXLine = d3.scalePoint()
        .domain(data)
        .range([scaleX.step() / 2, scaleX.step() * 24 + 3 * margin.top])

    var scaleXLine2 = d3.scalePoint()
        .domain(data2)
        .range([scaleX.step() / 2, scaleX.step() * 24 + 3 * margin.top])

    // Add scales to axis

    var x_axis = d3.axisBottom()
        .scale(scaleX);

    var y_axis = d3.axisLeft()
        .scale(scaleY).tickSize(-width - margin.left - (margin.left / 2) - 10);

    //color
    var colors = d3.scaleOrdinal(["#0569ff", "#ff0505"])

    //Append group and insert axis
    svg.append("g")
        .attr('class', 'x-axe')
        .call(x_axis)
        .style("transform", 'translate(0px,' + (height - margin.bottom) + 'px)')
        .style("font-size", "8px")
        .call(g => g.select('.domain').remove())
        .selectAll(".tick text")
        .style("text-anchor", "middle")
        .attr("x", scaleX.step() / 2)
        .call(wrap, scaleX.step());;

    svg.append("g")
        .attr('class', 'y-axe')
        .call(y_axis)
        .call(g => g.select('.domain').remove());

    //draw a line 
    var lineFunction = d3.line()
        .x(function (d) { return scaleXLine(d); })
        .y(function (d) { return scaleY(d); })
        .curve(d3.curveLinear);

    var lineFunction1 = d3.line()
        .x(function (d) { return scaleXLine2(d); })
        .y(function (d) { return scaleY(d); })
        .curve(d3.curveLinear);


    if ((draw == true || $("#select1").val() != "-") && select1 != "全て" && select1 != "0") {
        //defining the lines
        var path = svg.append('g').attr("class", "line").append("path");
        var path1 = svg.append('g').attr("class", "line1").append("path");

        //plotting lines
        path
            .attr("d", lineFunction(data))
            .attr("stroke", (d, i) => colors(i))
            .attr("stroke-width", 2)
            .attr("fill", "none");

        path1
            .attr("d", lineFunction1(data2))
            .attr("stroke", (d, i) => colors(i + 1))
            .attr("stroke-width", 2)
            .attr("fill", "none");
    }

    let dataNest = ["計画数", "実績"];

    var legendSpace = (width + (margin.left / 2) + margin.top) / dataNest.length;

    let userGuide = svg.append('g').attr('class', 'group-user')

    userGuide.selectAll('.text-user')
        .data(dataNest).enter()
        .append('text')
        .attr('class', 'text-user')
        .text((d, i) => { return d })
        .attr("x", (d, i) => {
            return (legendSpace / dataNest.length) + i * legendSpace + 3 * margin.top + (margin.left / 2) + 10;
        })
        .attr("y", height - margin.top)

    userGuide.selectAll('.color-user')
        .data(dataNest).enter()
        .append('rect')
        .attr('class', 'color-user')
        .attr('stroke-width', '2')
        .attr("x", (d, i) => {
            return (legendSpace / dataNest.length) + i * legendSpace + 3 * margin.top;
        })
        .attr("y", height - 2 * margin.top + 2)
        .attr("width", '50')
        .attr("height", "5px")
        .attr('fill', (d, i) => {
            return colors(i)
        });

    $("#togglePopup").click(function () {
        clearInterval(timer);
        $("#myModal_choukiteishi").attr("class", "modal");
        $("#modal_content_memo").show();
        
        $("#dateTime1").datepicker({
            buttonImage: "",        // カレンダーアイコン画像
            buttonText: "", // ツールチップ表示文言
            buttonImageOnly: true,           // 画像として表示
            showOn: "both"                   // カレンダー呼び出し元の定義
        });

        // 日本語化
        $.datepicker.regional['ja'] = {
            closeText: '閉じる',
            prevText: '<前',
            nextText: '次>',
            currentText: '今日',
            monthNames: ['1月', '2月', '3月', '4月', '5月', '6月',
                '7月', '8月', '9月', '10月', '11月', '12月'],
            monthNamesShort: ['1月', '2月', '3月', '4月', '5月', '6月',
                '7月', '8月', '9月', '10月', '11月', '12月'],
            dayNames: ['日曜日', '月曜日', '火曜日', '水曜日', '木曜日', '金曜日', '土曜日'],
            dayNamesShort: ['日', '月', '火', '水', '木', '金', '土'],
            dayNamesMin: ['日', '月', '火', '水', '木', '金', '土'],
            weekHeader: '週',
            dateFormat: 'yy/mm/dd',
            firstDay: 0,
            isRTL: false,
            showMonthAfterYear: true,
            yearSuffix: '年'
        };
        $.datepicker.setDefaults($.datepicker.regional['ja']);

        $(".icon-date-1").click(function (e) {
            e.preventDefault();
            $("#dateTime1").focus();
        });

        $("#dateTime2").datepicker({
            buttonImage: "",        // カレンダーアイコン画像
            buttonText: "", // ツールチップ表示文言
            buttonImageOnly: true,           // 画像として表示
            showOn: "both"                   // カレンダー呼び出し元の定義
        });

        // 日本語化
        $.datepicker.regional['ja'] = {
            closeText: '閉じる',
            prevText: '<前',
            nextText: '次>',
            currentText: '今日',
            monthNames: ['1月', '2月', '3月', '4月', '5月', '6月',
                '7月', '8月', '9月', '10月', '11月', '12月'],
            monthNamesShort: ['1月', '2月', '3月', '4月', '5月', '6月',
                '7月', '8月', '9月', '10月', '11月', '12月'],
            dayNames: ['日曜日', '月曜日', '火曜日', '水曜日', '木曜日', '金曜日', '土曜日'],
            dayNamesShort: ['日', '月', '火', '水', '木', '金', '土'],
            dayNamesMin: ['日', '月', '火', '水', '木', '金', '土'],
            weekHeader: '週',
            dateFormat: 'yy/mm/dd',
            firstDay: 0,
            isRTL: false,
            showMonthAfterYear: true,
            yearSuffix: '年'
        };
        $.datepicker.setDefaults($.datepicker.regional['ja']);

        $(".icon-date-2").click(function (e) {
            e.preventDefault();
            $("#dateTime2").focus();
        });

        var myDate = new Date();

        $('#dateTime1').datepicker('setDate', myDate);
        $('#dateTime2').datepicker('setDate', myDate);

    });

    $("#togglePopup1").click(function () {
        $("#myModal_choukiteishi").attr("class", "");
        $("#modal_content_memo").hide();
    });

    function wrap(text, width) {
        text.each(function () {
            var text = d3.select(this),
                words = text.text().split(/\s+/).reverse(),
                word,
                line = [],
                lineNumber = 0,
                lineHeight = 1.1, // ems
                y = text.attr("y"),
                dy = parseFloat(text.attr("dy")),
                tspan = text.text(null).append("tspan").attr("x", width / 2).attr("y", y).attr("dy", dy + "em");
            while (word = words.pop()) {
                line.push(word);
                tspan.text(line.join(" "));
                if (tspan.node().getComputedTextLength() > width) {
                    line.pop();
                    tspan.text(line.join(" "));
                    line = [word];
                    tspan = text.append("tspan").attr("x", width / 2).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
                }
            }
        });
    }
}

$(document).ready(function () {
    // create datearray, data array
    var data = [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10
    ];
    var date = ["00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00",
        "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00",
        "22:00", "23:00", "24:00"];
    drawGraph(date, data, data, false, "0");

});

var timer;

function settingButton() {
    $("#lineGraph").empty();
    var dateStart = $('#dateTime1').val();
    var dateEnd = $('#dateTime2').val();
    var graphDisplayTime = $('#select2').val();

    const options = { year: 'numeric', month: 'long', day: 'numeric' };

    var converTimeStart = new Date(dateStart);
    var converTimeEnd = new Date(dateEnd);
    var startDate = converTimeStart.toLocaleDateString('ja-JP', options);
    var endDate = converTimeEnd.toLocaleDateString('ja-JP', options);

    if (dateStart !== dateEnd) {
        alert("一日だけ入力が出来");
        return;
    }
    if (startDate == "Invalid Date" || endDate == "Invalid Date") {
        alert("日付を選んでください！！！")
        return;
    }

    var select1 = $("#select1").val();
    if (select1 == "0") {
        select1 = "全て";
    } else if (select1 == "-") {
        select1 = "0"
    }

    var data1 = ["00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00",
        "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00",
        "22:00", "23:00", "24:00"];

    if (graphDisplayTime == "2") {
        let data2 = [];
        data1.map((el) => {
            el = el.replace(":00", ':30');
            data2.push(el);
        });

        data1 = data2;

    } else if (graphDisplayTime == "3") {
        let data3 = [];
        data1.map((el) => {
            el = el.replace(":00", ':10');
            data3.push(el);

        });

        data1 = data3;
    }

    console.log(data1);

    var date = [];

    data1.forEach((el) => {
        date.push(startDate.concat(" ".concat(el)));
    });

    // console.log(dataForGraph);
    var dataRadom = [];
    var dataRadom2 = [];
    for (let index = 0; index < data1.length; index++) {
        var radomDate = Math.random() * index;
        var radomDate2 = Math.random() * (index / 2);
        dataRadom.push(radomDate);
        dataRadom2.push(radomDate2);
    }

    drawGraph(date, dataRadom, dataRadom2, true, select1);
    var index1 = 1;
    $("#graph-chart").ready(function () {
        var Ticks = 3000;

        timer = setInterval(() => {
            console.log(123);
            var dataRadom = [];
            var dataRadom2 = [];
            for (let index = 0; index < data1.length; index++) {
                var radomDate = Math.random() * index;
                var radomDate2 = Math.random() * (index / 2);
                dataRadom.push(radomDate);
                dataRadom2.push(radomDate2);
            }
            console.log(select1);
            if (select1 == "全て") {
                // select1 = "0";
                index1 = index1.toString();
                drawGraph(date, dataRadom, dataRadom2, true, index1);
                index1 = parseInt(index1);
                if (index1 > 9) {
                    index1 = 0;
                }
                index1++;
            } else {
                drawGraph(date, dataRadom, dataRadom2, true, select1);
            }

        }, Ticks);

        if ($("#select1").val() == "-") {
            clearInterval(timer);
            dataRadom = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            dataRadom2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            index1 = "0";
            drawGraph(date, dataRadom, dataRadom2, true, index1);
        }
        $("#togglePopup2").on('click', function (e) {
            clearInterval(timer);
        });


    });
    $("#myModal_choukiteishi").attr("class", "");
    $("#modal_content_memo").hide();
}

function refeshButton() {
    alert("リロードしました。！！！")
    var data = [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10
    ];
    var date = ["00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00",
        "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00",
        "22:00", "23:00", "24:00"];
    drawGraph(date, data, data, false, "0");
    clearInterval(timer);
}