//*************************************************
//*                                               *
//*                メモ登録modal                    *
//*                                               *
//*************************************************

	//memoModal
	var memoButton = 0;
	var currentID;
	var currentID2;
	var currentID3;
	var currentID4;
	var currentID5;
	var currentID6;
	var currentID7;
	var currentID8;
	var currentID9;
	var currentID10;

	let status1 = [], pathsry1 = [], pathsryRedo1 = [];
	let status2 = [], pathsry2 = [], pathsryRedo2 = [];
	let status3 = [], pathsry3 = [], pathsryRedo3 = [];
	let status4 = [], pathsry4 = [], pathsryRedo4 = [];
	let status5 = [], pathsry5 = [], pathsryRedo5 = [];
	let status6 = [], pathsry6 = [], pathsryRedo6 = [];
	let status7 = [], pathsry7 = [], pathsryRedo7 = [];
	let status8 = [], pathsry8 = [], pathsryRedo8 = [];
	let status9 = [], pathsry9 = [], pathsryRedo9 = [];
	let status10 = [], pathsry10 = [], pathsryRedo10 = [];

	var f1, f2, f3, f4, f5, f6, f7, f8, f9, f10;

	function memoModalOpen(id) {
		
		memoButton = id.slice(4);
		// alert(memoButton);
		$("#modal_content_memo").css('display', 'block');
		$("#div_tools").hide();
		$("#div_memo_4").show();		
	
		changeDivMemoCanvasNo(memoButton);
		changeDivMemoNo(memoButton);

		//-----------------------------------------------------
		if (memoButton == 1) {
			$('div.div_memo_1').children().css("display", "none");
			$('#divMemoTable' + memoButton).css("display", "block");
			///////////////////////////////////////////////////////

			$('div.div_memo_2').children().css("display", "none");
			$('#table_'+ memoButton + '_div_memo_2').css("display", "block");
			///////////////////////////////////////////////////////

			$('div.div_memo_3').children().css("display", "none");
			$('#table_'+ memoButton + '_div_memo_3').css("display", "block");

			reDrawCanvas1(memoButton);

		} else if (memoButton == 2) {
			$('div.div_memo_1').children().css("display", "none");
			$('#divMemoTable' + memoButton).css("display", "block");
			///////////////////////////////////////////////////////

			$('div.div_memo_2').children().css("display", "none");
			$('#table_'+ memoButton + '_div_memo_2').css("display", "block");
			///////////////////////////////////////////////////////

			$('div.div_memo_3').children().css("display", "none");
			$('#table_'+ memoButton + '_div_memo_3').css("display", "block");

			memoCanvasDefault2();
			reDrawCanvas2(memoButton);

		} else if (memoButton == 3) {
			$('div.div_memo_1').children().css("display", "none");
			$('#divMemoTable' + memoButton).css("display", "block");
			///////////////////////////////////////////////////////

			$('div.div_memo_2').children().css("display", "none");
			$('#table_'+ memoButton + '_div_memo_2').css("display", "block");
			///////////////////////////////////////////////////////

			$('div.div_memo_3').children().css("display", "none");
			$('#table_'+ memoButton + '_div_memo_3').css("display", "block");

			memoCanvasDefault3();
			// reDrawCanvas3(memoButton);

		} else if (memoButton == 4) {
			$('div.div_memo_1').children().css("display", "none");
			$('#divMemoTable' + memoButton).css("display", "block");
			///////////////////////////////////////////////////////

			$('div.div_memo_2').children().css("display", "none");
			$('#table_'+ memoButton + '_div_memo_2').css("display", "block");
			///////////////////////////////////////////////////////

			$('div.div_memo_3').children().css("display", "none");
			$('#table_'+ memoButton + '_div_memo_3').css("display", "block");		

			memoCanvasDefault4();
			reDrawCanvas4(memoButton);

		} else if (memoButton == 5) {

			reDrawCanvas5(memoButton);

		} else if (memoButton == 6) {

			reDrawCanvas6(memoButton);

		} else if (memoButton == 7) {

			reDrawCanvas7(memoButton);

		} else if (memoButton == 8) {

			reDrawCanvas8(memoButton);

		} else if (memoButton == 9) {

			reDrawCanvas9(memoButton);

		} else {

			reDrawCanvas10(memoButton);
		}
		//-----------------------------------------------------
	}

	//****************************20210513*****************************
	function reDrawCanvas1(id) {

		// alert(id);

		if (currentID == undefined) {
			currentID = "paint_canvas1";
		}

		const canvas = document.getElementById(currentID);
		const context = canvas.getContext('2d');
		// context.clearRect(0, 0, canvas.width, canvas.height);

		// var memoPathsry = [];
		// var memoPathsryRedo = [];
		// var memoStatus = [];

		// var text = $("#memo" + memoButton).text();

		// if (memoButton == "1" && text == "メモ編集") {

		// 	memoPathsry = pathsry1;
		// 	memoPathsryRedo = pathsryRedo1;
		// 	memoStatus = status1;

		// 	if (f1 != undefined) {

		// 		var img = new Image();
		// 		f = f1;
		// 		var url = window.URL || window.webkitURL;
		// 		var src = url.createObjectURL(f);

		// 		img.src = src;
		// 		img.onload = function() {
		// 			context.drawImage(img, 0, 0);
		// 			url.revokeObjectURL(src);
		// 		}
		// 	}

		// }

		// var h = 0;
		// memoPathsry.forEach(path=>{
		// 	context.strokeStyle = memoStatus[h][0];
		// 	context.lineWidth = memoStatus[h][1];
		// 	context.beginPath();

		// 	context.moveTo(path[0].x, path[0].y);

		// 	for(let i = 1; i < path.length; i++){

		// 		context.lineTo(path[i].x,path[i].y);
		// 	};

		// 	context.stroke();
		// 	h++;
		// });
	}
	//****************************20210513*****************************
	//-----------------------------------------------------------------
	//****************************20210513*****************************
	function reDrawCanvas2(id) {

		// alert(id);
		if (currentID2 == undefined) {
			currentID2 = "paint_canvas_2_Alt1";
		}

		const canvas2 = document.getElementById(currentID2);
		const context2 = canvas2.getContext('2d');
		// context2.clearRect(0, 0, canvas2.width, canvas2.height);
	}
	//****************************20210513*****************************
	//-----------------------------------------------------------------
	//****************************20210513*****************************
	function reDrawCanvas3(id) {

		// alert(id);

		if (currentID3 == undefined) {
			currentID3 = "paint_canvas_3_Alt1";
		}

		const canvas = document.getElementById(currentID3);
		const context = canvas.getContext('2d');

	}
	//****************************20210513*****************************
	//-----------------------------------------------------------------
	//****************************20210513*****************************
	function reDrawCanvas4(id) {

		// alert(id);

		if (currentID4 == undefined) {
			currentID4 = "paint_canvas_4_Alt1";
		}

		const canvas = document.getElementById(currentID4);
		const context = canvas.getContext('2d');

	}
	//****************************20210513*****************************
	//-----------------------------------------------------------------	
	//****************************20210513*****************************
	function reDrawCanvas5(id) {

		// alert(id);
		if (currentID5 == undefined) {
			currentID5 = "paint_canvas_5_Alt1";
		}

		const canvas = document.getElementById(currentID5);
		const context = canvas.getContext('2d');
		// context.clearRect(0, 0, canvas.width, canvas.height);

	}
	//****************************20210513*****************************
	//-----------------------------------------------------------------
	//****************************20210513*****************************
	function reDrawCanvas6(id) {

		// alert(id);

		if (currentID6 == undefined) {
			currentID6 = "paint_canvas_6_Alt1";
		}

		const canvas = document.getElementById(currentID6);
		const context = canvas.getContext('2d');

	}
	//****************************20210513*****************************
	//-----------------------------------------------------------------
	//****************************20210513*****************************
	function reDrawCanvas7(id) {

		// alert(id);

		if (currentID7 == undefined) {
			currentID7 = "paint_canvas_7_Alt1";
		}

		const canvas = document.getElementById(currentID7);
		const context = canvas.getContext('2d');

	}
	//****************************20210513*****************************
	//-----------------------------------------------------------------
	//****************************20210513*****************************
	function reDrawCanvas8(id) {

		// alert(id);

		if (currentID8 == undefined) {
			currentID8 = "paint_canvas_8_Alt1";
		}

		const canvas = document.getElementById(currentID8);
		const context = canvas.getContext('2d');

	}
	//****************************20210513*****************************
	//-----------------------------------------------------------------
	//****************************20210513*****************************
	function reDrawCanvas9(id) {

		// alert(id);

		if (currentID9 == undefined) {
			currentID9 = "paint_canvas_9_Alt1";
		}

		const canvas = document.getElementById(currentID9);
		const context = canvas.getContext('2d');

		// context.clearRect(0, 0, canvas.width, canvas.height);


		// var memoPathsry = [];
		// var memoPathsryRedo = [];
		// var memoStatus = [];

		// var text = $("#memo" + memoButton).text();

		// if (memoButton == "1" && text == "メモ編集") {

		// 	memoPathsry = pathsry1;
		// 	memoPathsryRedo = pathsryRedo1;
		// 	memoStatus = status1;

		// 	if (f1 != undefined) {

		// 		var img = new Image();
		// 		f = f1;
		// 		var url = window.URL || window.webkitURL;
		// 		var src = url.createObjectURL(f);

		// 		img.src = src;
		// 		img.onload = function() {
		// 			context.drawImage(img, 0, 0);
		// 			url.revokeObjectURL(src);
		// 		}
		// 	}

		// } else if (memoButton == "2" && text == "メモ編集") {

		// 	memoPathsry = pathsry2;
		// 	memoPathsryRedo = pathsryRedo2;
		// 	memoStatus = status2;

		// 	if (f2 != undefined) {

		// 		var img = new Image();
		// 		f = f2;
		// 		var url = window.URL || window.webkitURL;
		// 		var src = url.createObjectURL(f);

		// 		img.src = src;
		// 		img.onload = function() {
		// 			context.drawImage(img, 0, 0);
		// 			url.revokeObjectURL(src);
		// 		}
		// 	}

		// } else if (memoButton == "3" && text == "メモ編集") {

		// 	memoPathsry = pathsry3;
		// 	memoPathsryRedo = pathsryRedo3;
		// 	memoStatus = status3;

		// 	if (f3 != undefined) {

		// 		var img = new Image();
		// 		f = f3;
		// 		var url = window.URL || window.webkitURL;
		// 		var src = url.createObjectURL(f);

		// 		img.src = src;
		// 		img.onload = function() {
		// 			context.drawImage(img, 0, 0);
		// 			url.revokeObjectURL(src);
		// 		}
		// 	}

		// } else if (memoButton == "4" && text == "メモ編集") {

		// 	memoPathsry = pathsry4;
		// 	memoPathsryRedo = pathsryRedo4;
		// 	memoStatus = status4;


		// 	if (f4 != undefined) {

		// 		var img = new Image();
		// 		f = f4;
		// 		var url = window.URL || window.webkitURL;
		// 		var src = url.createObjectURL(f);

		// 		img.src = src;
		// 		img.onload = function() {
		// 			context.drawImage(img, 0, 0);
		// 			url.revokeObjectURL(src);
		// 		}
		// 	}

		// } else if (memoButton == "5" && text == "メモ編集") {

		// 	memoPathsry = pathsry5;
		// 	memoPathsryRedo = pathsryRedo5;
		// 	memoStatus = status5;

		// 	if (f5 != undefined) {

		// 		var img = new Image();
		// 		f = f5;
		// 		var url = window.URL || window.webkitURL;
		// 		var src = url.createObjectURL(f);

		// 		img.src = src;
		// 		img.onload = function() {
		// 			context.drawImage(img, 0, 0);
		// 			url.revokeObjectURL(src);
		// 		}
		// 	}

		// } else if (memoButton == "6" && text == "メモ編集") {

		// 	memoPathsry = pathsry6;
		// 	memoPathsryRedo = pathsryRedo6;
		// 	memoStatus = status6;

		// 	if (f6 != undefined) {

		// 		var img = new Image();
		// 		f = f6;
		// 		var url = window.URL || window.webkitURL;
		// 		var src = url.createObjectURL(f);

		// 		img.src = src;
		// 		img.onload = function() {
		// 			context.drawImage(img, 0, 0);
		// 			url.revokeObjectURL(src);
		// 		}
		// 	}

		// } else if (memoButton == "7" && text == "メモ編集") {

		// 	memoPathsry = pathsry7;
		// 	memoPathsryRedo = pathsryRedo7;
		// 	memoStatus = status7;

		// 	if (f7 != undefined) {

		// 		var img = new Image();
		// 		f = f7;
		// 		var url = window.URL || window.webkitURL;
		// 		var src = url.createObjectURL(f);

		// 		img.src = src;
		// 		img.onload = function() {
		// 			context.drawImage(img, 0, 0);
		// 			url.revokeObjectURL(src);
		// 		}
		// 	}

		// } else if (memoButton == "8" && text == "メモ編集") {

		// 	memoPathsry = pathsry8;
		// 	memoPathsryRedo = pathsryRedo8;
		// 	memoStatus = status8;

		// 	if (f8 != undefined) {

		// 		var img = new Image();
		// 		f = f8;
		// 		var url = window.URL || window.webkitURL;
		// 		var src = url.createObjectURL(f);

		// 		img.src = src;
		// 		img.onload = function() {
		// 			context.drawImage(img, 0, 0);
		// 			url.revokeObjectURL(src);
		// 		}
		// 	}

		// } else if (memoButton == "9" && text == "メモ編集") {

		// 	memoPathsry = pathsry9;
		// 	memoPathsryRedo = pathsryRedo9;
		// 	memoStatus = status9;

		// 	if (f9 != undefined) {

		// 		var img = new Image();
		// 		f = f9;
		// 		var url = window.URL || window.webkitURL;
		// 		var src = url.createObjectURL(f);

		// 		img.src = src;
		// 		img.onload = function() {
		// 			context.drawImage(img, 0, 0);
		// 			url.revokeObjectURL(src);
		// 		}
		// 	}

		// } else if (memoButton == "10" && text == "メモ編集"){

		// 	memoPathsry = pathsry10;
		// 	memoPathsryRedo = pathsryRedo10;
		// 	memoStatus = status10;

		// 	if (f10 != undefined) {

		// 		var img = new Image();
		// 		f = f10;
		// 		var url = window.URL || window.webkitURL;
		// 		var src = url.createObjectURL(f);

		// 		img.src = src;
		// 		img.onload = function() {
		// 			context.drawImage(img, 0, 0);
		// 			url.revokeObjectURL(src);
		// 		}
		// 	}
		// }

		// var h = 0;
		// memoPathsry.forEach(path=>{
		// 	context.strokeStyle = memoStatus[h][0];
		// 	context.lineWidth = memoStatus[h][1];
		// 	context.beginPath();

		// 	context.moveTo(path[0].x, path[0].y);

		// 	for(let i = 1; i < path.length; i++){

		// 		context.lineTo(path[i].x,path[i].y);
		// 	};

		// 	context.stroke();
		// 	h++;
		// });
	}
	//****************************20210513*****************************
	//-----------------------------------------------------------------
	//****************************20210513*****************************
	function reDrawCanvas10(id) {

		// alert(id);

		if (currentID10 == undefined) {
			currentID10 = "paint_canvas_10_Alt1";
		}

		const canvas = document.getElementById(currentID10);
		const context = canvas.getContext('2d');

		// context.clearRect(0, 0, canvas.width, canvas.height);

		// var memoPathsry = [];
		// var memoPathsryRedo = [];
		// var memoStatus = [];

		// var text = $("#memo" + memoButton).text();

		// if (memoButton == "1" && text == "メモ編集") {

		// 	memoPathsry = pathsry1;
		// 	memoPathsryRedo = pathsryRedo1;
		// 	memoStatus = status1;

		// 	if (f1 != undefined) {

		// 		var img = new Image();
		// 		f = f1;
		// 		var url = window.URL || window.webkitURL;
		// 		var src = url.createObjectURL(f);

		// 		img.src = src;
		// 		img.onload = function() {
		// 			context.drawImage(img, 0, 0);
		// 			url.revokeObjectURL(src);
		// 		}
		// 	}

		// } else if (memoButton == "2" && text == "メモ編集") {

		// 	memoPathsry = pathsry2;
		// 	memoPathsryRedo = pathsryRedo2;
		// 	memoStatus = status2;

		// 	if (f2 != undefined) {

		// 		var img = new Image();
		// 		f = f2;
		// 		var url = window.URL || window.webkitURL;
		// 		var src = url.createObjectURL(f);

		// 		img.src = src;
		// 		img.onload = function() {
		// 			context.drawImage(img, 0, 0);
		// 			url.revokeObjectURL(src);
		// 		}
		// 	}

		// } else if (memoButton == "3" && text == "メモ編集") {

		// 	memoPathsry = pathsry3;
		// 	memoPathsryRedo = pathsryRedo3;
		// 	memoStatus = status3;

		// 	if (f3 != undefined) {

		// 		var img = new Image();
		// 		f = f3;
		// 		var url = window.URL || window.webkitURL;
		// 		var src = url.createObjectURL(f);

		// 		img.src = src;
		// 		img.onload = function() {
		// 			context.drawImage(img, 0, 0);
		// 			url.revokeObjectURL(src);
		// 		}
		// 	}

		// } else if (memoButton == "4" && text == "メモ編集") {

		// 	memoPathsry = pathsry4;
		// 	memoPathsryRedo = pathsryRedo4;
		// 	memoStatus = status4;


		// 	if (f4 != undefined) {

		// 		var img = new Image();
		// 		f = f4;
		// 		var url = window.URL || window.webkitURL;
		// 		var src = url.createObjectURL(f);

		// 		img.src = src;
		// 		img.onload = function() {
		// 			context.drawImage(img, 0, 0);
		// 			url.revokeObjectURL(src);
		// 		}
		// 	}

		// } else if (memoButton == "5" && text == "メモ編集") {

		// 	memoPathsry = pathsry5;
		// 	memoPathsryRedo = pathsryRedo5;
		// 	memoStatus = status5;

		// 	if (f5 != undefined) {

		// 		var img = new Image();
		// 		f = f5;
		// 		var url = window.URL || window.webkitURL;
		// 		var src = url.createObjectURL(f);

		// 		img.src = src;
		// 		img.onload = function() {
		// 			context.drawImage(img, 0, 0);
		// 			url.revokeObjectURL(src);
		// 		}
		// 	}

		// } else if (memoButton == "6" && text == "メモ編集") {

		// 	memoPathsry = pathsry6;
		// 	memoPathsryRedo = pathsryRedo6;
		// 	memoStatus = status6;

		// 	if (f6 != undefined) {

		// 		var img = new Image();
		// 		f = f6;
		// 		var url = window.URL || window.webkitURL;
		// 		var src = url.createObjectURL(f);

		// 		img.src = src;
		// 		img.onload = function() {
		// 			context.drawImage(img, 0, 0);
		// 			url.revokeObjectURL(src);
		// 		}
		// 	}

		// } else if (memoButton == "7" && text == "メモ編集") {

		// 	memoPathsry = pathsry7;
		// 	memoPathsryRedo = pathsryRedo7;
		// 	memoStatus = status7;

		// 	if (f7 != undefined) {

		// 		var img = new Image();
		// 		f = f7;
		// 		var url = window.URL || window.webkitURL;
		// 		var src = url.createObjectURL(f);

		// 		img.src = src;
		// 		img.onload = function() {
		// 			context.drawImage(img, 0, 0);
		// 			url.revokeObjectURL(src);
		// 		}
		// 	}

		// } else if (memoButton == "8" && text == "メモ編集") {

		// 	memoPathsry = pathsry8;
		// 	memoPathsryRedo = pathsryRedo8;
		// 	memoStatus = status8;

		// 	if (f8 != undefined) {

		// 		var img = new Image();
		// 		f = f8;
		// 		var url = window.URL || window.webkitURL;
		// 		var src = url.createObjectURL(f);

		// 		img.src = src;
		// 		img.onload = function() {
		// 			context.drawImage(img, 0, 0);
		// 			url.revokeObjectURL(src);
		// 		}
		// 	}

		// } else if (memoButton == "9" && text == "メモ編集") {

		// 	memoPathsry = pathsry9;
		// 	memoPathsryRedo = pathsryRedo9;
		// 	memoStatus = status9;

		// 	if (f9 != undefined) {

		// 		var img = new Image();
		// 		f = f9;
		// 		var url = window.URL || window.webkitURL;
		// 		var src = url.createObjectURL(f);

		// 		img.src = src;
		// 		img.onload = function() {
		// 			context.drawImage(img, 0, 0);
		// 			url.revokeObjectURL(src);
		// 		}
		// 	}

		// } else if (memoButton == "10" && text == "メモ編集"){

		// 	memoPathsry = pathsry10;
		// 	memoPathsryRedo = pathsryRedo10;
		// 	memoStatus = status10;

		// 	if (f10 != undefined) {

		// 		var img = new Image();
		// 		f = f10;
		// 		var url = window.URL || window.webkitURL;
		// 		var src = url.createObjectURL(f);

		// 		img.src = src;
		// 		img.onload = function() {
		// 			context.drawImage(img, 0, 0);
		// 			url.revokeObjectURL(src);
		// 		}
		// 	}
		// }

		// var h = 0;
		// memoPathsry.forEach(path=>{
		// 	context.strokeStyle = memoStatus[h][0];
		// 	context.lineWidth = memoStatus[h][1];
		// 	context.beginPath();

		// 	context.moveTo(path[0].x, path[0].y);

		// 	for(let i = 1; i < path.length; i++){

		// 		context.lineTo(path[i].x,path[i].y);
		// 	};

		// 	context.stroke();
		// 	h++;
		// });
	}
	//****************************20210513*****************************
	//-----------------------------------------------------------------	

	//****************************20210513*****************************
	function changeDivMemoCanvasNo(id) {

		// alert(id);

		for (var i = 1; i < 11; i++) {
			$('#canvasAlt' + i).css("display", "none");
		}
		//-----------------------------------------------------
		if (id == 1) {

			$('#canvasAlt1').css("display", "block");
			$('div.canvas').children().css( "display", "none");
			$('#div1memo1').siblings().css("background-color", "");
			$('#div1memo1').css("background-color", "#ebdcb5");			
			$('#paint_canvas1').css("display", "block");

		} else if (id == 2) {

			$('#canvasAlt2').css("display", "block");
			$('div.canvasAlt2').children().css( "display", "none");
			$('#div2memo1').siblings().css("background-color", "");
			$('#div2memo1').css("background-color", "#ebdcb5");					
			$('#paint_canvas_2_Alt1').css("display", "block");

		} else if (id == 3) {

			$('#canvasAlt3').css("display", "block");
			$('div.canvasAlt3').children().css( "display", "none");
			$('#div3memo1').siblings().css("background-color", "");
			$('#div3memo1').css("background-color", "#ebdcb5");				
			$('#paint_canvas_3_Alt1').css("display", "block");

		} else if (id == 4) {

			$('#canvasAlt4').css("display", "block");
			$('div.canvasAlt4').children().css( "display", "none");
			$('#div4memo1').siblings().css("background-color", "");
			$('#div4memo1').css("background-color", "#ebdcb5");				
			$('#paint_canvas_4_Alt1').css("display", "block");

		} else if (id == 5) {

			$('#canvasAlt5').css("display", "block");
			$('div.canvasAlt5').children().css( "display", "none");
			$('#div5memo1').siblings().css("background-color", "");
			$('#div5memo1').css("background-color", "#ebdcb5");				
			$('#paint_canvas_5_Alt1').css("display", "block");

		} else if (id == 6) {

			$('#canvasAlt6').css("display", "block");
			$('div.canvasAlt6').children().css( "display", "none");
			$('#div6memo1').siblings().css("background-color", "");
			$('#div6memo1').css("background-color", "#ebdcb5");				
			$('#paint_canvas_6_Alt1').css("display", "block");

		} else if (id == 7) {

			$('#canvasAlt7').css("display", "block");
			$('div.canvasAlt7').children().css( "display", "none");
			$('#div7memo1').siblings().css("background-color", "");
			$('#div7memo1').css("background-color", "#ebdcb5");				
			$('#paint_canvas_7_Alt1').css("display", "block");

		} else if (id == 8) {

			$('#canvasAlt8').css("display", "block");
			$('div.canvasAlt8').children().css( "display", "none");
			$('#div8memo1').siblings().css("background-color", "");
			$('#div8memo1').css("background-color", "#ebdcb5");				
			$('#paint_canvas_8_Alt1').css("display", "block");

		} else if (id == 9) {

			$('#canvasAlt9').css("display", "block");
			$('div.canvasAlt9').children().css( "display", "none");
			$('#div9memo1').siblings().css("background-color", "");
			$('#div9memo1').css("background-color", "#ebdcb5");				
			$('#paint_canvas_9_Alt1').css("display", "block");

		} else {

			$('#canvasAlt10').css("display", "block");
			$('div.canvasAlt10').children().css( "display", "none");
			$('#div10memo1').siblings().css("background-color", "");
			$('#div10memo1').css("background-color", "#ebdcb5");				
			$('#paint_canvas_10_Alt1').css("display", "block");
		}
		//-----------------------------------------------------

	}
	//****************************20210513*****************************

	//****************************20210513*****************************
	function changeDivMemoNo(id) {

		// alert(id);
		// Button

		for (var i = 1; i < 11; i++) {
			$('#divMemo' + i).css("display", "none");
		}
		//-----------------------------------------------------
		if (id == 1) {

			$('#divMemo1').css("display", "block");

		} else if (id == 2) {

			$('#divMemo2').css("display", "block");

		} else if (id == 3) {

			$('#divMemo3').css("display", "block");

		} else if (id == 4) {

			$('#divMemo4').css("display", "block");

		} else if (id == 5) {

			$('#divMemo5').css("display", "block");

		} else if (id == 6) {

			$('#divMemo6').css("display", "block");

		} else if (id == 7) {

			$('#divMemo7').css("display", "block");

		} else if (id == 8) {

			$('#divMemo8').css("display", "block");

		} else if (id == 9) {

			$('#divMemo9').css("display", "block");

		} else {

			$('#divMemo10').css("display", "block");
			
		}
		//-----------------------------------------------------

	}
	//****************************20210513*****************************	

	//memoModal
	function closeMemoModalButton() {
		$("#modal_content_memo").css('display', 'none');
	}	

	//memoModal
	function setteiMemoModalButton(id) {

		if (memoButton == "1") {
			$("#iconNotes" + memoButton).css('opacity', '1');
			$("#memo" + memoButton).text("メモ");

		} else if (memoButton == "2") {
			$("#iconNotes" + memoButton).css('opacity', '1');
			$("#memo" + memoButton).text("メモ");

		} else if (memoButton == "3") {
			$("#iconNotes" + memoButton).css('opacity', '1');
			$("#memo" + memoButton).text("メモ");

		} else if (memoButton == "4") {
			$("#iconNotes" + memoButton).css('opacity', '1');
			$("#memo" + memoButton).text("メモ");

		} else if (memoButton == "5") {
			$("#iconNotes" + memoButton).css('opacity', '1');
			$("#memo" + memoButton).text("メモ");

		} else if (memoButton == "6") {
			$("#iconNotes" + memoButton).css('opacity', '1');
			$("#memo" + memoButton).text("メモ");

		} else if (memoButton == "7") {
			$("#iconNotes" + memoButton).css('opacity', '1');
			$("#memo" + memoButton).text("メモ");

		} else if (memoButton == "8") {
			$("#iconNotes" + memoButton).css('opacity', '1');
			$("#memo" + memoButton).text("メモ");

		} else if (memoButton == "9") {
			$("#iconNotes" + memoButton).css('opacity', '1');
			$("#memo" + memoButton).text("メモ");

		} else if (memoButton == "10"){
			$("#iconNotes" + memoButton).css('opacity', '1');
			$("#memo" + memoButton).text("メモ");

		} else {
			$("#iconNotes" + memoButton).css('opacity', '33%');
			$("#memo" + memoButton).text("メモ");
		}

		torokukensumemo();
		$("#modal_content_memo").css('display', 'none');


	}

	// const canvas = document.getElementById('paint_canvas1');
	// const context = canvas.getContext('2d');
//******************************************************************
//------------------------------------------------------------------
//******************************************************************
	$(document).ready(function(){
		
		$("#div_tools").hide();
		$("#div_memo_4").show();
		$("#sankakuUp").hide();

		$("#sankakuUp").on("click", function(){
			$("#div_tools").hide();
			$("#sankakuUp").hide();
			$("#sankakuDown").show();
			// $("#div_memo_4").show();
		});

		$("#sankakuUp2").on("click", function(){
			$("#div_tools").hide();
			$("#div_memo_4").show();
		});

		$("#sankakuUp3").on("click", function(){
			$("#div_tools").hide();
			$("#div_memo_4").show();
		});				

		$("#sankakuDown").on("click", function(){
			$("#sankakuDown").hide();
			$("#sankakuUp").show();
			$("#div_tools").show();
		});

	});
//******************************************************************
//------------------------------------------------------------------
//******************************************************************
	$(document).ready(function(){

		$("#plusCanvas").on("click", function(){

			var count;
			var x = this.parentNode.childElementCount;

			if (x == 3) {

				last =  this.parentNode.lastElementChild.previousElementSibling.previousElementSibling.id;
				count = last.slice(8);

			} else {

				last =  this.parentNode.lastElementChild.id;
				count = last.slice(8);
			}
			
			count++;


			// var button_html = "<button class='addCanvas' id='div1memo" + count + "' style='margin-left: 2px; float: left' onclick='memoCanvas(this.id)'>メモ" + count + "<span id='remove" + count + "'onclick='deleteButton(this.id)'>&times;</span></button>";
			var button_html = "<button class='addCanvas' id='div1memo" + count + "' style='margin-left: 2px; float: left' onclick='memoCanvas(this.id)'>メモ" + count + "</button>";
			var canvas_html = "<canvas id='paint_canvas" + count + "' width='805' height='300' style='display: none;'></canvas>"

			$('div.div_memo_5').append(button_html);
			$('div.canvas').append(canvas_html);

			torokukensumemo();
		});

	});

	// var torokukensucount;
	function torokukensumemo() {
		// alert("ahihi");

		var divthumbnail1 = document.getElementById('divMemo1');
		var torokukensucount = divthumbnail1.childElementCount - 2;
		$('#iconNotesCount1').text(torokukensucount);
		// debugger;

	}	

	function plusCanvasMemo(id) {

		// alert(id);

		var canvasButton = id.substring(3,4);

		//-----------------------------------------------------
		if (canvasButton == 2) {

			No = canvasButton;

		} else if (canvasButton == 3) {

			No = canvasButton;

		} else if (canvasButton == 4) {

			No = canvasButton;

		} else if (canvasButton == 5) {

			No = canvasButton;

		} else if (canvasButton == 6) {

			No = canvasButton;

		} else if (canvasButton == 7) {

			No = canvasButton;

		} else if (canvasButton == 8) {

			No = canvasButton;

		} else if (canvasButton == 9) {

			No = canvasButton;

		} else {

			No = canvasButton;
			
		}
		//-----------------------------------------------------

		var count;

		var t = document.getElementById(id);

		var x = t.parentNode.childElementCount;	

		if (x == 2) {

			last =  t.parentNode.lastElementChild.previousElementSibling.id;
			count = last.slice(8);

		} else {

			last =  t.parentNode.lastElementChild.id;
			count = last.slice(8);
		}
			
		count++;


		// var button_html = "<button class='addCanvas' id='div" + No + "memo" + count + "' style='margin-left: 2px; float: left' onclick='memoCanvas(this.id)'>メモ" + count + "<span id='div" + No + "remove" + count + "'onclick='deleteButton(this.id)'>&times;</span></button>";
		var button_html = "<button class='addCanvas' id='div" + No + "memo" + count + "' style='margin-left: 2px; float: left' onclick='memoCanvas(this.id)'>メモ" + count + "</button>";
		var canvas_html = "<canvas id='paint_canvas_" + No + "_Alt" + count + "' width='805' height='300' style='display: none;'></canvas>"

		$('div.classdivMemo' + No).append(button_html);
		$('div.canvasAlt' + No).append(canvas_html);

	}
//******************************************************************
//------------------------------------------------------------------
//******************************************************************
	var deletecanvas = 0;

	var deletecanvasAlt;

	function deleteButton(id){
		// alert(id);
		// alert(currentID);
		// debugger;



		if (currentID.length > 13) {
			var No = currentID.substring(3,4);
			var x = id.slice(10);

			var canvas = "paint_canvas_" + No + "_Alt"+ x;

			//---------------------------------------
			var button = document.getElementById(id);
			button.remove();
			//---------------------------------------

			//---------------------------------------
			var canvas_remove = document.getElementById(canvas);
			canvas_remove.remove();
			//---------------------------------------
			deletecanvasAlt = "deletecanvasAlt" + No; 

		} else {

			var x = currentID.slice(12);
			var button = "div1memo" + x;
			var canvas = "paint_canvas" + x;

			//---------------------------------------
			if (x != "1") {
				var button_remove = document.getElementById(button);
				button_remove.remove();
			}

			//---------------------------------------

			//---------------------------------------
			if (x != "1") {
				var canvas_remove = document.getElementById(canvas);
				canvas_remove.remove();
				$('#paint_canvas1').show();
				$('#div1memo1').css("background-color", "#ebdcb5");
				currentID = "paint_canvas1";
			}

			//---------------------------------------
			// deletecanvas = x;	
		}

		torokukensumemo();

	}
//******************************************************************
//------------------------------------------------------------------
//******************************************************************
	function getFilePathFromDialog() {

		document.getElementById('fileUpload').click();

	}
//******************************************************************
//------------------------------------------------------------------
//******************************************************************
	
	function memoCanvas(id){

		// alert(id);
		var No = id.substring(3,4);
		//---------------------------------------
		if (No != 1) {
			
			
			// var No = id.substring(3,4);
			var x = id.slice(8);
			$('div.canvasAlt' + No).children().css("display", "none");

			var canvas_change = "paint_canvas_" + No + "_Alt" + x;
			var canvas_Button = "div" + No + "memo" + x;

			$('#' + canvas_change).show();
			$('#' + canvas_Button).siblings().css("background-color", "");
			$('#' + canvas_Button).css("background-color", "#ebdcb5");

			if (No == "2") {

				currentID2 = canvas_change;
				CanvasChangeDraw(currentID2);		

			} else if (No == "3") {

				currentID3 = canvas_change;
				CanvasChangeDraw(currentID3);			

			} else if (No == "4") {

				currentID4 = canvas_change;
				CanvasChangeDraw(currentID4);					

			} else if (No == "5") {

				currentID5 = canvas_change;
				CanvasChangeDraw(currentID5);				

			} else if (No == "6") {

				currentID6 = canvas_change;
				CanvasChangeDraw(currentID6);				

			} else if (No == "7") {

				currentID7 = canvas_change;
				CanvasChangeDraw(currentID7);				

			} else if (No == "8") {

				currentID8 = canvas_change;
				CanvasChangeDraw(currentID8);				

			} else if (No == "9") {

				currentID9 = canvas_change;
				CanvasChangeDraw(currentID9);				

			} else {

				currentID10 = canvas_change;
				CanvasChangeDraw(currentID10);				

			}


			if (deletecanvasAlt == "deletecanvasAlt2") {

				$('#paint_canvas_2_Alt1').show();
				$('#div2memo1').css("background-color", "#ebdcb5");
				deletecanvasAlt = "";				

			} else if (deletecanvasAlt == "deletecanvasAlt3") {

				$('#paint_canvas_3_Alt1').show();
				$('#div3memo1').css("background-color", "#ebdcb5");
				deletecanvasAlt = "";				

			} else if (deletecanvasAlt == "deletecanvasAlt4") {

				$('#paint_canvas_4_Alt1').show();
				$('#div4memo1').css("background-color", "#ebdcb5");
				deletecanvasAlt = "";				

			} else if (deletecanvasAlt == "deletecanvasAlt5") {

				$('#paint_canvas_5_Alt1').show();
				$('#div5memo1').css("background-color", "#ebdcb5");
				deletecanvasAlt = "";				

			} else if (deletecanvasAlt == "deletecanvasAlt6") {

				$('#paint_canvas_6_Alt1').show();
				$('#div6memo1').css("background-color", "#ebdcb5");
				deletecanvasAlt = "";				

			} else if (deletecanvasAlt == "deletecanvasAlt7") {

				$('#paint_canvas_7_Alt1').show();
				$('#div7memo1').css("background-color", "#ebdcb5");
				deletecanvasAlt = "";				

			} else if (deletecanvasAlt == "deletecanvasAlt8") {

				$('#paint_canvas_8_Alt1').show();
				$('#div8memo1').css("background-color", "#ebdcb5");
				deletecanvasAlt = "";				

			} else if (deletecanvasAlt == "deletecanvasAlt9") {

				$('#paint_canvas_9_Alt1').show();
				$('#div9memo1').css("background-color", "#ebdcb5");
				deletecanvasAlt = "";				

			} else {

				$('#paint_canvas_10_Alt1').show();
				$('#div10memo1').css("background-color", "#ebdcb5");
				deletecanvasAlt = "";				

			}

			
		

		} else {

			var x = id.slice(8);
			$('div.canvas').children().css( "display", "none");

			var canvas_change = "paint_canvas" + x;
			var canvas_Button = "div1memo" + x;

			$('#' + canvas_change).show();
			$('#' + canvas_Button).siblings().css("background-color", "");
			$('#' + canvas_Button).css("background-color", "#ebdcb5");

			currentID = canvas_change;

			// if (deletecanvas != 0) {
			// 	$('#paint_canvas1').show();
			// 	$('#div1memo1').css("background-color", "#ebdcb5");	
			// 	deletecanvas = 0;
			// }

			//---------------------------------------
			CanvasChangeDraw(currentID);
			//---------------------------------------						
		}
	//--------------------------------------------------------------
	}
	//--------------------------------------------------------------
	function CanvasChangeDraw(id){

		// alert(id);
		//*********************************************************
		//定義・definiations
		const canvas = document.getElementById(id);
		const context = canvas.getContext('2d');
		//*********************************************************

		//Specifications
		context.strokeStyle = "black";			//最初時ブラック化・initial brush color black;
		context.lineWidth = 1;					//initial brush width;

		let drawing = false;
		let pathsry = [];
		let pathsryRedo = [];

		let points = [];

		var mouse = {x: 0, y: 0};
		var previous = {x: 0, y: 0};

		let status = [];

		//Mouse Down Event
		// mousedown
		// touchstart
		canvas.addEventListener('mousedown', function(e){

			var currentCanvasID = e.currentTarget.id;
			// console.log(currentCanvasID);


			drawing = true;

			previous = {x:mouse.x, y:mouse.y};

			// ************************
			mouse = setMouseCoordinates(canvas, e);
			// ************************


			points = [];
			points.push({x:mouse.x, y:mouse.y});


		});


		//Mouse Move Event
		// mousemove
		// touchmove
		canvas.addEventListener('mousemove', function(e){

			if (drawing) {

				previous = {x:mouse.x, y:mouse.y};


				// ************************
				mouse = setMouseCoordinates(canvas, e);
				// ************************

				// saving the points in the points array
				points.push({x:mouse.x, y:mouse.y})

				//start Drawing
				context.beginPath();
				context.moveTo(previous.x, previous.y);

				context.lineTo(mouse.x, mouse.y);
				context.stroke();
			}

		}, false);


		//Mouse Up Event
		// mouseup
		// touchend
		canvas.addEventListener('mouseup', function(e){
			drawing = false;

			// Adding the path to the array or the paths
			pathsry.push(points);
			pathsryRedo.push(points);

			// var text = $("#memo" + memoButton).text();
			// if (memoButton == "1") {

			// 	pathsry1.push(points);
			// 	pathsryRedo1.push(points);
			// 	// status1.push(data1);

			// } else if (memoButton == "2") {

			// 	pathsry2.push(points);
			// 	pathsryRedo2.push(points);
			// 	// status2.push(data1);

			// } else if (memoButton == "3") {

			// 	pathsry3.push(points);
			// 	pathsryRedo3.push(points);
			// 	// status3.push(data1);

			// } else if (memoButton == "4") {

			// 	pathsry4.push(points);
			// 	pathsryRedo4.push(points);
			// 	// status4.push(data1);

			// } else if (memoButton == "5") {

			// 	pathsry5.push(points);
			// 	pathsryRedo5.push(points);
			// 	// status5.push(data1);

			// } else if (memoButton == "6") {

			// 	pathsry6.push(points);
			// 	pathsryRedo6.push(points);
			// 	// status6.push(data1);

			// } else if (memoButton == "7") {

			// 	pathsry7.push(points);
			// 	pathsryRedo7.push(points);
			// 	// status7.push(data1);

			// } else if (memoButton == "8") {

			// 	pathsry8.push(points);
			// 	pathsryRedo8.push(points);
			// 	// status8.push(data1);

			// } else if (memoButton == "9") {

			// 	pathsry9.push(points);
			// 	pathsryRedo9.push(points);
			// 	// status9.push(data1);

			// } else if (memoButton == "10"){

			// 	pathsry10.push(points);
			// 	pathsryRedo10.push(points);
			// 	// status10.push(data1);
			// }

			var statusStrokeStyle = context.strokeStyle;
			var statusLineWidth = context.lineWidth;
			checkStroke(statusStrokeStyle, statusLineWidth);

		}, false);
		//*********************************************************

		//***************************************************************************
		// Handle Mouse Coordinates
		function setMouseCoordinates(canvas, e) {
			var boundings = canvas.getBoundingClientRect();

			//*********************************************************************************************************
			return {
				x : Math.round(event.clientX - boundings.left), y : Math.round(event.clientY - boundings.top)
				// x : Math.round(event.changedTouches[0].clientX - boundings.left), y : Math.round(event.changedTouches[0].clientY - boundings.top)
			}
			//*********************************************************************************************************

		}
		//***************************************************************************
		function checkStroke(statusStrokeStyle, statusLineWidth){

			var data1 = [];
			data1.push(statusStrokeStyle);
			data1.push(statusLineWidth);
			status.push(data1);

			// if (memoButton == "1") {

			// 	status1.push(data1);

			// } else if (memoButton == "2") {

			// 	status2.push(data1);

			// } else if (memoButton == "3") {

			// 	status3.push(data1);

			// } else if (memoButton == "4") {

			// 	status4.push(data1);

			// } else if (memoButton == "5") {

			// 	status5.push(data1);

			// } else if (memoButton == "6") {

			// 	status6.push(data1);

			// } else if (memoButton == "7") {

			// 	status7.push(data1);

			// } else if (memoButton == "8") {

			// 	status8.push(data1);

			// } else if (memoButton == "9") {

			// 	status9.push(data1);

			// } else if (memoButton == "10"){

			// 	status10.push(data1);

			// }

		}
		//***************************************************************************		
	}
//******************************************************************
//------------------------------------------------------------------
//******************************************************************
	function pickTool(id){
		//---------------------------------------
		// alert(id);
		//---------------------------------------
		if (id == "pencil" || id == "blackPen" || id == "redPen" || id == "bluePen") {
			$("#pencil").css("background-color", "#8598c7");

			$("#eraserBig").css("background-color", "#e8dfda");
			$("#eraserMiddle").css("background-color", "#e8dfda");
			$("#eraserSmall").css("background-color", "#e8dfda");
		} else {
			$("#pencil").css("background-color", "#e8dfda");
		};
		//*********************
		if (id == "eraser" || id == "eraserBig" || id == "eraserMiddle" || id == "eraserSmall") {
			$("#eraser").css("background-color", "#8598c7");

			$("#blackPen").css("background-color", "#e8dfda");
			$("#redPen").css("background-color", "#e8dfda");
			$("#bluePen").css("background-color", "#e8dfda");
			$("#bigDot").css("background-color", "#e8dfda");
			$("#middleDot").css("background-color", "#e8dfda");
			$("#smallDot").css("background-color", "#e8dfda");

		} else {
			$("#eraser").css("background-color", "#e8dfda");
		};
	}
	//--------------------------------------------------------------
	function pickPen(id){
		//---------------------------------------
		// alert(id);
		//---------------------------------------
		if (id == "blackPen") {
			$("#blackPen").css("background-color", "#8598c7");
		} else {
			$("#blackPen").css("background-color", "#e8dfda");
		};
		//*********************
		if (id == "redPen") {
			$("#redPen").css("background-color", "#8598c7");
		} else {
			$("#redPen").css("background-color", "#e8dfda");
		};
		//*********************
		if (id == "bluePen") {
			$("#bluePen").css("background-color", "#8598c7");
		} else {
			$("#bluePen").css("background-color", "#e8dfda");
		};

		pickTool(id);
	}
	//--------------------------------------------------------------
	function pickPenSize(id){
		//---------------------------------------
		// alert(id);
		//---------------------------------------
		if (id == "bigDot") {
			$("#bigDot").css("background-color", "#8598c7");
		} else {
			$("#bigDot").css("background-color", "#e8dfda");
		};
		//*********************
		if (id == "middleDot") {
			$("#middleDot").css("background-color", "#8598c7");
		} else {
			$("#middleDot").css("background-color", "#e8dfda");
		};
		//*********************
		if (id == "smallDot") {
			$("#smallDot").css("background-color", "#8598c7");
		} else {
			$("#smallDot").css("background-color", "#e8dfda");
		};
	}
	//--------------------------------------------------------------
	function pickEraserSize(id){
		//---------------------------------------
		// alert(id);
		//---------------------------------------
		if (id == "eraserBig") {
			$("#eraserBig").css("background-color", "#8598c7");
		} else {
			$("#eraserBig").css("background-color", "#e8dfda");
		};
		//*********************
		if (id == "eraserMiddle") {
			$("#eraserMiddle").css("background-color", "#8598c7");
		} else {
			$("#eraserMiddle").css("background-color", "#e8dfda");
		};
		//*********************
		if (id == "eraserSmall") {
			$("#eraserSmall").css("background-color", "#8598c7");
		} else {
			$("#eraserSmall").css("background-color", "#e8dfda");
		};

		$("#blackPen").css("background-color", "#e8dfda");
		$("#redPen").css("background-color", "#e8dfda");
		$("#bluePen").css("background-color", "#e8dfda");
		$("#bigDot").css("background-color", "#e8dfda");
		$("#middleDot").css("background-color", "#e8dfda");
		$("#smallDot").css("background-color", "#e8dfda");

		pickTool(id);
	}
//******************************************************************
//------------------------------------------------------------------
//*************************************************
//*                                               *
//*                メモ登録modal                    *
//*                                               *
//*************************************************