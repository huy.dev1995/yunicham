/*//////追加クリック////////////////////////////////////////////////////////*/

function clickTsuika() {

    $(".modalCommonContactInformation").css('display', 'block');

    $("#input_CommonContactInformation").val('');

    $("#buttonEdit").css('display', 'none');
    $("#buttonDelete").css('display', 'none');
    $("#buttonRegistration").css('display', 'block');
}
function buttonCancel() {
    $(".modalCommonContactInformation").css('display', 'none');
    // location.reload();
}
function buttonRegistration() {

    var inputCI = $("#input_CommonContactInformation").val();
    
    var inputHiddenTable = $("#inputHiddenTableCI").val();
    var convert = parseInt(inputHiddenTable);
    convert = convert + 1;

    $("#inputHiddenTableCI").val(convert);
   
    var htmlCI = " <tr class='item'><td id='CIA" + convert + "' style='width: 700px; border-color: lightgray; padding-left: 20px; border-top-style: none; height: 50px;'>"+ inputCI +"</td><td id='CIB" + convert + "' style='width: 150px; border-color: lightgray; padding-left: 20px; border-top-style: none;'>XXXX</td><td id='CIC" + convert + "'style='display:none'>" + convert + "</td></tr>"
    
    $("#myTbody").append(htmlCI);

    $(".modalCommonContactInformation").css('display', 'none');

    clickCauseTable();
    $("#buttonEdit").css('display', 'block');
    $("#buttonDelete").css('display', 'block');
    $("#buttonRegistration").css('display', 'none');
    // location.reload();
}
function buttonEdit() {

    var inputEditCI = $("#input_CommonContactInformation").val();
    var idhidden = $("#inputHiddenModalCI").val();

    $("#CIA" + idhidden).text(inputEditCI);
    $("#CIC" + idhidden).text(idhidden);

    $(".modalCommonContactInformation").css('display', 'none');
    // location.reload();
}
function buttonDelete() {

    $("#myTbody").find('tr[class="item selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }

    })
    
    $("#buttonEdit").css('display', 'block');
    $("#buttonDelete").css('display', 'block');
    $("#buttonRegistration").css('display', 'none');

    $(".modalCommonContactInformation").css('display', 'none');
    // location.reload();
}
/*//////データの取得(初期処理)///////////////////////////////////////////////*/

var dataCommonContactInformation = []

function getData() {

    var dataset_commoncontactinformation = []

    var lineCommonContactInformation = JSON.parse(CommonContactInformation);

    dataset_commoncontactinformation.push(lineCommonContactInformation);

    for (var i = 0; i < lineCommonContactInformation.length; i++) {
        Object.values(dataset_commoncontactinformation[0][i]);
        dataCommonContactInformation.push(Object.values(dataset_commoncontactinformation[0][i]));
    }

}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////メインテーブル作成(初期処理)/////////////////////////////////////////*/

function makeMainTable() {

    var tBody = document.getElementById("myTbody");

    for (var count = 0; count < dataCommonContactInformation.length; count++) {
        var row = tBody.insertRow(count);

        var c1 = row.insertCell(0);
        var c2 = row.insertCell(1);
        var c3 = row.insertCell(2);

        c1.setAttribute("id", "CIA" + count);
        c2.setAttribute("id", "CIB" + count);
        c3.setAttribute("id", "CIC" + count);
       
        c1.style.cssText = "width:700px; border-color:lightgray; padding-left: 20px; border-top-style:none; height: 50px;";
        c2.style.cssText = "width:150px; border-color:lightgray; padding-left: 20px; border-top-style:none;";
        c3.style.cssText = "display:none";

        c1.innerHTML = dataCommonContactInformation[count][0];
        c2.innerHTML = dataCommonContactInformation[count][1];
        c3.innerHTML = count;
        $('#inputHiddenTableCI').val(count);
     
    }
}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////詳細クリック/////////////////////////////////////////////////////////*/
var Common = "";
var Filler = "";

/*///////////////////////////////////////////////////////////////////////////*/

$(document).ready(function () {
    getData();
    makeMainTable();
    $('#myTbody tr').attr('class', 'item');
    clickCauseTable();
    

});
function clickCauseTable() {
    $("#myTbody tr").click(function () {
        // クリックした行の情報取得
        Common = $(this).children('td')[0].innerText;
        Filler = $(this).children('td')[1].innerText;

        // 選択されている他の行があれば選択解除
        $(this).siblings().removeClass("selected");
        //クリックされた行を選択
        $(this).addClass("selected");

        //Set item  save in localstorage 
        var index0 = $(this).children()[0].textContent;
        var index1 = $(this).children()[1].textContent;
        var index2 = $(this).children()[2].textContent;

        localStorage.setItem('key1', '1');
        localStorage.setItem('index0', index0);
        localStorage.setItem('index1', index1);
        localStorage.setItem('index2', index2);
        // 変更画面
        $(".modalCommonContactInformation").css('display', 'block');
        var KyotsuRenrakuJikou = localStorage.getItem("index0");
        var hidden = localStorage.getItem("index2")
        // load data
        $('#input_CommonContactInformation').val(KyotsuRenrakuJikou);

        $('#inputHiddenModalCI').val(hidden);
        
        $("#buttonEdit").css('visibility', 'visible');
        $("#buttonDelete").css('visibility', 'visible');
        $("#buttonRegistration").css('display', 'none');

    });
}



