function clickCancel() {
    window.location.href = "LineMaintenanceList.html";
}
$(document).ready(function () {
    //
    $(".area input[type=radio]").each(function (index, element) {
        // DOMに関する処理
        // console.log(element);
        // var valueCheckbox = $(this).val();

        if (this.value == "1") {

            $("#childList").append('<li><span>生産予定</span></li>')

        }

    });
    //
    var gouki = localStorage.getItem('Gouki');
    var hizukei = localStorage.getItem('Hizukei');
    var kimu = localStorage.getItem('Kimu');

    var splip1 = hizukei.split('/');
    var pt00 = splip1[0];
    var pt01 = splip1[1];
    var pt02 = splip1[2];
    if (pt01 < 10) {
        pt01 = "0" + pt01;
        console.log(pt01);
    }
    if (pt02 < 10) {
        pt02 = "0" + pt02;
        console.log(pt02);
    }
    $('#date1').val(pt00 + '-' + pt01 + '-' + pt02);

    $("#selectkimu option").each(function () {
        if ($(this).text() == kimu) {
            $(this).attr('selected', 'selected');
        }
    });

});
function changeRad1() {
    $("#childList").text('生産予定')
}
function changeRad2() {
    $("#childList").text('生産実績')
}
function changeRad3() {
    $("#childList").text('点検・保全予定')
}
function changeRad4() {
    $("#childList").text('活動予定')
}
function changeRad5() {
    $("#childList").text('連絡事項')
}
function changeRad6() {
    $("#childList").text('共通連絡事項')
}
function changeRad7() {
    $("#childList").text('使用工作機器')
}
function changeRad8() {
    $("#childList").text('部品管理')
}
function changeRad9() {
    $("#childList").text('品質管理者コメント')
}
