$(function () {
    const $userInfoButton = $('#CMN-BTN-003');
    const $maintenanceButton = $('#CMN-BTN-004');
    const $toolbarItems = $('#CMN-TAB-006').find('ul');
    const $alternativeUserSelect = $('#CMN-SEL-012');
    const $switchingUserButton = $('#CMN-BTN-013');
    const $localeSelect = $('#CMN-SEL-018');
    const $initialScreenBtn = $('#CMN-LNK-020');
    const $logoutButton = $('#CMN-LNK-021-001');
    const $backToOriginalUserButton = $('#CMN-LNK-021-002');
    const $launcher = $('#CMN-LNK-022');
    const $expandedAreas = $('.side-nav__caret');
    const $launcherTicker = $('#launcher-ticker');
    const $launcherTickerIcon = $launcherTicker.find('i');
    const $userInfo = $("#user-info");
    const $overlay = $('#overlay');

    const launcher = JSON.parse(localStorage.getItem('launcher'));
    let lastLocaleLanguage = $localeSelect.val();

    if (launcher && launcher.ticked) {
        $launcherTicker.data("is-ticked", "true");
        $launcherTickerIcon.removeClass('fa-thumbtack').addClass('fa-angle-double-right');
        $launcher.addClass('launcher--ticked');
    }

    $maintenanceButton.on('click', function () {
        window.location.href = `${contextPath}/MCN`;
    });

    $switchingUserButton.on('click', function () {
        const alternativeUserId = $alternativeUserSelect.val();
        post('/change-user', { userId: alternativeUserId });
    });

    $logoutButton.on('click', function () {
        showConfirmDialog({
            title: 'Confirmation',
            message: 'Do you want to logout of the system'
        }).then(function (result) {
            if (result) {
                post('/logout');
            }
        });
    });

    $backToOriginalUserButton.on('click', function () {
        post('/back-to-original-user');
    });

    $expandedAreas.each(function () {
        $(this).on('click', function (event) {
            event.preventDefault();
            event.stopPropagation();

            $(this).toggleClass('side-nav__caret--down');
            const $children = $(this).closest('li').find('> .side-nav--nested');
            if ($children.is(":visible")) {
                $children.slideUp('fast');
            } else {
                $children.slideDown('fast');
            }
        });
    });

    $userInfoButton.on('click', function (e) {
        $userInfo.toggleClass('user-info--show');
        $overlay.toggleClass('overlay--show');
    });

    $userInfo.on('click', function (e) {
        e.stopPropagation();
        e.preventDefault();
    });

    $overlay.on('click', function (e) {
        e.stopPropagation();
        $overlay.toggleClass('overlay--show');
        $userInfo.toggleClass('user-info--show');
    });

    $toolbarItems.on('mouseover', function (e) {
        const data = $launcherTicker.data();
        if (data.isTicked === 'true') {
            return;
        }
        $launcher.addClass('launcher--show');
    });

    $toolbarItems.on('mouseout', function (e) {
        $launcher.removeClass('launcher--show');
    });

    $launcherTicker.on('click', function () {
        const data = $launcherTicker.data();
        if (data.isTicked === 'true') {
            $launcherTicker.data("is-ticked", "false");
            $launcherTickerIcon.removeClass('fa-angle-double-right').addClass('fa-thumbtack');
            $launcher.removeClass('launcher--ticked');
            localStorage.removeItem('launcher');
        } else {
            $launcherTicker.data("is-ticked", "true");
            $launcherTickerIcon.removeClass('fa-thumbtack').addClass('fa-angle-double-right');
            $launcher.addClass('launcher--ticked');
            localStorage.setItem('launcher', JSON.stringify({ ticked: true }));
        }
    });

    $localeSelect.on('change', function () {
        const localeLanguage = $(this).val();
        const initialScreen = confirmDialog.initialScreen;
        showConfirmDialog({
            title: 'Confirmation',
            message: `Do you want to change locale language to ${localeLanguage}`,
            button: initialScreen.button
        }).then(function (result) {
            if (result) {
                $.ajax({
                    url: '/changeLocaleLanguage',
                    method: 'POST',
                    data: { localeLanguage }
                }).then(function () {
                    location.reload();
                }).catch(function () {
                    $localeSelect.val(lastLocaleLanguage);
                });
            } else {
                $localeSelect.val(lastLocaleLanguage);
            }
        })
    });

    $initialScreenBtn.on('click', function () {
        const initialScreen = confirmDialog.initialScreen;
        const formId = $(this).data().formId;
        showConfirmDialog({
            title: 'Confirmation',
            message: initialScreen.message,
            button: initialScreen.button
        }).then(function (result) {
            if (result) {
                $.ajax({
                    url: '/changeFormId',
                    method: 'POST',
                    data: { formId }
                }).then(function () {
                    location.reload();
                });
            }
        })
    });

    function post(url, data) {
        const form = document.createElement('form');
        form.action = `${contextPath}${url}`;
        form.method = 'POST';

        if (data) Object.keys(data).forEach(key => {
            form.appendChild(createElement('input', { type: 'hidden', value: data[key], name: key }));
        });

        form.appendChild(createElement('input', { type: 'hidden', value: getCsrfToken(), name: '_csrf' }));

        document.body.append(form);
        form.submit();

        setTimeout(function () {
            form.remove();
        }, 0);
    }
});

function bindClickEventToTreeView() {
    const $tree = $('.tree');
    const $carets = $tree.find('.caret');
    const $nodeLink = $tree.find('a');

    $carets.each(function () {
        $(this).on('click', function () {
            const $self = $(this);
            $self.toggleClass('caret--right');
            const $children = $self.parent().find('ul');

            if ($children.is(":visible")) {
                $children.hide('fast');
                $self.attr('title', 'Show');
            } else {
                $children.show('fast');
                $self.attr('title', 'Hide');
            }
        });
    });

    $nodeLink.each(function () {
        $(this).on('click', function (e) {
            e.preventDefault();
            $nodeLink.removeClass('node-active');

            const $self = $(this);
            $self.addClass('node-active');
            this.dispatchEvent(new CustomEvent('nodeChange', { bubbles: true, detail: $self.data() }))
        })
    });
}
