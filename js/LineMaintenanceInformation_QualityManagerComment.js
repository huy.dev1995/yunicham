/*//////追加クリック////////////////////////////////////////////////////////*/

function clickTsuika() {

    $(".modalQualityControlComment").css('display', 'block');
    $("#input_QualityControlComment").val('');

    $("#buttonEdit").css('display', 'none');
    $("#buttonDelete").css('display', 'none');
    $("#buttonRegistration").css('display', 'block');
}
function buttonCancel() {
    $(".modalQualityControlComment").css('display', 'none');
    // location.reload();
}
function buttonRegistration() {
    var inputPM = $("#input_QualityControlComment").val();

    var inputHiddenTable = $("#inputHiddenTableQM").val();
    var convert = parseInt(inputHiddenTable);
    convert = convert + 1;

    $("#inputHiddenTableQM").val(convert);

    var htmlQM = " <tr class='item'><td id='QMA" + convert + "' style='width: 700px; border-color: lightgray; padding-left: 20px; border-top-style: none; height: 50px;'>" + inputPM + "</td><td id='QMB" + convert + "' style='width: 150px; border-color: lightgray; padding-left: 20px; border-top-style: none;'>XXXX</td><td id='QMC" + convert + "'style='display:none'>" + convert + "</td></tr>"

    $("#myTbody").append(htmlQM);
    $(".modalQualityControlComment").css('display', 'none');
    clickCauseTable();
    $("#buttonEdit").css('display', 'block');
    $("#buttonDelete").css('display', 'block');
    $("#buttonRegistration").css('display', 'none');
    // location.reload();
}
function buttonEdit() {
    var inputEditQM = $("#input_QualityControlComment").val();
    var idhidden = $("#inputHiddenModalQM").val();

    $("#QMA" + idhidden).text(inputEditQM);
    $("#QMC" + idhidden).text(idhidden);
    $(".modalQualityControlComment").css('display', 'none');
    // location.reload();
}
function buttonDelete() {
    $("#myTbody").find('tr[class="item selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }

    })

    $("#buttonEdit").css('display', 'block');
    $("#buttonDelete").css('display', 'block');
    $("#buttonRegistration").css('display', 'none');
    $(".modalQualityControlComment").css('display', 'none');
    // location.reload();
}

/*//////データの取得(初期処理)///////////////////////////////////////////////*/

var dataQualityManagerComment = []

function getData() {

    var dataset_qualitymanagercomment = []

    var lineQualityManagerComment = JSON.parse(QualityManagerComment);

    dataset_qualitymanagercomment.push(lineQualityManagerComment);

    for (var i = 0; i < lineQualityManagerComment.length; i++) {
        Object.values(dataset_qualitymanagercomment[0][i]);
        dataQualityManagerComment.push(Object.values(dataset_qualitymanagercomment[0][i]));
    }

}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////メインテーブル作成(初期処理)/////////////////////////////////////////*/

function makeMainTable() {

    var tBody = document.getElementById("myTbody");

    for (var count = 0; count < dataQualityManagerComment.length; count++) {
        var row = tBody.insertRow(count);

        var c1 = row.insertCell(0);
        var c2 = row.insertCell(1);
        var c3 = row.insertCell(2);
        c1.setAttribute("id", "QMA" + count);
        c2.setAttribute("id", "QMB" + count);
        c3.setAttribute("id", "QMC" + count);

        c1.style.cssText = "width:700px; border-color:lightgray; padding-left: 20px; border-top-style:none; height: 50px;";
        c2.style.cssText = "width:150px; border-color:lightgray; padding-left: 20px; border-top-style:none;";
        c3.style.cssText = "display:none";

        c1.innerHTML = dataQualityManagerComment[count][0];
        c2.innerHTML = dataQualityManagerComment[count][1];
        c3.innerHTML = count;
        $('#inputHiddenTableQM').val(count);


    }
}


/*//////詳細クリック/////////////////////////////////////////////////////////*/
var QualityControlCommentinput = "";
var Filler = "";

//*///////////////////////////////////////////////////////////////////////////*/

$(document).ready(function () {
    getData();
    makeMainTable();
    $('#myTbody tr').attr('class', 'item');
   clickCauseTable();
});

function clickCauseTable() {
    $("#myTbody tr").click(function () {
        // クリックした行の情報取得
        QualityControlCommentinput = $(this).children('td')[0].innerText;
        Filler = $(this).children('td')[1].innerText;

        // 選択されている他の行があれば選択解除
        $(this).siblings().removeClass("selected");
        //クリックされた行を選択
        $(this).addClass("selected");

        //Set item  save in localstorage 
        var index0 = $(this).children()[0].textContent;
        var index1 = $(this).children()[1].textContent;
        var index2 = $(this).children()[2].textContent;
        localStorage.setItem('key1', '1');
        localStorage.setItem('index0', index0);
        localStorage.setItem('index1', index1);
        localStorage.setItem('index2', index2);
        // 変更画面
        $(".modalQualityControlComment").css('display', 'block');
        var komento = localStorage.getItem("index0");
        var hidden = localStorage.getItem("index2")
        // load data
        $('#input_QualityControlComment').val(komento);
        $('#inputHiddenModalQM').val(hidden);

        $("#buttonEdit").css('visibility', 'visible');
        $("#buttonDelete").css('visibility', 'visible');
        $("#buttonRegistration").css('display', 'none');

    });
}

