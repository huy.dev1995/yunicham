// Cancel button onclick
function btnCancel() {
    // window.close();
    window.location.href = "InspectionPlanList.html";
}
// When click button 登録
function btnToroku() {
    // window.close();
    window.location.href = "InspectionPlanList.html";
}
// When button Delete onclick
function buttonDelete() {
    // window.close();
    window.location.href = "InspectionPlanList.html";
}
// When button Edit onclick
function buttonEdit() {
    // window.close();
    window.location.href = "InspectionPlanList.html";
}
// 実施月は全選択
function btnCheckAll() {
    $(".area input[type=checkbox]").each(function (index, element) {

        $(this).prop("checked", "checked");

    });
}
//// 実施月は全解除
function btnDeleteAll() {

    $(".area input[type=checkbox]").each(function (index, element) {

        $(this).prop("checked", false);

    });

}

// change selectbox 実施頻度
// click button 直毎
function clickDirectly() {
    $("#tr_jisshihindo").css("display", "none");
    $("#tr_kinmu").css("display", "none");
    $("#tr_hizukei").css("display", "none");
    $("#tr_jisshibi").css("display", "none");
    $("#tr_Youbi").css("display", "none");
    $("#tr_jishishuy").css("display", "none");
    $("#tr_kyujitsu").css("display", "none");
    $("#tr_JisshiTsuki").css("display", "none");
    $("#hiddenhizo").css("display", "none");
}
// click button 日毎
function clickDaily() {
    $("#tr_jisshihindo").css("display", "table-row");
    $("#tr_kinmu").css("display", "table-row");
    $("#tr_hizukei").css("display", "none");
    $("#tr_jisshibi").css("display", "none");
    $("#tr_Youbi").css("display", "none");
    $("#tr_jishishuy").css("display", "none");
    $("#tr_kyujitsu").css("display", "none");
    $("#tr_JisshiTsuki").css("display", "none");
    $("#hiddenhizo").css("display", "none");
}
// click button 週毎
function clickWeekly() {
    $("#tr_jisshihindo").css("display", "table-row");
    $("#tr_Youbi").css("display", "table-row");
    $("#tr_kinmu").css("display", "table-row");
    $("#tr_jishishuy").css("display", "table-row");
    $("#tr_kyujitsu").css("display", "table-row");
    $("#tr_hizukei").css("display", "none");
    $("#tr_jisshibi").css("display", "none");
    $("#tr_JisshiTsuki").css("display", "none");
    $("#hiddenhizo").css("display", "none");

}
// click button 月毎
function clickMonthly() {
    $("#tr_jisshihindo").css("display", "table-row");
    $("#tr_JisshiTsuki").css("display", "table-row");
    $("#tr_Youbi").css("display", "none");
    $("#tr_kinmu").css("display", "table-row");
    $("#tr_hizukei").css("display", "table-row");
    $("#tr_jisshibi").css("display", "table-row");
    $("#tr_kyujitsu").css("display", "table-row");
    $("#tr_jishishuy").css("display", "none");
    $("#date1").css("display", "none");
    $("#date2").css("display", "table-row");
    $("#jisshibiSelect1").css("display", "none");
    $("#jisshibiSelect").css("display", "table-row");
    $("#lbNichi").css("display", "inline-block");
    $("#hiddenhizo").css("display", "none");
}

// click button 日付指定
function clickDateDesignation() {
    $("#tr_jisshihindo").css("display", "table-row");
    $("#tr_Youbi").css("display", "none");
    $("#tr_kinmu").css("display", "table-row");
    $("#tr_hizukei").css("display", "table-row");
    $("#tr_jisshibi").css("display", "none");
    $("#date2").css("display", "none");
    $("#date1").css("display", "table-row");
    $("#lbNichi").css("display", "none");
    $("#tr_JisshiTsuki").css("display", "none");
    $("#tr_kyujitsu").css("display", "none");
    //
    $("#hiddenhizo").css("display", "table-row");


}

//　Change selectbox 実施日
function changeSelectJisshibi(value) {
    if (value == 1) {
        date1Input = $('input#date2');
        date1Input.prop('disabled', false);

    }

    else {
        date1Input = $('input#date2');
        date1Input.prop('disabled', true);
        $('input#date2').val("");
    }
}

// Load page 
$(document).ready(function () {
    //load page
    $("#tr_jisshihindo").css("display", "none");
    $("#tr_kinmu").css("display", "none");
    $("#tr_hizukei").css("display", "none");
    $("#tr_jisshibi").css("display", "none");
    $("#tr_Youbi").css("display", "none");
    $("#tr_jishishuy").css("display", "none");
    $("#tr_kyujitsu").css("display", "none");
    $("#tr_JisshiTsuki").css("display", "none");
    // load page default 日付 display
    date1Input = $('input#date2');
    date1Input.prop('disabled', true);
    $('input#date2').val("");

    //get item from localStorage
    var key = localStorage.getItem('key1');
    // 変更画面 load data
    if (key == 1) {
        // set active button
        $("#buttonEdit").css('visibility', 'visible');
        $("#buttonDelete").css('visibility', 'visible');
        $("#buttonRegistration").css('display', 'none');
        //set menu header
        $('#child1').text("点検計画編集");
        $("#btnRegistration").css("display", "none");
        $("#btEdit").css("display", "table-row");
        $("#btnDelete").css("display", "table-row");
        //get data from loaclstrorage
        var tenkenshuybetsu = localStorage.getItem("index0");
        var jisshihindo = localStorage.getItem("index1");
        var jisshitsuki = localStorage.getItem("index2");
        var jisshishuu = localStorage.getItem("index3");
        var nichiyou = localStorage.getItem("index4");
        var jisshibi = localStorage.getItem("index5");
        var hidzuke = localStorage.getItem("index6");
        var kinmu = localStorage.getItem("index7");

        //直毎
        if (jisshihindo == "直毎") {

            document.getElementById("tabDirectly").checked = true;

        }

        $("#tenkenSelect1 option").each(function () {
            if ($(this).text() == tenkenshuybetsu) {
                $(this).attr('selected', 'selected');
            }
        });
        $("#jisshihindoSelect option").each(function () {
            if ($(this).text() == jisshihindo) {
                $(this).attr('selected', 'selected');
            }
        });
        //日付指定
        if (jisshihindo == "日付指定") {
            clickDateDesignation();
            document.getElementById("tabDateDesignation").checked = true;
            //
            var split1 = hidzuke.split('/');
            var pt00 = split1[0];
            var pt01 = split1[1];
            var pt02 = split1[2];

            if (pt01 < 10) {
                pt01 = "0" + pt01;
                console.log(pt01);
            }
            if (pt02 < 10) {
                pt02 = "0" + pt02;
                console.log(pt02);
            }
            $('#date1').val(pt00 + '-' + pt01 + '-' + pt02);
            //
            $("#kinmuSelect option").each(function () {
                if ($(this).text() == kinmu) {
                    $(this).attr('selected', 'selected');
                }
            });

        }
        // 日毎
        if (jisshihindo == "日毎") {
            clickDaily();
            document.getElementById("tabDaily").checked = true;
            $("#kinmuSelect option").each(function () {
                if ($(this).text() == kinmu) {
                    $(this).attr('selected', 'selected');
                }
            });
        }
        // 週毎
        if (jisshihindo == "週毎") {
            clickWeekly();
            document.getElementById("tabWeekly").checked = true;
            if (nichiyou == "日") {
                document.getElementById("tabSunday").checked = true;
            }
            if (nichiyou == "月") {
                document.getElementById("tabMonday").checked = true;
            }
            if (nichiyou == "火") {
                document.getElementById("tabTusday").checked = true;
            }
            if (nichiyou == "水") {
                document.getElementById("tabWednesday").checked = true;
            }
            if (nichiyou == "木") {
                document.getElementById("tabThursday").checked = true;
            }
            if (nichiyou == "金") {
                document.getElementById("tabFriday").checked = true;
            }
            if (nichiyou == "土") {
                document.getElementById("tabSaturday").checked = true;
            }
            $("#jisshishuuSelect option").each(function () {
                if ($(this).text() == jisshishuu) {
                    $(this).attr('selected', 'selected');
                }
            });
            $("#kinmuSelect option").each(function () {
                if ($(this).text() == kinmu) {
                    $(this).attr('selected', 'selected');
                }
            });
        }
        // 月毎
        date1Input = $('input#date2');
        date1Input.prop('disabled', false);
        if (jisshihindo == "月毎") {

            changeSelectJisshibi();
            clickMonthly();
            document.getElementById("tabMonthly").checked = true;
            $("#kinmuSelect option").each(function () {
                if ($(this).text() == kinmu) {
                    $(this).attr('selected', 'selected');
                }
            });

            // get data 実施日
            $("#jisshibiSelect option").each(function () {
                if ($(this).text() == jisshibi) {
                    $(this).attr('selected', 'selected');
                }
            });
            if (jisshibi == "日付指定") {
                date1Input = $('input#date2');
                date1Input.prop('disabled', false);
            }
            // get date if datetime 日付
            var splitHizuke = hidzuke.split(' ');
            var pt1 = splitHizuke[0];
            $('#date2').val(pt1);

            // get data set 実施月

            // c1 
            // var splitJishitsuki = jisshitsuki.split(',');
            // for( var i = 0; i < splitJishitsuki.length; i++) {
            //     $("#tab" + splitJishitsuki[i]).prop("checked", "checked");
            // }

            //c2
            var splitJishitsuki = jisshitsuki.split(',');
            // area input type=checkbox
            $(".area input[type=checkbox]").each(function (index, element) {
                // DOMに関する処理
                console.log(element);
                var valueCheckbox = $(this).val();
                var check = splitJishitsuki.includes(valueCheckbox);
                if (check == true) {
                    $(this).prop("checked", "checked");
                }
            });
        }

    }
    // 登録画面移動ため
    if (key == 0) {

        date1Input = $('input#date2');
        date1Input.prop('disabled', false);

        $("#buttonEdit").css('display', 'none');
        $("#buttonDelete").css('display', 'none');
        $("#buttonRegistration").css('display', 'block');
        $("#btnRegistration").css("display", "table-row");
        $('#child1').text("点検計画登録");
        // load page default 実施月 checked
        $(".area input[type=checkbox]").each(function (index, element) {

            $(this).prop("checked", "checked");

        });
    }
    //check input 日付
    function setInputFilter(textbox, inputFilter) {
        ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function (event) {
            textbox.addEventListener(event, function () {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        });
    }

    // Install input filters.
    setInputFilter(document.getElementById("date2"), function (value) {
        return /^-?\d*$/.test(value);
    });
});
//  check input 日付 1-31日
function inputdate2() {
    var checkinput = document.getElementById("date2").value;
    if (checkinput > 31) {
        $('#date2').val("31");
    }
    if (checkinput < 1) {
        $('#date2').val("1");
    }

}