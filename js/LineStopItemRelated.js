$( document ).ready(function() {

	var x = document.documentURI;
	var m = x.replace("html/LineStopItemRelated.html",'');

	// var linkPicture = "https://lh3.googleusercontent.com/d/1eDTUTLUF_x1jkXDUBqjOoQf_bc2_B1ib=s220?authuser=0";
	// var linkNote = "https://lh3.googleusercontent.com/d/1Fu0Ss-gudvrifG8rkMqJzgb0ZVlT2-iE=s220?authuser=0";
	// var linkPencil = "https://lh3.googleusercontent.com/d/1Rw6jF3kz0GRgCNLJ8V9iFvziQBt30hv-=s220?authuser=0";
	// var linkRedPen = "https://lh3.googleusercontent.com/d/1GChAtf8TA4_FnGP0rgSadfjh8EM9Q1Kz=s220?authuser=0";
	// var linkBluePen = "https://lh3.googleusercontent.com/d/1ojl-xTfbFZQS8em_j5SwJjP2NSZ2909M=s220?authuser=0";
	// var linkEraser = "https://lh3.googleusercontent.com/d/1sOkYJkYDstTA4QgvsRlQrSWcvszcT73p=s220?authuser=0";

	// var linkUndo = "https://lh3.googleusercontent.com/d/1rfgcQqzkoAnkXEo1k5nsRJI96P03bMnc=s220?authuser=0";
	// var linkRedo = "https://lh3.googleusercontent.com/d/14r9mC-KkeRjYdasJyrmjSljs2wkKHrRy=s220?authuser=0";	

	// var linkDot = "https://lh3.googleusercontent.com/d/1UHYjUFWMSP5gqSy0AMh79EBrgBriODlT=s220?authuser=0";	
	// var linkCircle = "https://lh3.googleusercontent.com/d/1PjqYdNrdK6dUWjXSkT-Od3aCRdJI_iVu=s220?authuser=0";
	// var linkTriangleUp = "https://lh3.googleusercontent.com/d/1RiyvjajI19VFDz-7z2u4OdF1WOuUri8I=s220?authuser=0";
	// var linkTriangleDown = "https://lh3.googleusercontent.com/d/1hnJeMknQ4bX_0JPSMfHrNBvRIkhIQ9sw=s220?authuser=0";

	var linkPicture = m + "Icons" + "/" + "pictures.jpg";
	var linkNote = m + "Icons" + "/" + "notes.jpg";
	var linkPencil = m + "Icons" + "/" + "pencil.jpg";
	var linkRedPen = m + "Icons" + "/" + "redPencil.jpg";
	var linkBluePen = m + "Icons" + "/" + "bluePencil.jpg";
	var linkEraser = m + "Icons" + "/" + "eraser.jpg";

	var linkUndo = m + "Icons" + "/" + "undo.jpg";
	var linkRedo = m + "Icons" + "/" + "redo.jpg";

	var linkDot = m + "Icons" + "/" + "Dot.jpg";	
	var linkCircle = m + "Icons" + "/" + "circle.jpg";
	var linkTriangleUp = m + "Icons" + "/" + "sankakuUp.jpg";
	var linkTriangleDown = m + "Icons" + "/" + "sankakuDown.jpg";


	document.getElementById("iconPictures1").src = linkPicture;
	document.getElementById("iconNotes1").src = linkNote;

	document.getElementById("iconPictures2").src = linkPicture;
	document.getElementById("iconNotes2").src = linkNote;

	document.getElementById("iconPictures3").src = linkPicture;
	document.getElementById("iconNotes3").src = linkNote;

	document.getElementById("iconPictures4").src = linkPicture;
	document.getElementById("iconNotes4").src = linkNote;

	document.getElementById("iconPictures5").src = linkPicture;
	document.getElementById("iconNotes5").src = linkNote;

	document.getElementById("iconPictures6").src = linkPicture;
	document.getElementById("iconNotes6").src = linkNote;

	document.getElementById("iconPictures7").src = linkPicture;
	document.getElementById("iconNotes7").src = linkNote;

	document.getElementById("iconPictures8").src = linkPicture;
	document.getElementById("iconNotes8").src = linkNote;

	document.getElementById("iconPictures9").src = linkPicture;
	document.getElementById("iconNotes9").src = linkNote;

	document.getElementById("iconPictures10").src = linkPicture;
	document.getElementById("iconNotes10").src = linkNote;

    /////////////////////////////////////////1/////////////////////////////////////////
	document.getElementById("imagePencil").src = linkPencil;
	document.getElementById("imagePencil1").src = linkPencil;
	document.getElementById("1imagePencil2").src = linkPencil;
	document.getElementById("imageRedPen").src = linkRedPen;
	document.getElementById("imageBluePen").src = linkBluePen;

	document.getElementById("imageEraser").src = linkEraser;
	document.getElementById("imageEraser1").src = linkEraser;

	document.getElementById("imageUndo").src = linkUndo;
	document.getElementById("imageRedo").src = linkRedo;

	document.getElementById("loadImages").src = linkPicture;

	document.getElementById("imageDot1").src = linkDot;
	document.getElementById("imageDot2").src = linkDot;
	document.getElementById("imageDot3").src = linkDot;

	document.getElementById("imageCircle1").src = linkCircle;
	document.getElementById("imageCircle2").src = linkCircle;
	document.getElementById("imageCircle3").src = linkCircle;

	document.getElementById("sankakuUp").src = linkTriangleUp;
	document.getElementById("sankakuDown").src = linkTriangleDown;


    /////////////////////////////////////////2/////////////////////////////////////////
    document.getElementById("imagePencil2").src = linkPencil;
    document.getElementById("imagePencil1_2").src = linkPencil;
    document.getElementById("imagePencil2_2").src = linkPencil;
    document.getElementById("imageRedPen2").src = linkRedPen;
    document.getElementById("imageBluePen2").src = linkBluePen;

    document.getElementById("imageEraser2").src = linkEraser;
    document.getElementById("imageEraser1_2").src = linkEraser;

    document.getElementById("imageUndo2").src = linkUndo;
    document.getElementById("imageRedo2").src = linkRedo;

    document.getElementById("loadImages2").src = linkPicture;

    document.getElementById("imageDot1_2").src = linkDot;
    document.getElementById("imageDot2_2").src = linkDot;
    document.getElementById("imageDot3_2").src = linkDot;

    document.getElementById("imageCircle1_2").src = linkCircle;
    document.getElementById("imageCircle2_2").src = linkCircle;
    document.getElementById("imageCircle3_2").src = linkCircle;

    document.getElementById("sankakuUp2").src = linkTriangleUp;
    // document.getElementById("sankakuDown").src = linkTriangleDown;


    /////////////////////////////////////////3/////////////////////////////////////////
    document.getElementById("imagePencil3").src = linkPencil;
    document.getElementById("imagePencil1_3").src = linkPencil;
    document.getElementById("imagePencil2_3").src = linkPencil;
    document.getElementById("imageRedPen3").src = linkRedPen;
    document.getElementById("imageBluePen3").src = linkBluePen;

    document.getElementById("imageEraser3").src = linkEraser;
    document.getElementById("imageEraser1_3").src = linkEraser;

    document.getElementById("imageUndo3").src = linkUndo;
    document.getElementById("imageRedo3").src = linkRedo;

    document.getElementById("loadImages3").src = linkPicture;

    document.getElementById("imageDot1_3").src = linkDot;
    document.getElementById("imageDot2_3").src = linkDot;
    document.getElementById("imageDot3_3").src = linkDot;

    document.getElementById("imageCircle1_3").src = linkCircle;
    document.getElementById("imageCircle2_3").src = linkCircle;
    document.getElementById("imageCircle3_3").src = linkCircle;

    document.getElementById("sankakuUp3").src = linkTriangleUp;
    // document.getElementById("sankakuDown").src = linkTriangleDown;    


    //////////////////////////////////////////////////////////////////////////////////	

});

/////////////////////////////////////atTableMovingRows//////////////////////////////
// ----------------------------------------------------------------------------------
// テーブルクリック時、テキストボックスにフォーカスをセット
$("#myTable1").on("click", function (e) {
    $("#tableEventShifter").focus();
});

// クリック時、行選択(ヘッダ除く)
$("#myTable1 tbody tr").on("click", function (e) {

    // 選択されている他の行があれば選択解除
    $(this).siblings().removeClass("selected");
    // クリックされた行を選択
    $(this).addClass("selected");
});
// ----------------------------------------------------------------------------------

$('#iJyouNaiYouGenJyou').prop('disabled', true);
function changeSelectBox2(value) {
	// alert(value);


	document.getElementById('iJyouNaiYouGenJyou').selectedIndex = 0;
	$("#iJyouNaiYouGenJyou_sonota_textbox").addClass("hidden");


    var temp_inside_teishijikan_kubun_1 = $("#teishijikan_kubun_1").text();
    var miwariate_jikan_atai_1 = $("#miwariate_jikan_atai_1").text();


    var temp_miwariate = miwariate_jikan_atai_1.split(':');
    var temp_miwariate_seconds = (temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;


    document.getElementById('selectBox1').selectedIndex = 0;
    $('#iJyouNaiYouGenJyou').prop('disabled', false);

    $('.option').addClass("displayDiv");
    $('.option' + value).removeClass("displayDiv");

    if (value == 0) {
        document.getElementById('iJyouNaiYouGenJyou').selectedIndex = 0;
        document.getElementById('selectBox1').selectedIndex = 0;
        $('.option').addClass("displayDiv");
        $('#iJyouNaiYouGenJyou').prop('disabled', true);

    }

    // -------------------------------------------------------------------
    if (value == 9) {

        $('#iJyouNaiYouGenJyou').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou').selectedIndex = 0;
        $("#teishijikan_kubun_1").html("不稼働");

        $("#tsuika_button1").addClass("disabledButton")
        $("#tsuika_button1").attr('disabled', 'disabled');

    }

    if (value == 10) {

        $('#iJyouNaiYouGenJyou').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou').selectedIndex = 0;
        $("#teishijikan_kubun_1").html("不稼働");


        $("#tsuika_button1").addClass("disabledButton")
        $("#tsuika_button1").attr('disabled', 'disabled');


    } else {

    	$("#teishikoumoku_sonota_textbox").addClass("hidden");

    }

    if (value == 11) {

        $('#iJyouNaiYouGenJyou').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou').selectedIndex = 0;
        $("#teishijikan_kubun_1").html("不稼働");


        $("#tsuika_button1").addClass("disabledButton")
        $("#tsuika_button1").attr('disabled', 'disabled');

    }

    if (value >= 9 && value <= 11) {

        $('#iJyouNaiYouGenJyou').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou').selectedIndex = 0;

        $("#teishijikan_kubun_1").html("不稼働");

        $("#tsuika_button1").addClass("disabledButton")
        $("#tsuika_button1").attr('disabled', 'disabled');


        $("#iJyouNaiYouGenJyou_sonota_textbox").addClass("hidden");

    } else {

        $("#tsuika_button1").removeClass("disabledButton")
        $("#tsuika_button1").prop('disabled', false);


		if (temp_miwariate_seconds >= 300) {
			$("#teishijikan_kubun_1").html("長期停止");
		} else {
			$("#teishijikan_kubun_1").html("チョコ停");
		}

    }
    // -------------------------------------------------------------------
}

// ----------------------------------------------------------------------
///checkBox-------------------Div_3.1------------------------------------

$(document).ready(function () {

    $("#aitemu_teishikoumoku_1").addClass("disabledButton");
    $("#aitemu_teishikoumoku_1").prop('disabled', true);
    $("#teishikoumoku_sonota_textbox").addClass("hidden");
    $("#iJyouNaiYouGenJyou_sonota_textbox").addClass("hidden");
});



function changeSelectBox1(value) {

	// alert(value);

    $("#aitemu_teishikoumoku_1").addClass("disabledButton");
    $("#aitemu_teishikoumoku_1").prop('disabled', true);

    if (value == 38) {

        $("#aitemu_teishikoumoku_1").removeClass("disabledButton");
        $("#aitemu_teishikoumoku_1").prop('disabled', false);

    }

    if (value >= 27) {

        $('#iJyouNaiYouGenJyou').prop('disabled', true);

    }

    if (value == 43) {

    	$("#teishikoumoku_sonota_textbox").removeClass("hidden");

    } else {

    	$("#teishikoumoku_sonota_textbox").addClass("hidden");

    }

}


function changeSelectBox3(value) {

    if (value == 8) {
        
        $("#iJyouNaiYouGenJyou_sonota_textbox").removeClass("hidden");

        //     // $("#iJyouNaiYouGenJyou_sonota_textbox").toggleClass("show");
        //     $("#iJyouNaiYouGenJyou_sonota_textbox").removeClass("hidden");

    } else {
        $("#iJyouNaiYouGenJyou_sonota_textbox").addClass("hidden");
        // $("#iJyouNaiYouGenJyou_sonota_textbox").addClass("hidden");
        // $("#iJyouNaiYouGenJyou_sonota_textbox").hide();
    }

}

/// ----------------------------------------------------------------------

///checkBox-------------------Div_3.2------------------------------------

$('#iJyouNaiYouGenJyou_2').prop('disabled', true);

function changeSelectBox2_2(value) {


	document.getElementById('iJyouNaiYouGenJyou_2').selectedIndex = 0;
	$("#iJyouNaiYouGenJyou_sonota_textbox_2").addClass("hidden");


    var temp_inside_teishijikan_kubun_2 = $("#teishijikan_kubun_2").text();
    var miwariate_jikan_atai_2 = $("#miwariate_jikan_atai_2").text();


    var temp_miwariate = miwariate_jikan_atai_2.split(':');
    var temp_miwariate_seconds = (temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    document.getElementById('selectBox1_2').selectedIndex = 0;
    $('#iJyouNaiYouGenJyou_2').prop('disabled', false);

    $('.optionNHTTOAN').addClass("displayDiv");
    $('.optionDTMH' + value).removeClass("displayDiv");

    if (value == 0) {
        document.getElementById('iJyouNaiYouGenJyou_2').selectedIndex = 0;
        document.getElementById('selectBox1_2').selectedIndex = 0;

        $('.optionNHTTOAN').addClass("displayDiv");
        $('#iJyouNaiYouGenJyou_2').prop('disabled', true);


    }


    // -------------------------------------------------------------------
    if (value == 9) {

        $('#iJyouNaiYouGenJyou_2').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_2').selectedIndex = 0;

        $("#teishijikan_kubun_2").html("不稼働");
        $("#tsuika_button2").addClass("disabledButton")
        $("#tsuika_button2").attr('disabled', 'disabled');

    }

    if (value == 10) {

        $('#iJyouNaiYouGenJyou_2').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_2').selectedIndex = 0;

        $("#teishijikan_kubun_2").html("不稼働");
        $("#tsuika_button2").addClass("disabledButton")
        $("#tsuika_button2").attr('disabled', 'disabled');

    } else {

    	$("#teishikoumoku_sonota_textbox_2").addClass("hidden");
    	
    }


    if (value == 11) {

        $('#iJyouNaiYouGenJyou_2').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_2').selectedIndex = 0;

        $("#teishijikan_kubun_2").html("不稼働");
        $("#tsuika_button2").addClass("disabledButton")
        $("#tsuika_button2").attr('disabled', 'disabled');

    }

    if (value >= 9 && value <= 11) {

        $('#iJyouNaiYouGenJyou_2').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_2').selectedIndex = 0;

        $("#teishijikan_kubun_2").html("不稼働");
        $("#tsuika_button2").addClass("disabledButton")
        $("#tsuika_button2").attr('disabled', 'disabled');

        $("#iJyouNaiYouGenJyou_sonota_textbox_2").addClass("hidden");
    } else {


        $("#tsuika_button2").removeClass("disabledButton")
        $("#tsuika_button2").prop('disabled', false);

		if (temp_miwariate_seconds >= 300) {
			$("#teishijikan_kubun_2").html("長期停止");
		} else {
			$("#teishijikan_kubun_2").html("チョコ停");
		}        


    }
    // -------------------------------------------------------------------

    // getSelectBox1(value)

}

$(document).ready(function () {

    $("#aitemu_teishikoumoku_2").addClass("disabledButton");
    $("#aitemu_teishikoumoku_2").prop('disabled', true);
    $("#teishikoumoku_sonota_textbox_2").addClass("hidden");
    $("#iJyouNaiYouGenJyou_sonota_textbox_2").addClass("hidden");

});


function changeSelectBox1_2(value) {

    $("#aitemu_teishikoumoku_2").addClass("disabledButton");
    $("#aitemu_teishikoumoku_2").prop('disabled', true);

    if (value == 38) {

        // $('#teiShiYouInTouRoku').prop('disabled', false);
        // $('#teiShiYouInTouRoku').removeClass("disabledButton");			


        $("#aitemu_teishikoumoku_2").removeClass("disabledButton");
        $("#aitemu_teishikoumoku_2").prop('disabled', false);

    }

    if (value >= 27) {
        // $('#teiShiYouInTouRoku').addClass("disabledButton");
        // $('#teiShiYouInTouRoku').prop('disabled', true);
        // $('#teiShiYouInTouRoku').addClass("disabledButton");
        $('#iJyouNaiYouGenJyou_2').prop('disabled', true);


    }

    // if (value == 11) {
    // 	// $('#teiShiYouInTouRoku').addClass("disabledButton");
    // 	// $('#teiShiYouInTouRoku').prop('disabled', true);
    // 	$('#iJyouNaiYouGenJyou').prop('disabled', true);
    // }
    if (value == 43) {

    	$("#teishikoumoku_sonota_textbox_2").removeClass("hidden");
    	
    } else {

    	$("#teishikoumoku_sonota_textbox_2").addClass("hidden");

    }    


}



function changeSelectBox3_2(value) {

    if (value == 8) {

        // $("#iJyouNaiYouGenJyou_sonota_textbox").toggleClass("show");
        $("#iJyouNaiYouGenJyou_sonota_textbox_2").removeClass("hidden");

    } else {

        $("#iJyouNaiYouGenJyou_sonota_textbox_2").addClass("hidden");
        // $("#iJyouNaiYouGenJyou_sonota_textbox").hide();
    }




}

// --------------------------------------------------------------------
//  START_<MODAL>_ アイテム登録
// Get the modal
var modal = document.getElementById("myModal");
var span = document.getElementsByClassName("close")[0];

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

//****************************************************************************************
// Get the myModal_bunwarisettei
var Modal_bunwarisettei = document.getElementById("myModal_bunwarisettei");
var bunwarisettei_button1 = document.getElementById("bunwarisettei_button1");

$(document).ready(function () {

    $("#buttonCancel_bunwarisettei").click(function () {
        //---------------------------------------------
        $("#myModal_bunwarisettei").hide();
        //---------------------------------------------
    })

});

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == Modal_bunwarisettei) {
        Modal_bunwarisettei.style.display = "none";
    }
}
// 
$(document).ready(function () {
    $("#myModal_bunwarisettei").click(function () {

        if (event.target == Modal_bunwarisettei) {
            Modal_bunwarisettei.style.display = "none";
        }

    });
});
//the end 
// Get the modal_チョコ停
var modal_chokotei = document.getElementById("myModal_chokoteishi");

// Get the modal_長期停止
var modal_choukiteishi = document.getElementById("myModal_choukiteishi");
// When the user clicks anywhere outside of the modal_chokotei, close it
$(document).ready(function () {
    $("#myModal_chokoteishi").click(function () {

        if (event.target == modal_chokotei) {
            modal_chokotei.style.display = "none";
        }

    });
});
// When the user clicks anywhere outside of the modal_長期停止, close it
window.onclick = function (event) {
    if (event.target == modal_choukiteishi) {
        modal_choukiteishi.style.display = "none";
    }
}

///checkBox-------------------Div_3.3------------------------------------

$('#iJyouNaiYouGenJyou_3').prop('disabled', true);

function changeSelectBox2_3(value) {

	document.getElementById('iJyouNaiYouGenJyou_3').selectedIndex = 0;
	$("#iJyouNaiYouGenJyou_sonota_textbox_3").addClass("hidden");


    var temp_inside_teishijikan_kubun_3 = $("#teishijikan_kubun_3").text();
    var miwariate_jikan_atai_3 = $("#miwariate_jikan_atai_3").text();


    var temp_miwariate = miwariate_jikan_atai_3.split(':');
    var temp_miwariate_seconds = (temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    document.getElementById('selectBox1_3').selectedIndex = 0;
    $('#iJyouNaiYouGenJyou_3').prop('disabled', false);

    $('.optionNHTTOAN').addClass("displayDiv");
    $('.optionDTMH' + value).removeClass("displayDiv");

    if (value == 0) {
        document.getElementById('iJyouNaiYouGenJyou_3').selectedIndex = 0;
        document.getElementById('selectBox1_3').selectedIndex = 0;

        $('.optionNHTTOAN').addClass("displayDiv");
        $('#iJyouNaiYouGenJyou_3').prop('disabled', true);

    }


    // -------------------------------------------------------------------
    if (value == 9) {

        // $('#teiShiYouInTouRoku').addClass("disabledButton");			
        // $('#teiShiYouInTouRoku').prop('disabled', true);
        $('#iJyouNaiYouGenJyou_3').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_3').selectedIndex = 0;
        $("#teishijikan_kubun3").html("不稼働");

        $("#tsuika_button3").addClass("disabledButton")
        $("#tsuika_button3").attr('disabled', 'disabled');

        // document.getElementById('selectBox1').selectedIndex = 0;

    }

    if (value == 10) {

        // $('#teiShiYouInTouRoku').addClass("disabledButton");
        // $('#teiShiYouInTouRoku').prop('disabled', true);
        $('#iJyouNaiYouGenJyou_3').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_3').selectedIndex = 0;
        $("#teishijikan_kubun_3").html("不稼働");


        $("#tsuika_button3").addClass("disabledButton")
        $("#tsuika_button3").attr('disabled', 'disabled');
        // document.getElementById('selectBox1').selectedIndex = 0;


    } else {

    	$("#teishikoumoku_sonota_textbox_3").addClass("hidden");
    	
    }

    if (value == 11) {

        // $('#teiShiYouInTouRoku').addClass("disabledButton");
        // $('#teiShiYouInTouRoku').prop('disabled', true);
        $('#iJyouNaiYouGenJyou_3').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_2').selectedIndex = 0;
        $("#teishijikan_kubun_3").html("不稼働");


        $("#tsuika_button3").addClass("disabledButton")
        $("#tsuika_button3").attr('disabled', 'disabled');
        // document.getElementById('selectBox1').selectedIndex = 0;
    }

    if (value >= 9 && value <= 11) {

        $('#iJyouNaiYouGenJyou_3').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_3').selectedIndex = 0;

        $("#teishijikan_kubun_3").html("不稼働");
        $("#tsuika_button3").addClass("disabledButton")
        $("#tsuika_button3").attr('disabled', 'disabled');

        $("#iJyouNaiYouGenJyou_sonota_textbox_3").addClass("hidden");
    } else {

        // $("#teishijikan_kubun3").html("");
        $("#tsuika_button3").removeClass("disabledButton")
        $("#tsuika_button3").prop('disabled', false);


		if (temp_miwariate_seconds >= 300) {
			$("#teishijikan_kubun_3").html("長期停止");
		} else {
			$("#teishijikan_kubun_3").html("チョコ停");
		}

    }
    // -------------------------------------------------------------------


    // getSelectBox1(value)

}


$(document).ready(function () {

    $("#aitemu_teishikoumoku_3").addClass("disabledButton");
    $("#aitemu_teishikoumoku_3").prop('disabled', true);
    $("#teishikoumoku_sonota_textbox_3").addClass("hidden");
    $("#iJyouNaiYouGenJyou_sonota_textbox_3").addClass("hidden");

});


function changeSelectBox1_3(value) {

    $("#aitemu_teishikoumoku_3").addClass("disabledButton");
    $("#aitemu_teishikoumoku_3").prop('disabled', true);

    if (value == 38) {

        // $('#teiShiYouInTouRoku').prop('disabled', false);
        // $('#teiShiYouInTouRoku').removeClass("disabledButton");			


        $("#aitemu_teishikoumoku_3").removeClass("disabledButton");
        $("#aitemu_teishikoumoku_3").prop('disabled', false);

    }

    if (value >= 27) {
        // $('#teiShiYouInTouRoku').addClass("disabledButton");
        // $('#teiShiYouInTouRoku').prop('disabled', true);
        // $('#teiShiYouInTouRoku').addClass("disabledButton");
        $('#iJyouNaiYouGenJyou_3').prop('disabled', true);


    }

    // if (value == 11) {
    // 	// $('#teiShiYouInTouRoku').addClass("disabledButton");
    // 	// $('#teiShiYouInTouRoku').prop('disabled', true);
    // 	$('#iJyouNaiYouGenJyou').prop('disabled', true);
    // }
    if (value == 43) {

    	$("#teishikoumoku_sonota_textbox_3").removeClass("hidden");
    	
    } else {

    	$("#teishikoumoku_sonota_textbox_3").addClass("hidden");

    }

}



function changeSelectBox3_3(value) {

    if (value == 8) {

        // $("#iJyouNaiYouGenJyou_sonota_textbox").toggleClass("show");
        $("#iJyouNaiYouGenJyou_sonota_textbox_3").removeClass("hidden");

    } else {

        $("#iJyouNaiYouGenJyou_sonota_textbox_3").addClass("hidden");
        // $("#iJyouNaiYouGenJyou_sonota_textbox").hide();
    }




}

// --------------------------------------------------------------------

///checkBox-------------------Div_3.4------------------------------------

$('#iJyouNaiYouGenJyou_4').prop('disabled', true);

function changeSelectBox2_4(value) {

	document.getElementById('iJyouNaiYouGenJyou_4').selectedIndex = 0;
	$("#iJyouNaiYouGenJyou_sonota_textbox_4").addClass("hidden");


    var temp_inside_teishijikan_kubun_4 = $("#teishijikan_kubun_4").text();
    var miwariate_jikan_atai_4 = $("#miwariate_jikan_atai_4").text();


    var temp_miwariate = miwariate_jikan_atai_4.split(':');
    var temp_miwariate_seconds = (temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;


    document.getElementById('selectBox1_4').selectedIndex = 0;
    $('#iJyouNaiYouGenJyou_4').prop('disabled', false);

    $('.optionNHTTOAN').addClass("displayDiv");
    $('.optionDTMH' + value).removeClass("displayDiv");

    if (value == 0) {
        document.getElementById('iJyouNaiYouGenJyou_4').selectedIndex = 0;
        document.getElementById('selectBox1_4').selectedIndex = 0;
        // $("#selectBox3").value('0');


        // $("#selectBox3").val(0);

        // var value3 = document.getElementById('selectBox3').value;

        // alert(value3);

        $('.optionNHTTOAN').addClass("displayDiv");
        $('#iJyouNaiYouGenJyou_4').prop('disabled', true);


        // value3 = 0;
    }


    // -------------------------------------------------------------------
    if (value == 9) {

        // $('#teiShiYouInTouRoku').addClass("disabledButton");			
        // $('#teiShiYouInTouRoku').prop('disabled', true);
        $('#iJyouNaiYouGenJyou_4').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_4').selectedIndex = 0;
        $("#teishijikan_kubun_4").html("不稼働");

        $("#tsuika_button4").addClass("disabledButton")
        $("#tsuika_button4").attr('disabled', 'disabled');

        // document.getElementById('selectBox1').selectedIndex = 0;

    }

    if (value == 10) {

        // $('#teiShiYouInTouRoku').addClass("disabledButton");
        // $('#teiShiYouInTouRoku').prop('disabled', true);
        $('#iJyouNaiYouGenJyou_4').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_4').selectedIndex = 0;
        $("#teishijikan_kubun_4").html("不稼働");


        $("#tsuika_button4").addClass("disabledButton")
        $("#tsuika_button4").attr('disabled', 'disabled');
        // document.getElementById('selectBox1').selectedIndex = 0;

    } else {

    	$("#teishikoumoku_sonota_textbox_4").addClass("hidden");
    	
    }

    if (value == 11) {

        // $('#teiShiYouInTouRoku').addClass("disabledButton");
        // $('#teiShiYouInTouRoku').prop('disabled', true);
        $('#iJyouNaiYouGenJyou_4').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_4').selectedIndex = 0;
        $("#teishijikan_kubun_4").html("不稼働");


        $("#tsuika_button4").addClass("disabledButton")
        $("#tsuika_button4").attr('disabled', 'disabled');
        // document.getElementById('selectBox1').selectedIndex = 0;
    }

    if (value >= 9 && value <= 11) {

        $('#iJyouNaiYouGenJyou_4').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_4').selectedIndex = 0;

        // $("#teishijikan_kubun4").html("区分　：　不稼働");
        $("#tsuika_button4").addClass("disabledButton")
        $("#tsuika_button4").attr('disabled', 'disabled');

        $("#iJyouNaiYouGenJyou_sonota_textbox_4").addClass("hidden");


    } else {

        // $("#teishijikan_kubun4").html("");
        $("#tsuika_button4").removeClass("disabledButton")
        $("#tsuika_button4").prop('disabled', false);

		if (temp_miwariate_seconds >= 300) {
			$("#teishijikan_kubun_4").html("長期停止");
		} else {
			$("#teishijikan_kubun_4").html("チョコ停");
		}        


    }
    // -------------------------------------------------------------------

    // getSelectBox1(value)

}


$(document).ready(function () {

    $("#aitemu_teishikoumoku_4").addClass("disabledButton");
    $("#aitemu_teishikoumoku_4").prop('disabled', true);
    $("#teishikoumoku_sonota_textbox_4").addClass("hidden");
    $("#iJyouNaiYouGenJyou_sonota_textbox_4").addClass("hidden");

});


function changeSelectBox1_4(value) {

    $("#aitemu_teishikoumoku_4").addClass("disabledButton");
    $("#aitemu_teishikoumoku_4").prop('disabled', true);

    if (value == 38) {

        // $('#teiShiYouInTouRoku').prop('disabled', false);
        // $('#teiShiYouInTouRoku').removeClass("disabledButton");


        $("#aitemu_teishikoumoku_4").removeClass("disabledButton");
        $("#aitemu_teishikoumoku_4").prop('disabled', false);

    }

    if (value >= 27) {
        // $('#teiShiYouInTouRoku').addClass("disabledButton");
        // $('#teiShiYouInTouRoku').prop('disabled', true);
        // $('#teiShiYouInTouRoku').addClass("disabledButton");
        $('#iJyouNaiYouGenJyou_4').prop('disabled', true);


    }

    // if (value == 11) {
    // 	// $('#teiShiYouInTouRoku').addClass("disabledButton");
    // 	// $('#teiShiYouInTouRoku').prop('disabled', true);
    // 	$('#iJyouNaiYouGenJyou').prop('disabled', true);
    // }
    if (value == 43) {

    	$("#teishikoumoku_sonota_textbox_4").removeClass("hidden");
    	
    } else {

    	$("#teishikoumoku_sonota_textbox_4").addClass("hidden");

    }

}



function changeSelectBox3_4(value) {

    if (value == 8) {

        // $("#iJyouNaiYouGenJyou_sonota_textbox").toggleClass("show");
        $("#iJyouNaiYouGenJyou_sonota_textbox_4").removeClass("hidden");

    } else {

        $("#iJyouNaiYouGenJyou_sonota_textbox_4").addClass("hidden");
        // $("#iJyouNaiYouGenJyou_sonota_textbox").hide();

    }



}

// --------------------------------------------------------------------

///checkBox-------------------Div_3.5------------------------------------

$('#iJyouNaiYouGenJyou_5').prop('disabled', true);

function changeSelectBox2_5(value) {

	document.getElementById('iJyouNaiYouGenJyou_5').selectedIndex = 0;
	$("#iJyouNaiYouGenJyou_sonota_textbox_5").addClass("hidden");


    var temp_inside_teishijikan_kubun_5 = $("#teishijikan_kubun_5").text();
    var miwariate_jikan_atai_5 = $("#miwariate_jikan_atai_5").text();

    var temp_miwariate = miwariate_jikan_atai_5.split(':');
    var temp_miwariate_seconds = (temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    document.getElementById('selectBox1_5').selectedIndex = 0;
    $('#iJyouNaiYouGenJyou_5').prop('disabled', false);

    $('.optionNHTTOAN').addClass("displayDiv");
    $('.optionDTMH' + value).removeClass("displayDiv");

    if (value == 0) {
        document.getElementById('iJyouNaiYouGenJyou_5').selectedIndex = 0;
        document.getElementById('selectBox1_5').selectedIndex = 0;

        $('.optionNHTTOAN').addClass("displayDiv");
        $('#iJyouNaiYouGenJyou_5').prop('disabled', true);

    }


    // -------------------------------------------------------------------
    if (value == 9) {

        $('#iJyouNaiYouGenJyou_5').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_5').selectedIndex = 0;
        $("#teishijikan_kubun_5").html("不稼働");

        $("#tsuika_button5").addClass("disabledButton")
        $("#tsuika_button5").attr('disabled', 'disabled');

    }

    if (value == 10) {

        $('#iJyouNaiYouGenJyou_5').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_5').selectedIndex = 0;
        $("#teishijikan_kubun_5").html("不稼働");


        $("#tsuika_button5").addClass("disabledButton")
        $("#tsuika_button5").attr('disabled', 'disabled');
        // document.getElementById('selectBox1').selectedIndex = 0;


    } else {

    	$("#teishikoumoku_sonota_textbox_5").addClass("hidden");
    	
    }

    if (value == 11) {

        $('#iJyouNaiYouGenJyou_5').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_5').selectedIndex = 0;
        $("#teishijikan_kubun_5").html("不稼働");


        $("#tsuika_button5").addClass("disabledButton")
        $("#tsuika_button5").attr('disabled', 'disabled');
        // document.getElementById('selectBox1').selectedIndex = 0;
    }

    if (value >= 9 && value <= 11) {

        $('#iJyouNaiYouGenJyou_5').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_5').selectedIndex = 0;

        $("#teishijikan_kubun_5").html("不稼働");
        $("#tsuika_button5").addClass("disabledButton")
        $("#tsuika_button5").attr('disabled', 'disabled');

        $("#iJyouNaiYouGenJyou_sonota_textbox_5").addClass("hidden");

    } else {

        $("#tsuika_button5").removeClass("disabledButton")
        $("#tsuika_button5").prop('disabled', false);

		if (temp_miwariate_seconds >= 300) {
			$("#teishijikan_kubun_5").html("長期停止");
		} else {
			$("#teishijikan_kubun_5").html("チョコ停");
		}
    }
    // -------------------------------------------------------------------

    // getSelectBox1(value)

}


$(document).ready(function () {
    //
    // $("#iJyouNaiYouGenJyou_sonota_textbox").css("display", "none");
    //
    $("#aitemu_teishikoumoku_5").addClass("disabledButton");
    $("#aitemu_teishikoumoku_5").prop('disabled', true);
    $("#teishikoumoku_sonota_textbox_5").addClass("hidden");
    $("#iJyouNaiYouGenJyou_sonota_textbox_5").addClass("hidden");

});


function changeSelectBox1_5(value) {

    $("#aitemu_teishikoumoku_5").addClass("disabledButton");
    $("#aitemu_teishikoumoku_5").prop('disabled', true);

    if (value == 38) {
        $("#aitemu_teishikoumoku_5").removeClass("disabledButton");
        $("#aitemu_teishikoumoku_5").prop('disabled', false);

    }

    if (value >= 27) {

        $('#iJyouNaiYouGenJyou_5').prop('disabled', true);

    }

    if (value == 43) {

    	$("#teishikoumoku_sonota_textbox_5").removeClass("hidden");
    	
    } else {

    	$("#teishikoumoku_sonota_textbox_5").addClass("hidden");

    }    

}


function changeSelectBox3_5(value) {

    if (value == 8) {

        // $("#iJyouNaiYouGenJyou_sonota_textbox").toggleClass("show");
        $("#iJyouNaiYouGenJyou_sonota_textbox_5").removeClass("hidden");

    } else {

        $("#iJyouNaiYouGenJyou_sonota_textbox_5").addClass("hidden");
        // $("#iJyouNaiYouGenJyou_sonota_textbox").hide();
    }

  
}

// --------------------------------------------------------------------

///checkBox-------------------Div_3.6------------------------------------

$('#iJyouNaiYouGenJyou_6').prop('disabled', true);

function changeSelectBox2_6(value) {

	document.getElementById('iJyouNaiYouGenJyou_6').selectedIndex = 0;
	$("#iJyouNaiYouGenJyou_sonota_textbox_6").addClass("hidden");


    var temp_inside_teishijikan_kubun_6 = $("#teishijikan_kubun_6").text();
    var miwariate_jikan_atai_6 = $("#miwariate_jikan_atai_6").text();

    var temp_miwariate = miwariate_jikan_atai_6.split(':');
    var temp_miwariate_seconds = (temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    document.getElementById('selectBox1_6').selectedIndex = 0;
    $('#iJyouNaiYouGenJyou_6').prop('disabled', false);

    $('.optionNHTTOAN').addClass("displayDiv");
    $('.optionDTMH' + value).removeClass("displayDiv");

    if (value == 0) {
        document.getElementById('iJyouNaiYouGenJyou_6').selectedIndex = 0;
        document.getElementById('selectBox1_6').selectedIndex = 0;

        $('.optionNHTTOAN').addClass("displayDiv");
        $('#iJyouNaiYouGenJyou_6').prop('disabled', true);

    }


    // -------------------------------------------------------------------
    if (value == 9) {


        $('#iJyouNaiYouGenJyou_6').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_6').selectedIndex = 0;

        $("#tsuika_button6").addClass("disabledButton")
        $("#tsuika_button6").attr('disabled', 'disabled');

    }

    if (value == 10) {

        $('#iJyouNaiYouGenJyou_6').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_6').selectedIndex = 0;
        $("#teishijikan_kubun_6").html("不稼働");


        $("#tsuika_button6").addClass("disabledButton")
        $("#tsuika_button6").attr('disabled', 'disabled');



    } else {

    	$("#teishikoumoku_sonota_textbox_6").addClass("hidden");
    	
    }

    if (value == 11) {


        $('#iJyouNaiYouGenJyou_6').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_6').selectedIndex = 0;
        $("#teishijikan_kubun_6").html("不稼働");


        $("#tsuika_button6").addClass("disabledButton")
        $("#tsuika_button6").attr('disabled', 'disabled');

    }

    if (value >= 9 && value <= 11) {

        $('#iJyouNaiYouGenJyou_6').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_6').selectedIndex = 0;

        $("#teishijikan_kubun_6").html("不稼働");
        $("#tsuika_button6").addClass("disabledButton")
        $("#tsuika_button6").attr('disabled', 'disabled');

        $("#iJyouNaiYouGenJyou_sonota_textbox_6").addClass("hidden");

    } else {

        $("#tsuika_button6").removeClass("disabledButton")
        $("#tsuika_button6").prop('disabled', false);

		if (temp_miwariate_seconds >= 300) {
			$("#teishijikan_kubun_6").html("長期停止");
		} else {
			$("#teishijikan_kubun_6").html("チョコ停");
		}

    }
    // -------------------------------------------------------------------

    // getSelectBox1(value)

}

$(document).ready(function () {

    $("#aitemu_teishikoumoku_6").addClass("disabledButton");
    $("#aitemu_teishikoumoku_6").prop('disabled', true);
    $("#teishikoumoku_sonota_textbox_6").addClass("hidden");
    $("#iJyouNaiYouGenJyou_sonota_textbox_6").addClass("hidden");
});



function changeSelectBox1_6(value) {

    $("#aitemu_teishikoumoku_6").addClass("disabledButton");
    $("#aitemu_teishikoumoku_6").prop('disabled', true);

    if (value == 38) {

        // $('#teiShiYouInTouRoku').prop('disabled', false);
        // $('#teiShiYouInTouRoku').removeClass("disabledButton");			


        $("#aitemu_teishikoumoku_6").removeClass("disabledButton");
        $("#aitemu_teishikoumoku_6").prop('disabled', false);

    }

    if (value >= 27) {
        // $('#teiShiYouInTouRoku').addClass("disabledButton");
        // $('#teiShiYouInTouRoku').prop('disabled', true);
        // $('#teiShiYouInTouRoku').addClass("disabledButton");
        $('#iJyouNaiYouGenJyou_6').prop('disabled', true);


    }

    // if (value == 11) {
    // 	// $('#teiShiYouInTouRoku').addClass("disabledButton");
    // 	// $('#teiShiYouInTouRoku').prop('disabled', true);
    // 	$('#iJyouNaiYouGenJyou').prop('disabled', true);
    // }
    if (value == 43) {

    	$("#teishikoumoku_sonota_textbox_6").removeClass("hidden");
    	
    } else {

    	$("#teishikoumoku_sonota_textbox_6").addClass("hidden");

    }

}



function changeSelectBox3_6(value) {

    if (value == 8) {

        // $("#iJyouNaiYouGenJyou_sonota_textbox").toggleClass("show");
        $("#iJyouNaiYouGenJyou_sonota_textbox_6").removeClass("hidden");

    } else {

        $("#iJyouNaiYouGenJyou_sonota_textbox_6").addClass("hidden");
        // $("#iJyouNaiYouGenJyou_sonota_textbox").hide();
    }



}

// --------------------------------------------------------------------

///checkBox-------------------Div_3.7------------------------------------

$('#iJyouNaiYouGenJyou_7').prop('disabled', true);

function changeSelectBox2_7(value) {

    var temp_inside_teishijikan_kubun_7 = $("#teishijikan_kubun_7").text();
    var miwariate_jikan_atai_7 = $("#miwariate_jikan_atai_7").text();

	$("#iJyouNaiYouGenJyou_sonota_textbox_7").addClass("hidden");


    var temp_miwariate = miwariate_jikan_atai_7.split(':');
    var temp_miwariate_seconds = (temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    document.getElementById('selectBox1_7').selectedIndex = 0;
    $('#iJyouNaiYouGenJyou_7').prop('disabled', false);

    $('.optionNHTTOAN').addClass("displayDiv");
    $('.optionDTMH' + value).removeClass("displayDiv");

    if (value == 0) {
        document.getElementById('iJyouNaiYouGenJyou_7').selectedIndex = 0;
        document.getElementById('selectBox1_7').selectedIndex = 0;
        // $("#selectBox3").value('0');


        // $("#selectBox3").val(0);

        // var value3 = document.getElementById('selectBox3').value;

        // alert(value3);

        $('.optionNHTTOAN').addClass("displayDiv");
        $('#iJyouNaiYouGenJyou_7').prop('disabled', true);


        // value3 = 0;
    }


    // -------------------------------------------------------------------
    if (value == 9) {

        // $('#teiShiYouInTouRoku').addClass("disabledButton");			
        // $('#teiShiYouInTouRoku').prop('disabled', true);
        $('#iJyouNaiYouGenJyou_7').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_7').selectedIndex = 0;
        $("#teishijikan_kubun_7").html("不稼働");

        $("#tsuika_button7").addClass("disabledButton")
        $("#tsuika_button7").attr('disabled', 'disabled');

        // document.getElementById('selectBox1').selectedIndex = 0;

    }

    if (value == 10) {

        // $('#teiShiYouInTouRoku').addClass("disabledButton");
        // $('#teiShiYouInTouRoku').prop('disabled', true);
        $('#iJyouNaiYouGenJyou_7').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_7').selectedIndex = 0;
        $("#teishijikan_kubun_7").html("不稼働");


        $("#tsuika_button7").addClass("disabledButton")
        $("#tsuika_button7").attr('disabled', 'disabled');
        // document.getElementById('selectBox1').selectedIndex = 0;


    } else {

    	$("#teishikoumoku_sonota_textbox_7").addClass("hidden");
    	
    }

    if (value == 11) {

        // $('#teiShiYouInTouRoku').addClass("disabledButton");
        // $('#teiShiYouInTouRoku').prop('disabled', true);
        $('#iJyouNaiYouGenJyou_7').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_7').selectedIndex = 0;
        $("#teishijikan_kubun_7").html("不稼働");


        $("#tsuika_button7").addClass("disabledButton")
        $("#tsuika_button7").attr('disabled', 'disabled');
        // document.getElementById('selectBox1').selectedIndex = 0;
    }

    if (value >= 9 && value <= 11) {

        $('#iJyouNaiYouGenJyou_7').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_7').selectedIndex = 0;

        $("#teishijikan_kubun_7").html("不稼働");
        $("#tsuika_button7").addClass("disabledButton")
        $("#tsuika_button7").attr('disabled', 'disabled');


    } else {

        // $("#teishijikan_kubun7").html("");
        $("#tsuika_button7").removeClass("disabledButton")
        $("#tsuika_button7").prop('disabled', false);

		if (temp_miwariate_seconds >= 300) {
			$("#teishijikan_kubun_7").html("長期停止");
		} else {
			$("#teishijikan_kubun_7").html("チョコ停");
		}

    }
    // -------------------------------------------------------------------


    // var value3 = document.getElementById('selectBox3').value;
    // // var value2 = document.getElementById('selectBox2').value;
    // alert(value3);

    // getSelectBox1(value)

}


$(document).ready(function () {

    $("#aitemu_teishikoumoku_7").addClass("disabledButton");
    $("#aitemu_teishikoumoku_7").prop('disabled', true);
    $("#teishikoumoku_sonota_textbox_7").addClass("hidden");
    $("#iJyouNaiYouGenJyou_sonota_textbox_7").addClass("hidden");
});


function changeSelectBox1_7(value) {

    $("#aitemu_teishikoumoku_7").addClass("disabledButton");
    $("#aitemu_teishikoumoku_7").prop('disabled', true);

    if (value == 38) {

        // $('#teiShiYouInTouRoku').prop('disabled', false);
        // $('#teiShiYouInTouRoku').removeClass("disabledButton");			

        $("#aitemu_teishikoumoku_7").removeClass("disabledButton");
        $("#aitemu_teishikoumoku_7").prop('disabled', false);

    }

    if (value >= 27) {
        $('#iJyouNaiYouGenJyou_7').prop('disabled', true);

    }

    if (value == 43) {

    	$("#teishikoumoku_sonota_textbox_7").removeClass("hidden");
    	
    } else {

    	$("#teishikoumoku_sonota_textbox_7").addClass("hidden");

    }     

}



function changeSelectBox3_7(value) {

    if (value == 8) {

        // $("#iJyouNaiYouGenJyou_sonota_textbox").toggleClass("show");
        $("#iJyouNaiYouGenJyou_sonota_textbox_7").removeClass("hidden");

    } else {

        $("#iJyouNaiYouGenJyou_sonota_textbox_7").addClass("hidden");
        // $("#iJyouNaiYouGenJyou_sonota_textbox").hide();
    }

   
}

// --------------------------------------------------------------------

///checkBox-------------------Div_3.8------------------------------------

$('#iJyouNaiYouGenJyou_8').prop('disabled', true);

function changeSelectBox2_8(value) {

	document.getElementById('iJyouNaiYouGenJyou_8').selectedIndex = 0;
	$("#iJyouNaiYouGenJyou_sonota_textbox_8").addClass("hidden");


    var temp_inside_teishijikan_kubun_8 = $("#teishijikan_kubun_8").text();
    var miwariate_jikan_atai_8 = $("#miwariate_jikan_atai_8").text();


    var temp_miwariate = miwariate_jikan_atai_8.split(':');
    var temp_miwariate_seconds = (temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;


    document.getElementById('selectBox1_8').selectedIndex = 0;
    $('#iJyouNaiYouGenJyou_8').prop('disabled', false);

    $('.optionNHTTOAN').addClass("displayDiv");
    $('.optionDTMH' + value).removeClass("displayDiv");

    if (value == 0) {
        document.getElementById('iJyouNaiYouGenJyou_8').selectedIndex = 0;

        $('.optionNHTTOAN').addClass("displayDiv");
        $('#iJyouNaiYouGenJyou_8').prop('disabled', true);

    }

    // -------------------------------------------------------------------
    if (value == 9) {

        $('#iJyouNaiYouGenJyou_8').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_8').selectedIndex = 0;

        $("#tsuika_button8").addClass("disabledButton")
        $("#tsuika_button8").attr('disabled', 'disabled');

    }

    if (value == 10) {

        $('#iJyouNaiYouGenJyou_8').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_8').selectedIndex = 0;
        $("#teishijikan_kubun_8").html("不稼働");

        $("#tsuika_button8").addClass("disabledButton")
        $("#tsuika_button8").attr('disabled', 'disabled');
        // document.getElementById('selectBox1').selectedIndex = 0;

    } else {

    	$("#teishikoumoku_sonota_textbox_8").addClass("hidden");
    	
    }

    if (value == 11) {

        // $('#teiShiYouInTouRoku').addClass("disabledButton");
        // $('#teiShiYouInTouRoku').prop('disabled', true);
        $('#iJyouNaiYouGenJyou_8').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_8').selectedIndex = 0;
        $("#teishijikan_kubun_8").html("不稼働");

        $("#tsuika_button8").addClass("disabledButton")
        $("#tsuika_button8").attr('disabled', 'disabled');
        // document.getElementById('selectBox1').selectedIndex = 0;
    }

    if (value >= 9 && value <= 11) {

        $('#iJyouNaiYouGenJyou_8').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_8').selectedIndex = 0;

        $("#teishijikan_kubun_8").html("不稼働");
        $("#tsuika_button8").addClass("disabledButton")
        $("#tsuika_button8").attr('disabled', 'disabled');

        $("#iJyouNaiYouGenJyou_sonota_textbox_8").addClass("hidden");

    } else {

        // $("#teishijikan_kubun8").html("");
        $("#tsuika_button8").removeClass("disabledButton")
        $("#tsuika_button8").prop('disabled', false);

		if (temp_miwariate_seconds >= 300) {
			$("#teishijikan_kubun_8").html("長期停止");
		} else {
			$("#teishijikan_kubun_8").html("チョコ停");
		}

    }

    // getSelectBox1(value)

}


$(document).ready(function () {

    $("#aitemu_teishikoumoku_8").addClass("disabledButton");
    $("#aitemu_teishikoumoku_8").prop('disabled', true);
    $("#teishikoumoku_sonota_textbox_8").addClass("hidden");
    $("#iJyouNaiYouGenJyou_sonota_textbox_8").addClass("hidden");

});


function changeSelectBox1_8(value) {

    $("#aitemu_teishikoumoku_8").addClass("disabledButton");
    $("#aitemu_teishikoumoku_8").prop('disabled', true);

    if (value == 38) {

        // $('#teiShiYouInTouRoku').prop('disabled', false);
        // $('#teiShiYouInTouRoku').removeClass("disabledButton");			

        $("#aitemu_teishikoumoku_8").removeClass("disabledButton");
        $("#aitemu_teishikoumoku_8").prop('disabled', false);

    }

    if (value >= 27) {
        // $('#teiShiYouInTouRoku').addClass("disabledButton");
        // $('#teiShiYouInTouRoku').prop('disabled', true);
        // $('#teiShiYouInTouRoku').addClass("disabledButton");
        $('#iJyouNaiYouGenJyou_8').prop('disabled', true);

    }

    if (value == 43) {

    	$("#teishikoumoku_sonota_textbox_8").removeClass("hidden");
    	
    } else {

    	$("#teishikoumoku_sonota_textbox_8").addClass("hidden");

    }    

}


function changeSelectBox3_8(value) {

    if (value == 8) {

        // $("#iJyouNaiYouGenJyou_sonota_textbox").toggleClass("show");
        $("#iJyouNaiYouGenJyou_sonota_textbox_8").removeClass("hidden");

    } else {

        $("#iJyouNaiYouGenJyou_sonota_textbox_8").addClass("hidden");
        // $("#iJyouNaiYouGenJyou_sonota_textbox").hide();
    }



}


// --------------------------------------------------------------------

///checkBox-------------------Div_3.9------------------------------------

$('#iJyouNaiYouGenJyou_9').prop('disabled', true);

function changeSelectBox2_9(value) {

	document.getElementById('iJyouNaiYouGenJyou_9').selectedIndex = 0;
	$("#iJyouNaiYouGenJyou_sonota_textbox_9").addClass("hidden");



    var temp_inside_teishijikan_kubun_9 = $("#teishijikan_kubun_9").text();
    var miwariate_jikan_atai_9= $("#miwariate_jikan_atai_9").text();


    var temp_miwariate = miwariate_jikan_atai_9.split(':');
    var temp_miwariate_seconds = (temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    document.getElementById('selectBox1_9').selectedIndex = 0;
    $('#iJyouNaiYouGenJyou_9').prop('disabled', false);

    $('.optionNHTTOAN').addClass("displayDiv");
    $('.optionDTMH' + value).removeClass("displayDiv");

    if (value == 0) {
        document.getElementById('iJyouNaiYouGenJyou_9').selectedIndex = 0;
        document.getElementById('selectBox1_9').selectedIndex = 0;
        // $("#selectBox3").value('0');


        // $("#selectBox3").val(0);

        // var value3 = document.getElementById('selectBox3').value;

        // alert(value3);

        $('.optionNHTTOAN').addClass("displayDiv");
        $('#iJyouNaiYouGenJyou_9').prop('disabled', true);


        // value3 = 0;
    }


    // -------------------------------------------------------------------
    if (value == 9) {

        // $('#teiShiYouInTouRoku').addClass("disabledButton");			
        // $('#teiShiYouInTouRoku').prop('disabled', true);
        $('#iJyouNaiYouGenJyou_9').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_9').selectedIndex = 0;
        $("#teishijikan_kubun_9").html("不稼働");

        $("#tsuika_button9").addClass("disabledButton")
        $("#tsuika_button9").attr('disabled', 'disabled');

        // document.getElementById('selectBox1').selectedIndex = 0;

    }

    if (value == 10) {

        // $('#teiShiYouInTouRoku').addClass("disabledButton");
        // $('#teiShiYouInTouRoku').prop('disabled', true);
        $('#iJyouNaiYouGenJyou_9').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_9').selectedIndex = 0;
        $("#teishijikan_kubun_9").html("不稼働");


        $("#tsuika_button9").addClass("disabledButton")
        $("#tsuika_button9").attr('disabled', 'disabled');
        // document.getElementById('selectBox1').selectedIndex = 0;

    } else {

    	$("#teishikoumoku_sonota_textbox_9").addClass("hidden");
    	
    }

    if (value == 11) {

        // $('#teiShiYouInTouRoku').addClass("disabledButton");
        // $('#teiShiYouInTouRoku').prop('disabled', true);
        $('#iJyouNaiYouGenJyou_9').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_9').selectedIndex = 0;
        $("#teishijikan_kubun_9").html("不稼働");


        $("#tsuika_button9").addClass("disabledButton")
        $("#tsuika_button9").attr('disabled', 'disabled');
        // document.getElementById('selectBox1').selectedIndex = 0;
    }

    if (value >= 9 && value <= 11) {

        $('#iJyouNaiYouGenJyou_9').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_9').selectedIndex = 0;

        $("#teishijikan_kubun_9").html("不稼働");
        $("#tsuika_button9").addClass("disabledButton")
        $("#tsuika_button9").attr('disabled', 'disabled');


    } else {

        // $("#teishijikan_kubun9").html("");
        $("#tsuika_button9").removeClass("disabledButton")
        $("#tsuika_button9").prop('disabled', false);

		if (temp_miwariate_seconds >= 300) {
			$("#teishijikan_kubun_9").html("長期停止");
		} else {
			$("#teishijikan_kubun_9").html("チョコ停");
		}

    }

    // getSelectBox1(value)

}

$(document).ready(function () {

    $("#aitemu_teishikoumoku_9").addClass("disabledButton");
    $("#aitemu_teishikoumoku_9").prop('disabled', true);
    $("#teishikoumoku_sonota_textbox_9").addClass("hidden");
    $("#iJyouNaiYouGenJyou_sonota_textbox_9").addClass("hidden");
});



function changeSelectBox1_9(value) {

    $("#aitemu_teishikoumoku_9").addClass("disabledButton");
    $("#aitemu_teishikoumoku_9").prop('disabled', true);

    if (value == 38) {

        // $('#teiShiYouInTouRoku').prop('disabled', false);
        // $('#teiShiYouInTouRoku').removeClass("disabledButton");			


        $("#aitemu_teishikoumoku_9").removeClass("disabledButton");
        $("#aitemu_teishikoumoku_9").prop('disabled', false);

    }

    if (value >= 27) {
        // $('#teiShiYouInTouRoku').addClass("disabledButton");
        // $('#teiShiYouInTouRoku').prop('disabled', true);
        // $('#teiShiYouInTouRoku').addClass("disabledButton");
        $('#iJyouNaiYouGenJyou_9').prop('disabled', true);

    }

    if (value == 43) {

    	$("#teishikoumoku_sonota_textbox_9").removeClass("hidden");
    	
    } else {

    	$("#teishikoumoku_sonota_textbox_9").addClass("hidden");

    }

}



function changeSelectBox3_9(value) {
	alert(value)

    if (value == 8) {

        // $("#iJyouNaiYouGenJyou_sonota_textbox").toggleClass("show");
        $("#iJyouNaiYouGenJyou_sonota_textbox_9").removeClass("hidden");

    } else {

        $("#iJyouNaiYouGenJyou_sonota_textbox_9").addClass("hidden");
        // $("#iJyouNaiYouGenJyou_sonota_textbox").hide();
    }


}


// --------------------------------------------------------------------

///checkBox-------------------Div_3.10------------------------------------

$('#iJyouNaiYouGenJyou_10').prop('disabled', true);

function changeSelectBox2_10(value) {

	document.getElementById('iJyouNaiYouGenJyou_10').selectedIndex = 0;
	$("#iJyouNaiYouGenJyou_sonota_textbox_10").addClass("hidden");



    var temp_inside_teishijikan_kubun_10 = $("#teishijikan_kubun_10").text();
    var miwariate_jikan_atai_10 = $("#miwariate_jikan_atai_10").text();


    var temp_miwariate = miwariate_jikan_atai_10.split(':');
    var temp_miwariate_seconds = (temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;


    document.getElementById('selectBox1_10').selectedIndex = 0;
    $('#iJyouNaiYouGenJyou_10').prop('disabled', false);

    $('.optionNHTTOAN').addClass("displayDiv");
    $('.optionDTMH' + value).removeClass("displayDiv");

    if (value == 0) {
        document.getElementById('iJyouNaiYouGenJyou_10').selectedIndex = 0;

        $('.optionNHTTOAN').addClass("displayDiv");
        $('#iJyouNaiYouGenJyou_10').prop('disabled', true);


        // value3 = 0;
    }

    // -------------------------------------------------------------------
    if (value == 9) {

        $('#iJyouNaiYouGenJyou_10').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_10').selectedIndex = 0;
        $("#teishijikan_kubun_10").html("不稼働");

        $("#tsuika_button10").addClass("disabledButton")
        $("#tsuika_button10").attr('disabled', 'disabled');

        // document.getElementById('selectBox1').selectedIndex = 0;

    }

    if (value == 10) {

        // $('#teiShiYouInTouRoku').addClass("disabledButton");
        // $('#teiShiYouInTouRoku').prop('disabled', true);
        $('#iJyouNaiYouGenJyou_10').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_10').selectedIndex = 0;
        $("#teishijikan_kubun_10").html("不稼働");


        $("#tsuika_button10").addClass("disabledButton")
        $("#tsuika_button10").attr('disabled', 'disabled');
        // document.getElementById('selectBox1').selectedIndex = 0;

    } else {

    	$("#teishikoumoku_sonota_textbox_10").addClass("hidden");
    	
    }

    if (value == 11) {

        // $('#teiShiYouInTouRoku').addClass("disabledButton");
        // $('#teiShiYouInTouRoku').prop('disabled', true);
        $('#iJyouNaiYouGenJyou_10').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_10').selectedIndex = 0;
        $("#teishijikan_kubun_10").html("不稼働");


        $("#tsuika_button10").addClass("disabledButton")
        $("#tsuika_button10").attr('disabled', 'disabled');
        // document.getElementById('selectBox1').selectedIndex = 0;
    }

    if (value >= 9 && value <= 11) {

        $('#iJyouNaiYouGenJyou_10').prop('disabled', true);
        document.getElementById('iJyouNaiYouGenJyou_10').selectedIndex = 0;

        $("#teishijikan_kubun_10").html("不稼働");
        $("#tsuika_button10").addClass("disabledButton")
        $("#tsuika_button10").attr('disabled', 'disabled');

        $("#iJyouNaiYouGenJyou_sonota_textbox_10").addClass("hidden");

    } else {

        // $("#teishijikan_kubun10").html("");
        $("#tsuika_button10").removeClass("disabledButton")
        $("#tsuika_button10").prop('disabled', false);


		if (temp_miwariate_seconds >= 300) {
			$("#teishijikan_kubun_10").html("長期停止");
		} else {
			$("#teishijikan_kubun_10").html("チョコ停");
		}

    }

    // getSelectBox1(value)

}


$(document).ready(function () {

    $("#aitemu_teishikoumoku_10").addClass("disabledButton");
    $("#aitemu_teishikoumoku_10").prop('disabled', true);
    $("#teishikoumoku_sonota_textbox_10").addClass("hidden");
    $("#iJyouNaiYouGenJyou_sonota_textbox_10").addClass("hidden");

});


function changeSelectBox1_10(value) {

    $("#aitemu_teishikoumoku_10").addClass("disabledButton");
    $("#aitemu_teishikoumoku_10").prop('disabled', true);

    if (value == 38) {

        $("#aitemu_teishikoumoku_10").removeClass("disabledButton");
        $("#aitemu_teishikoumoku_10").prop('disabled', false);

    }

    if (value >= 27) {

        $('#iJyouNaiYouGenJyou_10').prop('disabled', true);


    }

    if (value == 43) {

    	$("#teishikoumoku_sonota_textbox_10").removeClass("hidden");
    	
    } else {

    	$("#teishikoumoku_sonota_textbox_10").addClass("hidden");

    }    

}

function changeSelectBox3_10(value) {

    if (value == 8) {

        // $("#iJyouNaiYouGenJyou_sonota_textbox").toggleClass("show");
        $("#iJyouNaiYouGenJyou_sonota_textbox_10").removeClass("hidden");

    } else {

        $("#iJyouNaiYouGenJyou_sonota_textbox_10").addClass("hidden");
        // $("#iJyouNaiYouGenJyou_sonota_textbox").hide();
    }

    if (value == 43) {

    	$("#teishikoumoku_sonota_textbox_10").removeClass("hidden");
    	
    } else {

    	$("#teishikoumoku_sonota_textbox_10").addClass("hidden");

    }

}

//////////////////////////////////////theModal//////////////////////////////////////

// 戻るbutton-----------------------------------------------------------
// $(document).ready(function() {});
$(document).ready(function () {

    $("#teishitouroku_button").css("display", "block");
    $("#teishisakujyo_button").css("display", "none");
    $("#teishihenshuu_button").css("display", "none");

});


function directBack() {

	var x = document.documentURI;
	// alert(x);
	var m = x.replace("html/LineStopItemRelated.html",'');
	// alert(m);
	window.location.href = m + "html/LineStopItemOperationStatusSelection.html";	

    // window.location.href = "LineStopItemOperationStatusSelection.html";

}

// --------------------------------------------------------------------
// change time in label of Div1 to Div 10
var count1 = 0;
function changeTime() {

    // if (localStorage.getItem("checkbox") == "checked") {
    //-------------------------------------------------------------------------------

    var time1 = document.getElementById("inputBox_bunwarisettei1");
    var time2 = document.getElementById("inputBox_bunwarisettei2");
    var time3 = document.getElementById("inputBox_bunwarisettei3");
    var time4 = document.getElementById("inputBox_bunwarisettei4");
    var time5 = document.getElementById("inputBox_bunwarisettei5");
    var time6 = document.getElementById("inputBox_bunwarisettei6");
    var time7 = document.getElementById("inputBox_bunwarisettei7");
    var time8 = document.getElementById("inputBox_bunwarisettei8");
    var time9 = document.getElementById("inputBox_bunwarisettei9");
    var time10 = document.getElementById("inputBox_bunwarisettei10");

    var time_dtmhien_1 = $("#inputBox_bunwarisettei1").val();
    var time_dtmhien_2 = $("#inputBox_bunwarisettei2").val();
    var time_dtmhien_3 = $("#inputBox_bunwarisettei3").val();
    var time_dtmhien_4 = $("#inputBox_bunwarisettei4").val();
    var time_dtmhien_5 = $("#inputBox_bunwarisettei5").val();
    var time_dtmhien_6 = $("#inputBox_bunwarisettei6").val();
    var time_dtmhien_7 = $("#inputBox_bunwarisettei7").val();
    var time_dtmhien_8 = $("#inputBox_bunwarisettei8").val();
    var time_dtmhien_9 = $("#inputBox_bunwarisettei9").val();
    var time_dtmhien_10 = $("#inputBox_bunwarisettei10").val();

    if (time_dtmhien_1 == "") {
        time_dtmhien_1 = "00:00:00";
    }

    if (time_dtmhien_2 == "") {
        time_dtmhien_2 = "00:00:00";
    }

    if (time_dtmhien_3 == "") {
        time_dtmhien_3 = "00:00:00";
    }

    if (time_dtmhien_4 == "") {
        time_dtmhien_4 = "00:00:00";
    }

    if (time_dtmhien_5 == "") {
        time_dtmhien_5 = "00:00:00";
    }

    if (time_dtmhien_6 == "") {
        time_dtmhien_6 = "00:00:00";
    }

    if (time_dtmhien_7 == "") {
        time_dtmhien_7 = "00:00:00";
    }

    if (time_dtmhien_8 == "") {
        time_dtmhien_8 = "00:00:00";
    }

    if (time_dtmhien_9 == "") {
        time_dtmhien_9 = "00:00:00";
    }

    if (time_dtmhien_10 == "") {
        time_dtmhien_10 = "00:00:00";
    }
    //-------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------
    var bunwarisuu_number = $("#inputBox_bunwarisettei0").val();
    // alert(bunwarisuu_number);

    if (bunwarisuu_number == 1) {


        $("#miwariate_jikan_atai_1").html(time_dtmhien_1);
    }

    if (bunwarisuu_number == 2) {

        $("#miwariate_jikan_atai_2").html(time_dtmhien_2);

    }

    if (bunwarisuu_number == 3) {

        $("#miwariate_jikan_atai_3").html(time_dtmhien_3);

    }

    if (bunwarisuu_number == 4) {

        $("#miwariate_jikan_atai_4").html(time_dtmhien_4);

    }

    if (bunwarisuu_number == 5) {

        $("#miwariate_jikan_atai_5").html(time_dtmhien_5);

    }

    if (bunwarisuu_number == 6) {

        $("#miwariate_jikan_atai_6").html(time_dtmhien_6);
    }

    if (bunwarisuu_number == 7) {

        $("#miwariate_jikan_atai_7").html(time_dtmhien_7);
    }

    if (bunwarisuu_number == 8) {

        $("#miwariate_jikan_atai_8").html(time_dtmhien_8);
    }

    if (bunwarisuu_number == 9) {

        $("#miwariate_jikan_atai_9").html(time_dtmhien_9);
    }

    if (bunwarisuu_number == 10) {

        $("#miwariate_jikan_atai_10").html(time_dtmhien_10);
    }
    //-------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------
    var temp = $("#labelDefault_goukei").text();
    // var temp = $("#labelDefault_miwariate").text();
    var temp_miwariate = temp.split(':');
    var temp_miwariate_seconds = (temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;
    //-------------------------------------------------------------------------------
    //*******************************************************************************
    //-------------------------------------------------------------------------------

    let [hour1, min1, sec1] = (time1.value).split(':');
    if (min1 == undefined) {
        hour1 = "00";
        min1 = "00";

    }

    if (sec1 == undefined) {

        sec1 = "00";

    }

    var temp_inputBox_seconds1 = hour1 * 60 * 60 + min1 * 60 + sec1 * 1;

    minus = temp_miwariate_seconds - temp_inputBox_seconds1;

    // **************************************************************************************
    temp_miwariate_seconds_to_hour = Math.floor(minus / 3600);
    temp_miwariate_seconds_to_minute_temp = minus - temp_miwariate_seconds_to_hour * 60 * 60;
    temp_miwariate_seconds_to_minute = Math.floor(temp_miwariate_seconds_to_minute_temp / 60);

    temp_miwariate_seconds_to_second = Math.floor(temp_miwariate_seconds_to_minute_temp % 60);
    if (temp_miwariate_seconds_to_hour < 10) {
        temp_miwariate_seconds_to_hour = "0" + temp_miwariate_seconds_to_hour;
    }

    if (temp_miwariate_seconds_to_minute < 10) {
        temp_miwariate_seconds_to_minute = "0" + temp_miwariate_seconds_to_minute;
    }

    if (temp_miwariate_seconds_to_second < 10) {
        temp_miwariate_seconds_to_second = "0" + temp_miwariate_seconds_to_second;
    }

    var final_value = temp_miwariate_seconds_to_hour + ':' + temp_miwariate_seconds_to_minute + ':' + temp_miwariate_seconds_to_second;
    // **************************************************************************************


    $("#labelDefault_miwariate").text(final_value);


    //-------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------

    let [hour2, min2, sec2] = (time2.value).split(':');
    if (min2 == undefined) {
        hour2 = "00";
        min2 = "00";

    }

    if (sec2 == undefined) {

        sec2 = "00";

    }

    var temp_inputBox_seconds2 = hour2 * 60 * 60 + min2 * 60 + sec2 * 1;

    minus = temp_miwariate_seconds - temp_inputBox_seconds1 - temp_inputBox_seconds2;

    // **************************************************************************************
    temp_miwariate_seconds_to_hour = Math.floor(minus / 3600);
    temp_miwariate_seconds_to_minute_temp = minus - temp_miwariate_seconds_to_hour * 60 * 60;
    temp_miwariate_seconds_to_minute = Math.floor(temp_miwariate_seconds_to_minute_temp / 60);

    temp_miwariate_seconds_to_second = Math.floor(temp_miwariate_seconds_to_minute_temp % 60);
    if (temp_miwariate_seconds_to_hour < 10) {
        temp_miwariate_seconds_to_hour = "0" + temp_miwariate_seconds_to_hour;
    }

    if (temp_miwariate_seconds_to_minute < 10) {
        temp_miwariate_seconds_to_minute = "0" + temp_miwariate_seconds_to_minute;
    }

    if (temp_miwariate_seconds_to_second < 10) {
        temp_miwariate_seconds_to_second = "0" + temp_miwariate_seconds_to_second;
    }

    var final_value = temp_miwariate_seconds_to_hour + ':' + temp_miwariate_seconds_to_minute + ':' + temp_miwariate_seconds_to_second;
    // **************************************************************************************

    $("#labelDefault_miwariate").text(final_value);
    //-------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------

    let [hour3, min3, sec3] = (time3.value).split(':');
    if (min3 == undefined) {
        hour3 = "00";
        min3 = "00";

    }

    if (sec3 == undefined) {

        sec3 = "00";

    }

    var temp_inputBox_seconds3 = hour3 * 60 * 60 + min3 * 60 + sec3 * 1;

    minus = temp_miwariate_seconds - temp_inputBox_seconds1 - temp_inputBox_seconds2 - temp_inputBox_seconds3;


    // **************************************************************************************
    temp_miwariate_seconds_to_hour = Math.floor(minus / 3600);
    temp_miwariate_seconds_to_minute_temp = minus - temp_miwariate_seconds_to_hour * 60 * 60;
    temp_miwariate_seconds_to_minute = Math.floor(temp_miwariate_seconds_to_minute_temp / 60);

    temp_miwariate_seconds_to_second = Math.floor(temp_miwariate_seconds_to_minute_temp % 60);
    if (temp_miwariate_seconds_to_hour < 10) {
        temp_miwariate_seconds_to_hour = "0" + temp_miwariate_seconds_to_hour;
    }

    if (temp_miwariate_seconds_to_minute < 10) {
        temp_miwariate_seconds_to_minute = "0" + temp_miwariate_seconds_to_minute;
    }

    if (temp_miwariate_seconds_to_second < 10) {
        temp_miwariate_seconds_to_second = "0" + temp_miwariate_seconds_to_second;
    }

    var final_value = temp_miwariate_seconds_to_hour + ':' + temp_miwariate_seconds_to_minute + ':' + temp_miwariate_seconds_to_second;
    // **************************************************************************************


    $("#labelDefault_miwariate").text(final_value);
    //-------------------------------------------------------------------------------		
    //-------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------
    let [hour4, min4, sec4] = (time4.value).split(':');
    if (min4 == undefined) {
        hour4 = "00";
        min4 = "00";

    }

    if (sec4 == undefined) {

        sec4 = "00";

    }

    var temp_inputBox_seconds4 = hour4 * 60 * 60 + min4 * 60 + sec4 * 1;

    minus = temp_miwariate_seconds - temp_inputBox_seconds1 - temp_inputBox_seconds2 - temp_inputBox_seconds3 - temp_inputBox_seconds4;

    // **************************************************************************************
    temp_miwariate_seconds_to_hour = Math.floor(minus / 3600);
    temp_miwariate_seconds_to_minute_temp = minus - temp_miwariate_seconds_to_hour * 60 * 60;
    temp_miwariate_seconds_to_minute = Math.floor(temp_miwariate_seconds_to_minute_temp / 60);

    temp_miwariate_seconds_to_second = Math.floor(temp_miwariate_seconds_to_minute_temp % 60);
    if (temp_miwariate_seconds_to_hour < 10) {
        temp_miwariate_seconds_to_hour = "0" + temp_miwariate_seconds_to_hour;
    }

    if (temp_miwariate_seconds_to_minute < 10) {
        temp_miwariate_seconds_to_minute = "0" + temp_miwariate_seconds_to_minute;
    }

    if (temp_miwariate_seconds_to_second < 10) {
        temp_miwariate_seconds_to_second = "0" + temp_miwariate_seconds_to_second;
    }

    var final_value = temp_miwariate_seconds_to_hour + ':' + temp_miwariate_seconds_to_minute + ':' + temp_miwariate_seconds_to_second;
    // **************************************************************************************


    $("#labelDefault_miwariate").text(final_value);
    //-------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------

    let [hour5, min5, sec5] = (time5.value).split(':');
    if (min5 == undefined) {
        hour5 = "00";
        min5 = "00";

    }

    if (sec5 == undefined) {

        sec5 = "00";

    }

    var temp_inputBox_seconds5 = hour5 * 60 * 60 + min5 * 60 + sec5 * 1;

    minus = temp_miwariate_seconds - temp_inputBox_seconds1 - temp_inputBox_seconds2 - temp_inputBox_seconds3 - temp_inputBox_seconds4
        - temp_inputBox_seconds5;

    // **************************************************************************************
    temp_miwariate_seconds_to_hour = Math.floor(minus / 3600);
    temp_miwariate_seconds_to_minute_temp = minus - temp_miwariate_seconds_to_hour * 60 * 60;
    temp_miwariate_seconds_to_minute = Math.floor(temp_miwariate_seconds_to_minute_temp / 60);

    temp_miwariate_seconds_to_second = Math.floor(temp_miwariate_seconds_to_minute_temp % 60);
    if (temp_miwariate_seconds_to_hour < 10) {
        temp_miwariate_seconds_to_hour = "0" + temp_miwariate_seconds_to_hour;
    }

    if (temp_miwariate_seconds_to_minute < 10) {
        temp_miwariate_seconds_to_minute = "0" + temp_miwariate_seconds_to_minute;
    }

    if (temp_miwariate_seconds_to_second < 10) {
        temp_miwariate_seconds_to_second = "0" + temp_miwariate_seconds_to_second;
    }

    var final_value = temp_miwariate_seconds_to_hour + ':' + temp_miwariate_seconds_to_minute + ':' + temp_miwariate_seconds_to_second;
    // **************************************************************************************


    $("#labelDefault_miwariate").text(final_value);
    //-------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------
    let [hour6, min6, sec6] = (time6.value).split(':');
    if (min6 == undefined) {
        hour6 = "00";
        min6 = "00";

    }

    if (sec6 == undefined) {

        sec6 = "00";

    }

    var temp_inputBox_seconds6 = hour6 * 60 * 60 + min6 * 60 + sec6 * 1;

    minus = temp_miwariate_seconds - temp_inputBox_seconds1 - temp_inputBox_seconds2 - temp_inputBox_seconds3 - temp_inputBox_seconds4
        - temp_inputBox_seconds5 - temp_inputBox_seconds6;

    // **************************************************************************************
    temp_miwariate_seconds_to_hour = Math.floor(minus / 3600);
    temp_miwariate_seconds_to_minute_temp = minus - temp_miwariate_seconds_to_hour * 60 * 60;
    temp_miwariate_seconds_to_minute = Math.floor(temp_miwariate_seconds_to_minute_temp / 60);

    temp_miwariate_seconds_to_second = Math.floor(temp_miwariate_seconds_to_minute_temp % 60);
    if (temp_miwariate_seconds_to_hour < 10) {
        temp_miwariate_seconds_to_hour = "0" + temp_miwariate_seconds_to_hour;
    }

    if (temp_miwariate_seconds_to_minute < 10) {
        temp_miwariate_seconds_to_minute = "0" + temp_miwariate_seconds_to_minute;
    }

    if (temp_miwariate_seconds_to_second < 10) {
        temp_miwariate_seconds_to_second = "0" + temp_miwariate_seconds_to_second;
    }

    var final_value = temp_miwariate_seconds_to_hour + ':' + temp_miwariate_seconds_to_minute + ':' + temp_miwariate_seconds_to_second;
    // **************************************************************************************

    $("#labelDefault_miwariate").text(final_value);
    //-------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------
    let [hour7, min7, sec7] = (time7.value).split(':');
    if (min7 == undefined) {
        hour7 = "00";
        min7 = "00";

    }

    if (sec7 == undefined) {

        sec7 = "00";

    }

    var temp_inputBox_seconds7 = hour7 * 60 * 60 + min7 * 60 + sec7 * 1;

    minus = temp_miwariate_seconds - temp_inputBox_seconds1 - temp_inputBox_seconds2 - temp_inputBox_seconds3 - temp_inputBox_seconds4
        - temp_inputBox_seconds5 - temp_inputBox_seconds6 - temp_inputBox_seconds7;

    // **************************************************************************************
    temp_miwariate_seconds_to_hour = Math.floor(minus / 3600);
    temp_miwariate_seconds_to_minute_temp = minus - temp_miwariate_seconds_to_hour * 60 * 60;
    temp_miwariate_seconds_to_minute = Math.floor(temp_miwariate_seconds_to_minute_temp / 60);

    temp_miwariate_seconds_to_second = Math.floor(temp_miwariate_seconds_to_minute_temp % 60);
    if (temp_miwariate_seconds_to_hour < 10) {
        temp_miwariate_seconds_to_hour = "0" + temp_miwariate_seconds_to_hour;
    }

    if (temp_miwariate_seconds_to_minute < 10) {
        temp_miwariate_seconds_to_minute = "0" + temp_miwariate_seconds_to_minute;
    }

    if (temp_miwariate_seconds_to_second < 10) {
        temp_miwariate_seconds_to_second = "0" + temp_miwariate_seconds_to_second;
    }

    var final_value = temp_miwariate_seconds_to_hour + ':' + temp_miwariate_seconds_to_minute + ':' + temp_miwariate_seconds_to_second;
    // **************************************************************************************


    $("#labelDefault_miwariate").text(final_value);
    //-------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------
    let [hour8, min8, sec8] = (time8.value).split(':');
    if (min8 == undefined) {
        hour8 = "00";
        min8 = "00";

    }

    if (sec8 == undefined) {

        sec8 = "00";

    }

    var temp_inputBox_seconds8 = hour8 * 60 * 60 + min8 * 60 + sec8 * 1;

    minus = temp_miwariate_seconds - temp_inputBox_seconds1 - temp_inputBox_seconds2 - temp_inputBox_seconds3 - temp_inputBox_seconds4
        - temp_inputBox_seconds5 - temp_inputBox_seconds6 - temp_inputBox_seconds7 - temp_inputBox_seconds8;

    // **************************************************************************************
    temp_miwariate_seconds_to_hour = Math.floor(minus / 3600);
    temp_miwariate_seconds_to_minute_temp = minus - temp_miwariate_seconds_to_hour * 60 * 60;
    temp_miwariate_seconds_to_minute = Math.floor(temp_miwariate_seconds_to_minute_temp / 60);

    temp_miwariate_seconds_to_second = Math.floor(temp_miwariate_seconds_to_minute_temp % 60);
    if (temp_miwariate_seconds_to_hour < 10) {
        temp_miwariate_seconds_to_hour = "0" + temp_miwariate_seconds_to_hour;
    }

    if (temp_miwariate_seconds_to_minute < 10) {
        temp_miwariate_seconds_to_minute = "0" + temp_miwariate_seconds_to_minute;
    }

    if (temp_miwariate_seconds_to_second < 10) {
        temp_miwariate_seconds_to_second = "0" + temp_miwariate_seconds_to_second;
    }

    var final_value = temp_miwariate_seconds_to_hour + ':' + temp_miwariate_seconds_to_minute + ':' + temp_miwariate_seconds_to_second;
    // **************************************************************************************


    $("#labelDefault_miwariate").text(final_value);
    //-------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------
    let [hour9, min9, sec9] = (time9.value).split(':');
    if (min9 == undefined) {
        hour9 = "00";
        min9 = "00";

    }

    if (sec9 == undefined) {

        sec9 = "00";

    }

    var temp_inputBox_seconds9 = hour9 * 60 * 60 + min9 * 60 + sec9 * 1;

    minus = temp_miwariate_seconds - temp_inputBox_seconds1 - temp_inputBox_seconds2 - temp_inputBox_seconds3 - temp_inputBox_seconds4
        - temp_inputBox_seconds5 - temp_inputBox_seconds6 - temp_inputBox_seconds7 - temp_inputBox_seconds8 - temp_inputBox_seconds9;


    // **************************************************************************************
    temp_miwariate_seconds_to_hour = Math.floor(minus / 3600);
    temp_miwariate_seconds_to_minute_temp = minus - temp_miwariate_seconds_to_hour * 60 * 60;
    temp_miwariate_seconds_to_minute = Math.floor(temp_miwariate_seconds_to_minute_temp / 60);

    temp_miwariate_seconds_to_second = Math.floor(temp_miwariate_seconds_to_minute_temp % 60);
    if (temp_miwariate_seconds_to_hour < 10) {
        temp_miwariate_seconds_to_hour = "0" + temp_miwariate_seconds_to_hour;
    }

    if (temp_miwariate_seconds_to_minute < 10) {
        temp_miwariate_seconds_to_minute = "0" + temp_miwariate_seconds_to_minute;
    }

    if (temp_miwariate_seconds_to_second < 10) {
        temp_miwariate_seconds_to_second = "0" + temp_miwariate_seconds_to_second;
    }

    var final_value = temp_miwariate_seconds_to_hour + ':' + temp_miwariate_seconds_to_minute + ':' + temp_miwariate_seconds_to_second;
    // **************************************************************************************


    $("#labelDefault_miwariate").text(final_value);
    //-------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------
    let [hour10, min10, sec10] = (time10.value).split(':');
    if (min10 == undefined) {
        hour10 = "00";
        min10 = "00";

    }

    if (sec10 == undefined) {

        sec10 = "00";

    }

    var temp_inputBox_seconds10 = hour10 * 60 * 60 + min10 * 60 + sec10 * 1;

    minus = temp_miwariate_seconds - temp_inputBox_seconds1 - temp_inputBox_seconds2 - temp_inputBox_seconds3 - temp_inputBox_seconds4
        - temp_inputBox_seconds5 - temp_inputBox_seconds6 - temp_inputBox_seconds7 - temp_inputBox_seconds8 - temp_inputBox_seconds9 - temp_inputBox_seconds10;

    // **************************************************************************************
    temp_miwariate_seconds_to_hour = Math.floor(minus / 3600);
    temp_miwariate_seconds_to_minute_temp = minus - temp_miwariate_seconds_to_hour * 60 * 60;
    temp_miwariate_seconds_to_minute = Math.floor(temp_miwariate_seconds_to_minute_temp / 60);

    temp_miwariate_seconds_to_second = Math.floor(temp_miwariate_seconds_to_minute_temp % 60);
    if (temp_miwariate_seconds_to_hour < 10) {
        temp_miwariate_seconds_to_hour = "0" + temp_miwariate_seconds_to_hour;
    }

    if (temp_miwariate_seconds_to_minute < 10) {
        temp_miwariate_seconds_to_minute = "0" + temp_miwariate_seconds_to_minute;
    }

    if (temp_miwariate_seconds_to_second < 10) {
        temp_miwariate_seconds_to_second = "0" + temp_miwariate_seconds_to_second;
    }

    var final_value = temp_miwariate_seconds_to_hour + ':' + temp_miwariate_seconds_to_minute + ':' + temp_miwariate_seconds_to_second;
    // **************************************************************************************


    $("#labelDefault_miwariate").text(final_value);
    //-------------------------------------------------------------------------------
    //*******************************************************************************
    //-------------------------------------------------------------------------------
    //*******************************************************************************
    if (minus < 0) {

        $("#labelDefault_miwariate").text("--:--:--");
        $("#buttonAdd_bunwarisettei").addClass("disabledButton");
        $("#buttonAdd_bunwarisettei").prop('disabled', true);

    }

    //-------------------------------------------------------------------------------
    if (minus == 0) {

        $("#buttonAdd_bunwarisettei").removeClass("disabledButton");
        $("#buttonAdd_bunwarisettei").prop('disabled', false);
        $("#labelDefault_miwariate").text(final_value);

    }


    //-------------------------------------------------------------------------------
    timeCheck();
    // }
}

// ----------------------------------------------------------------------
// ---------------------------------------------------------------------
function timeCheck() {

    var time1 = document.getElementById("inputBox_bunwarisettei1");
    var time2 = document.getElementById("inputBox_bunwarisettei2");
    var time3 = document.getElementById("inputBox_bunwarisettei3");
    var time4 = document.getElementById("inputBox_bunwarisettei4");
    var time5 = document.getElementById("inputBox_bunwarisettei5");
    var time6 = document.getElementById("inputBox_bunwarisettei6");
    var time7 = document.getElementById("inputBox_bunwarisettei7");
    var time8 = document.getElementById("inputBox_bunwarisettei8");
    var time9 = document.getElementById("inputBox_bunwarisettei9");
    var time10 = document.getElementById("inputBox_bunwarisettei10");

    if (time1.value == "") {
        time1.value = "00:00:00";
    }

    if (time2.value == "") {
        time2.value = "00:00:00";
    }

    if (time3.value == "") {
        time3.value = "00:00:00";
    }

    if (time4.value == "") {
        time4.value = "00:00:00";
    }

    if (time5.value == "") {
        time5.value = "00:00:00";
    }

    if (time6.value == "") {
        time6.value = "00:00:00";
    }

    if (time7.value == "") {
        time7.value = "00:00:00";
    }

    if (time8.value == "") {
        time8.value = "00:00:00";
    }

    if (time9.value == "") {
        time9.value = "00:00:00";
    }

    if (time10.value == "") {
        time10.value = "00:00:00";
    }

    var bunwarisettei1 = (time1.value).split(':');
    var bunwarisettei2 = (time2.value).split(':');
    var bunwarisettei3 = (time3.value).split(':');
    var bunwarisettei4 = (time4.value).split(':');
    var bunwarisettei5 = (time5.value).split(':');
    var bunwarisettei6 = (time6.value).split(':');
    var bunwarisettei7 = (time7.value).split(':');
    var bunwarisettei8 = (time8.value).split(':');
    var bunwarisettei9 = (time9.value).split(':');
    var bunwarisettei10 = (time10.value).split(':');

    if (bunwarisettei1[2] == undefined) {
        bunwarisettei1[2] = "00";
    }

    if (bunwarisettei2[2] == undefined) {
        bunwarisettei2[2] = "00";
    }

    if (bunwarisettei3[2] == undefined) {
        bunwarisettei3[2] = "00";
    }

    if (bunwarisettei4[2] == undefined) {
        bunwarisettei4[2] = "00";
    }

    if (bunwarisettei5[2] == undefined) {
        bunwarisettei5[2] = "00";
    }

    if (bunwarisettei6[2] == undefined) {
        bunwarisettei6[2] = "00";
    }

    if (bunwarisettei7[2] == undefined) {
        bunwarisettei7[2] = "00";
    }

    if (bunwarisettei8[2] == undefined) {
        bunwarisettei8[2] = "00";
    }

    if (bunwarisettei9[2] == undefined) {
        bunwarisettei9[2] = "00";
    }

    if (bunwarisettei10[2] == undefined) {
        bunwarisettei10[2] = "00";
    }

    // minutes are worth 60 seconds. Hours are worth 60 minutes.
    var bunwarisettei1_seconds = (+bunwarisettei1[0]) * 60 * 60 + (bunwarisettei1[1]) * 60 + (bunwarisettei1[2]) * 1;
    var bunwarisettei2_seconds = (+bunwarisettei2[0]) * 60 * 60 + (bunwarisettei2[1]) * 60 + (bunwarisettei2[2]) * 1;
    var bunwarisettei3_seconds = (+bunwarisettei3[0]) * 60 * 60 + (bunwarisettei3[1]) * 60 + (bunwarisettei3[2]) * 1;
    var bunwarisettei4_seconds = (+bunwarisettei4[0]) * 60 * 60 + (bunwarisettei4[1]) * 60 + (bunwarisettei4[2]) * 1;
    var bunwarisettei5_seconds = (+bunwarisettei5[0]) * 60 * 60 + (bunwarisettei5[1]) * 60 + (bunwarisettei5[2]) * 1;
    var bunwarisettei6_seconds = (+bunwarisettei6[0]) * 60 * 60 + (bunwarisettei6[1]) * 60 + (bunwarisettei6[2]) * 1;
    var bunwarisettei7_seconds = (+bunwarisettei7[0]) * 60 * 60 + (bunwarisettei7[1]) * 60 + (bunwarisettei7[2]) * 1;
    var bunwarisettei8_seconds = (+bunwarisettei8[0]) * 60 * 60 + (bunwarisettei8[1]) * 60 + (bunwarisettei8[2]) * 1;
    var bunwarisettei9_seconds = (+bunwarisettei9[0]) * 60 * 60 + (bunwarisettei9[1]) * 60 + (bunwarisettei9[2]) * 1;
    var bunwarisettei10_seconds = (+bunwarisettei10[0]) * 60 * 60 + (bunwarisettei10[1]) * 60 + (bunwarisettei10[2]) * 1;

    //<!-- **************************************** -->
    //<!-- *                                      * -->
    //<!-- *	　　　　　　分割精算   　　　　　　　* -->
    //<!-- *                                      * -->
    //<!-- **************************************** -->

    var bunwarisuu_number = $("#inputBox_bunwarisettei0").val();

    if (bunwarisuu_number == "" || bunwarisuu_number == 1) {
        var sum = bunwarisettei1_seconds;
    }

    if (bunwarisuu_number == 2) {
        var sum = bunwarisettei1_seconds + bunwarisettei2_seconds;
    }

    if (bunwarisuu_number == 3) {
        var sum = bunwarisettei1_seconds + bunwarisettei2_seconds + bunwarisettei3_seconds;
    }

    if (bunwarisuu_number == 4) {
        var sum = bunwarisettei1_seconds + bunwarisettei2_seconds + bunwarisettei3_seconds + bunwarisettei4_seconds;
    }

    if (bunwarisuu_number == 5) {
        var sum = bunwarisettei1_seconds + bunwarisettei2_seconds + bunwarisettei3_seconds + bunwarisettei4_seconds + bunwarisettei5_seconds;
    }

    if (bunwarisuu_number == 6) {

        var sum = bunwarisettei1_seconds + bunwarisettei2_seconds + bunwarisettei3_seconds + bunwarisettei4_seconds + bunwarisettei5_seconds
            + bunwarisettei6_seconds;
    }

    if (bunwarisuu_number == 7) {

        var sum = bunwarisettei1_seconds + bunwarisettei2_seconds + bunwarisettei3_seconds + bunwarisettei4_seconds + bunwarisettei5_seconds
            + bunwarisettei6_seconds + bunwarisettei7_seconds;
    }

    if (bunwarisuu_number == 8) {
        var sum = bunwarisettei1_seconds + bunwarisettei2_seconds + bunwarisettei3_seconds + bunwarisettei4_seconds + bunwarisettei5_seconds
            + bunwarisettei6_seconds + bunwarisettei7_seconds + bunwarisettei8_seconds;
    }

    if (bunwarisuu_number == 9) {
        var sum = bunwarisettei1_seconds + bunwarisettei2_seconds + bunwarisettei3_seconds + bunwarisettei4_seconds + bunwarisettei5_seconds
            + bunwarisettei6_seconds + bunwarisettei7_seconds + bunwarisettei8_seconds + bunwarisettei9_seconds;
    }

    if (bunwarisuu_number == 10) {
        var sum = bunwarisettei1_seconds + bunwarisettei2_seconds + bunwarisettei3_seconds + bunwarisettei4_seconds + bunwarisettei5_seconds
            + bunwarisettei6_seconds + bunwarisettei7_seconds + bunwarisettei8_seconds + bunwarisettei9_seconds + bunwarisettei10_seconds;
    }

    var bunwarisettei_goukei = $("#labelDefault_goukei").text();
    // var bunwarisettei_goukei = $("#miwariate_jikan_atai_1").text();
    var goukei = bunwarisettei_goukei.split(':');
    var goukei_seconds = (+goukei[0]) * 60 * 60 + (goukei[1]) * 60 + (goukei[2]) * 1;
    var bunwarisettei_minus = goukei_seconds - sum;



    sum_hour = Math.floor(sum / 3600);

    sum_minute_temp = sum - sum_hour * 60 * 60

    sum_minute = Math.floor(sum_minute_temp / 60);
    sum_second = Math.floor(sum_minute_temp % 60);


    if (sum_hour < 10) { sum_hour = "0" + sum_hour };
    if (sum_minute < 10) { sum_minute = "0" + sum_minute };
    if (sum_second < 10) { sum_second = "0" + sum_second };

    $("#labelDefault_goukei_hidden").text(sum_hour + ":" + sum_minute + ":" + sum_second);


    minus_minute_hour = Math.floor(bunwarisettei_minus / 3600);

    minus_minute_temp = bunwarisettei_minus - minus_minute_hour * 60 * 60

    minus_minute = Math.floor(minus_minute_temp / 60);
    minus_second = Math.floor(bunwarisettei_minus % 60);

    if (minus_minute_hour < 10) {

        minus_minute_hour = "0" + minus_minute_hour;
    }

    if (minus_minute < 10) {

        minus_minute = "0" + minus_minute;
    }

    if (minus_second < 10) {

        minus_second = "0" + minus_second;
    }

    var final_value = minus_minute_hour + ':' + minus_minute + ':' + minus_second;

    // if (bunwarisettei_minus == 0) {

    // 	$("#labelDefault_miwariate").text(final_value);

    // }

    $("#labelDefault_miwariate").text(final_value);

    if (bunwarisettei_minus < 0) {

        $("#labelDefault_miwariate").text("--:--:--");
        $("#buttonAdd_bunwarisettei").addClass("disabledButton");
        $("#buttonAdd_bunwarisettei").prop('disabled', true);
    }

    // $("#miwariate_jikan_atai_1").text(final_value);

    var value1 = document.getElementById('selectBox1').value;
    var value2 = document.getElementById('selectBox2').value;

    var value1_2 = document.getElementById('selectBox1_2').value;
    var value2_2 = document.getElementById('selectBox2_2').value;

    // localStorage.setItem("takedTime", time);
    // alert(time);

    // console.log(time);
    if (time1 !== "") {

        if (value1 == 35) {

            $("#myBtn").removeClass("disabledButton");
            $("#myBtn").prop('disabled', false);

            // timeCheck();
        }

        if (value1 >= 27) {

            $('#iJyouNaiYouGenJyou').prop('disabled', true);



        }

        if (value1 < 27) {

            $('#iJyouNaiYouGenJyou').prop('disabled', false);

        }


        if (value2 == 0) {
            $('.option').addClass("displayDiv");
            $('#iJyouNaiYouGenJyou').prop('disabled', true);
        }


        // ----------------------------------------------------------
        if (value2 == 9) {
            // $('#teiShiYouInTouRoku').addClass("disabledButton");			
            // $('#teiShiYouInTouRoku').prop('disabled', true);
            // $("#teishijikan_kubun").html("区分　：　不稼働");
            $('#iJyouNaiYouGenJyou').prop('disabled', true);
        }

        if (value2 == 10) {
            // $('#teiShiYouInTouRoku').addClass("disabledButton");
            // $('#teiShiYouInTouRoku').prop('disabled', true);
            // $("#teishijikan_kubun").html("区分　：　不稼働");
            $('#iJyouNaiYouGenJyou').prop('disabled', true);
        }

        if (value2 == 11) {
            // $('#teiShiYouInTouRoku').addClass("disabledButton");
            // $('#teiShiYouInTouRoku').prop('disabled', true);
            // $("#teishijikan_kubun").html("区分　：　不稼働");
            $('#iJyouNaiYouGenJyou').prop('disabled', true);
        }
        // ----------------------------------------------------------

    }


    // console.log(time);
    if (time2 !== "") {

        // $('#teiShiYouInTouRoku').removeClass("disabledButton");			
        // $('#teiShiYouInTouRoku').prop('disabled', false);
        // changeSelectBox2();
        if (value1_2 == 35) {

            // $('#teiShiYouInTouRoku').prop('disabled', false);
            // $('#teiShiYouInTouRoku').removeClass("disabledButton");			


            $("#myBtn").removeClass("disabledButton");
            $("#myBtn").prop('disabled', false);


        }

        if (value1_2 >= 27) {
            // $('#teiShiYouInTouRoku').addClass("disabledButton");
            // $('#teiShiYouInTouRoku').prop('disabled', true);
            // $('#teiShiYouInTouRoku').addClass("disabledButton");
            $('#iJyouNaiYouGenJyou').prop('disabled', true);


        }

        if (value1_2 < 27) {

            // $('#teiShiYouInTouRoku').prop('disabled', false);
            // $('#teiShiYouInTouRoku').removeClass("disabledButton");
            $('#iJyouNaiYouGenJyou').prop('disabled', false);

        }


        if (value2_2 == 0) {
            $('.option').addClass("displayDiv");
            $('#iJyouNaiYouGenJyou').prop('disabled', true);
        }


        // ----------------------------------------------------------
        if (value2_2 == 9) {
            // $('#teiShiYouInTouRoku').addClass("disabledButton");			
            // $('#teiShiYouInTouRoku').prop('disabled', true);
            // $("#teishijikan_kubun2").html("区分　：　不稼働");
            $('#iJyouNaiYouGenJyou_2').prop('disabled', true);
        }

        if (value2_2 == 10) {
            // $('#teiShiYouInTouRoku').addClass("disabledButton");
            // $('#teiShiYouInTouRoku').prop('disabled', true);
            // $("#teishijikan_kubun2").html("区分　：　不稼働");
            $('#iJyouNaiYouGenJyou_2').prop('disabled', true);
        }

        if (value2_2 == 11) {
            // $('#teiShiYouInTouRoku').addClass("disabledButton");
            // $('#teiShiYouInTouRoku').prop('disabled', true);
            // $("#teishijikan_kubun2").html("区分　：　不稼働");
            $('#iJyouNaiYouGenJyou_2').prop('disabled', true);
        }
        // ----------------------------------------------------------

    }

    check();

}

function check() {

    var time1 = document.getElementById("inputBox_bunwarisettei1");
    var time2 = document.getElementById("inputBox_bunwarisettei2");
    var time3 = document.getElementById("inputBox_bunwarisettei3");
    var time4 = document.getElementById("inputBox_bunwarisettei4");
    var time5 = document.getElementById("inputBox_bunwarisettei5");
    var time6 = document.getElementById("inputBox_bunwarisettei6");
    var time7 = document.getElementById("inputBox_bunwarisettei7");
    var time8 = document.getElementById("inputBox_bunwarisettei8");
    var time9 = document.getElementById("inputBox_bunwarisettei9");
    var time10 = document.getElementById("inputBox_bunwarisettei10");

    if (time1.value == "") {
        time1.value = "00:00:00";
    }

    if (time2.value == "") {
        time2.value = "00:00:00";
    }

    if (time3.value == "") {
        time3.value = "00:00:00";
    }

    if (time4.value == "") {
        time4.value = "00:00:00";
    }

    if (time5.value == "") {
        time5.value = "00:00:00";
    }

    if (time6.value == "") {
        time6.value = "00:00:00";
    }

    if (time7.value == "") {
        time7.value = "00:00:00";
    }

    if (time8.value == "") {
        time8.value = "00:00:00";
    }

    if (time9.value == "") {
        time9.value = "00:00:00";
    }

    if (time10.value == "") {
        time10.value = "00:00:00";
    }

    var bunwarisettei1 = (time1.value).split(':');
    var bunwarisettei2 = (time2.value).split(':');
    var bunwarisettei3 = (time3.value).split(':');
    var bunwarisettei4 = (time4.value).split(':');
    var bunwarisettei5 = (time5.value).split(':');
    var bunwarisettei6 = (time6.value).split(':');
    var bunwarisettei7 = (time7.value).split(':');
    var bunwarisettei8 = (time8.value).split(':');
    var bunwarisettei9 = (time9.value).split(':');
    var bunwarisettei10 = (time10.value).split(':');

    if (bunwarisettei1[2] == undefined) {
        bunwarisettei1[2] = "00";
    }

    if (bunwarisettei2[2] == undefined) {
        bunwarisettei2[2] = "00";
    }

    if (bunwarisettei3[2] == undefined) {
        bunwarisettei3[2] = "00";
    }

    if (bunwarisettei4[2] == undefined) {
        bunwarisettei4[2] = "00";
    }

    if (bunwarisettei5[2] == undefined) {
        bunwarisettei5[2] = "00";
    }

    if (bunwarisettei6[2] == undefined) {
        bunwarisettei6[2] = "00";
    }

    if (bunwarisettei7[2] == undefined) {
        bunwarisettei7[2] = "00";
    }

    if (bunwarisettei8[2] == undefined) {
        bunwarisettei8[2] = "00";
    }

    if (bunwarisettei9[2] == undefined) {
        bunwarisettei9[2] = "00";
    }

    if (bunwarisettei10[2] == undefined) {
        bunwarisettei10[2] = "00";
    }

    // minutes are worth 60 seconds. Hours are worth 60 minutes.
    var bunwarisettei1_seconds = (+bunwarisettei1[0]) * 60 * 60 + (bunwarisettei1[1]) * 60 + (bunwarisettei1[2]) * 1;
    var bunwarisettei2_seconds = (+bunwarisettei2[0]) * 60 * 60 + (bunwarisettei2[1]) * 60 + (bunwarisettei2[2]) * 1;
    var bunwarisettei3_seconds = (+bunwarisettei3[0]) * 60 * 60 + (bunwarisettei3[1]) * 60 + (bunwarisettei3[2]) * 1;
    var bunwarisettei4_seconds = (+bunwarisettei4[0]) * 60 * 60 + (bunwarisettei4[1]) * 60 + (bunwarisettei4[2]) * 1;
    var bunwarisettei5_seconds = (+bunwarisettei5[0]) * 60 * 60 + (bunwarisettei5[1]) * 60 + (bunwarisettei5[2]) * 1;
    var bunwarisettei6_seconds = (+bunwarisettei6[0]) * 60 * 60 + (bunwarisettei6[1]) * 60 + (bunwarisettei6[2]) * 1;
    var bunwarisettei7_seconds = (+bunwarisettei7[0]) * 60 * 60 + (bunwarisettei7[1]) * 60 + (bunwarisettei7[2]) * 1;
    var bunwarisettei8_seconds = (+bunwarisettei8[0]) * 60 * 60 + (bunwarisettei8[1]) * 60 + (bunwarisettei8[2]) * 1;
    var bunwarisettei9_seconds = (+bunwarisettei9[0]) * 60 * 60 + (bunwarisettei9[1]) * 60 + (bunwarisettei9[2]) * 1;
    var bunwarisettei10_seconds = (+bunwarisettei10[0]) * 60 * 60 + (bunwarisettei10[1]) * 60 + (bunwarisettei10[2]) * 1;

    var sum = bunwarisettei1_seconds + bunwarisettei2_seconds + bunwarisettei3_seconds + bunwarisettei4_seconds + bunwarisettei5_seconds
        + bunwarisettei6_seconds + bunwarisettei7_seconds + bunwarisettei8_seconds + bunwarisettei9_seconds + bunwarisettei10_seconds;

    var bunwarisettei_goukei = $("#labelDefault_goukei").text();
    // $("#labelDefault_miwariate").text("00:00:00");
    var goukei = bunwarisettei_goukei.split(':');
    var goukei_seconds = (+goukei[0]) * 60 * 60 + (goukei[1]) * 60 + (goukei[2]) * 1;
    var bunwarisettei_minus = goukei_seconds - sum;

    if (sum >= goukei_seconds) {

        for (var i = 1; i < 11; i++) {

            // $("#button_waritou" + i).addClass("disabledButton");
            // $("#button_waritou" + i).prop('disabled', true);

        }

    }

}
// ---------------------------------------------------------------------

////////////////////////////////////addTeishikomokuNo2///////////////////////////////
$("#teishikoumokuNo2").hide();
$("#teishikoumokuNo3").hide();
$("#teishikoumokuNo4").hide();
$("#teishikoumokuNo5").hide();
$("#teishikoumokuNo6").hide();
$("#teishikoumokuNo7").hide();
$("#teishikoumokuNo8").hide();
$("#teishikoumokuNo9").hide();
$("#teishikoumokuNo10").hide();

////////////////////////////////////addTeishikomokuNo2///////////////////////////////
// ----------------------------------------------------------------------------------
///////////////////////////////////分割数テーブル///////////////////////////////////////

$(document).ready(function () {
    $("#buttonAdd_bunwarisettei").addClass("disabledButton");
    $("#buttonAdd_bunwarisettei").prop('disabled', true);


    $("#inputBox_bunwarisettei1").val("00:00:00");

    $("#bunwarisu_rows_2").hide();
    $("#bunwarisu_rows_3").hide();
    $("#bunwarisu_rows_4").hide();
    $("#bunwarisu_rows_5").hide();
    $("#bunwarisu_rows_6").hide();
    $("#bunwarisu_rows_7").hide();
    $("#bunwarisu_rows_8").hide();
    $("#bunwarisu_rows_9").hide();
    $("#bunwarisu_rows_10").hide();

    $("#teishikoumokuNo2").hide();
    $("#teishikoumokuNo3").hide();
    $("#teishikoumokuNo4").hide();
    $("#teishikoumokuNo5").hide();
    $("#teishikoumokuNo6").hide();
    $("#teishikoumokuNo7").hide();
    $("#teishikoumokuNo8").hide();
    $("#teishikoumokuNo9").hide();
    $("#teishikoumokuNo10").hide();

    $("#inputBox_bunwarisettei0").change(function () {


        var bunwarisuu_number = $("#inputBox_bunwarisettei0").val();
        // alert(bunwarisuu_number);

        if (bunwarisuu_number == 1) {
            // $("#myModal_bunwarisettei").css("padding-top", "3%");

            $("#bunwarisu_rows_2").hide();
            $("#bunwarisu_rows_3").hide();
            $("#bunwarisu_rows_4").hide();
            $("#bunwarisu_rows_5").hide();
            $("#bunwarisu_rows_6").hide();
            $("#bunwarisu_rows_7").hide();
            $("#bunwarisu_rows_8").hide();
            $("#bunwarisu_rows_9").hide();
            $("#bunwarisu_rows_10").hide();

        }

        if (bunwarisuu_number == 2) {
            // $("#myModal_bunwarisettei").css("padding-top", "3%");


            $("#bunwarisu_rows_2").show();
            $("#inputBox_bunwarisettei1").val("00:00:00");
            $("#inputBox_bunwarisettei2").val("00:00:00");
            // --------------------------
            $("#bunwarisu_rows_3").hide();
            $("#bunwarisu_rows_4").hide();
            $("#bunwarisu_rows_5").hide();
            $("#bunwarisu_rows_6").hide();
            $("#bunwarisu_rows_7").hide();
            $("#bunwarisu_rows_8").hide();
            $("#bunwarisu_rows_9").hide();
            $("#bunwarisu_rows_10").hide();

        }

        if (bunwarisuu_number == 3) {
            // $("#myModal_bunwarisettei").css("padding-top", "3%");

            $("#bunwarisu_rows_2").show();
            $("#bunwarisu_rows_3").show();
            $("#inputBox_bunwarisettei1").val("00:00:00");
            $("#inputBox_bunwarisettei2").val("00:00:00");
            $("#inputBox_bunwarisettei3").val("00:00:00");
            // --------------------------
            $("#bunwarisu_rows_4").hide();
            $("#bunwarisu_rows_5").hide();
            $("#bunwarisu_rows_6").hide();
            $("#bunwarisu_rows_7").hide();
            $("#bunwarisu_rows_8").hide();
            $("#bunwarisu_rows_9").hide();
            $("#bunwarisu_rows_10").hide();

        }

        if (bunwarisuu_number == 4) {
            // $("#myModal_bunwarisettei").css("padding-top", "3%");

            $("#bunwarisu_rows_2").show();
            $("#bunwarisu_rows_3").show();
            $("#bunwarisu_rows_4").show();
            $("#inputBox_bunwarisettei1").val("00:00:00");
            $("#inputBox_bunwarisettei2").val("00:00:00");
            $("#inputBox_bunwarisettei3").val("00:00:00");
            $("#inputBox_bunwarisettei4").val("00:00:00");
            // --------------------------
            $("#bunwarisu_rows_5").hide();
            $("#bunwarisu_rows_6").hide();
            $("#bunwarisu_rows_7").hide();
            $("#bunwarisu_rows_8").hide();
            $("#bunwarisu_rows_9").hide();
            $("#bunwarisu_rows_10").hide();

        }

        if (bunwarisuu_number == 5) {
            // $("#myModal_bunwarisettei").css("padding-top", "3%");

            $("#bunwarisu_rows_2").show();
            $("#bunwarisu_rows_3").show();
            $("#bunwarisu_rows_4").show();
            $("#bunwarisu_rows_5").show();
            $("#inputBox_bunwarisettei1").val("00:00:00");
            $("#inputBox_bunwarisettei2").val("00:00:00");
            $("#inputBox_bunwarisettei3").val("00:00:00");
            $("#inputBox_bunwarisettei4").val("00:00:00");
            $("#inputBox_bunwarisettei5").val("00:00:00");
            // --------------------------
            $("#bunwarisu_rows_6").hide();
            $("#bunwarisu_rows_7").hide();
            $("#bunwarisu_rows_8").hide();
            $("#bunwarisu_rows_9").hide();
            $("#bunwarisu_rows_10").hide();

        }

        if (bunwarisuu_number == 6) {
            // $("#myModal_bunwarisettei").css("padding-top", "3%");

            $("#bunwarisu_rows_2").show();
            $("#bunwarisu_rows_3").show();
            $("#bunwarisu_rows_4").show();
            $("#bunwarisu_rows_5").show();
            $("#bunwarisu_rows_6").show();
            $("#inputBox_bunwarisettei1").val("00:00:00");
            $("#inputBox_bunwarisettei2").val("00:00:00");
            $("#inputBox_bunwarisettei3").val("00:00:00");
            $("#inputBox_bunwarisettei4").val("00:00:00");
            $("#inputBox_bunwarisettei5").val("00:00:00");
            $("#inputBox_bunwarisettei6").val("00:00:00");
            // --------------------------
            $("#bunwarisu_rows_7").hide();
            $("#bunwarisu_rows_8").hide();
            $("#bunwarisu_rows_9").hide();
            $("#bunwarisu_rows_10").hide();
        }

        if (bunwarisuu_number == 7) {
            // $("#myModal_bunwarisettei").css("padding-top", "3%");

            $("#bunwarisu_rows_2").show();
            $("#bunwarisu_rows_3").show();
            $("#bunwarisu_rows_4").show();
            $("#bunwarisu_rows_5").show();
            $("#bunwarisu_rows_6").show();
            $("#bunwarisu_rows_7").show();
            $("#inputBox_bunwarisettei1").val("00:00:00");
            $("#inputBox_bunwarisettei2").val("00:00:00");
            $("#inputBox_bunwarisettei3").val("00:00:00");
            $("#inputBox_bunwarisettei4").val("00:00:00");
            $("#inputBox_bunwarisettei5").val("00:00:00");
            $("#inputBox_bunwarisettei6").val("00:00:00");
            $("#inputBox_bunwarisettei7").val("00:00:00");
            // --------------------------
            $("#bunwarisu_rows_8").hide();
            $("#bunwarisu_rows_9").hide();
            $("#bunwarisu_rows_10").hide();
        }

        if (bunwarisuu_number == 8) {
            // $("#myModal_bunwarisettei").css("padding-top", "3%");

            $("#bunwarisu_rows_2").show();
            $("#bunwarisu_rows_3").show();
            $("#bunwarisu_rows_4").show();
            $("#bunwarisu_rows_5").show();
            $("#bunwarisu_rows_6").show();
            $("#bunwarisu_rows_7").show();
            $("#bunwarisu_rows_8").show();
            $("#inputBox_bunwarisettei1").val("00:00:00");
            $("#inputBox_bunwarisettei2").val("00:00:00");
            $("#inputBox_bunwarisettei3").val("00:00:00");
            $("#inputBox_bunwarisettei4").val("00:00:00");
            $("#inputBox_bunwarisettei5").val("00:00:00");
            $("#inputBox_bunwarisettei6").val("00:00:00");
            $("#inputBox_bunwarisettei7").val("00:00:00");
            $("#inputBox_bunwarisettei8").val("00:00:00");
            // --------------------------
            $("#bunwarisu_rows_9").hide();
            $("#bunwarisu_rows_10").hide();
        }

        if (bunwarisuu_number == 9) {
            // $("#myModal_bunwarisettei").css("padding-top", "3%");

            $("#bunwarisu_rows_2").show();
            $("#bunwarisu_rows_3").show();
            $("#bunwarisu_rows_4").show();
            $("#bunwarisu_rows_5").show();
            $("#bunwarisu_rows_6").show();
            $("#bunwarisu_rows_7").show();
            $("#bunwarisu_rows_8").show();
            $("#bunwarisu_rows_9").show();
            $("#inputBox_bunwarisettei1").val("00:00:00");
            $("#inputBox_bunwarisettei2").val("00:00:00");
            $("#inputBox_bunwarisettei3").val("00:00:00");
            $("#inputBox_bunwarisettei4").val("00:00:00");
            $("#inputBox_bunwarisettei5").val("00:00:00");
            $("#inputBox_bunwarisettei6").val("00:00:00");
            $("#inputBox_bunwarisettei7").val("00:00:00");
            $("#inputBox_bunwarisettei8").val("00:00:00");
            $("#inputBox_bunwarisettei9").val("00:00:00");
            // --------------------------
            $("#bunwarisu_rows_10").hide();
        }

        if (bunwarisuu_number == 10) {
            // $("#myModal_bunwarisettei").css("padding-top", "1%");

            $("#bunwarisu_rows_2").show();
            $("#bunwarisu_rows_3").show();
            $("#bunwarisu_rows_4").show();
            $("#bunwarisu_rows_5").show();
            $("#bunwarisu_rows_6").show();
            $("#bunwarisu_rows_7").show();
            $("#bunwarisu_rows_8").show();
            $("#bunwarisu_rows_9").show();
            $("#bunwarisu_rows_10").show();
            $("#inputBox_bunwarisettei1").val("00:00:00");
            $("#inputBox_bunwarisettei2").val("00:00:00");
            $("#inputBox_bunwarisettei3").val("00:00:00");
            $("#inputBox_bunwarisettei4").val("00:00:00");
            $("#inputBox_bunwarisettei5").val("00:00:00");
            $("#inputBox_bunwarisettei6").val("00:00:00");
            $("#inputBox_bunwarisettei7").val("00:00:00");
            $("#inputBox_bunwarisettei8").val("00:00:00");
            $("#inputBox_bunwarisettei9").val("00:00:00");
            $("#inputBox_bunwarisettei10").val("00:00:00");
            // --------------------------
        }
    });

});

function timeSumBunwari() {

    var time1 = document.getElementById("inputBox_bunwarisettei1");
    var time2 = document.getElementById("inputBox_bunwarisettei2");
    var time3 = document.getElementById("inputBox_bunwarisettei3");
    var time4 = document.getElementById("inputBox_bunwarisettei4");
    var time5 = document.getElementById("inputBox_bunwarisettei5");
    var time6 = document.getElementById("inputBox_bunwarisettei6");
    var time7 = document.getElementById("inputBox_bunwarisettei7");
    var time8 = document.getElementById("inputBox_bunwarisettei8");
    var time9 = document.getElementById("inputBox_bunwarisettei9");
    var time10 = document.getElementById("inputBox_bunwarisettei10");

    //-------------------------停止項目No.1--------------------------
    let [hour1, min1, sec1] = (time1.value).split(':');
    if (sec1 == undefined) {

        sec1 = "00";

    }

    if (hour1 > 0) {
        // 長期停止
        $("#miwariate_jikan_atai_1").html(hour1 + ":" + min1 + ":" + sec1);
        $("#teishijikan_kubun_1").html("長期停止");
        // timeCheck();
    } else if (min1 >= 5) {
        // 長期停止
        $("#miwariate_jikan_atai_1").html(hour1 + ":" + min1 + ":" + sec1);
        $("#teishijikan_kubun_1").html("長期停止");
        // timeCheck();
    } else {
        // チョコ停
        $("#miwariate_jikan_atai_1").html(hour1 + ":" + min1 + ":" + sec1);
        $("#teishijikan_kubun_1").html("チョコ停");
        // timeCheck();
    }
    //-------------------------停止項目No.1--------------------------
    // -------------------------------------------------------------
    //-------------------------停止項目No.2--------------------------
    let [hour2, min2, sec2] = (time2.value).split(':');
    if (sec2 == undefined) {

        sec2 = "00";

    }

    if (hour2 > 0) {
        // 長期停止
        $("#miwariate_jikan_atai_2").html(hour2 + ":" + min2 + ":" + sec2);
        $("#teishijikan_kubun_2").html("長期停止");
        // timeCheck();
    } else if (min2 >= 5) {
        // 長期停止
        $("#miwariate_jikan_atai_2").html(hour2 + ":" + min2 + ":" + sec2);
        $("#teishijikan_kubun_2").html("長期停止");
        // timeCheck();
    } else {
        // チョコ停
        $("#miwariate_jikan_atai_2").html(hour2 + ":" + min2 + ":" + sec2);
        $("#teishijikan_kubun_2").html("チョコ停");
        // timeCheck();
    }
    //-------------------------停止項目No.2--------------------------
    // -------------------------------------------------------------
    //-------------------------停止項目No.3--------------------------
    let [hour3, min3, sec3] = (time3.value).split(':');
    if (sec3 == undefined) {

        sec3 = "00";

    }

    if (hour3 > 0) {
        // 長期停止
        $("#miwariate_jikan_atai_3").html(hour3 + ":" + min3 + ":" + sec3);
        $("#teishijikan_kubun_3").html("長期停止");
        // timeCheck();
    } else if (min3 >= 5) {
        // 長期停止
        $("#miwariate_jikan_atai_3").html(hour3 + ":" + min3 + ":" + sec3);
        $("#teishijikan_kubun_3").html("長期停止");
        // timeCheck();
    } else {
        // チョコ停
        $("#miwariate_jikan_atai_3").html(hour3 + ":" + min3 + ":" + sec3);
        $("#teishijikan_kubun_3").html("チョコ停");
        // timeCheck();
    }
    //-------------------------停止項目No.3--------------------------
    // -------------------------------------------------------------
    //-------------------------停止項目No.4--------------------------
    let [hour4, min4, sec4] = (time4.value).split(':');
    if (sec4 == undefined) {

        sec4 = "00";

    }

    if (hour4 > 0) {
        // 長期停止
        $("#miwariate_jikan_atai_4").html(hour4 + ":" + min4 + ":" + sec4);
        $("#teishijikan_kubun_4").html("長期停止");
        // timeCheck();
    } else if (min4 >= 5) {
        // 長期停止
        $("#miwariate_jikan_atai_4").html(hour4 + ":" + min4 + ":" + sec4);
        $("#teishijikan_kubun_4").html("長期停止");
        // timeCheck();
    } else {
        // チョコ停
        $("#miwariate_jikan_atai_4").html(hour4 + ":" + min4 + ":" + sec4);
        $("#teishijikan_kubun_4").html("チョコ停");
        // timeCheck();
    }
    //-------------------------停止項目No.4--------------------------
    // -------------------------------------------------------------
    //-------------------------停止項目No.5--------------------------
    let [hour5, min5, sec5] = (time5.value).split(':');
    if (sec5 == undefined) {

        sec5 = "00";

    }

    if (hour5 > 0) {
        // 長期停止
        $("#miwariate_jikan_atai_5").html(hour5 + ":" + min5 + ":" + sec5);
        $("#teishijikan_kubun_5").html("長期停止");
        // timeCheck();
    } else if (min5 >= 5) {
        // 長期停止
        $("#miwariate_jikan_atai_5").html(hour5 + ":" + min5 + ":" + sec5);
        $("#teishijikan_kubun_5").html("長期停止");
        // timeCheck();
    } else {
        // チョコ停
        $("#miwariate_jikan_atai_5").html(hour5 + ":" + min5 + ":" + sec5);
        $("#teishijikan_kubun_5").html("チョコ停");
        // timeCheck();
    }
    //-------------------------停止項目No.5--------------------------
    // -------------------------------------------------------------
    //-------------------------停止項目No.6--------------------------
    let [hour6, min6, sec6] = (time6.value).split(':');
    if (sec6 == undefined) {

        sec6 = "00";

    }

    if (hour6 > 0) {
        // 長期停止
        $("#miwariate_jikan_atai_6").html(hour6 + ":" + min6 + ":" + sec6);
        $("#teishijikan_kubun_6").html("長期停止");
        // timeCheck();
    } else if (min6 >= 5) {
        // 長期停止
        $("#miwariate_jikan_atai_6").html(hour6 + ":" + min6 + ":" + sec6);
        $("#teishijikan_kubun_6").html("長期停止");
        // timeCheck();
    } else {
        // チョコ停
        $("#miwariate_jikan_atai_6").html(hour6 + ":" + min6 + ":" + sec6);
        $("#teishijikan_kubun_6").html("チョコ停");
        // timeCheck();
    }
    //-------------------------停止項目No.6--------------------------
    // -------------------------------------------------------------
    //-------------------------停止項目No.7--------------------------
    let [hour7, min7, sec7] = (time7.value).split(':');
    if (sec7 == undefined) {

        sec7 = "00";

    }


    if (hour7 > 0) {
        // 長期停止
        $("#miwariate_jikan_atai_7").html(hour7 + ":" + min7 + ":" + sec7);
        $("#teishijikan_kubun_7").html("長期停止");
        // timeCheck();
    } else if (min7 >= 5) {
        // 長期停止
        $("#miwariate_jikan_atai_7").html(hour7 + ":" + min7 + ":" + sec7);
        $("#teishijikan_kubun_7").html("長期停止");
        // timeCheck();
    } else {
        // チョコ停
        $("#miwariate_jikan_atai_7").html(hour7 + ":" + min7 + ":" + sec7);
        $("#teishijikan_kubun_7").html("チョコ停");
        // timeCheck();
    }
    //-------------------------停止項目No.7--------------------------
    // -------------------------------------------------------------
    //-------------------------停止項目No.8--------------------------
    let [hour8, min8, sec8] = (time8.value).split(':');
    if (sec8 == undefined) {

        sec8 = "00";

    }


    if (hour8 > 0) {
        // 長期停止
        $("#miwariate_jikan_atai_8").html(hour8 + ":" + min8 + ":" + sec8);
        $("#teishijikan_kubun_8").html("長期停止");
        // timeCheck();
    } else if (min8 >= 5) {
        // 長期停止
        $("#miwariate_jikan_atai_8").html(hour8 + ":" + min8 + ":" + sec8);
        $("#teishijikan_kubun_8").html("長期停止");
        // timeCheck();
    } else {
        // チョコ停
        $("#miwariate_jikan_atai_8").html(hour8 + ":" + min8 + ":" + sec8);
        $("#teishijikan_kubun_8").html("チョコ停");
        // timeCheck();
    }
    //-------------------------停止項目No.8--------------------------
    // -------------------------------------------------------------
    //-------------------------停止項目No.9--------------------------
    let [hour9, min9, sec9] = (time9.value).split(':');
    if (sec9 == undefined) {

        sec9 = "00";

    }

    if (hour9 > 0) {
        // 長期停止
        $("#miwariate_jikan_atai_9").html(hour9 + ":" + min9 + ":" + sec9);
        $("#teishijikan_kubun_9").html("長期停止");
        // timeCheck();
    } else if (min9 >= 5) {
        // 長期停止
        $("#miwariate_jikan_atai_9").html(hour9 + ":" + min9 + ":" + sec9);
        $("#teishijikan_kubun_9").html("長期停止");
        // timeCheck();
    } else {
        // チョコ停
        $("#miwariate_jikan_atai_9").html(hour9 + ":" + min9 + ":" + sec9);
        $("#teishijikan_kubun_9").html("チョコ停");
        // timeCheck();
    }
    //-------------------------停止項目No.9--------------------------
    // -------------------------------------------------------------
    //-------------------------停止項目No.10--------------------------
    let [hour10, min10, sec10] = (time10.value).split(':');
    if (sec10 == undefined) {

        sec10 = "00";

    }

    if (hour9 > 0) {
        // 長期停止
        $("#miwariate_jikan_atai_10").html(hour10 + ":" + min10 + ":" + sec10);
        $("#teishijikan_kubun_10").html("長期停止");
        // timeCheck();
    } else if (min9 >= 5) {
        // 長期停止
        $("#miwariate_jikan_atai_10").html(hour10 + ":" + min10 + ":" + sec10);
        $("#teishijikan_kubun_10").html("長期停止");
        // timeCheck();
    } else {
        // チョコ停
        $("#miwariate_jikan_atai_10").html(hour10 + ":" + min10 + ":" + sec10);
        $("#teishijikan_kubun_10").html("チョコ停");
        // timeCheck();
    }
    //-------------------------停止項目No.10--------------------------

    timeCheck();

}

//****************************************************************************************
/////////////////////////////// soi code edit //////////////////////////////////////////
// button wariai 1 to 10
function button_waritou1() {
    wariateClick(1);
} function button_waritou2() {
    wariateClick(2);
}
function button_waritou3() {
    wariateClick(3);
}
function button_waritou4() {
    wariateClick(4);
}
function button_waritou5() {
    wariateClick(5);
}
function button_waritou6() {
    wariateClick(6);
}
function button_waritou7() {
    wariateClick(7);
}
function button_waritou8() {
    wariateClick(8);
}
function button_waritou9() {
    wariateClick(9);
}
function button_waritou10() {
    wariateClick(10);
}
function wariateClick(no) {
    var miwariateTime = document.getElementById("labelDefault_miwariate").innerText;
    if (miwariateTime != "--:--:--") {

        // –¢Š„“–
        var miHour = miwariateTime.substr(0, 2);
        var miMin = miwariateTime.substr(3, 2);
        var miSec = miwariateTime.substr(6, 2);

        // Œ»Ý‚Ì“ü—Í’l
        var idInputBox = "inputBox_bunwarisettei" + no;
        var nowTime = document.getElementById(idInputBox).value;
        var [nowHour, nowMin, nowSec] = nowTime.split(':');
        if (nowSec == undefined) nowSec = '00';

        // ÅI“I‚È“ü—Í’l(mi + now)
        var inHour = 0;
        var inMin = 0;
        var inSec = 0;

        if (parseInt(nowSec) + parseInt(miSec) < 60) {
            inSec = parseInt(nowSec) + parseInt(miSec);
        } else {
            nowMin += parseInt(1);
            inSec = parseInt(nowSec) + parseInt(miSec) - 60;
        }
        if (parseInt(nowMin) + parseInt(miMin) < 60) {
            inMin = parseInt(nowMin) + parseInt(miMin);
        } else {
            nowHour += parseInt(1);
            inMin = parseInt(nowMin) + parseInt(miMin) - 60;
        }

        var vHour = ('00' + inHour).slice(-2);
        var vMin = ('00' + inMin).slice(-2);
        var vSec = ('00' + inSec).slice(-2);

        document.getElementById(idInputBox).value = vHour + ":" + vMin + ":" + vSec;
        changeTime();
    }
}

//****************************************************************************************
/////////////////////////////// soi code edit //////////////////////////////////////////
// Get sceen Change item When button Item registration of Stop project no.1 to no.10
//No.1
function aitemu_teishikoumoku_1() {
    aitemu_btn_counter = 1;

    $("#myModal").css('display', 'block');

}
//No.2
function aitemu_teishikoumoku_2() {
    aitemu_btn_counter = 2;

    $("#myModal").css('display', 'block');

}
//No.3
function aitemu_teishikoumoku_3() {
    aitemu_btn_counter = 3;

    $("#myModal").css('display', 'block');

}
//No.4
function aitemu_teishikoumoku_4() {
    aitemu_btn_counter = 4;

    $("#myModal").css('display', 'block');

}
//No.5
function aitemu_teishikoumoku_5() {
    aitemu_btn_counter = 5;

    $("#myModal").css('display', 'block');

}
//No.6
function aitemu_teishikoumoku_6() {
    aitemu_btn_counter = 6;

    $("#myModal").css('display', 'block');

}
//No.7  
function aitemu_teishikoumoku_7() {
    aitemu_btn_counter = 7;

    $("#myModal").css('display', 'block');

}
//No.8  
function aitemu_teishikoumoku_8() {
    aitemu_btn_counter = 8;

    $("#myModal").css('display', 'block');

}
//No.9 
function aitemu_teishikoumoku_9() {
    aitemu_btn_counter = 9;

    $("#myModal").css('display', 'block');

}
//No.10
function aitemu_teishikoumoku_10() {
    aitemu_btn_counter = 10;

    $("#myModal").css('display', 'block');

}
//****************************************************************************************         
// Click row 1 of sceen Change item
function aitemu_toroku1() {

    $("#myModal").css('display', 'none');
    $("#aitemu_row_1_div3_" + aitemu_btn_counter).css("display", "inline");
    $("#aitemu_row_2_div3_" + aitemu_btn_counter).css("display", "none");
    $("#mitouroku_div3_" + aitemu_btn_counter).css("display", "none");
}
// Click row 2 of sceen Change item
function aitemu_toroku2() {

    $("#myModal").css('display', 'none');
    $("#aitemu_row_1_div3_" + aitemu_btn_counter).css("display", "none");
    $("#aitemu_row_2_div3_" + aitemu_btn_counter).css("display", "inline");
    $("#mitouroku_div3_" + aitemu_btn_counter).css("display", "none");
}
//****************************************************************************************
// Get screen split setting
function bunwarisettei_button() {

    var Modal_bunwarisettei = document.getElementById("myModal_bunwarisettei");
    Modal_bunwarisettei.style.display = "block";

}
// When on click button setting
function buttonAdd_bunwarisettei() {



	var bunwarisu = document.getElementById("inputBox_bunwarisettei0").value;
	$("#bunwarisuu_default").html(bunwarisu);


    $("#teishikoumokuNo2").hide();
    $("#teishikoumokuNo3").hide();
    $("#teishikoumokuNo4").hide();
    $("#teishikoumokuNo5").hide();
    $("#teishikoumokuNo6").hide();
    $("#teishikoumokuNo7").hide();
    $("#teishikoumokuNo8").hide();
    $("#teishikoumokuNo9").hide();
    $("#teishikoumokuNo10").hide();

    var bunwarisuu_number = $("#inputBox_bunwarisettei0").val();

    for (var i = 0; i < bunwarisuu_number; i++) {
        var idTeishiKoumoku = "teishikoumokuNo" + (i + 1);
        $('#' + idTeishiKoumoku).show();
    }

    //---------------------------------------------
    $("#myModal_bunwarisettei").hide();
    //---------------------------------------------
    timeSumBunwari();
}
//****************************************************************************************
// Get the button add to that opens the modal 1 to 10
var tsuika_btn1_counter = 0;
function tsuika_teshikomoku1() {

    tsuika_btn1_counter++;
    document.getElementById("No").value = 1;
    document.getElementById("buttonEdit_chokotei").style.visibility = "hidden";
    document.getElementById("buttonDelete_chokotei").style.visibility = "hidden";
    document.getElementById("buttonAdd_chokotei").style.display = "block";
    document.getElementById("buttonCancel_chokotei").style.display = "block";

    document.getElementById("buttonEdit_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonDelete_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonAdd_choukiteishi").style.display = "block";
    document.getElementById("buttonCancel_choukiteishi").style.display = "block";

    var time = $("#miwariate_jikan_atai_1").text();

    var temp_miwariate = time.split(':');
    var temp_miwariate_seconds = (+temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    if (temp_miwariate_seconds >= 300) {
        // 長期停止
        $("#myModal_choukiteishi").css('display', 'block');
        resetModalChoukiteishi();
    } else {
        // チョコ停
        $("#myModal_chokoteishi").css('display', 'block');
        resetMoalChokotei();

    }
}
var tsuika_btn2_counter = 0;
function tsuika_teshikomoku2() {
    tsuika_btn2_counter++;

    document.getElementById("No").value = 2;

    document.getElementById("buttonEdit_chokotei").style.visibility = "hidden";
    document.getElementById("buttonDelete_chokotei").style.visibility = "hidden";
    document.getElementById("buttonAdd_chokotei").style.display = "block";
    document.getElementById("buttonCancel_chokotei").style.display = "block";

    document.getElementById("buttonEdit_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonDelete_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonAdd_choukiteishi").style.display = "block";
    document.getElementById("buttonCancel_choukiteishi").style.display = "block";

    var time = $("#miwariate_jikan_atai_2").text();

    var temp_miwariate = time.split(':');
    var temp_miwariate_seconds = (+temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    if (temp_miwariate_seconds >= 300) {
        // 長期停止
        $("#myModal_choukiteishi").css('display', 'block');
        resetModalChoukiteishi();
    } else {
        // チョコ停
        $("#myModal_chokoteishi").css('display', 'block');
        resetMoalChokotei();
    }

}
var tsuika_btn3_counter = 0;
function tsuika_teshikomoku3() {
    tsuika_btn3_counter++;
    document.getElementById("No").value = 3;
    document.getElementById("buttonEdit_chokotei").style.visibility = "hidden";
    document.getElementById("buttonDelete_chokotei").style.visibility = "hidden";
    document.getElementById("buttonAdd_chokotei").style.display = "block";
    document.getElementById("buttonCancel_chokotei").style.display = "block";

    document.getElementById("buttonEdit_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonDelete_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonAdd_choukiteishi").style.display = "block";
    document.getElementById("buttonCancel_choukiteishi").style.display = "block";

    var time = $("#miwariate_jikan_atai_3").text();

    var temp_miwariate = time.split(':');
    var temp_miwariate_seconds = (+temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    if (temp_miwariate_seconds >= 300) {
        // 長期停止
        $("#myModal_choukiteishi").css('display', 'block');
        resetModalChoukiteishi();
    } else {
        // チョコ停
        $("#myModal_chokoteishi").css('display', 'block');
        resetMoalChokotei();
    }

}
var tsuika_btn4_counter = 0;
function tsuika_teshikomoku4() {
    tsuika_btn4_counter++;

    document.getElementById("No").value = 4;
    document.getElementById("buttonEdit_chokotei").style.visibility = "hidden";
    document.getElementById("buttonDelete_chokotei").style.visibility = "hidden";
    document.getElementById("buttonAdd_chokotei").style.display = "block";
    document.getElementById("buttonCancel_chokotei").style.display = "block";

    document.getElementById("buttonEdit_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonDelete_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonAdd_choukiteishi").style.display = "block";
    document.getElementById("buttonCancel_choukiteishi").style.display = "block";

    var time = $("#miwariate_jikan_atai_4").text();

    var temp_miwariate = time.split(':');
    var temp_miwariate_seconds = (+temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    if (temp_miwariate_seconds >= 300) {
        // 長期停止
        $("#myModal_choukiteishi").css('display', 'block');
        resetModalChoukiteishi();
    } else {
        // チョコ停
        $("#myModal_chokoteishi").css('display', 'block');
        resetMoalChokotei();
    }
}
var tsuika_btn5_counter = 0;
function tsuika_teshikomoku5() {
    tsuika_btn5_counter++;

    document.getElementById("No").value = 5;
    document.getElementById("buttonEdit_chokotei").style.visibility = "hidden";
    document.getElementById("buttonDelete_chokotei").style.visibility = "hidden";
    document.getElementById("buttonAdd_chokotei").style.display = "block";
    document.getElementById("buttonCancel_chokotei").style.display = "block";
    document.getElementById("buttonEdit_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonDelete_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonAdd_choukiteishi").style.display = "block";
    document.getElementById("buttonCancel_choukiteishi").style.display = "block";

    var time = $("#miwariate_jikan_atai_5").text();

    var temp_miwariate = time.split(':');
    var temp_miwariate_seconds = (+temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    if (temp_miwariate_seconds >= 300) {
        // 長期停止
        $("#myModal_choukiteishi").css('display', 'block');
        resetModalChoukiteishi();
    } else {
        // チョコ停
        $("#myModal_chokoteishi").css('display', 'block');
        resetMoalChokotei();
    }
}
var tsuika_btn6_counter = 0;
function tsuika_teshikomoku6() {
    tsuika_btn6_counter++;

    document.getElementById("No").value = 6;

    document.getElementById("buttonEdit_chokotei").style.visibility = "hidden";
    document.getElementById("buttonDelete_chokotei").style.visibility = "hidden";
    document.getElementById("buttonAdd_chokotei").style.display = "block";
    document.getElementById("buttonCancel_chokotei").style.display = "block";

    document.getElementById("buttonEdit_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonDelete_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonAdd_choukiteishi").style.display = "block";
    document.getElementById("buttonCancel_choukiteishi").style.display = "block";

    var time = $("#miwariate_jikan_atai_6").text();

    var temp_miwariate = time.split(':');
    var temp_miwariate_seconds = (+temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    if (temp_miwariate_seconds >= 300) {
        // 長期停止
        $("#myModal_choukiteishi").css('display', 'block');
        resetModalChoukiteishi();
    } else {
        // チョコ停
        $("#myModal_chokoteishi").css('display', 'block');
        resetMoalChokotei();
    }

}
var tsuika_btn7_counter = 0;
function tsuika_teshikomoku7() {
    tsuika_btn7_counter++;
    resetModalChoukiteishi();
    document.getElementById("No").value = 7;

    document.getElementById("buttonEdit_chokotei").style.visibility = "hidden";
    document.getElementById("buttonDelete_chokotei").style.visibility = "hidden";
    document.getElementById("buttonAdd_chokotei").style.display = "block";
    document.getElementById("buttonCancel_chokotei").style.display = "block";

    document.getElementById("buttonEdit_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonDelete_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonAdd_choukiteishi").style.display = "block";
    document.getElementById("buttonCancel_choukiteishi").style.display = "block";

    var time = $("#miwariate_jikan_atai_7").text();

    var temp_miwariate = time.split(':');
    var temp_miwariate_seconds = (+temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    if (temp_miwariate_seconds >= 300) {
        // 長期停止
        $("#myModal_choukiteishi").css('display', 'block');
        resetModalChoukiteishi();
    } else {
        // チョコ停
        $("#myModal_chokoteishi").css('display', 'block');
        resetMoalChokotei();
    }

}
var tsuika_btn8_counter = 0;
function tsuika_teshikomoku8() {
    tsuika_btn8_counter++;

    document.getElementById("No").value = 8;

    document.getElementById("buttonEdit_chokotei").style.visibility = "hidden";
    document.getElementById("buttonDelete_chokotei").style.visibility = "hidden";
    document.getElementById("buttonAdd_chokotei").style.display = "block";
    document.getElementById("buttonCancel_chokotei").style.display = "block";

    document.getElementById("buttonEdit_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonDelete_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonAdd_choukiteishi").style.display = "block";
    document.getElementById("buttonCancel_choukiteishi").style.display = "block";

    var time = $("#miwariate_jikan_atai_8").text();

    var temp_miwariate = time.split(':');
    var temp_miwariate_seconds = (+temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    if (temp_miwariate_seconds >= 300) {
        // 長期停止
        $("#myModal_choukiteishi").css('display', 'block');
        resetModalChoukiteishi();
    } else {
        // チョコ停
        $("#myModal_chokoteishi").css('display', 'block');
        resetMoalChokotei();
    }
}
var tsuika_btn9_counter = 0;
function tsuika_teshikomoku9() {
    tsuika_btn9_counter++;

    document.getElementById("No").value = 9;
    document.getElementById("buttonEdit_chokotei").style.visibility = "hidden";
    document.getElementById("buttonDelete_chokotei").style.visibility = "hidden";
    document.getElementById("buttonAdd_chokotei").style.display = "block";
    document.getElementById("buttonCancel_chokotei").style.display = "block";

    document.getElementById("buttonEdit_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonDelete_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonAdd_choukiteishi").style.display = "block";
    document.getElementById("buttonCancel_choukiteishi").style.display = "block";

    var time = $("#miwariate_jikan_atai_9").text();

    var temp_miwariate = time.split(':');
    var temp_miwariate_seconds = (+temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    if (temp_miwariate_seconds >= 300) {
        // 長期停止
        $("#myModal_choukiteishi").css('display', 'block');
        resetModalChoukiteishi();
    } else {
        // チョコ停
        $("#myModal_chokoteishi").css('display', 'block');
        resetMoalChokotei();
    }

}
var tsuika_btn10_counter = 0;
function tsuika_teshikomoku10() {
    tsuika_btn10_counter++;

    document.getElementById("No").value = 10;
    document.getElementById("buttonEdit_chokotei").style.visibility = "hidden";
    document.getElementById("buttonDelete_chokotei").style.visibility = "hidden";
    document.getElementById("buttonAdd_chokotei").style.display = "block";
    document.getElementById("buttonCancel_chokotei").style.display = "block";

    document.getElementById("buttonEdit_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonDelete_choukiteishi").style.visibility = "hidden";
    document.getElementById("buttonAdd_choukiteishi").style.display = "block";
    document.getElementById("buttonCancel_choukiteishi").style.display = "block";

    var time = $("#miwariate_jikan_atai_10").text();

    var temp_miwariate = time.split(':');
    var temp_miwariate_seconds = (+temp_miwariate[0]) * 60 * 60 + (temp_miwariate[1]) * 60 + (temp_miwariate[2]) * 1;

    if (temp_miwariate_seconds >= 300) {
        // 長期停止
        $("#myModal_choukiteishi").css('display', 'block');
        resetModalChoukiteishi();
    } else {
        // チョコ停
        $("#myModal_chokoteishi").css('display', 'block');
        resetMoalChokotei();
    }

}
// Reset modal choukiteushi
function resetModalChoukiteishi() {
    document.getElementById("inputBox_choukiteishi1").value = "";
    document.getElementById("inputBox_choukiteishi2").value = "";
    document.getElementById("inputBox_choukiteishi3").value = "";
}
// Reset modal chokotei
function resetMoalChokotei() {
    document.getElementById("inputBox_chokotei1").value = "";
    document.getElementById("inputBox_chokotei2").value = "";
    document.getElementById("inputBox_chokotei3").value = "";
}
//****************************************************************************************
// When click button add choukiteishi 
var rows_choukiteishi = 0;
function buttonAdd_choukiteishi() {
    rows_choukiteishi++;
    var No = document.getElementById("No").value;
    var taisakushubetsu_choukiteishi = $("#select_choukiteishi3").val();
    var taisakureberu_choukiteishi = $("#select_choukiteishi2").val();
    var youinshubetsu_choukiteishi = $("#select_choukiteishi1").val();
    var youin1_choukiteishi = $("#inputBox_choukiteishi1").val();
    var youin2_choukiteishi = $("#inputBox_choukiteishi2").val();
    var taisaku_choukiteishi = $("#inputBox_choukiteishi3").val();

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var markup = "<tr>" + "<input type='hidden' id='rowA" + rows_choukiteishi + "' value='" + rows_choukiteishi + "'>" +
        "<td id='rowBB" + No + rows_choukiteishi + "'style='text-align: center; height: 50px;'>" + taisakushubetsu_choukiteishi + "</td><td style='text-align: center' id='rowCC" + No + rows_choukiteishi + "'>" + taisakureberu_choukiteishi + "</td><td id='rowDD" + No + rows_choukiteishi + "' style='text-align: center'>" + youinshubetsu_choukiteishi + "</td><td id='rowEE" + No + rows_choukiteishi + "'>" + youin1_choukiteishi + "</td><td id='rowFF" + No + rows_choukiteishi + "'>" + youin2_choukiteishi + "</td><td id='rowGG" + No + rows_choukiteishi + "'>" + taisaku_choukiteishi + "<input type='hidden' name='youintaisaku_choukiteishi' value='2'></td></tr>";
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    //***************************************************
    //-----------------------1---------------------------
    if (tsuika_btn1_counter > 0) {
        $(".tableBody_teishikomoku_No1").append(markup);
        tsuika_btn1_counter = 0;

    }
    //-----------------------1---------------------------
    //***************************************************
    //------------------------2--------------------------
    if (tsuika_btn2_counter > 0) {
        $(".tableBody_teishikomoku_No2").append(markup);
        tsuika_btn2_counter = 0;

    }
    //------------------------2--------------------------
    //***************************************************
    //------------------------3--------------------------
    if (tsuika_btn3_counter > 0) {
        $(".tableBody_teishikomoku_No3").append(markup);
        tsuika_btn3_counter = 0;

    }
    //------------------------3--------------------------
    //***************************************************
    //------------------------4--------------------------
    if (tsuika_btn4_counter > 0) {
        $(".tableBody_teishikomoku_No4").append(markup);
        tsuika_btn4_counter = 0;

    }
    //------------------------4--------------------------
    //***************************************************
    //------------------------5--------------------------
    if (tsuika_btn5_counter > 0) {
        $(".tableBody_teishikomoku_No5").append(markup);
        tsuika_btn5_counter = 0;

    }
    //------------------------5--------------------------
    //***************************************************
    //------------------------6--------------------------
    if (tsuika_btn6_counter > 0) {
        $(".tableBody_teishikomoku_No6").append(markup);
        tsuika_btn6_counter = 0;

    }
    //------------------------6--------------------------
    //***************************************************
    //------------------------7--------------------------
    if (tsuika_btn7_counter > 0) {
        $(".tableBody_teishikomoku_No7").append(markup);
        tsuika_btn7_counter = 0;

    }
    //------------------------7--------------------------
    //***************************************************
    //------------------------8--------------------------
    if (tsuika_btn8_counter > 0) {
        $(".tableBody_teishikomoku_No8").append(markup);
        tsuika_btn8_counter = 0;

    }
    //------------------------8--------------------------
    //***************************************************
    //------------------------9--------------------------
    if (tsuika_btn9_counter > 0) {
        $(".tableBody_teishikomoku_No9").append(markup);
        tsuika_btn9_counter = 0;

    }
    //------------------------9--------------------------
    //***************************************************
    //------------------------10-------------------------
    if (tsuika_btn10_counter > 0) {
        $(".tableBody_teishikomoku_No10").append(markup);
        tsuika_btn10_counter = 0;

    }
    //------------------------10-------------------------
    //***************************************************

    $('input[name=youintaisaku_choukiteishi]').addClass("choukiteishi");

    $("#myModal_choukiteishi").hide();


    // -----------------------------------teishikomoku_No1--------------------------------------
    $("#teishikomoku_No1").on("click", function (e) {
        $("#tableEventShifter").focus();
    });

    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No1 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 1;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_1 = document.getElementById("miwariate_jikan_atai_1").textContent;

            var arai1 = miwariate_jikan_atai_1.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            } else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("select_choukiteishi1").value = "";
                document.getElementById("select_choukiteishi2").value = "";
                document.getElementById("select_choukiteishi3").value = "";

                document.getElementById("inputBox_choukiteishi1").value = "";
                document.getElementById("inputBox_choukiteishi2").value = "";
                document.getElementById("inputBox_choukiteishi3").value = "";

                var select1_1 = document.getElementById("rowDD1" + rowValue).textContent;
                var select2_1 = document.getElementById("rowCC1" + rowValue).textContent;
                var select3_1 = document.getElementById("rowBB1" + rowValue).textContent;

                var inpt1_1 = document.getElementById("rowEE1" + rowValue).textContent;
                var inpt2_1 = document.getElementById("rowFF1" + rowValue).textContent;
                var inpt3_1 = document.getElementById("rowGG1" + rowValue).textContent;

                $("#select_choukiteishi1 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1_1;
                }).prop('selected', true);

                $("#select_choukiteishi2 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2_1;
                }).prop('selected', true);

                $("#select_choukiteishi3 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3_1;
                }).prop('selected', true);

                document.getElementById("inputBox_choukiteishi1").value = inpt1_1
                document.getElementById("inputBox_choukiteishi2").value = inpt2_1
                document.getElementById("inputBox_choukiteishi3").value = inpt3_1

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }

            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end
        }


    });
    // -----------------------------------teishikomoku_No1--------------------------------------


    // -----------------------------------teishikomoku_No2--------------------------------------
    $("#teishikomoku_No2").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No2 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 2;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_2 = document.getElementById("miwariate_jikan_atai_2").textContent;

            var arai1 = miwariate_jikan_atai_2.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            } else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("select_choukiteishi1").value = "";
                document.getElementById("select_choukiteishi2").value = "";
                document.getElementById("select_choukiteishi3").value = "";

                document.getElementById("inputBox_choukiteishi1").value = "";
                document.getElementById("inputBox_choukiteishi2").value = "";
                document.getElementById("inputBox_choukiteishi3").value = "";

                var select1_2 = document.getElementById("rowDD2" + rowValue).textContent;
                var select2_2 = document.getElementById("rowCC2" + rowValue).textContent;
                var select3_2 = document.getElementById("rowBB2" + rowValue).textContent;

                var inpt1_2 = document.getElementById("rowEE2" + rowValue).textContent;
                var inpt2_2 = document.getElementById("rowFF2" + rowValue).textContent;
                var inpt3_3 = document.getElementById("rowGG2" + rowValue).textContent;

                $("#select_choukiteishi1 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1_2;
                }).prop('selected', true);

                $("#select_choukiteishi2 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2_2;
                }).prop('selected', true);

                $("#select_choukiteishi3 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3_2;
                }).prop('selected', true);

                document.getElementById("inputBox_choukiteishi1").value = inpt1_2;
                document.getElementById("inputBox_choukiteishi2").value = inpt2_2;
                document.getElementById("inputBox_choukiteishi3").value = inpt3_3;
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }

            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end
        }

    });
    // -----------------------------------teishikomoku_No2--------------------------------------

    // -----------------------------------teishikomoku_No3--------------------------------------
    $("#teishikomoku_No3").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No3 tbody tr").on("click", function (e) {


        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 3;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_3 = document.getElementById("miwariate_jikan_atai_3").textContent;

            var arai1 = miwariate_jikan_atai_3.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            } else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("select_choukiteishi1").value = "";
                document.getElementById("select_choukiteishi2").value = "";
                document.getElementById("select_choukiteishi3").value = "";

                document.getElementById("inputBox_choukiteishi1").value = "";
                document.getElementById("inputBox_choukiteishi2").value = "";
                document.getElementById("inputBox_choukiteishi3").value = "";

                var select1_3 = document.getElementById("rowDD3" + rowValue).textContent;
                var select2_3 = document.getElementById("rowCC3" + rowValue).textContent;
                var select3_3 = document.getElementById("rowBB3" + rowValue).textContent;

                var inpt1_3 = document.getElementById("rowEE3" + rowValue).textContent;
                var inpt2_3 = document.getElementById("rowFF3" + rowValue).textContent;
                var inpt3_3 = document.getElementById("rowGG3" + rowValue).textContent;

                $("#select_choukiteishi1 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1_3;
                }).prop('selected', true);

                $("#select_choukiteishi2 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2_3;
                }).prop('selected', true);

                $("#select_choukiteishi3 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3_3;
                }).prop('selected', true);

                document.getElementById("inputBox_choukiteishi1").value = inpt1_3;
                document.getElementById("inputBox_choukiteishi2").value = inpt2_3;
                document.getElementById("inputBox_choukiteishi3").value = inpt3_3;

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }

            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }


    });

    // -----------------------------------teishikomoku_No3--------------------------------------

    // -----------------------------------teishikomoku_No4--------------------------------------
    $("#teishikomoku_No4").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No4 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 4;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_4 = document.getElementById("miwariate_jikan_atai_4").textContent;

            var arai1 = miwariate_jikan_atai_4.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }
            else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("select_choukiteishi1").value = "";
                document.getElementById("select_choukiteishi2").value = "";
                document.getElementById("select_choukiteishi3").value = "";

                document.getElementById("inputBox_choukiteishi1").value = "";
                document.getElementById("inputBox_choukiteishi2").value = "";
                document.getElementById("inputBox_choukiteishi3").value = "";

                var select1_4 = document.getElementById("rowDD4" + rowValue).textContent;
                var select2_4 = document.getElementById("rowCC4" + rowValue).textContent;
                var select3_4 = document.getElementById("rowBB4" + rowValue).textContent;

                var inpt1_4 = document.getElementById("rowEE4" + rowValue).textContent;
                var inpt2_4 = document.getElementById("rowFF4" + rowValue).textContent;
                var inpt3_4 = document.getElementById("rowGG4" + rowValue).textContent;

                $("#select_choukiteishi1 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1_4;
                }).prop('selected', true);

                $("#select_choukiteishi2 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2_4;
                }).prop('selected', true);

                $("#select_choukiteishi3 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3_4;
                }).prop('selected', true);

                // document.getElementById("select1_chokotei").value = select1;
                // document.getElementById("select2_chokotei").value = select2;
                // document.getElementById("select3_chokotei").value = select3;
                document.getElementById("inputBox_choukiteishi1").value = inpt1_4;
                document.getElementById("inputBox_choukiteishi2").value = inpt2_4;
                document.getElementById("inputBox_choukiteishi3").value = inpt3_4;

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }


            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end
        }

    });
    // -----------------------------------teishikomoku_No4--------------------------------------

    // -----------------------------------teishikomoku_No5--------------------------------------
    $("#teishikomoku_No5").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No5 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 5;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_5 = document.getElementById("miwariate_jikan_atai_5").textContent;

            var arai1 = miwariate_jikan_atai_5.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }
            else {

                $("#myModal_choukiteishi").css('display', 'block');
                document.getElementById("select_choukiteishi1").value = "";
                document.getElementById("select_choukiteishi2").value = "";
                document.getElementById("select_choukiteishi3").value = "";

                document.getElementById("inputBox_choukiteishi1").value = "";
                document.getElementById("inputBox_choukiteishi2").value = "";
                document.getElementById("inputBox_choukiteishi3").value = "";

                var select1 = document.getElementById("rowDD5" + rowValue).textContent;
                var select2 = document.getElementById("rowCC5" + rowValue).textContent;
                var select3 = document.getElementById("rowBB5" + rowValue).textContent;

                var inpt1 = document.getElementById("rowEE5" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFF5" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGG5" + rowValue).textContent;

                $("#select_choukiteishi1 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select_choukiteishi2 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select_choukiteishi3 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);

                document.getElementById("inputBox_choukiteishi1").value = inpt1;
                document.getElementById("inputBox_choukiteishi2").value = inpt2;
                document.getElementById("inputBox_choukiteishi3").value = inpt3;
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }

            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }

    });
    // -----------------------------------teishikomoku_No5--------------------------------------

    // -----------------------------------teishikomoku_No6--------------------------------------
    $("#teishikomoku_No6").on("click", function (e) {
        $("#tableEventShifter").focus();
    });

    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No6 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 6;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_6 = document.getElementById("miwariate_jikan_atai_6").textContent;

            var arai1 = miwariate_jikan_atai_6.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            } else {

                $("#myModal_choukiteishi").css('display', 'block');
                document.getElementById("select_choukiteishi1").value = "";
                document.getElementById("select_choukiteishi2").value = "";
                document.getElementById("select_choukiteishi3").value = "";

                document.getElementById("inputBox_choukiteishi1").value = "";
                document.getElementById("inputBox_choukiteishi2").value = "";
                document.getElementById("inputBox_choukiteishi3").value = "";

                var select1 = document.getElementById("rowDD6" + rowValue).textContent;
                var select2 = document.getElementById("rowCC6" + rowValue).textContent;
                var select3 = document.getElementById("rowBB6" + rowValue).textContent;

                var inpt1 = document.getElementById("rowEE6" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFF6" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGG6" + rowValue).textContent;

                $("#select_choukiteishi1 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select_choukiteishi2 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select_choukiteishi3 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);
                document.getElementById("inputBox_choukiteishi1").value = inpt1;
                document.getElementById("inputBox_choukiteishi2").value = inpt2;
                document.getElementById("inputBox_choukiteishi3").value = inpt3;
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }

            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }


    });
    // -----------------------------------teishikomoku_No6--------------------------------------

    // -----------------------------------teishikomoku_No7--------------------------------------
    $("#teishikomoku_No7").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No7 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 7;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {


            var miwariate_jikan_atai_7 = document.getElementById("miwariate_jikan_atai_7").textContent;

            var arai1 = miwariate_jikan_atai_7.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            } else {

                $("#myModal_choukiteishi").css('display', 'block');
                document.getElementById("select_choukiteishi1").value = "";
                document.getElementById("select_choukiteishi2").value = "";
                document.getElementById("select_choukiteishi3").value = "";

                document.getElementById("inputBox_choukiteishi1").value = "";
                document.getElementById("inputBox_choukiteishi2").value = "";
                document.getElementById("inputBox_choukiteishi3").value = "";

                var select1 = document.getElementById("rowDD7" + rowValue).textContent;
                var select2 = document.getElementById("rowCC7" + rowValue).textContent;
                var select3 = document.getElementById("rowBB7" + rowValue).textContent;

                var inpt1 = document.getElementById("rowEE7" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFF7" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGG7" + rowValue).textContent;

                $("#select_choukiteishi1 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select_choukiteishi2 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select_choukiteishi3 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);


                document.getElementById("inputBox_choukiteishi1").value = inpt1;
                document.getElementById("inputBox_choukiteishi2").value = inpt2;
                document.getElementById("inputBox_choukiteishi3").value = inpt3;
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }


            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }

    });
    // -----------------------------------teishikomoku_No7--------------------------------------

    // -----------------------------------teishikomoku_No8--------------------------------------
    $("#teishikomoku_No8").on("click", function (e) {
        $("#tableEventShifter").focus();
    });

    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No8 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 8;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {


            var miwariate_jikan_atai_8 = document.getElementById("miwariate_jikan_atai_8").textContent;

            var arai1 = miwariate_jikan_atai_8.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            } else {

                $("#myModal_choukiteishi").css('display', 'block');
                document.getElementById("select_choukiteishi1").value = "";
                document.getElementById("select_choukiteishi2").value = "";
                document.getElementById("select_choukiteishi3").value = "";

                document.getElementById("inputBox_choukiteishi1").value = "";
                document.getElementById("inputBox_choukiteishi2").value = "";
                document.getElementById("inputBox_choukiteishi3").value = "";

                var select1 = document.getElementById("rowDD8" + rowValue).textContent;
                var select2 = document.getElementById("rowCC8" + rowValue).textContent;
                var select3 = document.getElementById("rowBB8" + rowValue).textContent;

                var inpt1 = document.getElementById("rowEE8" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFF8" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGG8" + rowValue).textContent;

                $("#select_choukiteishi1 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select_choukiteishi2 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select_choukiteishi3 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);

                document.getElementById("inputBox_choukiteishi1").value = inpt1;
                document.getElementById("inputBox_choukiteishi2").value = inpt2;
                document.getElementById("inputBox_choukiteishi3").value = inpt3;
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }


            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end
        }

    });
    // -----------------------------------teishikomoku_No8--------------------------------------

    // -----------------------------------teishikomoku_No9--------------------------------------
    $("#teishikomoku_No9").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No9 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 9;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {


            var miwariate_jikan_atai_9 = document.getElementById("miwariate_jikan_atai_9").textContent;

            var arai1 = miwariate_jikan_atai_9.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            } else {

                $("#myModal_choukiteishi").css('display', 'block');
                document.getElementById("select_choukiteishi1").value = "";
                document.getElementById("select_choukiteishi2").value = "";
                document.getElementById("select_choukiteishi3").value = "";

                document.getElementById("inputBox_choukiteishi1").value = "";
                document.getElementById("inputBox_choukiteishi2").value = "";
                document.getElementById("inputBox_choukiteishi3").value = "";

                var select1 = document.getElementById("rowDD9" + rowValue).textContent;
                var select2 = document.getElementById("rowCC9" + rowValue).textContent;
                var select3 = document.getElementById("rowBB9" + rowValue).textContent;

                var inpt1 = document.getElementById("rowEE9" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFF9" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGG9" + rowValue).textContent;

                $("#select_choukiteishi1 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select_choukiteishi2 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select_choukiteishi3 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);


                document.getElementById("inputBox_choukiteishi1").value = inpt1;
                document.getElementById("inputBox_choukiteishi2").value = inpt2;
                document.getElementById("inputBox_choukiteishi3").value = inpt3;
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }

            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }

    });
    // -----------------------------------teishikomoku_No9--------------------------------------

    // -----------------------------------teishikomoku_No10--------------------------------------
    $("#teishikomoku_No10").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No10 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 10;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {


            var miwariate_jikan_atai_10 = document.getElementById("miwariate_jikan_atai_10").textContent;

            var arai1 = miwariate_jikan_atai_10.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            } else {

                $("#myModal_choukiteishi").css('display', 'block');
                document.getElementById("select_choukiteishi1").value = "";
                document.getElementById("select_choukiteishi2").value = "";
                document.getElementById("select_choukiteishi3").value = "";

                document.getElementById("inputBox_choukiteishi1").value = "";
                document.getElementById("inputBox_choukiteishi2").value = "";
                document.getElementById("inputBox_choukiteishi3").value = "";

                var select1 = document.getElementById("rowDD10" + rowValue).textContent;
                var select2 = document.getElementById("rowCC10" + rowValue).textContent;
                var select3 = document.getElementById("rowBB10" + rowValue).textContent;

                var inpt1 = document.getElementById("rowEE10" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFF10" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGG10" + rowValue).textContent;

                $("#select_choukiteishi1 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select_choukiteishi2 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select_choukiteishi3 option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);


                document.getElementById("inputBox_choukiteishi1").value = inpt1;
                document.getElementById("inputBox_choukiteishi2").value = inpt2;
                document.getElementById("inputBox_choukiteishi3").value = inpt3;
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }

            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }

    });
    // -----------------------------------teishikomoku_No10--------------------------------------

}
// When click button delete choukiteishi
function buttonDelete_choukiteishi() {


    $("#teishikomoku_No1 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }

    });

    $("#teishikomoku_No2 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    });

    $("#teishikomoku_No3 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    });

    $("#teishikomoku_No4 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    });

    $("#teishikomoku_No5 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    });

    $("#teishikomoku_No6 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    });


    $("#teishikomoku_No7 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    });


    $("#teishikomoku_No8 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    });

    $("#teishikomoku_No9 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    })


    $("#teishikomoku_No10 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    });


    //---------------------------------------------
    $("#myModal_choukiteishi").hide();
    //---------------------------------------------

}
// When click button edit choukiteishi
function buttonEdit_choukiteishi() {
    var no = document.getElementById("No").value;
    var indexe = document.getElementById("rowA").value;
    var aa = document.getElementById("inputBox_choukiteishi1").value;
    var bb = document.getElementById("inputBox_choukiteishi2").value;
    var cc = document.getElementById("select_choukiteishi1").value;
    var dd = document.getElementById("inputBox_choukiteishi3").value;
    var ee = document.getElementById("select_choukiteishi2").value;
    var ff = document.getElementById("select_choukiteishi3").value;
    // No.1
    if (no == "1") {
        $("#rowAA1" + indexe).text(indexe);
        $("#rowBB1" + indexe).text(ff);
        $("#rowCC1" + indexe).text(ee);
        $("#rowDD1" + indexe).text(cc);
        $("#rowEE1" + indexe).text(aa);
        $("#rowFF1" + indexe).text(bb);

        document.getElementById('rowGG1' + indexe).innerHTML = dd + "<input type='hidden' name='youintaisaku_choukiteishi' value='2'>";

        $("#myModal_choukiteishi").css('display', 'none');

    }

    //No.2
    if (no == "2") {
        $("#rowAA2" + indexe).text(indexe);
        $("#rowBB2" + indexe).text(ff);
        $("#rowCC2" + indexe).text(ee);
        $("#rowDD2" + indexe).text(cc);
        $("#rowEE2" + indexe).text(aa);
        $("#rowFF2" + indexe).text(bb);

        document.getElementById('rowGG2' + indexe).innerHTML = dd + "<input type='hidden' name='youintaisaku_choukiteishi' value='2'>";

        $("#myModal_choukiteishi").css('display', 'none');

    }
    // No.3
    if (no == "3") {

        $("#rowAA3" + indexe).text(indexe);
        $("#rowBB3" + indexe).text(ff);
        $("#rowCC3" + indexe).text(ee);
        $("#rowDD3" + indexe).text(cc);
        $("#rowEE3" + indexe).text(aa);
        $("#rowFF3" + indexe).text(bb);

        document.getElementById('rowGG3' + indexe).innerHTML = dd + "<input type='hidden' name='youintaisaku_choukiteishi' value='2'>";

        $("#myModal_choukiteishi").css('display', 'none');

    }
    // No.4
    if (no == "4") {

        $("#rowAA4" + indexe).text(indexe);
        $("#rowBB4" + indexe).text(ff);
        $("#rowCC4" + indexe).text(ee);
        $("#rowDD4" + indexe).text(cc);
        $("#rowEE4" + indexe).text(aa);
        $("#rowFF4" + indexe).text(bb);

        document.getElementById('rowGG4' + indexe).innerHTML = dd + "<input type='hidden' name='youintaisaku_choukiteishi' value='2'>";

        $("#myModal_choukiteishi").css('display', 'none');

    }

    //No.5
    if (no == "5") {

        $("#rowAA5" + indexe).text(indexe);
        $("#rowBB5" + indexe).text(ff);
        $("#rowCC5" + indexe).text(ee);
        $("#rowDD5" + indexe).text(cc);
        $("#rowEE5" + indexe).text(aa);
        $("#rowFF5" + indexe).text(bb);

        document.getElementById('rowGG5' + indexe).innerHTML = dd + "<input type='hidden' name='youintaisaku_choukiteishi' value='2'>";

        $("#myModal_choukiteishi").css('display', 'none');

    }
    // No.6
    if (no == "6") {

        $("#rowAA6" + indexe).text(indexe);
        $("#rowBB6" + indexe).text(ff);
        $("#rowCC6" + indexe).text(ee);
        $("#rowDD6" + indexe).text(cc);
        $("#rowEE6" + indexe).text(aa);
        $("#rowFF6" + indexe).text(bb);

        document.getElementById('rowGG6' + indexe).innerHTML = dd + "<input type='hidden' name='youintaisaku_choukiteishi' value='2'>";

        $("#myModal_choukiteishi").css('display', 'none');

    }
    // No.7
    if (no == "7") {

        $("#rowAA7" + indexe).text(indexe);
        $("#rowBB7" + indexe).text(ff);
        $("#rowCC7" + indexe).text(ee);
        $("#rowDD7" + indexe).text(cc);
        $("#rowEE7" + indexe).text(aa);
        $("#rowFF7" + indexe).text(bb);

        document.getElementById('rowGG7' + indexe).innerHTML = dd + "<input type='hidden' name='youintaisaku_choukiteishi' value='2'>";

        $("#myModal_choukiteishi").css('display', 'none');

    }
    // No.8
    if (no == "8") {

        $("#rowAA8" + indexe).text(indexe);
        $("#rowBB8" + indexe).text(ff);
        $("#rowCC8" + indexe).text(ee);
        $("#rowDD8" + indexe).text(cc);
        $("#rowEE8" + indexe).text(aa);
        $("#rowFF8" + indexe).text(bb);

        document.getElementById('rowGG8' + indexe).innerHTML = dd + "<input type='hidden' name='youintaisaku_choukiteishi' value='2'>";

        $("#myModal_choukiteishi").css('display', 'none');

    }
    // No.9
    if (no == "9") {

        $("#rowAA9" + indexe).text(indexe);
        $("#rowBB9" + indexe).text(ff);
        $("#rowCC9" + indexe).text(ee);
        $("#rowDD9" + indexe).text(cc);
        $("#rowEE9" + indexe).text(aa);
        $("#rowFF9" + indexe).text(bb);

        document.getElementById('rowGG9' + indexe).innerHTML = dd + "<input type='hidden' name='youintaisaku_choukiteishi' value='2'>";

        $("#myModal_choukiteishi").css('display', 'none');

    }
    //No.10
    if (no == "10") {

        $("#rowAA10" + indexe).text(indexe);
        $("#rowBB10" + indexe).text(ff);
        $("#rowCC10" + indexe).text(ee);
        $("#rowDD10" + indexe).text(cc);
        $("#rowEE10" + indexe).text(aa);
        $("#rowFF10" + indexe).text(bb);

        document.getElementById('rowGG10' + indexe).innerHTML = dd + "<input type='hidden' name='youintaisaku_choukiteishi' value='2'>";

        $("#myModal_choukiteishi").css('display', 'none');

    }
}
// When click button cancel chokiteishi
function buttonCancel_choukiteishi() {
    $("#myModal_choukiteishi").hide();
}
//****************************************************************************************
// When click button add chokotei
var rows_chokotei = 0;
function buttonAdd_chokotei() {
    rows_chokotei++;
    var No = document.getElementById("No").value;
    //get the value from チョコ停 modal
    var taisakushubetsu_chokotei = $("#select1_chokotei").val();
    var taisakureberu_chokotei = $("#select2_chokotei").val();
    var youinshubetsu_chokotei = $("#select3_chokotei").val();
    var youin1_chokotei = $("#inputBox_chokotei1").val();
    var youin2_chokotei = $("#inputBox_chokotei2").val();
    var taisaku_chokotei = $("#inputBox_chokotei3").val();

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var markup = "<tr>" + "<input type='hidden' id='rowA" + rows_chokotei + "' value='" + rows_chokotei + "'>" +
        "<td id='rowBS" + No + rows_chokotei + "'style='text-align: center; height: 50px;'>" + taisakushubetsu_chokotei + "</td><td style='text-align: center' id='rowCS" + No + rows_chokotei + "'>" + taisakureberu_chokotei + "</td><td id='rowDS" + No + rows_chokotei + "' style='text-align: center'>" + youinshubetsu_chokotei + "</td><td id='rowES" + No + rows_chokotei + "'>" + youin1_chokotei + "</td><td id='rowFS" + No + rows_chokotei + "'>" + youin2_chokotei + "</td><td id='rowGS" + No + rows_chokotei + "'>" + taisaku_chokotei + "<input type='hidden' name='youintaisaku_chokotei' value='2'></td></tr>";
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //***************************************************
    //-----------------------1---------------------------
    if (tsuika_btn1_counter > 0) {
        $(".tableBody_teishikomoku_No1").append(markup);
        tsuika_btn1_counter = 0;

    }
    //-----------------------1---------------------------
    //***************************************************
    //------------------------2--------------------------
    if (tsuika_btn2_counter > 0) {

        $(".tableBody_teishikomoku_No2").append(markup);
        tsuika_btn2_counter = 0;

    }
    //------------------------2--------------------------
    //***************************************************
    //------------------------3--------------------------
    if (tsuika_btn3_counter > 0) {
        $(".tableBody_teishikomoku_No3").append(markup);
        tsuika_btn3_counter = 0;

    }
    //------------------------3--------------------------
    //***************************************************
    //------------------------4--------------------------
    if (tsuika_btn4_counter > 0) {
        $(".tableBody_teishikomoku_No4").append(markup);
        tsuika_btn4_counter = 0;

    }
    //------------------------4--------------------------
    //***************************************************
    //------------------------5--------------------------
    if (tsuika_btn5_counter > 0) {
        $(".tableBody_teishikomoku_No5").append(markup);
        tsuika_btn5_counter = 0;

    }
    //------------------------5--------------------------
    //***************************************************
    //------------------------6--------------------------
    if (tsuika_btn6_counter > 0) {

        $(".tableBody_teishikomoku_No6").append(markup);
        tsuika_btn6_counter = 0;

    }
    //------------------------6--------------------------
    //***************************************************
    //------------------------7--------------------------
    if (tsuika_btn7_counter > 0) {
        $(".tableBody_teishikomoku_No7").append(markup);
        tsuika_btn7_counter = 0;

    }
    //------------------------7--------------------------
    //***************************************************
    //------------------------8--------------------------
    if (tsuika_btn8_counter > 0) {
        $(".tableBody_teishikomoku_No8").append(markup);
        tsuika_btn8_counter = 0;

    }
    //------------------------8--------------------------
    //***************************************************
    //------------------------9--------------------------
    if (tsuika_btn9_counter > 0) {
        $(".tableBody_teishikomoku_No9").append(markup);
        tsuika_btn9_counter = 0;

    }
    //------------------------9--------------------------
    //***************************************************
    //------------------------10-------------------------
    if (tsuika_btn10_counter > 0) {
        $(".tableBody_teishikomoku_No10").append(markup);
        tsuika_btn10_counter = 0;

    }
    //------------------------10-------------------------
    //***************************************************

    $('input[name=youintaisaku_chokotei]').addClass("chokotei");


    $("#myModal_chokoteishi").css('display', 'none');


    // -----------------------------------teishikomoku_No1--------------------------------------
    $("#teishikomoku_No1").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No1 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 1;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_1 = document.getElementById("miwariate_jikan_atai_1").textContent;

            var arai1 = miwariate_jikan_atai_1.split(":");
            console.log(arai1);
            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');

                document.getElementById("select1_chokotei").value = "";
                document.getElementById("select2_chokotei").value = "";
                document.getElementById("select3_chokotei").value = "";
                document.getElementById("inputBox_chokotei1").value = "";
                document.getElementById("inputBox_chokotei2").value = "";
                document.getElementById("inputBox_chokotei3").value = "";

                var select1 = document.getElementById("rowBS1" + rowValue).textContent;
                var select2 = document.getElementById("rowCS1" + rowValue).textContent;
                var select3 = document.getElementById("rowDS1" + rowValue).textContent;
                var inpt1 = document.getElementById("rowES1" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFS1" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGS1" + rowValue).textContent;

                $("#select1_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select2_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select3_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);
                document.getElementById("inputBox_chokotei1").value = inpt1;
                document.getElementById("inputBox_chokotei2").value = inpt2;
                document.getElementById("inputBox_chokotei3").value = inpt3;

                document.getElementById("buttonEdit_chokotei").style.visibility = "visible";
                document.getElementById("buttonDelete_chokotei").style.visibility = "visible";
                document.getElementById("buttonAdd_chokotei").style.display = "none";
                document.getElementById("buttonCancel_chokotei").style.display = "inline";

            } else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";

            }

            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }

    });
    // -----------------------------------teishikomoku_No1--------------------------------------


    // -----------------------------------teishikomoku_No2--------------------------------------
    $("#teishikomoku_No2").on("click", function (e) {
        $("#tableEventShifter").focus();
    });

    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No2 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;
        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 2;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_2 = document.getElementById("miwariate_jikan_atai_2").textContent;

            var arai1 = miwariate_jikan_atai_2.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');

                document.getElementById("select1_chokotei").value = "";
                document.getElementById("select2_chokotei").value = "";
                document.getElementById("select3_chokotei").value = "";
                document.getElementById("inputBox_chokotei1").value = "";
                document.getElementById("inputBox_chokotei2").value = "";
                document.getElementById("inputBox_chokotei3").value = "";

                var select1 = document.getElementById("rowBS2" + rowValue).textContent;
                var select2 = document.getElementById("rowCS2" + rowValue).textContent;
                var select3 = document.getElementById("rowDS2" + rowValue).textContent;
                var inpt1 = document.getElementById("rowES2" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFS2" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGS2" + rowValue).textContent;

                $("#select1_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select2_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select3_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);
                document.getElementById("inputBox_chokotei1").value = inpt1;
                document.getElementById("inputBox_chokotei2").value = inpt2;
                document.getElementById("inputBox_chokotei3").value = inpt3;

                document.getElementById("buttonEdit_chokotei").style.visibility = "visible";
                document.getElementById("buttonDelete_chokotei").style.visibility = "visible";
                document.getElementById("buttonAdd_chokotei").style.display = "none";
                document.getElementById("buttonCancel_chokotei").style.display = "inline";

            } else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }

            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end


        }

    });
    // -----------------------------------teishikomoku_No2--------------------------------------
    //soi add code teshikomoku
    // -----------------------------------teishikomoku_No3--------------------------------------
    $("#teishikomoku_No3").on("click", function (e) {
        $("#tableEventShifter").focus();
    });
    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No3 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 3;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_3 = document.getElementById("miwariate_jikan_atai_3").textContent;

            var arai1 = miwariate_jikan_atai_3.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("select1_chokotei").value = "";
                document.getElementById("select2_chokotei").value = "";
                document.getElementById("select3_chokotei").value = "";
                document.getElementById("inputBox_chokotei1").value = "";
                document.getElementById("inputBox_chokotei2").value = "";
                document.getElementById("inputBox_chokotei3").value = "";

                var select1 = document.getElementById("rowBS3" + rowValue).textContent;
                var select2 = document.getElementById("rowCS3" + rowValue).textContent;
                var select3 = document.getElementById("rowDS3" + rowValue).textContent;
                var inpt1 = document.getElementById("rowES3" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFS3" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGS3" + rowValue).textContent;
                $("#select1_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select2_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select3_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);

                document.getElementById("inputBox_chokotei1").value = inpt1;
                document.getElementById("inputBox_chokotei2").value = inpt2;
                document.getElementById("inputBox_chokotei3").value = inpt3;

                document.getElementById("buttonEdit_chokotei").style.visibility = "visible";
                document.getElementById("buttonDelete_chokotei").style.visibility = "visible";
                document.getElementById("buttonAdd_chokotei").style.display = "none";
                document.getElementById("buttonCancel_chokotei").style.display = "inline";

            } else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }


            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }

    });
    // -----------------------------------teishikomoku_No3--------------------------------------
    // -----------------------------------teishikomoku_No4--------------------------------------
    $("#teishikomoku_No4").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く) teishikoumokuNo4
    $("#teishikomoku_No4 tbody tr").on("click", function (e) {


        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 4;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {


            var miwariate_jikan_atai_4 = document.getElementById("miwariate_jikan_atai_4").textContent;

            var arai1 = miwariate_jikan_atai_4.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');

                document.getElementById("select1_chokotei").value = "";
                document.getElementById("select2_chokotei").value = "";
                document.getElementById("select3_chokotei").value = "";
                document.getElementById("inputBox_chokotei1").value = "";
                document.getElementById("inputBox_chokotei2").value = "";
                document.getElementById("inputBox_chokotei3").value = "";

                var select1 = document.getElementById("rowBS4" + rowValue).textContent;
                var select2 = document.getElementById("rowCS4" + rowValue).textContent;
                var select3 = document.getElementById("rowDS4" + rowValue).textContent;
                var inpt1 = document.getElementById("rowES4" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFS4" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGS4" + rowValue).textContent;

                $("#select1_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select2_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select3_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);
                document.getElementById("inputBox_chokotei1").value = inpt1;
                document.getElementById("inputBox_chokotei2").value = inpt2;
                document.getElementById("inputBox_chokotei3").value = inpt3;

                document.getElementById("buttonEdit_chokotei").style.visibility = "visible";
                document.getElementById("buttonDelete_chokotei").style.visibility = "visible";
                document.getElementById("buttonAdd_chokotei").style.display = "none";
                document.getElementById("buttonCancel_chokotei").style.display = "inline";

            } else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }


            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }

    });
    // -----------------------------------teishikomoku_No4--------------------------------------/
    // -----------------------------------teishikomoku_No5--------------------------------------
    $("#teishikomoku_No5").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No5 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 5;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_5 = document.getElementById("miwariate_jikan_atai_5").textContent;

            var arai1 = miwariate_jikan_atai_5.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');

                document.getElementById("select1_chokotei").value = "";
                document.getElementById("select2_chokotei").value = "";
                document.getElementById("select3_chokotei").value = "";
                document.getElementById("inputBox_chokotei1").value = "";
                document.getElementById("inputBox_chokotei2").value = "";
                document.getElementById("inputBox_chokotei3").value = "";

                var select1 = document.getElementById("rowBS5" + rowValue).textContent;
                var select2 = document.getElementById("rowCS5" + rowValue).textContent;
                var select3 = document.getElementById("rowDS5" + rowValue).textContent;
                var inpt1 = document.getElementById("rowES5" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFS5" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGS5" + rowValue).textContent;


                $("#select1_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select2_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select3_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);

                document.getElementById("inputBox_chokotei1").value = inpt1;
                document.getElementById("inputBox_chokotei2").value = inpt2;
                document.getElementById("inputBox_chokotei3").value = inpt3;

                document.getElementById("buttonEdit_chokotei").style.visibility = "visible";
                document.getElementById("buttonDelete_chokotei").style.visibility = "visible";
                document.getElementById("buttonAdd_chokotei").style.display = "none";
                document.getElementById("buttonCancel_chokotei").style.display = "inline";

            } else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }


            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end
        }

    });
    // -----------------------------------teishikomoku_No5--------------------------------------
    // -----------------------------------teishikomoku_No6--------------------------------------
    $("#teishikomoku_No6").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No6 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 6;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_6 = document.getElementById("miwariate_jikan_atai_6").textContent;

            var arai1 = miwariate_jikan_atai_6.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("select1_chokotei").value = "";
                document.getElementById("select2_chokotei").value = "";
                document.getElementById("select3_chokotei").value = "";
                document.getElementById("inputBox_chokotei1").value = "";
                document.getElementById("inputBox_chokotei2").value = "";
                document.getElementById("inputBox_chokotei3").value = "";

                var select1 = document.getElementById("rowBS6" + rowValue).textContent;
                var select2 = document.getElementById("rowCS6" + rowValue).textContent;
                var select3 = document.getElementById("rowDS6" + rowValue).textContent;
                var inpt1 = document.getElementById("rowES6" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFS6" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGS6" + rowValue).textContent;

                $("#select1_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select2_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select3_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);
                document.getElementById("inputBox_chokotei1").value = inpt1;
                document.getElementById("inputBox_chokotei2").value = inpt2;
                document.getElementById("inputBox_chokotei3").value = inpt3;

                document.getElementById("buttonEdit_chokotei").style.visibility = "visible";
                document.getElementById("buttonDelete_chokotei").style.visibility = "visible";
                document.getElementById("buttonAdd_chokotei").style.display = "none";
                document.getElementById("buttonCancel_chokotei").style.display = "inline";

            } else {

                $("#myModal_choukiteishi").css('display', 'block');

            }

            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }

    });
    // -----------------------------------teishikomoku_No6--------------------------------------
    // -----------------------------------teishikomoku_No7--------------------------------------
    $("#teishikomoku_No7").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No7 tbody tr").on("click", function (e) {


        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 7;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_7 = document.getElementById("miwariate_jikan_atai_7").textContent;

            var arai1 = miwariate_jikan_atai_7.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("select1_chokotei").value = "";
                document.getElementById("select2_chokotei").value = "";
                document.getElementById("select3_chokotei").value = "";
                document.getElementById("inputBox_chokotei1").value = "";
                document.getElementById("inputBox_chokotei2").value = "";
                document.getElementById("inputBox_chokotei3").value = "";
                var select1 = document.getElementById("rowBS7" + rowValue).textContent;
                var select2 = document.getElementById("rowCS7" + rowValue).textContent;
                var select3 = document.getElementById("rowDS7" + rowValue).textContent;
                var inpt1 = document.getElementById("rowES7" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFS7" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGS7" + rowValue).textContent;
                $("#select1_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select2_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select3_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);

                document.getElementById("inputBox_chokotei1").value = inpt1;
                document.getElementById("inputBox_chokotei2").value = inpt2;
                document.getElementById("inputBox_chokotei3").value = inpt3;

                document.getElementById("buttonEdit_chokotei").style.visibility = "visible";
                document.getElementById("buttonDelete_chokotei").style.visibility = "visible";
                document.getElementById("buttonAdd_chokotei").style.display = "none";
                document.getElementById("buttonCancel_chokotei").style.display = "inline";

            } else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }


            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }

    });
    // -----------------------------------teishikomoku_No7--------------------------------------
    // -----------------------------------teishikomoku_No8--------------------------------------
    $("#teishikomoku_No8").on("click", function (e) {
        $("#tableEventShifter").focus();
    });


    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No8 tbody tr").on("click", function (e) {


        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 8;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_8 = document.getElementById("miwariate_jikan_atai_8").textContent;

            var arai1 = miwariate_jikan_atai_8.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("select1_chokotei").value = "";
                document.getElementById("select2_chokotei").value = "";
                document.getElementById("select3_chokotei").value = "";
                document.getElementById("inputBox_chokotei1").value = "";
                document.getElementById("inputBox_chokotei2").value = "";
                document.getElementById("inputBox_chokotei3").value = "";

                var select1 = document.getElementById("rowBS8" + rowValue).textContent;
                var select2 = document.getElementById("rowCS8" + rowValue).textContent;
                var select3 = document.getElementById("rowDS8" + rowValue).textContent;
                var inpt1 = document.getElementById("rowES8" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFS8" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGS8" + rowValue).textContent;
                $("#select1_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select2_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select3_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);
                document.getElementById("inputBox_chokotei1").value = inpt1;
                document.getElementById("inputBox_chokotei2").value = inpt2;
                document.getElementById("inputBox_chokotei3").value = inpt3;

                document.getElementById("buttonEdit_chokotei").style.visibility = "visible";
                document.getElementById("buttonDelete_chokotei").style.visibility = "visible";
                document.getElementById("buttonAdd_chokotei").style.display = "none";
                document.getElementById("buttonCancel_chokotei").style.display = "inline";

            } else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }


            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }

    });
    // -----------------------------------teishikomoku_No8--------------------------------------
    // -----------------------------------teishikomoku_No9--------------------------------------
    $("#teishikomoku_No9").on("click", function (e) {
        $("#tableEventShifter").focus();
    });

    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No9 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 9;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_9 = document.getElementById("miwariate_jikan_atai_9").textContent;

            var arai1 = miwariate_jikan_atai_9.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("select1_chokotei").value = "";
                document.getElementById("select2_chokotei").value = "";
                document.getElementById("select3_chokotei").value = "";
                document.getElementById("inputBox_chokotei1").value = "";
                document.getElementById("inputBox_chokotei2").value = "";
                document.getElementById("inputBox_chokotei3").value = "";
                var select1 = document.getElementById("rowBS9" + rowValue).textContent;
                var select2 = document.getElementById("rowCS9" + rowValue).textContent;
                var select3 = document.getElementById("rowDS9" + rowValue).textContent;
                var inpt1 = document.getElementById("rowES9" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFS9" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGS9" + rowValue).textContent;
                $("#select1_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select2_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select3_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);
                document.getElementById("inputBox_chokotei1").value = inpt1;
                document.getElementById("inputBox_chokotei2").value = inpt2;
                document.getElementById("inputBox_chokotei3").value = inpt3;

                document.getElementById("buttonEdit_chokotei").style.visibility = "visible";
                document.getElementById("buttonDelete_chokotei").style.visibility = "visible";
                document.getElementById("buttonAdd_chokotei").style.display = "none";
                document.getElementById("buttonCancel_chokotei").style.display = "inline";
            } else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }


            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }

    });
    // -----------------------------------teishikomoku_No9--------------------------------------
    // -----------------------------------teishikomoku_No10--------------------------------------
    $("#teishikomoku_No10").on("click", function (e) {
        $("#tableEventShifter").focus();
    });

    // クリック時、行選択(ヘッダ除く)
    $("#teishikomoku_No10 tbody tr").on("click", function (e) {

        //soi ahhihi
        var rowValue = $(this).children()[0].value;

        // console.log($(this));
        document.getElementById("rowA").value = rowValue;
        document.getElementById("No").value = 10;

        console.log($(this));
        if ($(this).children().last().children()[0].defaultValue == 2) {

            var miwariate_jikan_atai_10 = document.getElementById("miwariate_jikan_atai_10").textContent;

            var arai1 = miwariate_jikan_atai_10.split(":");
            console.log(arai1);

            var pt0 = parseInt(arai1[0]);
            var pt1 = parseInt(arai1[1]);

            if (pt0 == 0 && pt1 < 5) {

                $("#myModal_chokoteishi").css('display', 'block');
                document.getElementById("select1_chokotei").value = "";
                document.getElementById("select2_chokotei").value = "";
                document.getElementById("select3_chokotei").value = "";
                document.getElementById("inputBox_chokotei1").value = "";
                document.getElementById("inputBox_chokotei2").value = "";
                document.getElementById("inputBox_chokotei3").value = "";

                var select1 = document.getElementById("rowBS10" + rowValue).textContent;
                var select2 = document.getElementById("rowCS10" + rowValue).textContent;
                var select3 = document.getElementById("rowDS10" + rowValue).textContent;
                var inpt1 = document.getElementById("rowES10" + rowValue).textContent;
                var inpt2 = document.getElementById("rowFS10" + rowValue).textContent;
                var inpt3 = document.getElementById("rowGS10" + rowValue).textContent;
                $("#select1_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select1;
                }).prop('selected', true);

                $("#select2_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select2;
                }).prop('selected', true);

                $("#select3_chokotei option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == select3;
                }).prop('selected', true);

                document.getElementById("inputBox_chokotei1").value = inpt1;
                document.getElementById("inputBox_chokotei2").value = inpt2;
                document.getElementById("inputBox_chokotei3").value = inpt3;

                document.getElementById("buttonEdit_chokotei").style.visibility = "visible";
                document.getElementById("buttonDelete_chokotei").style.visibility = "visible";
                document.getElementById("buttonAdd_chokotei").style.display = "none";
                document.getElementById("buttonCancel_chokotei").style.display = "inline";

            } else {

                $("#myModal_choukiteishi").css('display', 'block');

                document.getElementById("inputBox_choukiteishi1").value = inpt1;
                document.getElementById("inputBox_choukiteishi2").value = inpt2;
                document.getElementById("inputBox_choukiteishi3").value = inpt3;
                document.getElementById("buttonEdit_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonDelete_choukiteishi").style.visibility = "visible";
                document.getElementById("buttonAdd_choukiteishi").style.display = "none";
                document.getElementById("buttonCancel_choukiteishi").style.display = "inline";
            }

            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            //soi end

        }

    });
    // -----------------------------------teishikomoku_No10--------------------------------------
}
// When click button cancel chokotei
function buttonCancel_chokotei() {
    $("#myModal_chokoteishi").css('display', 'none');
}
// When click button edit chokotei
function buttonEdit_chokotei() {
    var no = document.getElementById("No").value;
    var indexe = document.getElementById("rowA").value;
    var a = document.getElementById("inputBox_chokotei1").value;
    var b = document.getElementById("inputBox_chokotei2").value;
    var c = document.getElementById("select1_chokotei").value;
    var d = document.getElementById("inputBox_chokotei3").value;
    var e = document.getElementById("select2_chokotei").value;
    var f = document.getElementById("select3_chokotei").value;
    // No.1
    if (no == "1") {
        $("#rowAS1" + indexe).text(indexe);
        $("#rowBS1" + indexe).text(f);
        $("#rowCS1" + indexe).text(e);
        $("#rowDS1" + indexe).text(c);
        $("#rowES1" + indexe).text(a);
        $("#rowFS1" + indexe).text(b);
        document.getElementById('rowGS1' + indexe).innerHTML = d + "<input type='hidden' name='youintaisaku_chokotei' value='2'>";

        //---------------------------------------------
        $("#myModal_chokoteishi").css('display', 'none');

    }
    // No.2
    if (no == "2") {
        $("#rowAS2" + indexe).text(indexe);
        $("#rowBS2" + indexe).text(f);
        $("#rowCS2" + indexe).text(e);
        $("#rowDS2" + indexe).text(c);
        $("#rowES2" + indexe).text(a);
        $("#rowFS2" + indexe).text(b);

        document.getElementById('rowGS2' + indexe).innerHTML = d + "<input type='hidden' name='youintaisaku_chokotei' value='2'>";

        //---------------------------------------------
        $("#myModal_chokoteishi").css('display', 'none');

    }
    // No.3
    if (no == "3") {
        $("#rowAS3" + indexe).text(indexe);
        $("#rowBS3" + indexe).text(f);
        $("#rowCS3" + indexe).text(e);
        $("#rowDS3" + indexe).text(c);
        $("#rowES3" + indexe).text(a);
        $("#rowFS3" + indexe).text(b);
        document.getElementById('rowGS3' + indexe).innerHTML = d + "<input type='hidden' name='youintaisaku_chokotei' value='2'>";

        //---------------------------------------------
        $("#myModal_chokoteishi").css('display', 'none');

    }
    // No.4
    if (no == "4") {
        $("#rowAS4" + indexe).text(indexe);
        $("#rowBS4" + indexe).text(f);
        $("#rowCS4" + indexe).text(e);
        $("#rowDS4" + indexe).text(c);
        $("#rowES4" + indexe).text(a);
        $("#rowFS4" + indexe).text(b);

        document.getElementById('rowGS4' + indexe).innerHTML = d + "<input type='hidden' name='youintaisaku_chokotei' value='2'>";

        //---------------------------------------------
        $("#myModal_chokoteishi").css('display', 'none');

    }
    // No.5
    if (no == "5") {
        $("#rowAS5" + indexe).text(indexe);
        $("#rowBS5" + indexe).text(f);
        $("#rowCS5" + indexe).text(e);
        $("#rowDS5" + indexe).text(c);
        $("#rowES5" + indexe).text(a);
        $("#rowFS5" + indexe).text(b);

        document.getElementById('rowGS5' + indexe).innerHTML = d + "<input type='hidden' name='youintaisaku_chokotei' value='2'>";

        //---------------------------------------------
        $("#myModal_chokoteishi").css('display', 'none');

    }
    // No.6
    if (no == "6") {
        $("#rowAS6" + indexe).text(indexe);
        $("#rowBS6" + indexe).text(f);
        $("#rowCS6" + indexe).text(e);
        $("#rowDS6" + indexe).text(c);
        $("#rowES6" + indexe).text(a);
        $("#rowFS6" + indexe).text(b);

        document.getElementById('rowGS6' + indexe).innerHTML = d + "<input type='hidden' name='youintaisaku_chokotei' value='2'>";

        //---------------------------------------------
        $("#myModal_chokoteishi").css('display', 'none');

    }
    // No.7
    if (no == "7") {

        $("#rowAS7" + indexe).text(indexe);
        $("#rowBS7" + indexe).text(f);
        $("#rowCS7" + indexe).text(e);
        $("#rowDS7" + indexe).text(c);
        $("#rowES7" + indexe).text(a);
        $("#rowFS7" + indexe).text(b);

        document.getElementById('rowGS7' + indexe).innerHTML = d + "<input type='hidden' name='youintaisaku_chokotei' value='2'>";

        //---------------------------------------------
        $("#myModal_chokoteishi").css('display', 'none');

    }
    // No.8
    if (no == "8") {

        $("#rowAS8" + indexe).text(indexe);
        $("#rowBS8" + indexe).text(f);
        $("#rowCS8" + indexe).text(e);
        $("#rowDS8" + indexe).text(c);
        $("#rowES8" + indexe).text(a);
        $("#rowFS8" + indexe).text(b);

        document.getElementById('rowGS8' + indexe).innerHTML = d + "<input type='hidden' name='youintaisaku_chokotei' value='2'>";

        //---------------------------------------------
        $("#myModal_chokoteishi").css('display', 'none');

    }
    // No9
    if (no == "9") {

        $("#rowAS9" + indexe).text(indexe);
        $("#rowBS9" + indexe).text(f);
        $("#rowCS9" + indexe).text(e);
        $("#rowDS9" + indexe).text(c);
        $("#rowES9" + indexe).text(a);
        $("#rowFS9" + indexe).text(b);

        document.getElementById('rowGS9' + indexe).innerHTML = d + "<input type='hidden' name='youintaisaku_chokotei' value='2'>";

        //---------------------------------------------
        $("#myModal_chokoteishi").css('display', 'none');

    }
    // No.10
    if (no == "10") {

        $("#rowAS10" + indexe).text(indexe);
        $("#rowBS10" + indexe).text(f);
        $("#rowCS10" + indexe).text(e);
        $("#rowDS10" + indexe).text(c);
        $("#rowES10" + indexe).text(a);
        $("#rowFS10" + indexe).text(b);

        document.getElementById('rowGS10' + indexe).innerHTML = d + "<input type='hidden' name='youintaisaku_chokotei' value='2'>";

        //---------------------------------------------
        $("#myModal_chokoteishi").css('display', 'none');

    }

}
// When click button delete chokotei
function buttonDelete_chokotei() {
    $("#teishikomoku_No1 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }

    })
    //soi add code delete teshikomoku
    $("#teishikomoku_No2 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    })
    $("#teishikomoku_No3 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    })
    $("#teishikomoku_No4 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    })
    $("#teishikomoku_No5 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    })
    $("#teishikomoku_No6 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    })
    $("#teishikomoku_No7 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    })
    $("#teishikomoku_No8 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    })
    $("#teishikomoku_No9 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    })
    $("#teishikomoku_No10 tbody").find('tr[class="selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }
    })

    //---------------------------------------------
    $("#myModal_chokoteishi").css('display', 'none');
    //---------------------------------------------
}
//****************************************************************************************
//When tojiru_button onclick
function tojiru_button() {
    //---------------------------------------------
    $("#myModal").hide();
    //---------------------------------------------
}
$(document).ready(function () {
    getTime();
});
//soi add code
function getTime() {
    //Get time Stop time
    var length = localStorage.getItem('lengthSBV');
    if (length > 0) {

        $('tr.disabledtr').css('display', 'none');

    }
    var theFirtA;
    var theEndB;

    var c_temp_go = 0;
    for (var i = 0; i < length; i++) {
        var a = localStorage.getItem('A' + i);
        var b = localStorage.getItem('B' + i);
        var c = localStorage.getItem('C' + i);

        var c_temp_zen = c;
        // let[hours, minutes, seconds] = c_temp_zen.split(':');
        let [hours, minutes, seconds] = c_temp_zen.split(':');

        seconds_temp = hours * 60 * 60 + minutes * 60 + seconds * 1;

        c_temp_go = c_temp_go + seconds_temp;

        var d = localStorage.getItem('D' + i);
        // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        var html = "<tr>" +
            "<td style='text-align: left; padding-left: 10px' class='textColor text_align'>" + a + "</td>" +
            "<td style='text-align: left; padding-left: 10px' class='textColor text_align'>" + b + "</td>" +
            "<td style='text-align: left; padding-left: 10px' class='textColor text_align'>" + c + "</td>" +
            "<td style='text-align: left; padding-left: 10px' class='textColor' text-align:='' left=''>" + d + "</td>" +
            "</tr>";

        $('table.table_Div2 > tbody').append(html);

        if (i == 0) {
            theFirtA = a;
        }
        if (i == length - 1) {
            theEndB = b;
        }

    }

    $('#timeView1').text(theFirtA + " ~ " + theEndB);
    $('#dtmhien').css('display', 'none');

    if (length == 0) {
        $('#timeView1').css('display', 'none');
        $("#timeView0").text("7:13:30 ～ 7:21:00(7:30)");
    }

    var inTime2 = theFirtA;
    var outTime2 = theEndB;

    var hoursIn2 = inTime2.split(':', 1);
    var hoursOut2 = outTime2.split(':', 1);
    var minutesArrayIn2 = inTime2.split(':', 2);
    var minutesArrayOut2 = outTime2.split(':', 2);
    var secondArrayIn2 = inTime2.split(':', 3);
    var secondArrayOut2 = outTime2.split(':', 3);

    var minutesIn2 = minutesArrayIn2[1];
    var minutesOut2 = minutesArrayOut2[1];

    var secondIn2 = secondArrayIn2[2];
    var secondOut2 = secondArrayOut2[2];

    var hoursInInt2 = parseInt(hoursIn2);
    var hoursOutInt2 = parseInt(hoursOut2);

    var sumHours2 = "00";
    var sumMinutes2 = "00";
    var sumSecond2 = "00";

    if (secondOut2 >= secondIn2) {
        sumSecond2 = secondOut2 - secondIn2;
    } else {
        minutesOut2 -= 1;
        sumSecond2 = 60 - (secondIn2 - secondOut2);
    }
    if (minutesOut2 >= minutesIn2) {
        sumMinutes2 = minutesOut2 - minutesIn2;
    } else {
        hoursOutInt2 -= 1;
        sumMinutes2 = 60 - (minutesIn2 - minutesOut2);
    }
    if (hoursOutInt2 >= hoursInInt2) {
        sumHours2 = hoursOutInt2 - hoursInInt2;
    } else {
        sumHours2 = 24 - (hoursInInt2 - hoursOutInt2);
    }

    var vHour = ('00' + sumHours2).slice(-2);
    var vMin = ('00' + sumMinutes2).slice(-2);
    var vSec = ('00' + sumSecond2).slice(-2);

    // $('#timeView2').text("(" + vHour + ":" + vMin + ":" + vSec + ")");
    // end
    hour_temp = Math.floor(c_temp_go / 3600);
    c_temp_go_temp = c_temp_go - hour_temp * 60 * 60;
    minute_temp = Math.floor(c_temp_go_temp / 60);
    second_temp = Math.floor(c_temp_go_temp % 60);
    if (hour_temp < 10) {
        hour_temp = "0" + hour_temp;
    }

    if (minute_temp < 10) {
        minute_temp = "0" + minute_temp;
    }

    if (second_temp < 10) {
        second_temp = "0" + second_temp;
    }

    $("#miwariate_jikan_atai_1").text(hour_temp + ":" + minute_temp + ":" + second_temp);

    //20210430 miwariate_jikan_atai_1 = timeView2
    $('#timeView2').text("(" + hour_temp + ":" + minute_temp + ":" + second_temp + ")");
    //20210430 miwariate_jikan_atai_1 = timeView2  

    if (c_temp_go >= 300) {

        $("#teishijikan_kubun_1").text("長期停止");

    } else {

        $("#teishijikan_kubun_1").text("チョコ停");

    }

    $("#labelDefault").removeClass("displayDiv");
    $("#labelDefault").addClass("displayDiv");

    $("#labelDefault_goukei").text(hour_temp + ":" + minute_temp + ":" + second_temp);
    $("#labelDefault_goukei_hidden").text("00:00:00");
    $("#labelDefault_miwariate").text(hour_temp + ":" + minute_temp + ":" + second_temp);
}