document.write("<script type=\"text/javascript\" src=\"/js/lib/jquery.event.drag.js\"></script>");

webapp.namespace("kensaku");

webapp.kensaku.Function = {

    showWindow : function(buttonNode, windowId, url, option, pParam) {
        if (!option) {
            option = {};
        }
        var config = {};
        $.extend(config, option);

        var selectCallBack = config.selectCallBack;
        if (!selectCallBack) {
            selectCallBack = this.setKensaku;
            config.selectCallBack = selectCallBack;
        }
        var className = $(buttonNode).attr("class");
        var targetSelector = config.targetSelector;
        if (!targetSelector) {
            var tmpClassName = className.replace("imgButton", "");
            tmpClassName = tmpClassName.replace("kensaku", "");
            tmpClassName = tmpClassName.replace("active", "");
            tmpClassName = tmpClassName.replace("common-btn", "");
            tmpClassName = tmpClassName.replace("mini", "");
            tmpClassName = tmpClassName.replace("text-field__action", "");
            tmpClassName = tmpClassName.replace("button-icon-button", "");
            tmpClassName = tmpClassName.replace("button-icon-button--secondary", "");
            tmpClassName = tmpClassName.replace("button", "");
            tmpClassName = tmpClassName.replace(/d_\w*/g, "");
            tmpClassName = tmpClassName.replace(/btn_\w*/g, "");
            tmpClassName = tmpClassName.replace(/accesskey\w*/g, "");
            tmpClassName = f.trim(tmpClassName);
            if (tmpClassName) {
                targetSelector = "input[name='" + tmpClassName + "']";
                config.targetSelector = targetSelector;
            }
        }
        var $text;
        if (targetSelector) {
            var $tr = $(buttonNode).closest("tr");
            $text = $tr.find(targetSelector);
            if ($text.length === 0) {
                $text = $(targetSelector);
            }
        } else {
            $text = $(buttonNode).prev();
        }
        config.el = config.isMulti ? $(buttonNode) : $text;
        $text.triggerHandler("focus.cache");

        var replaceWindowId = config.replaceWindowId;

        if (url && url.match(/\%[s,d]/) && targetSelector) {
            var targetClassName = $(targetSelector).attr("class");
            var paramIndex = 1;
            while (url.match(/\%[s,d]/)) {
                var regexp = new RegExp("p" + paramIndex + "_([\\w\\(\\)@,]+)");
                if (targetClassName.match(regexp)) {
                    var name = RegExp.$1;
                    var param;
                    if (name.match(/^\d+$/)) {
                        param = name;
                    } else {
                        var $param;
                        if (matches = name.match(/(.+)@(.+)/)) {
                            name = matches[1];
                            var wrapperId = matches[2];
                            $param = $("#" + wrapperId + " input[name='" + name + "']," + "#" + wrapperId + " select[name='" + name + "']");
                        } else {
                            $param = $("input[name='" + name + "'],select[name='" + name + "']");
                        }
                        var type = $param.attr("type");
                        if (type == "radio" || type == "checkbox") {
                            param = $param.filter(":checked").val();
                        } else {
                            param = $param.val();
                        }
                    }
                    if (!param) {
                        param = "";
                    }
                    url = url.replace(/\%[s,d]/, encodeURIComponent(param));
                } else {
                    url = url.replace(/\%[s,d]/, "");
                }
                paramIndex++;
            }
        }

        var $window;
        if (replaceWindowId) {
            $window = $("#" + replaceWindowId);
        } else {
            $window = $("#" + windowId);
        }
        if ($window.length <= 0) {
            if (!url || url.match(/\%[s,d]/)) {
                return;
            }

            if (pParam) {
                $.ajax({
                    type : "POST",
                    data : pParam,
                    url: url,
                    cache: false,
                    success: function(html) {
                        kf._showWindowForSuccess(buttonNode, html, windowId, replaceWindowId, selectCallBack, config);
                    }
                });
            } else {
                $.ajax({
                    url: url,
                    cache: false,
                    success: function(html) {
                        kf._showWindowForSuccess(buttonNode, html, windowId, replaceWindowId, selectCallBack, config);
                    }
                });
            }
        } else {
            this._showWindow(buttonNode, $window);
        }
    },

    _showWindowForSuccess: function(buttonNode, html, windowId, replaceWindowId, selectCallBack, config) {
        var html = f.trim(html);
        if (!f.checkResponseText(html)) {
            return;
        }
        $("body").append(html);
        var $window = $("#" + windowId);
        kf._scrollTable($window);
        f.bindAll($window);
        if (replaceWindowId) {
            $window.attr("id", replaceWindowId);
            if (!kc.kensakuButtonNode[replaceWindowId]) {
                kc.kensakuButtonNode[replaceWindowId] = kc.kensakuButtonNode[windowId];
                kc.kensakuButtonNode[windowId] = null;
            }
        }
        var $draggable = $window.find("div.draggable").bind("drag", function(ev) {
            var scroll = f.scroll();
            $window.css({top:ev.offsetY - scroll.y, left:ev.offsetX - scroll.x});
            $(window).resize();
        });
        if ($draggable.width() <= 0) {
            $draggable.bind("xresize", function(ev) {
                $draggable.css("width", $window.width());
            }).triggerHandler("xresize");
        }
        if ((typeof selectCallBack) == "function") {
            if (config.isMulti) {
                kf._setSelectedValues($window, config);

                $window.find("button.kakutei").unbind("click").bind("click", function(ev) {
                    kf._setMultiValue($window);
                    var dispData = [];
                    if ($("input[name='selectedCd']").val()) {
                        var responseText = kf._downloadNameForMultiWindow($window);
                        if (responseText) {
                            dispData = eval("(" + responseText + ")");
                        }
                    }

                    selectCallBack(buttonNode, dispData);

                    config.el = $(buttonNode);
                    kf.hideWindow(this, config);
                });
            } else {
                $window.find("button.select").bind("click", function(ev) {
                    selectCallBack(this, config);
                });
            }
        }
        $window.find("button.cancel").bind("click", function(ev) {
            kf.hideWindow(this, config);
        });
        $window.find("input.kensaku").bind("click", function(ev, initFlag) {
            kf.search(this, config, initFlag);
        });
        $window.find("button.kensaku").bind("click", function(ev, initFlag) {
            kf.search(this, config, initFlag);
        });
        $window.find("a.returnTop").bind("focus", function(ev) {
            kf.focusReturnTop(this);
        });
        $window.find("a.returnBottom").bind("focus", function(ev) {
            kf.focusReturnBottom(this);
        });

        $window.find("a.initKensakuWindow").triggerHandler("click");
        kf._showWindow(buttonNode, $window);
    },

    _scrollTable : function($window) {
        var currentWindow = window;
        var topWindow = currentWindow.parent;
        for (var i = 0; i < 10; i++) {
            if (topWindow == currentWindow) {
                break;
            }
            currentWindow = topWindow;
            topWindow = currentWindow.parent;
        }
        var $kensakuTable = $window.find("table.kensakuTable");
        var $categoryMenu = $("div.category-menu", $window);
        var isIE7 = ($.browser.msie && $.browser.version == 7) ? true : false;
        if ($categoryMenu.length > 0) {
            var $activeCategory = $categoryMenu.find("a.nv_active");
            if ($activeCategory.length > 0) {
                $activeCategory.css("padding-right", "13px");
                var categoryId = $activeCategory.attr("data-category");
                if (categoryId != "0" && categoryId != "-3") {
                    var $span = $window.append('<span id="active-ghost" style="display:block;position:absolute;font-weight:bold;">' + $activeCategory.html() + '</span>').find("#active-ghost");
                    if ($span.width() < $activeCategory.width()) {
                        $activeCategory.find("span").removeAttr("title");
                    }
                    $span.remove();
                }
            }
            $categoryMenu.find("a:not(.nv_active)").each(function() {
                var categoryId = $(this).attr("data-category");
                if (categoryId == "0" || categoryId == "-3") {
                    return;
                }
                var $span = $window.append('<span id="active-ghost" style="display:block;position:absolute;">' + $(this).html() + '</span>').find("#active-ghost");
                if ($span.width() < $(this).width()) {
                    $(this).find("span").removeAttr("title");
                }
                $span.remove();
            });
            $kensakuTable.find("td.category span").each(function() {
                var $span = $window.append('<span id="active-ghost" style="display:block;position:absolute;">' + $(this).html() + '</span>').find("#active-ghost");
                if ($span.width() < $(this).width()) {
                    $(this).removeAttr("title");
                }
                $span.remove();
            });
        }
        if ($kensakuTable.length === 0) {
            kf._setCategoryMenuHeight($categoryMenu, topWindow, $window);
            return;
        }
        if ($categoryMenu.length > 0 && $kensakuTable.width() > 499) {
            var $toolTipItems = $kensakuTable.find("span.tooltipItem");
            if (isIE7) {
                $kensakuTable.find("thead th").css({"word-break" : "keep-all"});
            }
            var setOverFlowCss = function($selector, isIE7) {
                $selector.css({"word-wrap": "break-word", "word-break" : "break-all"});
                if (!isIE7 && !$selector.is("td")) {
                    $selector.css({"display": "block"});
                }
            };
            if ($toolTipItems.size() > 0) {
                $toolTipItems.each(function() {
                    var $toolTip = $(this);
                    if ($toolTip.width() > 210) {
                        setOverFlowCss($toolTip, isIE7);
                    }
                });
            } else {
                var nameColIndex = 2;
                if ($window.find("div.labelCount").size() > 0) {
                    nameColIndex = 3;
                }
                $kensakuTable.find("tr").each(function() {
                    $(this).find("td:eq(" + nameColIndex + ")").each(function() {
                        setOverFlowCss($(this), isIE7);
                    });
                });
            }
        }
        if ($categoryMenu.length > 0) {
            $window.find("div.result").css("margin-bottom", 0);
            if ($kensakuTable.width() > 499) {
                var increaseWidth = $kensakuTable.width() - 499;
                var resultWidth = 670 + increaseWidth;
                $window.find("div.draggable").css("width", resultWidth + 76);
                $window.find("div.result").css("width", resultWidth);
                $window.find("div.submitButton").css("width", resultWidth);
            } else {
                $window.find("div.draggable").css("width", 746);
                $window.find("div.result").css("width", 670);
                $window.find("div.submitButton").css("width", 670);
            }
        }
        var diffHeight;
        if (topWindow.$) {
            diffHeight = topWindow.$(topWindow).height() - $window.height() - 9;
        } else {
            diffHeight = $(topWindow).height() - $window.height();
        }
        if (diffHeight < 0) {
            var tableHeight =  ($categoryMenu.length > 0 ? Math.max($kensakuTable.height(), $categoryMenu.find("ul").height()) : $kensakuTable.height()) + diffHeight - 20;
            if (0 < tableHeight) {
                if ($categoryMenu.length > 0) {
                    tableHeight = tableHeight + 44;
                }
                if ($window.find(".labelCount").length > 0) {
                    tableHeight = tableHeight + $window.find(".labelCount").height();
                }
                f.scrollTable($kensakuTable, tableHeight);

                var scrolTableWidth = $window.find("table.kensakuTable").width();
                var cssTableWidth = parseInt($window.find("table.kensakuTable").css("width"));
                var increaseWidth = scrolTableWidth - cssTableWidth;
                if ($categoryMenu.length > 0) {
                    if (increaseWidth > 0) {
                        $window.find("div.draggable").css("width", $window.find("div.draggable").width() + 18 + increaseWidth);
                        $window.find("div.result").css("width", $window.find("div.result").width() + 18 + increaseWidth);
                        $window.find("div.submitButton").css("width", $window.find("div.submitButton").width() + 18 + increaseWidth);
                    } else {
                        $window.find("div.draggable").css("width", $window.find("div.draggable").width() + 18);
                        $window.find("div.result").css("width", $window.find("div.result").width() + 18);
                        $window.find("div.submitButton").css("width", $window.find("div.submitButton").width() + 18);
                    }
                } else {
                    if (increaseWidth > 0) {
                        $window.find("div.searchArea").css("width", scrolTableWidth);
                        $window.find("div.result > table").css("width", scrolTableWidth);
                        $window.find("div.scrollTable").prev().css("width", scrolTableWidth);
                        $window.find("div.submitButton").css("width", scrolTableWidth);
                    }
                }
            }
            kf._setCategoryMenuHeight($categoryMenu, topWindow, $window);
        }
        if ($categoryMenu.length == 0) {
            var windowWidth = $window.width();
            $window.find("div.draggable").css("width", windowWidth);
            $window.find("div.result").css("margin-bottom", 5);
        }
        if (kf.hasSentakuButtonHelpTooltip) {
            kf.sentakuButtonHelpTooltip();
        }
    },

    _setCategoryMenuHeight : function($categoryMenu, topWindow, $window) {
        if ($categoryMenu.lenght == 0) {
            return;
        }
        var $categoryMenuUl = $categoryMenu.find("ul");
        var categoryDiffHeight;
        if (topWindow.$) {
            categoryDiffHeight = topWindow.$(topWindow).height() - $window.height() - 9;
        } else {
            categoryDiffHeight = $(topWindow).height() - $window.height();
        }
        if (categoryDiffHeight < 0) {
            $categoryMenuUl.css("height", $categoryMenuUl.height() + categoryDiffHeight - 20);
        }
    },

    _scrollList : function($list, $prevList, prevScrollTop) {
        var $prevListElement = $prevList.find("li");
        var prevSelectedPosition = 0;
        for (var i = 0; i < $prevListElement.length; i++) {
            if ($prevListElement.eq(i).find("a.nv_active").length > 0) {
                prevSelectedPosition = i;
                break;
            }
        }
        var $listElement = $list.find("li");
        var selectedPosition = 0;
        for (var i = 0; i < $listElement.length; i++) {
            if ($listElement.eq(i).find("a.nv_active").length > 0) {
                selectedPosition = i;
                break;
            }
        }
        if (prevSelectedPosition == selectedPosition) {
            $list.scrollTop(prevScrollTop);
            return;
        }
        var page = Math.floor(selectedPosition / 19);
        $list.scrollTop(page * 532);
    },

    _showWindow : function(buttonNode, $window) {
        f.makeMask(true);
        var scroll = f.scroll();
        var windowWidth = $(window).width();
        var windowHeight = $(window).height();
        var kensakuWindowWidth = $window.width();
        var kensakuWindowHeight = $window.height();
        if (windowHeight < kensakuWindowHeight && window.name && kc.framesetRows) {
            var $frameset = window.parent.$("frameset");
            $.data($frameset.get(0), "defaultRows", $frameset.attr("rows"));
            var rows = kc.framesetRows;
            rows = rows.replace(kensakuWindowHeight);
            $frameset.attr("rows", rows);

            windowWidth = $(window).width();
            windowHeight = $(window).height();
        }
        var left = Math.floor((windowWidth - kensakuWindowWidth) / 2);
        var top  = Math.floor((windowHeight - kensakuWindowHeight) / 2);
        if (top < 0) {
            top = 0;
        }
        $window.css("top", top);
        if ($window.hasClass("modal-window")) {
            if ($window.find(".draggable").length > 0) {
                $window.css("left", left + kensakuWindowWidth / 2);
            }
        } else {
            $window.css("left", left);
        }
        var hasWrapper = $window.closest(".shonin_modal_wrapper").length !== 0;
        if (hasWrapper) {
            $window.closest(".shonin_modal_wrapper").css("visibility", "");
        } else {
            $window.css("visibility", "");
        }
        $window.addClass("activeKensakuWindow");
        var $input = $("input[type!=hidden]:visible", $window);
        kf.bindCheckboxByTableTdClickArea();
        if ($input.not(":disabled").length > 0) {
            $input.not(":disabled").get(0).focus();
        } else {
            var $button = $("button:visible:enabled", $window);
            if ($button.length > 0) {
                $button.get(0).focus();
            }
        }
        if (hasWrapper) {
            $window.closest(".shonin_modal_innerWrapper").scrollTop(0);
        }
    },

    hideWindow : function(buttonNode, option) {
        kf.editFavorite(buttonNode);
        var targetSelector = option.targetSelector;
        var winCacheFlag = option.cache;
        var $window = $(buttonNode).closest("div.kensakuWindow,div.modal-window,div.confirmWindow");
        var windowId = $window.attr("id");
        var $text = option.el;
        if (0 < $text.not(":hidden").length && $text.css("display") != "none") {
            var cacheFlag = $text.hasClass("cache");
            if (cacheFlag) {
                c.cacheFlag = false;
            }
            $text.get(0).focus();
            if (cacheFlag) {
                setTimeout(function() {
                    c.cacheFlag = true;
                }, 0);
            }
        } else if (option.focusTargetSelector) {
            $(option.focusTargetSelector).focus();
        } else if (!option.notFocusWhenHideWindow) {
            f.focusFirstElement();
        }
        var hasWrapper = $window.closest(".shonin_modal_wrapper").length !== 0;
        if (hasWrapper) {
            $window.closest(".shonin_modal_wrapper").css("visibility", "hidden");
        } else {
            $window.css("visibility", "hidden");
        }
        if (winCacheFlag) {
            $window.removeClass("activeKensakuWindow");
        } else {
            setTimeout(function() {
                if (hasWrapper) {
                    $window.closest(".shonin_modal_wrapper").remove();
                } else {
                    $window.remove();
                }
            }, 0);
        }

        if (window.name && kc.framesetRows) {
            var $frameset = window.parent.$("frameset");
            var defaultRows = $.data($frameset.get(0), "defaultRows");
            if (defaultRows) {
                $frameset.attr("rows", defaultRows);
            }
        }

        f.removeMask();
        if (option.scrollHeight != undefined) {
            $(window).scrollTop(option.scrollHeight);
            $("html").css("overflow", "");
        }
    },

    editFavorite : function(buttonNode) {
        var favoriteFlag = $("input[name=favoriteFlag]").val();
        if (favoriteFlag != 1) {
            return;
        }
        var $favorites = $("input[name='addFavorites'], input[name='deleteFavorites']");
        if ($favorites.length === 0) {
            return;
        }
        var $window = $(buttonNode).closest("div.kensakuWindow");
        var $form = $window.find("form");
        var action = $form.attr("action");
        var url = action.replace(/[^/]+\/?$/, "editFavorite");
        var param = "favoriteFlag" + "=" + encodeURIComponent(favoriteFlag);
        if ($form.attr("name") == "sapHanyoKensakuActionForm") {
            var hanyoTypeKbn = $form.find("input[name=hanyoTypeKbn]").val();
            param += "&hanyoTypeKbn=" + encodeURIComponent(hanyoTypeKbn);
        }
        for (var i = 0; i < $favorites.length; i++) {
            var $favorite = $favorites.eq(i);
            var name = $favorite.attr("name");
            var value = $favorite.val();
            param += "&" + name + "=" + encodeURIComponent(value);
        }
        var xmlHttpRequest = $.ajax({
            type: "POST",
            url: url,
            data: param,
            cache: false
        });
    },

    setKensaku : function(buttonNode, option) {
        var $span = $(buttonNode).closest("tr").find("span.masterKey");
        var cd = $span.text();
        if (cd) {
            var $text = option.el;
            $text.val(cd);
            var windowId = $(buttonNode).closest("div.kensakuWindow").attr("id");
            $("#" + windowId + " button.cancel").click();
            return $text;
        }
    },

    setKensakuYubin : function(buttonNode, option) {
        var $span = $("span.masterKey", $(buttonNode).closest("tr"));
        var cd = $span.text();
        var cdFront = cd.substr(0,3);
        var cdEnd = cd.substr(4,4);
        var $text = option.el;
        if (cdFront) {
            $text.val(cdFront);
        }
        if (cdEnd) {
            $text = $text.nextAll(".end");
            $text.val(cdEnd);
        }
        var windowId = $(buttonNode).closest("div.kensakuWindow").attr("id");
        $("#" + windowId + " input.cancel").click();
        return;
    },

    search : function(buttonNode, option, initFlag) {
        var $window = $(buttonNode).closest("div.kensakuWindow");
        if (option.isMulti) {
            this._setMultiValue($window);
        }
        if (typeof initFlag == "undefined") {
            initFlag = true;
        }
        var selectCallBack = option.selectCallBack;
        var targetSelector = option.targetSelector;
        var $form = $(buttonNode).closest("form");
        if (initFlag) {
            var $orderByProperty = $("input[name=orderByProperty]", $form);
            var $orderByDirection = $("input[name=orderByDirection]", $form);
            var $pageNum = $("input[name=pageNum]", $form);
            $orderByProperty.val($orderByProperty.prop("defaultValue"));
            $orderByDirection.val($orderByDirection.prop("defaultValue"));
            $pageNum.val($pageNum.prop("defaultValue"));
        }
        var url = $form.attr("action");
        var $componentList = $("[name]", $form);
        var param = "";
        for (var i = 0; i < $componentList.length; i++) {
            var $component = $componentList.eq(i);
            var name = $component.attr("name");
            var value = $component.val();

            if ($component.attr("type") == "radio" || $component.attr("type") == "checkbox") {
                if (!$component.prop("checked")) {
                    $component.prop("defaultChecked", false);
                    continue;
                } else {
                    $component.prop("defaultChecked", true);
                }
            } else {
                if (name != "orderByProperty" && name != "orderByDirection" && name != "pageNum") {
                    $component.prop("defaultValue", value);
                }
            }
            if (0 < param.length) {
                param += "&";
            }
            param += name + "=" + encodeURIComponent(value);
        }
        var favoriteFlag = $("input[name=favoriteFlag]").val();
        if (favoriteFlag == 1) {
            var $favorites = $("input[name='addFavorites'], input[name='deleteFavorites']");
            if ($favorites.length > 0) {
                for (var i = 0; i < $favorites.length; i++) {
                    var $favorite = $favorites.eq(i);
                    var name = $favorite.attr("name");
                    var value = $favorite.val();
                    if (0 < param.length) {
                        param += "&";
                    }
                    param += name + "=" + encodeURIComponent(value);
                }
            }
        }
        var xmlHttpRequest = $.ajax({
            type: "POST",
            url: url,
            data: param,
            cache: false,
            success: function(html) {
                var $result = $("div.result", $(html));
                var prevKensakuWindowHeight = $window.height();
                var $target = $("div.result", $window);
                var $prevCategoryList = $target.find("div.category-menu ul");
                var prevScrollTop = $prevCategoryList.scrollTop();
                $target.find("tr.tooltip, span.anbunTooltip").triggerHandler("mouseleave");
                $target.children().remove();
                if ((typeof selectCallBack) == "function") {
                    $("button.select", $result).bind("click", function(event) {
                        selectCallBack(this, option);
                    });
                }
                $target.append($result.children());
                var $categoryMenu = $("div.category-menu", $(html));
                $("div.searchForm", $window).removeClass("category-form");
                if ($categoryMenu.length > 0) {
                    if ($("input[name=categoryId]", $window).length <= 0) {
                        var inputCategory = "<input type=\"text\" name=\"categoryId\" style=\"display:none;\" />";
                        $("form", $window).append($(inputCategory));
                    }
                    $("div.searchForm", $window).addClass("category-form");
                    $target.css({"width" : "670px",
                                 "overflow-x:" : "hidden"});
                    $("div.labelCount", $window).css({"width" : "550px",
                                                           "text-align" : "center",
                                                           "padding-top" : "1px",
                                                           "margin-left" : "70px",
                                                           "clear" : "both"});
                    $("div.submitButton", $window).css({"width" : "666px",
                                                        "clear" : "both"});
                    var $categoryList = $target.find("div.category-menu ul");
                    if ($categoryList.get(0).scrollHeight > $categoryList.get(0).clientHeight) {
                        kf._scrollList($categoryList, $prevCategoryList, prevScrollTop);
                    }
                    if (favoriteFlag == 1) {
                        Favorite.init();
                    }
                } else {
                    $("input[name=categoryId]", $window).remove();
                    $target.css({"width" : "",
                                 "overflow-x:" : "visible"});
                    $("div.labelCount", $window).css({"text-align" : "center",
                                                           "padding-top" : "",
                                                           "margin-left" : "",
                                                           "clear" : ""});
                }
                $window.find("a.initKensakuWindow").triggerHandler("click");
                kf._scrollTable($window);
                f.bindAll($target);
                var $draggable = $window.find("div.draggable");
                $draggable.css("width", 0);
                if ($draggable.width() <= 0) {
                    $draggable.unbind("xresize").bind("xresize", function(ev) {
                        $draggable.css("width", $window.width());
                    }).triggerHandler("xresize");
                }
                var $categoryMenu = $("div.category-menu", $window);
                if ($categoryMenu.length > 0) {
                    var $kensakuTable = $window.find("table.kensakuTable");
                    if ($kensakuTable.width() > 499) {
                        var increaseWidth = $kensakuTable.width() - 499;
                        var resultWidth = 670 + increaseWidth;
                        $draggable.css("width", resultWidth + 76);
                        $window.find("div.result").css("width", resultWidth);
                        $window.find("div.submitButton").css("width", resultWidth);
                    } else {
                        $draggable.css("width", 746);
                        $window.find("div.result").css("width", 670);
                        $window.find("div.submitButton").css("width", 670);
                    }
                    if ($kensakuTable.parent("div").hasClass("scrollTable")) {
                        $draggable.css("width", $draggable.width() + 18);
                        $window.find("div.result").css("width", $window.find("div.result").width() + 18);
                        $window.find("div.submitButton").css("width", $window.find("div.submitButton").width() + 18);
                    }
                }

                var kensakuWindowHeight = $window.height();
                if (prevKensakuWindowHeight < kensakuWindowHeight) {
                    var scroll = f.scroll();
                    var windowHeight = $(window).height();
                    var top = Math.floor((windowHeight - kensakuWindowHeight) / 2);
                    if (top < 0) {
                        top = 0;
                    }
                    $window.css("top", top);
                }

                kf._setSelectedValues($window);
                var $inputControl = $("input[type!=hidden]:visible", $window);
                kf.bindCheckboxByTableTdClickArea();
                if ($inputControl.not(":disabled").length > 0) {
                    f.focusFirstElement($inputControl.not(":disabled"));
                } else {
                    var $button = $("button:visible:enabled", $window);
                    if ($button.length > 0) {
                        $button.get(0).focus();
                    }
                }
            }
        });
    },

    sort : function(anchorNode, propertyName) {
        var $window = $(anchorNode).closest("div.kensakuWindow");
        var $form = $("form", $window);
        this._resetValue($form);
        var $orderByProperty = $("input[name=orderByProperty]", $form);
        var changeDirectionFlag = $orderByProperty.val() == propertyName;
        $orderByProperty.val(propertyName);
        var $orderByDirection = $("input[name=orderByDirection]", $form);
        var orderByDirection;
        if (!changeDirectionFlag) {
            orderByDirection = "desc";
        } else if ($orderByDirection.val() == "desc") {
            orderByDirection = "asc";
        } else {
            orderByDirection = "desc";
        }
        $orderByDirection.val(orderByDirection);

        $("input[name=pageNum]", $form).val(0);
        var $search = $("input.kensaku,button.kensaku", $form);
        $search.triggerHandler("click", false);
    },

    pageFeed : function(anchorNode, pageNum) {
        var $window = $(anchorNode).closest("div.kensakuWindow");
        var $form = $("form", $window);
        this._resetValue($form);
        var $pageNum = $("input[name=pageNum]", $form);
        $pageNum.val(pageNum);
        var $search = $("input.kensaku,button.kensaku", $form);
        $search.triggerHandler("click", false);
    },

    allSentaku : function(checkNode) {
        var $allCheck = $(checkNode);
        var $window = $allCheck.closest("div.kensakuWindow,div.confirmWindow");
        var isChecked = $allCheck.prop("checked");
        var denpyoShukeiKbn = $("input[name='denpyoShukeiKbn']").val();
        $allCheck.closest("div.result").find("input[name='sentaku']").each(function() {
            $(this).prop("checked", isChecked);

            var pageCount = $window.find("input[name='sentaku']:checked").length;
            var newCount = kc.otherPageCdList.length + pageCount;
            $window.find("span.count").html(newCount);
            if ($window.attr("id") == "bumonKensakuWindow") {
                kf.checkLimitSelected(newCount);
            }
            if (denpyoShukeiKbn == 1 && $(this).hasClass( "isContainChild" )) {
                $(this).triggerHandler("click");
                $(checkNode).prop("checked", isChecked);
            }
        });
    },

    _resetValue : function($form) {
        $("[name]", $form).each(function() {
            var $component = $(this);
            var name = $component.attr("name");
            if ($component.attr("type") == "radio" || $component.attr("type") == "checkbox") {
                if ($component.prop("defaultChecked")) {
                    $component.prop("checked", true);
                } else {
                    $component.prop("checked", false);
                }
            } else if (name != "orderByProperty" && name != "orderByDirection" && name != "pageNum") {
                $component.val($component.prop("defaultValue"));
            }
        });
    },

    _setSelectedValues : function($window, config) {
        kc.otherPageCdList = [];
        var selectValues = [];
        var countNum = 0;
        var selected = $window.find("input[name='selectedCd']").val();
        var denpyoShukeiKbn = $window.find("input[name='denpyoShukeiKbn']").val();
        var belongSelected = "";
        if (selected) {
            selectValues = selected.split(kc.separator);

            var $sentakuList = $window.find("input[name='sentaku']");
            var thisPageCdList = [];
            for (var i = 0; i < $sentakuList.length; i++) {
                var $sentaku = $sentakuList.eq(i);
                if (0 <= $.inArray($sentaku.val(), selectValues)) {
                    $sentaku.prop("checked", true);
                    belongSelected += kc.separator + $sentaku.val();
                    countNum = selectValues.length;
                }
                thisPageCdList.push($sentaku.val());
            }
            if(selectValues.length === 1 && config && $.inArray(selectValues[0], thisPageCdList) < 0) {
                var targetSelector = config.targetSelector;
                if (targetSelector) {
                    var $span = $(targetSelector).nextAll("span");
                    if (f.trim($span.text()) != '') {
                        kc.otherPageCdList.push(selectValues[0]);
                    }
                }
            } else {
                for (var i = 0; i < selectValues.length; i++) {
                    if ($.inArray(selectValues[i], thisPageCdList) < 0) {
                        kc.otherPageCdList.push(selectValues[i]);
                    }
                }
            }
        }
        if (denpyoShukeiKbn == 1) {
            selected = belongSelected.substr(1);
            $window.find("input[name='selectedCd']").val(selected);
            kc.otherPageCdList = [];
        }
        $window.find("span.count").html(countNum);

        $window.find("input[name='sentaku']").bind("click", function(ev) {
            var pageCount = $window.find("input[name='sentaku']:checked").length;
            var newCount = kc.otherPageCdList.length + pageCount;
            $window.find("span.count").html(newCount);

            if ($window.find("input[name='sentaku']").length === pageCount) {
                $window.find("input[name='allSentaku']").prop("checked", true);
            } else {
                $window.find("input[name='allSentaku']").prop("checked", false);
            }

            if ($window.attr("id") == "bumonKensakuWindow") {
                kf.checkLimitSelected(newCount);
            }
        }).eq(0).triggerHandler("click");

        if ($window.find('table.isMultiSelectLayoutDisplay').length !== 0) {
            var $count = $(".count");
            $("input[name='sentaku']").each(function() {
                if ($(this).hasClass( "isContainChild" )) {
                    var bumonCd = $(this).val();
                    kf.bindCheckboxLayer(bumonCd, $count);
                }
            });
        }

        if (selected && denpyoShukeiKbn == 1) {
            var $sentakuList = $window.find("input[name='sentaku']");
            for (var i = 0; i < $sentakuList.length; i++) {
                var $sentaku = $sentakuList.eq(i);
                if (0 <= $.inArray($sentaku.val(), selectValues)) {
                    if ($sentaku.hasClass( "isContainChild" )) {
                        $sentaku.triggerHandler("click");
                    }
                }
            }
        }
    },

    _setMultiValue : function($window) {
        var selected = "";
        if (kc.otherPageCdList.length > 0) {
            for (var i = 0; i < kc.otherPageCdList.length; i++) {
                selected = selected + kc.separator + kc.otherPageCdList[i];
            }
            selected = selected.substr(1);
        }
        $window.find("input[name='sentaku']:checked").each(function() {
            selected = selected + kc.separator + $(this).val();
        });
        selected = kc.otherPageCdList.length === 0 ? selected.substr(1) : selected;
        $window.find("input[name='selectedCd']").val(selected);
    },

    _downloadNameForMultiWindow : function($window) {
        var $form = $window.find("form");
        var action = $form.attr("action");
        var url = action.replace(/[^/]+\/?$/, "downloadNameForMultiWindow");
        var $componentList = $form.find("input[name]");
        var param = f.getRequestParam($componentList);
        var xmlHttpRequest = $.ajax({
            type : "POST",
            data : param,
            url : url,
            cache: true,
            async: false
        });
        var responseText = xmlHttpRequest.responseText;
        responseText = f.trim(responseText);
        if (!f.checkResponseText(responseText)) {
            return;
        }

        return responseText;
    },

    defineKensaku : function(kensakuUrl, windowId, option, param) {

        return {

            kensakuUrl : kensakuUrl,
            windowId : windowId,
            option : option,
            param : param,

            search : function(buttonNode) {
                kf.showWindow(buttonNode, this.windowId, this.kensakuUrl, option, param);

                if (!option) {
                    option = {};
                }
                var replaceWindowId = option.replaceWindowId;
                if (replaceWindowId) {
                    kc.kensakuButtonNode[replaceWindowId] = buttonNode;
                } else {
                    kc.kensakuButtonNode[this.windowId] = buttonNode;
                }
            }

        };
    },

    setName : function(textNode, name, emptyMessage, targetSelector, targetWrapperSelector) {
        var $text = $(textNode);
        var $wrapper;
        if (targetWrapperSelector) {
            $wrapper = $text.closest(targetWrapperSelector);
        } else {
            $wrapper = $text.closest("tr");
        }
        var $target;
        if (!targetSelector) {
            $target = $wrapper.find("span.name");
        } else {
            $target = $wrapper.find(targetSelector);
            if ($target.length === 0) {
                $target = $(targetSelector);
            }
        }
        if (name || name == 0) {
            if (0 < $target.filter("[name]").length) {
                $target.val(name);
            } else {
                $target.text(name);
            }
        } else {
            if (0 < $target.filter("[name]").length) {
                $target.val("");
            } else {
                $target.html("&nbsp;");
            }
        }
    },

    defineDownload : function(nameKensakuUrl, propertyName, option) {

        return {

            cache : {},
            nameKensakuUrl : nameKensakuUrl,
            propertyName : propertyName,
            option : option,

            setName : function(textNode) {
                var $text = $(textNode);
                var nameAttr = $text.attr("name");

                var cd = $text.val();
                var targetSelector = null;
                var targetWrapperSelector = null;
                var downloadCallBack = null;
                if (this.option) {
                    targetSelector = this.option.targetSelector;
                    targetWrapperSelector = option.targetWrapperSelector;
                    downloadCallBack = this.option.downloadCallBack;
                }
                if (c.cacheFlag && $text.hasClass("cache")) {
                    if (c.cache[nameAttr] == cd) {
                        return;
                    }
                }
                if (cd) {
                    var url = this.nameKensakuUrl;
                    if (url && url.match(/\%[s,d].+\%[s,d]/)) {
                        var className = $(textNode).attr("class");
                        var paramIndex = 1;
                        while (url.match(/\%[s,d].+\%[s,d]/)) {
                            var regexp = new RegExp("p" + paramIndex + "_([\\w\\(\\)@,]+)");
                            if (className.match(regexp)) {
                                var name = RegExp.$1;
                                var param;
                                if (name.match(/^\d+$/)) {
                                    param = name;
                                } else {
                                    var $param;
                                    if (matches = name.match(/(.+)@(.+)/)) {
                                        name = matches[1];
                                        var wrapperId = matches[2];
                                        $param = $("#" + wrapperId + " input[name='" + name + "']," + "#" + wrapperId + " select[name='" + name + "']");
                                    } else {
                                        $param = $("input[name='" + name + "'],select[name='" + name + "']");
                                    }
                                    var type = $param.attr("type");
                                    if (type == "radio" || type == "checkbox") {
                                        param = $param.filter(":checked").val();
                                    } else if ($param.hasClass("kingaku")) {
                                        param = f.removeComma($param.val());
                                    } else {
                                        param = $param.val();
                                    }
                                }
                                if (!param) {
                                    param = "";
                                }
                                url = url.replace(/\%[s,d]/, encodeURIComponent(param));
                            } else {
                                url = url.replace(/\%[s,d]/, "");
                            }
                            paramIndex++;
                        }
                    }
                    url = url.replace(/\%[s,d]/, encodeURIComponent(cd));
                    this.sendRequest(url, textNode, cd);
                } else {
                    if ($.isArray(targetSelector)) {
                        for (var i = 0; i < targetSelector.length; i++) {
                            kf.setName(textNode, "", null, targetSelector[i], targetWrapperSelector);
                        }
                    } else {
                        kf.setName(textNode, "", null, targetSelector, targetWrapperSelector);
                    }
                    if (typeof downloadCallBack == "function") {
                        downloadCallBack(textNode, "", {});
                    }
                }
                if (0 < $text.filter(".cache").length) {
                    c.cache[nameAttr] = cd;
                }
            },

            setData : function(textNode) {
                var $text = $(textNode);
                var nameAttr = $text.attr("name");

                var cd = $text.val();
                var targetSelector = null;
                var targetWrapperSelector = null;
                var downloadCallBack = null;
                var submitFunction = null;
                if (this.option) {
                    targetSelector = this.option.targetSelector;
                    targetWrapperSelector = option.targetWrapperSelector;
                    downloadCallBack = this.option.downloadCallBack;
                    submitFunction = this.option.submitFunction;
                }

                if (cd) {
                    if (c.cacheFlag && $text.hasClass("cache")) {
                        if (c.cache[nameAttr] == cd) {
                            return;
                        }
                    }
                }

                if (typeof submitFunction == "function") {
                    submitFunction(textNode);

                    if (0 < $text.filter(".cache").length) {
                        c.cache[nameAttr] = cd;
                    }
                    return true;
                }

                if (cd) {
                    var url = this.nameKensakuUrl;
                    if (url && url.match(/\%[s,d].+\%[s,d]/)) {
                        var className = $(textNode).attr("class");
                        var paramIndex = 1;
                        while (url.match(/\%[s,d].+\%[s,d]/)) {
                            var regexp = new RegExp("p" + paramIndex + "_([\\w\\(\\)@,]+)");
                            if (className.match(regexp)) {
                                var name = RegExp.$1;
                                var param;
                                if (name.match(/^\d+$/)) {
                                    param = name;
                                } else {
                                    var $param;
                                    if (matches = name.match(/(.+)@(.+)/)) {
                                        name = matches[1];
                                        var wrapperId = matches[2];
                                        $param = $("#" + wrapperId + " input[name='" + name + "']," + "#" + wrapperId + " select[name='" + name + "']");
                                    } else {
                                        $param = $("input[name='" + name + "'],select[name='" + name + "']");
                                    }
                                    var type = $param.attr("type");
                                    if (type == "radio" || type == "checkbox") {
                                        param = $param.filter(":checked").val();
                                    } else if ($param.hasClass("kingaku")) {
                                        param = f.removeComma($param.val());
                                    } else {
                                        param = $param.val();
                                    }
                                }
                                if (!param) {
                                    param = "";
                                }
                                url = url.replace(/\%[s,d]/, encodeURIComponent(param));
                            } else {
                                url = url.replace(/\%[s,d]/, "");
                            }
                            paramIndex++;
                        }
                    }
                    url = url.replace(/\%[s,d]/, encodeURIComponent(cd));
                    this.sendRequest(url, textNode, cd);
                } else {
                    if ($.isArray(targetSelector)) {
                        for (var i = 0; i < targetSelector.length; i++) {
                            kf.setName(textNode, "", null, targetSelector[i], targetWrapperSelector);
                        }
                    } else {
                        kf.setName(textNode, "", null, targetSelector, targetWrapperSelector);
                    }
                    if (typeof downloadCallBack == "function") {
                        downloadCallBack(textNode, "", {});
                    }
                }
                if (0 < $text.filter(".cache").length) {
                    c.cache[nameAttr] = cd;
                }
            },

            sendRequest : function(url, textNode, cd) {
                var async = false;
                var targetSelector = null;
                var targetWrapperSelector = null;
                var downloadCallBack = null;
                var emptyMessage = null;
                if (this.option) {
                    if (typeof this.option.async == "undefined") {
                        async = false;
                    } else {
                        async = this.option.async;
                    }
                    targetSelector = this.option.targetSelector;
                    targetWrapperSelector = this.option.targetWrapperSelector;
                    downloadCallBack = this.option.downloadCallBack;
                    emptyMessage = this.option.emptyMessage;
                }
                var propertyName = this.propertyName;
                var cache = this.cache;
                if (async) {
                    var xmlHttpRequest = $.ajax({
                        url: url,
                        cache: false,
                        async: async,
                        success: function(html) {
                            var responseText = xmlHttpRequest.responseText;
                            responseText = f.trim(responseText);
                            if (!f.checkResponseText(responseText)) {
                                return;
                            }
                            var data = eval("("+responseText+")");
                            var propertyNameList = $.isArray(propertyName) ? propertyName : [propertyName];
                            var name;
                            for (var i = 0; i < propertyNameList.length; i++) {
                                var property = data[propertyNameList[i]];
                                kf.setName(textNode, property, emptyMessage, $.isArray(targetSelector) ? targetSelector[i] : targetSelector, targetWrapperSelector);
                                if (i == 0) {
                                    name = property;
                                }
                            }
                            if (name) {
                                cache[cd] = name;
                            }
                            if (typeof downloadCallBack == "function") {
                                downloadCallBack(textNode, name, data);
                            }
                        }
                    });
                } else {
                    f.makeLoader();
                    setTimeout(function() {
                        var xmlHttpRequest = $.ajax({
                            url: url,
                            cache: false,
                            async: async
                        });
                        var responseText = xmlHttpRequest.responseText;
                        responseText = f.trim(responseText);
                        if (!f.checkResponseText(responseText)) {
                            return;
                        }
                        var data = eval("("+responseText+")");
                        var propertyNameList = $.isArray(propertyName) ? propertyName : [propertyName];
                        var name;
                        for (var i = 0; i < propertyNameList.length; i++) {
                            var property = data[propertyNameList[i]];
                            kf.setName(textNode, property, emptyMessage, $.isArray(targetSelector) ? targetSelector[i] : targetSelector, targetWrapperSelector);
                            if (i == 0) {
                                name = property;
                            }
                        }
                        if (name) {
                            cache[cd] = name;
                        }
                        if (typeof downloadCallBack == "function") {
                            downloadCallBack(textNode, name, data);
                        }
                    }, 0);
                }
            }

        };
    },

    focusReturnTop : function(anchorNode) {
        var $window = $(anchorNode).closest("div.kensakuWindow,div.modal-window,div.confirmWindow");
        f.focusFirstElement(f.getFirstElement($window).not("a.returnBottom"));
    },

    focusReturnBottom : function(anchorNode) {
        var $window = $(anchorNode).closest("div.kensakuWindow,div.modal-window,div.confirmWindow");
        $("input.cancel,button.cancel", $window).get(0).focus();
    },

    tooltip : function(anchorNode) {
        var $window = $(anchorNode).closest("div.kensakuWindow");
        $window.find("tr.tooltip").tooltip({wrapper:$window});
    },

    anbunTooltip : function(anchorNode) {
        var $window = $(anchorNode).closest("div.kensakuWindow");
        var $tooltipTargetList = $window.find("span.anbunTooltip");
        var tpl = "<div class=\"anbunTooltipPanel\"></div>";
        $tooltipTargetList.each(function() {
            var $tooltipTarget = $(this);
            $tooltipTarget.removeData("tooltipContent");
            var tooltipContent = $tooltipTarget.attr("title");
            $tooltipTarget.removeAttr("title");
            if (!tooltipContent) {
                return;
            }
            var data = eval("(" + tooltipContent + ")");
            $tooltipTarget.data("tooltipContent", data.anbunTooltip);
        });
        $tooltipTargetList.bind("mouseenter", function(ev) {
            $(this).closest("tr.tooltip").triggerHandler("mouseleave");
            var $tooltipTarget = $(this);
            var tooltipContent = $tooltipTarget.data("tooltipContent");
            if (!tooltipContent) {
                return;
            }
            var $tooltip = $(tpl);

            var $content = $("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"anbunTooltipContent d_kensaku_table_contents\"></table>");
            var $header = $("<thead><tr><th class=\"cd\"><div>" + Locale["kensaku_code"] + "</div></th><th class=\"name\"><div>" + Locale["kensaku_name"] + "</div></th><th class=\"prop\"><div>" + Locale["kensaku_hiritsu"] + "</div></th></tr></thead>");
            $content.append($header);
            var $body = $("<tbody></tbody>");
            for (var i = 0; i < tooltipContent.length; i++) {
                var row = tooltipContent[i];
                var $tr = $("<tr><td class=\"cd\"><div class=\"d_widthTxt15\"></div></td><td class=\"name\"><div class=\"d_widthTxt40\"></div></td><td class=\"prop\"><div></div></td></tr>");
                $tr.find("td.cd div").text(row.anbunCode);
                $tr.find("td.name div").text(row.anbunName);
                $tr.find("td.prop div").text(f.addComma(row.anbunProp, 1) + " %");
                $body.append($tr);
            }
            $content.append($body);
            $tooltip.append($content);
            $tooltip.bind("mouseleave", function() {
                $tooltipTarget.triggerHandler("mouseleave");
            });

            var top = ev.pageY;
            var left = ev.pageX;
            $window.append($tooltip);
            var offset = $window.offset();
            top = top < offset.top ? 0 : top - offset.top;
            left = left < offset.left ? 0 : left - offset.left;

            $tooltip.css({
                "position" : "absolute",
                "top": (top + 10),
                "left": 0
            });

            var bodyWidth = $("body").width();
            var tooltipLeft = left + 20;
            var tooltipWidth = $tooltip.width();
            var rightEdgeFlag = false;
            if (bodyWidth <= (offset.left + tooltipLeft + tooltipWidth)) {
                $tooltip.css("left", bodyWidth - tooltipWidth - offset.left - 10);
                rightEdgeFlag = true;
            } else {
                $tooltip.css("left", tooltipLeft);
            }

            var windowHeight = $(window).height();
            var tooltipTop = top + 10;
            var tooltipHeight = $tooltip.height();
            if (windowHeight <= (offset.top + tooltipTop + tooltipHeight)) {
                if (rightEdgeFlag) {
                    tooltipTop = top - tooltipHeight - 10;
                } else {
                    tooltipTop = windowHeight - tooltipHeight - offset.top;
                }
                var scrollY = f.scroll().y;
                tooltipTop = Math.max(tooltipTop, - offset.top + scrollY);
                $tooltip.css("top", tooltipTop);
            }

            $tooltipTarget.data("tooltipPanel", $tooltip);
        }).bind("mouseleave", function(ev){
            var $tooltipTarget = $(this);
            var $tooltip = $tooltipTarget.data("tooltipPanel");
            if ($tooltip) {
                var tooltipOffset = $tooltip.offset();
                if (tooltipOffset.left <= ev.pageX && ev.pageX <= tooltipOffset.left + $tooltip.width()
                        && tooltipOffset.top <= ev.pageY && ev.pageY <= tooltipOffset.top + $tooltip.height()) {
                    return false;
                }
                $tooltip.remove();
            }
        });
    },

    bindCheckboxLayer : function(bumonCd, $countTag) {
        $parentCheckbox = $("input[type=checkbox][value='" + bumonCd + "']");
        $parentCheckbox.bind("click.bindCheckboxLayer",function() {
            $childCheckbox =  $("input[class*='sentaku(" + bumonCd + ")']");
            var denpyoShukeiKbn = $("input[name='denpyoShukeiKbn']").val();
            kc.checkboxClickFlag = true;
            if (denpyoShukeiKbn == 1) {
                if ($(this).is(":checked")) {
                    f.addDisabled($childCheckbox);
                    $childCheckbox.prop("checked",true).click();
                } else {
                    f.removeDisabled($childCheckbox);
                    $childCheckbox.prop("checked",false).click();
                }
            } else {
                if ($(this).is(":checked")) {
                    $childCheckbox.not(":disabled").prop("checked",true).click();
                } else {
                    $childCheckbox.prop("checked",false).click();
                }
            }
            $countTag.text($("input[name='sentaku']").filter("input:checked").length);
        });
    },

    bindCheckboxByTableTdClickArea : function() {
        $(".js_checkboxtabletdwindow").click(function(event) {
            if ($(event.target).is("input[type=checkbox]")) {
                return;
            }
            var $checkbox = $(this).find("input[type=checkbox]");
            if ($checkbox.is(":disabled")) {
                return;
            }
            $checkbox.prop("checked", !$checkbox.prop("checked"));
            $checkbox.triggerHandler("click");
            $checkbox.triggerHandler("change");
        });
    },

    changeLayout: function(anchorNode){
        var $window = $(anchorNode).closest("div.kensakuWindow");
        var $form = $("form", $window);
        var $search = $("input.kensaku,button.kensaku", $form);
        var value = $(anchorNode).val();
        $("input[name='displayMode']").val(value);
        var $presetTargetKbn = $("#presetTargetKbn", $form);
        if (value == 1) {
           $presetTargetKbn.hide();
        } else {
            $("#searchBox").css('display', 'flex');
            $("input[name='pageNum']").val("0");
            if ($("input[name='superGroupFlag']").val() == '1' && $("input[name='targetGroupFlag']").val() != '1') {
                $("input[name='orderByProperty']").val("groupId");
            } else {
                $("input[name='orderByProperty']").val("bumonCd");
            }
            $("input[name='orderByDirection']").val("asc");
            $presetTargetKbn.css('display', 'flex');
        }
        $search.triggerHandler("click", false);

    },

    switchSearchBumonMode : function(anchorNode, mode, switchFlag, targetCd, targetName, selectedBumonCd) {
        var $tr = $(anchorNode).closest("tr");
        $("input[name=" + switchFlag + "]").val(mode);
        $tr.hide();
        if(mode == 1) {
            var $trRange = $tr.next();
            $trRange.show();
            $trRange.find('input[type=text]:visible:first').focus();
        } else {
            var $trScope = $tr.prev();
            $trScope.show();
            BumonSelectMulti.loadDataMultiBumon($trScope.find('td.searchBumon'), targetCd, targetName, selectedBumonCd, true);
        }
    },

    setFoldShosai : function() {
        if ($("input[name='bumonShosaiKensakuFlag']").val() == 1) {
            $("tr.bumonReload .extend").click();
        } else {
            $("tr.bumonReload .fold").click();
        }
        if ($("input[name='hutanBumonShosaiKensakuFlag']").val() == 1) {
            $("tr.hutanBumonReload .extend").click();
        } else {
            $("tr.hutanBumonReload .fold").click();
        }
    },
    sentakuButtonHelpTooltip : function() {
        var tpl = "<div class=\"tooltipPanel tooltipNewDesign is-active\"></div>";
        if ($.browser.msie && $.browser.version.match(/^10|^9|^8|^7/)) {
            var $tooltipTargetList = $("td.sentakuButtonHelpTooltip");
            $("span.sentakuButtonHelpTooltip").remove();
        } else {
            var $tooltipTargetList = $("span.sentakuButtonHelpTooltip");
            $("td.sentakuButtonHelpTooltip").removeAttr("title");
            $("td.sentakuButtonHelpTooltip").removeClass("sentakuButtonHelpTooltip");
        }

        $tooltipTargetList.each(function() {
            var $tooltipTarget = $(this);
            $tooltipTarget.removeData("tooltipContent");
            var tooltipContent = $tooltipTarget.attr("title");
            $tooltipTarget.removeAttr("title");
            if (!tooltipContent) {
                return;
            }
            $tooltipTarget.data("tooltipContent", tooltipContent);
            if ($tooltipTarget.hasClass("mask")) {
                $tooltipTarget.css("top", $tooltipTarget.parent("td").outerHeight()/2 - 12);
                $tooltipTarget.parent("td").css("position", "relative");
            }
        });
        $tooltipTargetList.bind("mouseenter", function(ev) {
            var $tooltipTarget = $(this);
            var tooltipContent = $tooltipTarget.data("tooltipContent");
            if (tooltipContent.length <= 0) {
                return;
            }
            var $tooltip = $(tpl);

            var $content = $("<div class=\"tooltipContent tooltip__content\"></div>");
            $content.html(tooltipContent);
            $tooltip.append($content);

            var $window = $tooltipTarget.closest(".kensakuWindow");
            $window.append($tooltip);

            var offsetTop = $window[0].offsetTop;
            var offsetLeft = $window[0].offsetLeft;
            var tooltipTop = ev.pageY;
            var tooltipLeft = ev.pageX;
            var winScrollTop = 0;
            var winScrollLeft = 0;

            if ($("#d_denpyo").length >= 1) {
                winScrollTop = $(window).scrollTop();
                winScrollLeft = $(window).scrollLeft();
            }
            tooltipTop = tooltipTop < offsetTop ? 0 : tooltipTop - offsetTop - winScrollTop ;
            tooltipLeft = tooltipLeft < offsetLeft ? 0 : tooltipLeft - offsetLeft;

            $tooltip.css({
                "top": (tooltipTop + 10),
                "left": 0,
                "position": "absolute",
                "z-index": "11000"
            });

            var wrapperWidth = $("body").width();
            tooltipLeft = tooltipLeft + 10;
            var tooltipWidth = $tooltip.outerWidth(true);
            if(wrapperWidth <= (tooltipLeft + tooltipWidth)){
                $tooltip.css("left", (tooltipLeft - tooltipWidth - winScrollLeft));
            } else {
                $tooltip.css("left", tooltipLeft - winScrollLeft);
                var maxWidth = wrapperWidth - offsetLeft - tooltipLeft - winScrollLeft - 10;
                if(tooltipWidth + 1 > maxWidth){
                    $tooltip.css("width", maxWidth);
                } else {
                    $tooltip.css("width", tooltipWidth + 1);
                }
            }
            $tooltipTarget.data("tooltipPanel", $tooltip);
        }).bind("mouseleave", function(ev){
            var $tooltipTarget = $(this);
            var $tooltip = $tooltipTarget.data("tooltipPanel");
            if ($tooltip) {
                $tooltip.remove();
            }
        });
    },

    checkLimitSelected : function(count) {
        var limitSelectedBumon = kc.limitSelectedBumon;
        var $submitButton = $("#bumonKensakuWindow").find("button.kakutei");
        if (count > limitSelectedBumon) {
            f.addDisabled($submitButton);
            if (!$("span.exceedingLimit").length) {
                $("div.labelCount").find("div").append("<span class='exceedingLimit'>" + Locale["kensaku_exceedingLimit"].replace("{0}", limitSelectedBumon) + "</span>");
            }
        } else {
            $("span.exceedingLimit").remove();
            f.removeDisabled($submitButton);
        }
    },

    getKeyCode : function(ev) {
        if (!window.event) {
            return ev.keyCode;
        }
        return window.event.which ? window.event.which : window.event.keyCode;
    },

    keyPressEnter : function(){
        $("input[name='keyword']").unbind("keypress").bind("keypress", function(ev) {
            if (kf.getKeyCode(ev) == kc.ENTER) {
                var $window = $("div.kensakuWindow");
                var $form = $("form", $window);
                var $search = $("input.kensaku,button.kensaku", $form);
                $search.triggerHandler("click", true);
            }
        });
    }
};

webapp.kensaku.Constants = {

    kensakuButtonNode : {},

    framesetRows : null,

    otherPageCdList : [],

    separator : String.fromCharCode(0x1f),

    limitSelectedBumon : 10,

    ENTER: 13,

    checkboxClickFlag : false
};

var kf = webapp.kensaku.Function;
var kc = webapp.kensaku.Constants;

var BumonSelectMulti = {
    separator : String.fromCharCode(0x1f),
    kensakuInfo : {},
    downLoadMultibumonUrl: null,
    changeLayoutCallBack: null,


    getSelectDispHtml : function(dispData, selectedBumonCd) {
        if (dispData.length === 0) {
            $("input[name='" + selectedBumonCd + "']").val("");
            return "<div class=\"selectDispBox\"></div>";
        }
        var selectedCd = "";
        if ($("input[name='selectedCd']").length) {
            selectedCd = $("input[name='selectedCd']").val().split(BumonSelectMulti.separator);
        } else {
            selectedCd = $("input[name='" + selectedBumonCd + "']").val().split(BumonSelectMulti.separator);
        }
        var sortedSelectedCd = "";
        var html = "<div class=\"selectDispBox\" tabindex=\"-1\">";
        for (var i = 0; i < selectedCd.length; i++) {
            var values = null;
            if ($("input[name='displayMode']").val() != "0") {
                for (var j = 0; j < dispData.length; j++) {
                    if (selectedCd[i] == dispData[j].code) {
                        values = dispData[j];
                        break;
                    }
                }
            } else {
                values = dispData[i];
            }

            if (values != null && values.code != "" && values.name != "") {
                sortedSelectedCd = sortedSelectedCd + BumonSelectMulti.separator + values.code;
                var onclick = "BumonSelectMulti.deleteMultiBumonValue(this,'" + selectedBumonCd + "')";
                html = html + "<div class=\"dispItem\"><table class=\"d_innerTable\" cellspacing=\"0\" cellpadding=\"0\"><tr><td style=\"word-break:break-all;\">" + f.escapeHTML(values.name) +
                "</td><td><input type=\"button\" class=\"imgButton btn_deleteblock_m_tab\" style=\"margin:0px 15px 0px 2px;\" onclick=\"" +
                onclick +
                "\" " +
                " tabindex=\"-1\"/><input type=\"hidden\" name=\"code\" value=\"" +
                f.escapeHTML(values.code) +
                "\" /></td></tr></table></div>";
            }
        }
        if (sortedSelectedCd) {
            sortedSelectedCd = sortedSelectedCd.substr(1);
        }
        $("input[name='" + selectedBumonCd + "']").val(sortedSelectedCd);
        return html + "</div>";
    },

    showMultiBumonWindow : function(linkNode, targetCd, targetName, selectedBumonCd) {
        var kensakuInfo = BumonSelectMulti.kensakuInfo;
        var $searchBumonTd = $(linkNode).closest("td.searchBumon");
        var selected = $searchBumonTd.find("input[name='" + selectedBumonCd + "']").val();
        var url = kensakuInfo.kensakuUrl;
        var param = kensakuInfo.param.length > 0 ? kensakuInfo.param + "&" : "";
        param = param + "multiFlag=1&selectedCd=" + encodeURIComponent(selected);
        var option = {};
        option.isMulti = true;
        option.selectCallBack = function(linkNode, dispData) {
            BumonSelectMulti.displayData(linkNode, targetCd, targetName, dispData, selectedBumonCd, true);
            if ((typeof BumonSelectMulti.changeLayoutCallBack) === "function") {
                BumonSelectMulti.changeLayoutCallBack();
            }
        };
        kf.defineKensaku(url, kensakuInfo.windowName, option, param).search(linkNode);
    },

    displayData : function(linkNode, targetCd, targetName, dispData, selectedBumonCd) {
        var html = BumonSelectMulti.getSelectDispHtml(dispData, selectedBumonCd);
        var selectedCd = "";
        var selectedName = "";
        for (var i = 0; i < dispData.length; i++) {
            selectedCd = selectedCd + BumonSelectMulti.separator + dispData[i].code;
            selectedName = dispData[i].name;
        }
        if (selectedCd) {
            selectedCd = selectedCd.substr(1);
        }
        var $searchBumonTd = $(linkNode).closest("td.searchBumon");

        if (dispData.length === 1 || (dispData.length === 0)) {
            $searchBumonTd.find("div.selectDisp").hide();
            $searchBumonTd.find(targetCd).show();
            $searchBumonTd.find(targetName).show();
            setTimeout(function(){
                var $text = $(targetCd);
                $text.val(selectedCd);
                $(linkNode).attr('tabindex', '-1');
                $(linkNode).removeAttr("style");
                if ($text.is(":disabled")) {
                    $searchBumonTd.find(targetName).text(selectedName);
                }
                var cacheFlag = $text.hasClass("cache");
                if (cacheFlag) {
                    c.cacheFlag = false;
                }
                $text.get(0).focus();
                if (cacheFlag) {
                    setTimeout(function(){
                        c.cacheFlag = true;
                    }, 0);
                }
            }, 10);

        } else {
            $searchBumonTd.find("div.selectDisp").html(html);
            $searchBumonTd.find("div.selectDisp").show();
            $searchBumonTd.find(targetCd).val("").hide();
            $searchBumonTd.find(targetName).text("").hide();
            BumonSelectMulti.resizeIconSearch($searchBumonTd);
            $searchBumonTd.find('.btn_search_s').attr('tabindex', $(targetCd).attr('tabindex'));
        }
    },

    deleteMultiBumonValue : function(buttonNode, selectedBumonCd) {
        var $searchBumonTd = $(buttonNode).closest("td.searchBumon");
        var $dispItem = $(buttonNode).closest("div.dispItem");
        var code = $dispItem.find("input[name='code']").val();
        var $selectedBumon = $searchBumonTd.find("input[name='" + selectedBumonCd + "']");
        var newValues = "";
        var codes = $selectedBumon.val().split(BumonSelectMulti.separator);
        for (var i = 0; i < codes.length; i++) {
            var tempCode = codes[i];
            if (tempCode != code) {
                newValues = newValues + BumonSelectMulti.separator + tempCode;
            }
        }
        $dispItem.remove();
        if (newValues.length !== 0) {
            newValues = newValues.substr(1);
        }
        $selectedBumon.val(newValues);
        BumonSelectMulti.resizeIconSearch($searchBumonTd);
    },

    loadDataMultiBumon : function(linkNode, targetCd, targetName, selectedBumonCd, isFocus) {
        if($(linkNode).length === 0) {
            return;
        }
        var $searchBumonTr = $(linkNode).closest("tr");
        var $searchBumonTd = $searchBumonTr.find(".searchBumon");
        var selected = $searchBumonTr.find("input[name='" + selectedBumonCd + "']").val();
        var codes = selected.split(BumonSelectMulti.separator);

        if ((codes.length === 1 && f.trim($searchBumonTr.find(targetName).text()) == ""
            && f.trim($searchBumonTr.find('div.selectDisp').text()) == "" && selected != "")
            || (codes.length > 1 && f.trim($searchBumonTr.find('div.selectDisp').text()) == "")) {
            var url = BumonSelectMulti.downLoadMultibumonUrl;
            var param = "selectedCd=" + encodeURIComponent(selected);
            var xmlHttpRequest = $.ajax({
                type: "POST",
                data: param,
                url: url,
                cache: true,
                async: false
            });
            var responseText = xmlHttpRequest.responseText;
            responseText = f.trim(responseText);
            var dispData = [];
            dispData = eval("(" + responseText + ")");
            if (dispData.length > 0) {
                if (dispData.length === 1 && f.trim($searchBumonTr.find('div.selectDisp').text()) == "") {
                    $searchBumonTr.find("div.selectDisp").hide();
                    $searchBumonTr.find(targetCd).show();
                    $searchBumonTr.find(targetName).show();
                    $searchBumonTr.find(targetName).text(dispData[0].name);
                    $(targetCd).val(dispData[0].code);
                    $(targetCd)[0].defaultValue = dispData[0].code;
                } else {
                    var html = BumonSelectMulti.getSelectDispHtml(dispData, selectedBumonCd);
                    $searchBumonTr.find("div.selectDisp").html(html);
                    $searchBumonTr.find("div.selectDisp").show();
                    $searchBumonTr.find(targetCd).val("").hide();
                    $searchBumonTr.find(targetName).text("").hide();
                    BumonSelectMulti.resizeIconSearch($searchBumonTd);
                }
            } else {
                if (codes.length === 1) {
                    $(targetCd).val(selected);
                }
            }
        }

        if(isFocus) {
            if ($searchBumonTr.find(targetCd).is(":visible")) {
                $searchBumonTr.find('input[type=text]:visible:first').focus();
            } else {
                $searchBumonTd.find('.btn_search_s').get(0).focus();
            }
        }
        if ($searchBumonTr.find(targetCd).is(":visible")) {
            $searchBumonTd.find('.btn_search_s').attr('tabindex', "-1");
        } else {
            $searchBumonTd.find('.btn_search_s').attr('tabindex', $(targetCd).attr('tabindex'));
        }
    },

    resizeIconSearch: function(linkNode) {
        var $selectDisp = linkNode.find("div.selectDisp").clone();
        $selectDisp.css({
                position:   'absolute',
                visibility: 'hidden',
                display:    'block'
            });
        $('body').append($selectDisp);
        var optionHeight = $selectDisp.height();
        $selectDisp.remove();
        $(linkNode.find(".btn_search_s")).css({"margin-top": (optionHeight /2) - 7 +"px"});
    }
};
var Category = {
      chooseCategory : function(categoryLink, orderBy) {
          if (typeof orderBy === "undefined") {
              orderBy = "orderNo";
          }
          $("div.category-menu ul a").removeClass("nv_active");
          $(categoryLink).addClass("nv_active");
          var $window = $(categoryLink).closest("div.kensakuWindow");
          var $form = $("form", $window);
          kf._resetValue($form);
          $("input[name=pageNum]", $form).val("0");
          var categoryId = $(categoryLink).attr("data-category");
          $("input[name=categoryId]", $form).val(categoryId);
          $("input[name=orderByProperty]", $form).val(orderBy);
          $("input[name=orderByDirection]", $form).val("asc");
          var $search = $("input.kensaku,button.kensaku", $form);
          $search.triggerHandler("click", false);
      }
};

var MypatternKensaku = {
    init : function() {
        f.focusFirstElement();
        $("button.kakutei").unbind("click").bind("click", function(ev) {
            MasterKensaku._resetValue();
            var $form = $(this).closest("form");
            var action = $form.attr("action");
            $form.attr("action", action.replace(/[^/]+\/?$/, 'multiMeisaiView'));
            $form.submit();
        });
        if (window.opener && window.opener.Denpyo) {
            $(window).one("unload", function() {
                try {
                    window.opener.Mypattern.afterCloseMypatternForm();
                } catch (e) {
                }
            });

            window.opener.Mypattern.afterOpenMypatternForm();

            $("button.close").one("click", function(ev) {
                window.opener.Mypattern.closeMypatternForm();
            });
            $("input[name='" + "kakutei" + "']").filter("input:checked").closest("tr").addClass("checked");
        }
    }
};

var Favorite = {
    init : function() {
        Favorite.chooseFavorite();
        Favorite.changeStatusActiveSearchArea();
    },
    chooseFavorite : function() {
        $(".btn_mini_favorite.d_btn_selected.darkblue").attr('title', Locale["favorite_remove"]);
        $(".btn_mini_favorite.lightgrey").attr('title', Locale["favorite_register"]);
        $(".btn_mini_favorite").bind('click', function(event) {
            event.preventDefault();
            var $favorite = $(this).children();
            var favoriteCd = f.escapeHTML($(this).parent().siblings(".cd").find("span").text());
            if ($(this).hasClass('d_btn_selected')) {
                $(this).removeClass('d_btn_selected darkblue').addClass("lightgrey").attr('title', Locale["favorite_register"]);
                if ($favorite.val() != undefined) {
                    $(this).find("input").remove();
                } else {
                    $(this).append("<input type=\"hidden\" name=\"deleteFavorites\" value=\"" + favoriteCd + "\"/>");
                }
            } else {
                $(this).removeClass('lightgrey').addClass("d_btn_selected darkblue").attr('title', Locale["favorite_remove"]);
                if ($favorite.val() != undefined) {
                    $(this).find("input").remove();
                } else {
                    $(this).append("<input type=\"hidden\" name=\"addFavorites\" value=\"" + favoriteCd + "\"/>");
                }
            }
        });
    },

    changeStatusActiveSearchArea : function() {
        var favoriteFlag = $("input[name=favoriteFlag]").val();
        if (favoriteFlag != 1) {
            return;
        }
        var categoryId = $("input[name=categoryId]").val();
        var favoriteCount = $("input[name=favoriteCount]").val();
        if (categoryId == '-3' || (categoryId == "" && favoriteCount > 0)) {
            $('.btn-search-m').addClass("disableButton").prop("disabled", true);
            $("input[name=keyword]").addClass("d_FormNoWrite").prop("disabled", true);
        } else {
            $('.btn-search-m').removeClass("disableButton").prop("disabled", false);
            $("input[name=keyword]").removeClass("d_FormNoWrite").prop("disabled", false);
        }
    }
};

CatalogOrderKensaku = {
    kakuteiForKeisei : function(denpyoNo, dispDenno) {
        window.opener.KeiseiDenpyo.downloadShinseiDenpyoByCatalogOrderId(denpyoNo, dispDenno);
    },

    kakuteiForKeishin : function(catalogOrderId, maxCatalogMeisai, remainingCatalogMeisai, catalogMeisaiNo) {
        window.opener.KeishinDenpyo.inputCatalogOrderMeisai(catalogOrderId, maxCatalogMeisai, remainingCatalogMeisai, catalogMeisaiNo);
    }
};
