////////////////////////////////setDefaultDayIsToday////////////////////////////////
// $(document).ready(function () {

//     var dateControl1 = document.querySelector('input[id="date1"]');
//     var dateControl2 = document.querySelector('input[id="date2"]');

//     ///////////////////////////////////
//     var getToday = new Date();
//     var dd = String(getToday.getDate()).padStart(2, '0');
//     var mm = String(getToday.getMonth() + 1).padStart(2, '0'); //January is 0!
//     var yyyy = getToday.getFullYear();
//     ///////////////////////////////////
//     getToday = yyyy + '-' + mm + '-' + dd;
//     dateControl1.value = getToday;
//     dateControl2.value = getToday;

// });
// ------------------------------------------------------------------
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/date
// https://stackoverflow.com/questions/1531093/how-do-i-get-the-current-date-in-javascript
////////////////////////////////setDefaultDayIsToday////////////////////////////////
// -------------------------------------------------------------------------------//
//////////////////////////////////////theModal//////////////////////////////////////

// ------------------------------------------------------
// https://www.w3schools.com/howto/howto_css_modals.asp
//////////////////////////////////////theModal//////////////////////////////////////

// テーブルクリック時、テキストボックスにフォーカスをセット
$("#myTable1").on("click", function (e) {
    $("#tableEventShifter").focus();
});
$("#myTable2").on("click", function (e) {
    $("#tableEventShifter").focus();
});
$("#myTable3").on("click", function (e) {
    $("#tableEventShifter").focus();
});
$("#myTable4").on("click", function (e) {
    $("#tableEventShifter").focus();
})
$("#myTable5").on("click", function (e) {
    $("#tableEventShifter").focus();
});
$("#myTable7").on("click", function (e) {
    $("#tableEventShifter").focus();
});

$(document).ready(function () {
    $("#denpyoKensakuInputArea").slideToggle();
    $(".btn_minus_s").css("display", "none");
    $(".btn_plus_s").css("display", "table-row");
    ///////////////////////////////////////////////
    $(".tab_wrap input[type=radio]").each(function (index, element) {
        // DOMに関する処理
        console.log(element);
        // var valueCheckbox = $(this).val();

        if (this.value == "1") {

            $("#childList").append('<li><span>生産実績</span></li>')
        }
    });
    // クリック時、行選択(ヘッダ除く)
    $("#myTable1 tbody tr").on("click", function (e) {
        // 選択されている他の行があれば選択解除
        $(this).siblings().removeClass("selected");
        // クリックされた行を選択
        $(this).addClass("selected");
    });
    $("#myTable2 tbody tr").on("click", function (e) {
        // 選択されている他の行があれば選択解除
        $(this).siblings().removeClass("selected");
        // クリックされた行を選択
        $(this).addClass("selected");
        //
        localStorage.setItem('keyCancel', '0');
        var index0 = $(this).children()[0].textContent;
        var index1 = $(this).children()[1].textContent;
        var index2 = $(this).children()[2].textContent;
        var index3 = $(this).children()[3].textContent;
        var index4 = $(this).children()[4].textContent;
        var index5 = $(this).children()[5].textContent;

        localStorage.setItem("goukiCL", index0);
        localStorage.setItem("jisshibiCL", index1);
        localStorage.setItem("kimuCL", index2);
        localStorage.setItem("tenkenCL", index3);
        localStorage.setItem("SLCL", index4);
        localStorage.setItem("TLCL", index5);

        window.location.href = "InspectionResultReference.html";
    });
    $("#myTable3 tbody tr").on("click", function (e) {
        // 選択されている他の行があれば選択解除
        $(this).siblings().removeClass("selected");
        // クリックされた行を選択
        $(this).addClass("selected");
        localStorage.setItem('keyHozen', '0');
        var index01 = $(this).children()[0].textContent;
        var index11 = $(this).children()[1].textContent;
        var index21 = $(this).children()[2].textContent;
        var index31 = $(this).children()[3].textContent;
        var index41 = $(this).children()[4].textContent;
        var index51 = $(this).children()[5].textContent;

        localStorage.setItem("goukihouzen", index01);
        localStorage.setItem("jisshibihouzen", index11);
        localStorage.setItem("kimuhozen", index21);
        localStorage.setItem("tenkenhozen", index31);
        localStorage.setItem("SLhozen", index41);
        localStorage.setItem("TLhozen", index51);

        window.location.href = "ConservationPlanReference1.html";
    });
    $("#myTable4 tbody tr").on("click", function (e) {
        // 選択されている他の行があれば選択解除
        $(this).siblings().removeClass("selected");
        // クリックされた行を選択
        $(this).addClass("selected");
        localStorage.setItem('keyHozenkekka', '0');
        window.location.href = "MaintenanceResultConfirm.html";
    });
    $("#myTable5 tbody tr").on("click", function (e) {
        // 選択されている他の行があれば選択解除
        $(this).siblings().removeClass("selected");
        // クリックされた行を選択
        $(this).addClass("selected");
    });
    $("#myTable7 tbody tr").on("click", function (e) {
        // 選択されている他の行があれば選択解除
        $(this).siblings().removeClass("selected");
        // クリックされた行を選択
        $(this).addClass("selected");
    });
});
// 確認済みを判定する文字列
var check = "〇";

function kensakuClick() {
    /*
    // タブの選択状態(true/false)
    var tab1 = document.getElementById("tab1").checked;
    var tab2 = document.getElementById("tab2").checked;
    var tab3 = document.getElementById("tab3").checked;
    var tab4 = document.getElementById("tab4").checked;
    var tab5 = document.getElementById("tab5").checked;
    var tab7 = document.getElementById("tab7").checked;
    */
    var all = document.getElementById("all");
    var kakunin = document.getElementById("kakunin");
    var mikakunin = document.getElementById("mikakunin");

    if (all.checked == true) {
        // 結果：全て
        $('#myTable1 tr').show();
        $('#myTable2 tr').show();
        $('#myTable3 tr').show();
        $('#myTable4 tr').show();
        $('#myTable5 tr').show();
        $('#myTable7 tr').show();

    } else if (kakunin.checked == true) {
        // 結果：確認
        $("#myTable1 tbody tr").each(function () {
            var txt = $(this).find("td:eq(7)").html();
            if (txt.match(check) != null) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });

        $("#myTable2 tbody tr").each(function () {
            var txt = $(this).find("td:eq(8)").html();
            if (txt.match(check) != null) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });

        $("#myTable3 tbody tr").each(function () {
            var txt = $(this).find("td:eq(7)").html();
            if (txt.match(check) != null) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });

        $("#myTable4 tbody tr").each(function () {
            var txt = $(this).find("td:eq(7)").html();
            if (txt.match(check) != null) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });

        $("#myTable5 tbody tr").each(function () {
            var txt = $(this).find("td:eq(8)").html();
            if (txt.match(check) != null) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });

        $("#myTable7 tbody tr").each(function () {
            var txt = $(this).find("td:eq(7)").html();
            if (txt.match(check) != null) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });

    } else {
        // 結果：未確認
        $("#myTable1 tbody tr").each(function () {
            var txt = $(this).find("td:eq(7)").html();
            if (txt.match(check) != null) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });

        $("#myTable2 tbody tr").each(function () {
            var txt = $(this).find("td:eq(8)").html();
            if (txt.match(check) != null) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });

        $("#myTable3 tbody tr").each(function () {
            var txt = $(this).find("td:eq(7)").html();
            if (txt.match(check) != null) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });

        $("#myTable4 tbody tr").each(function () {
            var txt = $(this).find("td:eq(7)").html();
            if (txt.match(check) != null) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });

        $("#myTable5 tbody tr").each(function () {
            var txt = $(this).find("td:eq(8)").html();
            if (txt.match(check) != null) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });

        $("#myTable7 tbody tr").each(function () {
            var txt = $(this).find("td:eq(7)").html();
            if (txt.match(check) != null) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });
    }
}

window.onload = function () {
    // 初期表示で未確認のもののみを表示
    $("#myTable1 tbody tr").each(function () {
        var txt = $(this).find("td:eq(7)").html();
        if (txt.match(check) != null) {
            $(this).hide();
        } else {
            $(this).show();
        }
    });

    $("#myTable2 tbody tr").each(function () {
        var txt = $(this).find("td:eq(8)").html();
        if (txt.match(check) != null) {
            $(this).hide();
        } else {
            $(this).show();
        }
    });

    $("#myTable3 tbody tr").each(function () {
        var txt = $(this).find("td:eq(7)").html();
        if (txt.match(check) != null) {
            $(this).hide();
        } else {
            $(this).show();
        }
    });

    $("#myTable4 tbody tr").each(function () {
        var txt = $(this).find("td:eq(7)").html();
        if (txt.match(check) != null) {
            $(this).hide();
        } else {
            $(this).show();
        }
    });

    $("#myTable5 tbody tr").each(function () {
        var txt = $(this).find("td:eq(8)").html();
        if (txt.match(check) != null) {
            $(this).hide();
        } else {
            $(this).show();
        }
    });

    $("#myTable7 tbody tr").each(function () {
        var txt = $(this).find("td:eq(7)").html();
        if (txt.match(check) != null) {
            $(this).hide();
        } else {
            $(this).show();
        }
    });
}
function changeRad1() {
    $("#childList").text('生産実績')
}
function changeRad2() {
    $("#childList").text('点検結果')
}
function changeRad3() {
    $("#childList").text('保全計画')
}
function changeRad4() {
    $("#childList").text('保全結果')
}
function changeRad5() {
    $("#childList").text('工程内チェック')
}
function changeRad6() {
    $("#childList").text('製造条件チェック')
}
function testHome() {
    window.onload();
}
/*///////////////////////////////////ShouSaiKenSaku(this.id)////////////////////////////////////////*/
// $(document).ready(function () {
//     $('.ShouSaiRow').hide();
// });
// //-----------------------------
// function ShouSaiKenSaku(id) {

//     var sankaku_zen = $('#ShouSaiKenSaku').text();
//     var sankaku_go = sankaku_zen.replace("詳細検索", '');
//     if (sankaku_go == "▼") {
//         $('.ShouSaiRow').show();
//         $('#ShouSaiKenSaku').text("▲詳細検索");
//     } else {
//         $('.ShouSaiRow').hide();
//         $('#ShouSaiKenSaku').text("▼詳細検索");
//     }
// }
var count = 0;

function shosaiClick() {
    count++

    if (count % 2 == 0) {
        $("#denpyoKensakuInputArea").slideToggle(1);
        $(".btn_minus_s").css("display", "none");
        $(".btn_plus_s").css("display", "table-row");
    } else {
        $("#denpyoKensakuInputArea").slideToggle();
        $(".btn_minus_s").css("display", "table-row");
        $(".btn_plus_s").css("display", "none");
    }

}
/*///////////////////////////////////ShouSaiKenSaku(this.id)////////////////////////////////////////*/