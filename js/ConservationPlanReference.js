/*//////データの取得(初期処理)///////////////////////////////////////////////*/

var dataConservationPlanReference = []
var dataConservationPlanConfirmation = []
function getData() {

    var dataset_ConservationPlanReference = []
    var lineConservationPlanReference = JSON.parse(ConservationPlanReference);
    dataset_ConservationPlanReference.push(lineConservationPlanReference);
    //
    var dataset_conservationplanconfirmation = []
    var lineConservationPlanConfirmation = JSON.parse(ConservationPlanConfirmation);
    dataset_conservationplanconfirmation.push(lineConservationPlanConfirmation);
    //
    for (var i = 0; i < lineConservationPlanReference.length; i++) {
        Object.values(dataset_ConservationPlanReference[0][i]);
        dataConservationPlanReference.push(Object.values(dataset_ConservationPlanReference[0][i]));
    }
    //
    for (var i = 0; i < lineConservationPlanConfirmation.length; i++) {
        Object.values(dataset_conservationplanconfirmation[0][i]);
        dataConservationPlanConfirmation.push(Object.values(dataset_conservationplanconfirmation[0][i]));
    }

}
/*///////////////////////////////////////////////////////////////////////////*/

/*//////メインテーブル作成(初期処理)/////////////////////////////////////////*/

function makeMainTable1() {

    var tBody = document.getElementById("myTbody1");

    for (var count = 0; count < dataConservationPlanReference.length; count++) {
        var row = tBody.insertRow(count);

        var c1 = row.insertCell(0);
        var c2 = row.insertCell(1);
        var c3 = row.insertCell(2);
        var c4 = row.insertCell(3);
        var c5 = row.insertCell(4);
        var c6 = row.insertCell(5);
        var c7 = row.insertCell(6);

        c1.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none; height: 50px;";
        c2.style.cssText = "border-color:lightgray; text-align: center; border-top-style:none;";
        c3.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none;";
        c4.style.cssText = "border-color:lightgray; text-align: center; border-top-style:none;";
        c5.style.cssText = "border-color:lightgray; padding-right: 15px;text-align: end;  border-top-style:none;";
        c6.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none;";
        c7.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none;";

        c1.innerHTML = dataConservationPlanReference[count][0];
        c2.innerHTML = dataConservationPlanReference[count][1];
        c3.innerHTML = dataConservationPlanReference[count][2];
        c4.innerHTML = dataConservationPlanReference[count][3];
        c5.innerHTML = dataConservationPlanReference[count][4];
        c6.innerHTML = dataConservationPlanReference[count][5];
        c7.innerHTML = dataConservationPlanReference[count][6];

        if (c2.innerHTML == "点検") {

            c1.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none; height: 50px; color: red;";
            c2.style.cssText = "border-color:lightgray; text-align: center; border-top-style:none; color: red;";
            c3.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none; color: red;";
            c4.style.cssText = "border-color:lightgray; text-align: center; border-top-style:none; color: red;";
            c5.style.cssText = "border-color:lightgray; padding-right: 15px;text-align: end;  border-top-style:none; color: red;";
            c6.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none; color: red;";
            c7.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none; color: red;";
        }

    }

}
function makeMainTable3() {

    var tBody = document.getElementById("myTbody3");

    for (var count = 0; count < dataConservationPlanConfirmation.length; count++) {
        var row = tBody.insertRow(count);

        var c13 = row.insertCell(0);
        var c23 = row.insertCell(1);
        var c33 = row.insertCell(2);
        var c43 = row.insertCell(3);

        c13.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none; height: 50px;";
        c23.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none;";
        c33.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none;";
        c43.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none;";

        c13.innerHTML = dataConservationPlanConfirmation[count][0];
        c23.innerHTML = dataConservationPlanConfirmation[count][1];
        c33.innerHTML = dataConservationPlanConfirmation[count][2];
        c43.innerHTML = dataConservationPlanConfirmation[count][3];

    }

}

var key1 = localStorage.getItem("keyHozen");
//button cancel
function btnCancel() {
    if (key1 == 1) {

        window.location.href = "ConservationPlan.html";
    }
    else {

        window.location.href = "ConfirmationList.html";
    }
}
// button 保存
function btnSave() {

    if (key1 == 1) {

        window.location.href = "ConservationPlan.html";
    }
    else {

        window.location.href = "ConfirmationList.html";
    }

}
//button 確認
function buttonConfirm() {
    if (key1 == 1) {

        window.location.href = "ConservationPlan.html";
    }
    else {

        window.location.href = "ConfirmationList.html";
    }
}
//Button 追加
function buttonTsuika() {
    localStorage.setItem('HozenToroku', '0')
    window.location.href = "ConservationPlanRegistration.html";
}
/*///////////////////////////////////////////////////////////////////////////*/

/*//////詳細クリック/////////////////////////////////////////////////////////*/
var Youtei = "";
var Yuusen = "";
var Komoku = "";
var Buhin = "";
var YouTeijikan = "";
var Tantosha = "";
var KakuninJikou = "";

$(document).ready(function () {
    getData();
    makeMainTable1();
    makeMainTable3();
    if (key1 == 1) {
        $('#divconmentto').css("display", "none");
        $('#buttonConfirm').css("visibility", "hidden");
        $('#btnSave').css("display", "block");
        /////////////////////////////get data on localstrorage////////////////////////////
        $("#myTbody1 tr").click(function () {
            // クリックした行の情報取得
            Youtei = $(this).children('td')[0].innerText;
            Yuusen = $(this).children('td')[1].innerText;
            Komoku = $(this).children('td')[2].innerText;
            Buhin = $(this).children('td')[3].innerText;
            YouTeijikan = $(this).children('td')[4].innerText;
            Tantosha = $(this).children('td')[5].innerText;
            KakuninJikou = $(this).children('td')[6].innerText;

            // 選択されている他の行があれば選択解除
            $(this).siblings().removeClass("selected");
            // クリックされた行を選択
            $(this).addClass("selected");
            // Set item  save in localstorage 
            var index0 = $(this).children()[0].textContent;
            var index1 = $(this).children()[1].textContent;
            var index2 = $(this).children()[2].textContent;
            var index3 = $(this).children()[3].textContent;
            var index4 = $(this).children()[4].textContent;
            var index5 = $(this).children()[5].textContent;
            var index6 = $(this).children()[6].textContent;

            localStorage.setItem('HozenToroku', '1');

            localStorage.setItem('cell0', index0);
            localStorage.setItem('cell1', index1);

            localStorage.setItem('cell2', index2);
            localStorage.setItem('cell3', index3);
            localStorage.setItem('cell4', index4);

            localStorage.setItem('cell5', index5);
            localStorage.setItem('cell6', index6);

            window.location.href = "ConservationPlanRegistration.html";

        });
    }
    if (key1 == 0) {
        $('#divconmentto').css("display", "block");
        $('#buttonConfirm').css("visibility", "visible");
        $('#btnSave').css("display", "none");
    }
    var labelyoutei = localStorage.getItem("cell01");
    var labejoutai = localStorage.getItem("cell03");
    $("#idPlans").text(labelyoutei);
    $("#idjoutai").text(labejoutai);
    // if (labejoutai == "登録済") {

    //     $("#btnSave").prop('onclick', null);
    //     $("#btnSave").css('background', 'grey');
    // }
  

    ///////////////////add class item///////////////////////
    $('#myTbody1 tr').attr('class', 'item');
    $('#myTbody3 tr').attr('class', 'item');
    ////////////////////Kommento hidden////////////////////
    // $('#divconmentto').css("display", "none");
    // $('#buttonConfirm').css("visibility", "hidden");

    /////////////////////////////SL table///////////////////////////////////////
    var html = "<tr class='tableHeaderBackground'>" +
        "<th class='w_100' style ='text-align: end;'>SL</th > " +
        "</tr >" +
        "<tr>" +
        "<td style='padding-right: 15px; height: 50px; text-align: end;'>0人</td>" +

        "</tr>";
    $('#myTable2').find('#myTbody2').append(html);

});
// function tagKekaku() {
//     $('#divconmentto').css("display", "none");
//     $('#buttonConfirm').css("visibility", "hidden");
//     $('#btnSave').css("display", "block");
// }

// function tagKakunin() {
//     $('#divconmentto').css("display", "block");
//     $('#buttonConfirm').css("visibility", "visible");
//     $('#btnSave').css("display", "none");
// }
