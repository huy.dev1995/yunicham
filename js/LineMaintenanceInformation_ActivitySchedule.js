/*//////追加クリック////////////////////////////////////////////////////////*/

function clickTsuika() {

    $(".modalActivitySchedule").css('display', 'block');

    $("#input_ActivitySchedule").val('');

    $("#buttonEdit").css('display', 'none');
    $("#buttonDelete").css('display', 'none');
    $("#buttonRegistration").css('display', 'block');

}
function buttonCancel() {
    $(".modalActivitySchedule").css('display', 'none');
    // location.reload();
}
function buttonRegistration() {

    var select1 = $("#selectKimu").val();
    var input1 = $("#input_ActivitySchedule").val();

    var inputHiddenTable = $("#inputHiddenTable").val();

    var convert = parseInt(inputHiddenTable);
    convert = convert + 1;
    $("#inputHiddenTable").val(convert);

    var html = "<tr class='item'><td id='AA" + convert + "'style='width: 150px; border-color: lightgray; padding-left: 20px; border-top-style: none; height: 50px;'>" + select1 + "</td><td id='BB" + convert + "' style='width: 150px; border-color: lightgray; padding-left: 20px; border-top-style: none;'>XXXX</td><td id='CC" + convert + "'style='width: 500px; border-color: lightgray; padding-left: 20px; border-top-style: none;'>" + input1 + "</td><td id='DD" + convert + "'style='display:none'>" + convert + "</td></tr>"
    $("#myTbody").append(html);

    $(".modalActivitySchedule").css('display', 'none');
    clickCauseTable();
    //location.reload();
    $("#buttonEdit").css('display', 'block');
    $("#buttonDelete").css('display', 'block');
    $("#buttonRegistration").css('display', 'none');

}
// 編集
function buttonEdit() {

    var selectkimu = $("#selectKimu").val();
    var input1 = $("#input_ActivitySchedule").val();
    var idhidden = $("#inputHiddenModal").val();

    $("#AA" + idhidden).text(selectkimu);
    $("#CC" + idhidden).text(input1);
    $("#DD" + idhidden).text(idhidden);

    $(".modalActivitySchedule").css('display', 'none');
    // location.reload();
}
// 削除
function buttonDelete() {

    $("#myTbody").find('tr[class="item selected"]').each(function () {
        if ($(this).hasClass("selected")) {
            $(this).remove();
        }

    })
    $("#buttonEdit").css('display', 'block');
    $("#buttonDelete").css('display', 'block');
    $("#buttonRegistration").css('display', 'none');
    $(".modalActivitySchedule").css('display', 'none');
    // location.reload();
}

/*//////データの取得(初期処理)///////////////////////////////////////////////*/

var dataActivitySchedule = []

function getData() {

    var dataset_activityschedule = []

    var lineActivitySchedule = JSON.parse(ActivitySchedule);

    dataset_activityschedule.push(lineActivitySchedule);

    for (var i = 0; i < lineActivitySchedule.length; i++) {
        Object.values(dataset_activityschedule[0][i]);
        dataActivitySchedule.push(Object.values(dataset_activityschedule[0][i]));
    }

}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////メインテーブル作成(初期処理)/////////////////////////////////////////*/

function makeMainTable() {

    var tBody = document.getElementById("myTbody");

    for (var count = 0; count < dataActivitySchedule.length; count++) {
        var row = tBody.insertRow(count);

        // var last = dataActivitySchedule.length - 1;

        var c1 = row.insertCell(0);
        var c2 = row.insertCell(1);
        var c3 = row.insertCell(2);
        var c4 = row.insertCell(3);
        // var c5 = row.insertCell(4);

        c1.style.cssText = " border-color:lightgray; padding-left: 20px; border-top-style:none; height: 50px;";
        c2.style.cssText = " border-color:lightgray; padding-left: 20px; border-top-style:none;";
        c3.style.cssText = " border-color:lightgray; padding-left: 20px; border-top-style:none;";
        c4.style.cssText = "display:none";
        c1.setAttribute("id", "AA" + count);
        c2.setAttribute("id", "BB" + count);
        c3.setAttribute("id", "CC" + count);
        c4.setAttribute("id", "DD" + count);
        c1.innerHTML = dataActivitySchedule[count][0];
        c2.innerHTML = dataActivitySchedule[count][1];
        c3.innerHTML = dataActivitySchedule[count][2];
        c4.innerHTML = count;
        // c5.innerHTML = last;
        $('#inputHiddenTable').val(count);

    }
}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////詳細クリック/////////////////////////////////////////////////////////*/
var Work = "";
var Filler = "";
var ActivityScheduleInput = "";

/*///////////////////////////////////////////////////////////////////////////*/

$(document).ready(function () {
    getData();
    makeMainTable();
    $('#myTbody tr').attr('class', 'item');
    localStorage.removeItem('index0');
    localStorage.clear();
    //メインテーブルのクリック時の処理
    clickCauseTable();
    // $('#myTbody tr').attr('onclick', "myFunction()");

});

function clickCauseTable() {
    //メインテーブルのクリック時の処理
    $("#myTbody tr").click(function () {
        // クリックした行の情報取得
        Work = $(this).children('td')[0].innerText;
        Filler = $(this).children('td')[1].innerText;
        ActivityScheduleInput = $(this).children('td')[2].innerText;

        // 選択されている他の行があれば選択解除
        $(this).siblings().removeClass("selected");
        //クリックされた行を選択
        $(this).addClass("selected");

        //Set item  save in localstorage 
        var index0 = $(this).children()[0].textContent;
        var index1 = $(this).children()[1].textContent;
        var index2 = $(this).children()[2].textContent;
        var index3 = $(this).children()[3].textContent;

        localStorage.setItem('key1', '1');
        localStorage.setItem('index0', index0);
        localStorage.setItem('index1', index1);
        localStorage.setItem('index2', index2);
        localStorage.setItem('index3', index3);

        // 変更画面
        $(".modalActivitySchedule").css('display', 'block');
        var Kinmu = localStorage.getItem("index0");
        var KatsudoYotei = localStorage.getItem("index2");
        var hidden = localStorage.getItem("index3")
        // load data
        $("#selectKimu option").each(function () {
            if ($(this).text() == Kinmu) {
                $(this).attr('selected', 'selected');
            }
        });
        $('#input_ActivitySchedule').val(KatsudoYotei);

        $('#inputHiddenModal').val(hidden);

        $("#buttonEdit").css('visibility', 'visible');
        $("#buttonDelete").css('visibility', 'visible');
        $("#buttonRegistration").css('display', 'none');

    });

}


