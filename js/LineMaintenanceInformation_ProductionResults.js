// get data file json 
var dataProductionResults = []

function getData() {

    var dataset_productionresults = []

    var lineProductionResults = JSON.parse(ProductionResults);

    dataset_productionresults.push(lineProductionResults);

    for (var i = 0; i < lineProductionResults.length; i++) {
        Object.values(dataset_productionresults[0][i]);
        dataProductionResults.push(Object.values(dataset_productionresults[0][i]));
    }

}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////メインテーブル作成(初期処理)/////////////////////////////////////////*/

function makeMainTable() {

    var tBody = document.getElementById("myTbody");

    for (var count = 0; count < dataProductionResults.length; count++) {
        var row = tBody.insertRow(count);

        var c1 = row.insertCell(0);
        var c2 = row.insertCell(1);
        var c3 = row.insertCell(2);
        var c4 = row.insertCell(3);
        var c5 = row.insertCell(4);
        var c6 = row.insertCell(5);
        var c7 = row.insertCell(6);
        var c8 = row.insertCell(7);
        var c9 = row.insertCell(8);
        var c10 = row.insertCell(9);

        var style0 = "width:100px; border-color:lightgray;padding-right: 15px;text-align: end; ; border-top-style:none;";
        var style1 = "width:100px; border-color:lightgray;padding-right: 15px;text-align: end; padding-left: 20px; border-top-style:none;";

        c1.style.cssText = "width:150px; border-color:lightgray;padding-left: 20px; border-top-style:none; height: 50px;";
        c2.style.cssText = "width:200px; border-color:lightgray; padding-left: 20px; border-top-style:none;";
        c3.style.cssText = "width:200px; border-color:lightgray; padding-left: 20px; border-top-style:none;";
        c4.style.cssText = "width:200px; border-color:lightgray; padding-left: 20px; border-top-style:none;";
        c5.style.cssText = style0;
        c6.style.cssText = style0;
        c7.style.cssText = style0;
        c8.style.cssText = style0;
        c9.style.cssText = style0;
        c10.style.cssText = style1;

        c1.innerHTML = dataProductionResults[count][0];
        c2.innerHTML = dataProductionResults[count][1];
        c3.innerHTML = dataProductionResults[count][2];
        c4.innerHTML = dataProductionResults[count][3];
        c5.innerHTML = dataProductionResults[count][4];
        c6.innerHTML = dataProductionResults[count][5];
        c7.innerHTML = dataProductionResults[count][6];
        c8.innerHTML = dataProductionResults[count][7];
        c9.innerHTML = dataProductionResults[count][8];
        c10.innerHTML = dataProductionResults[count][9];

        if ( c5.innerHTML>= c6.innerHTML ){
            c6.style.cssText = style0 + "color : red";
        }

        if ( c7.innerHTML>= c8.innerHTML ){
            c8.style.cssText = style0 + "color : red";
        }

        if (c9.innerHTML >= c10.innerHTML) {

            c10.style.cssText = style1 + "color : red";

        }

    }
}


/*///////////////////////////////////////////////////////////////////////////*/

$(document).ready(function () {
    getData();
    makeMainTable();
    $('#myTbody tr').addClass('item')


});


