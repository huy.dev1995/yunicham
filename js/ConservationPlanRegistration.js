// Cancel button onclick
function btnCancel() {
    window.location.href = "ConservationPlanReference.html";
}
// When click button 登録
function btnToroku() {

    window.location.href = "ConservationPlanReference.html";
}
// When button Delete onclick
function buttonDelete() {

    window.location.href = "ConservationPlanReference.html";
}
// When button Edit onclick
function buttonEdit() {

    window.location.href = "ConservationPlanReference.html";
}

// change selectbox 実施頻度

function changeSelectYuusen(value) {
    if (value == 1) {
        ConductedMonthly();
    }
    if (value == 2) {
        ConductedWeekly();
    }
    if ((value == 3) || (value == 4) || (value == 5) || (value == 6) || (value == 7) || (value == 8)) {

        Other();
    }
}
//未入力
function ConductedMonthly() {
    $("#tr_jisshihindo").css("display", "table-row");
    $("#tr_jisshibi").css("display", "table-row");
    $("#tr_kinmu").css("display", "table-row");
    $("#tr_Youbi").css("display", "none");
    $("#datejisshibi").css("display", "table-row");
    $("#jisshibiSelect").css("display", "none");
}
//毎月実施
function ConductedWeekly() {
    $("#tr_jisshihindo").css("display", "table-row");
    $("#tr_jisshibi").css("display", "table-row");
    $("#tr_kinmu").css("display", "table-row");
    $("#tr_hizukei").css("display", "table-row");
    $("#date1").css("display", "none");
    $("#date2").css("display", "table-row");
    $("#datejisshibi").css("display", "none");
    $("#jisshibiSelect").css("display", "table-row");
}
//All
function Other() {
    $("#tr_jisshihindo").css("display", "table-row");
    $("#tr_kinmu").css("display", "table-row");
    $("#tr_Youbi").css("display", "table-row");
    $("#tr_jisshibi").css("display", "none");
    $("#tr_hizukei").css("display", "none");
}
//　Change selectbox 実施日
function changeSelectJisshibi(value) {
    if (value == 1) {
        date1Input = $('input#date2');
        date1Input.prop('disabled', false);

    }

    else {
        date1Input = $('input#date2');
        date1Input.prop('disabled', true);
        $('input#date2').val("");
    }
}

// Load page 
$(document).ready(function () {
    //load page
    $("#tr_jisshihindo").css("display", "table-row");
    $("#tr_jisshibi").css("display", "table-row");
    $("#tr_kinmu").css("display", "table-row");
    $("#tr_Youbi").css("display", "none");
    $("#tr_hizukei").css("display", "none");
    $("#datejisshibi").css("display", "table-row");
    $("#jisshibiSelect").css("display", "none");

    // load page default 日付 display
    date1Input = $('input#date2');
    date1Input.prop('disabled', false);

    $('input#date2').val("");

    //get item from localStorage
    var key = localStorage.getItem('HozenToroku');
    // 変更画面 load data
    if (key == 1) {
        date1Input = $('input#date2');
        date1Input.prop('disabled', false);
        // set active button
        $("#buttonEdit").css('visibility', 'visible');
        $("#buttonDelete").css('visibility', 'visible');
        $("#buttonRegistration").css('display', 'none');
        //set menu header
        $('#child1').text("点検計画編集");
        $("#btnRegistration").css("display", "none");
        $("#btEdit").css("display", "table-row");
        $("#btnDelete").css("display", "table-row");
        //get data from loaclstrorage
        var Youtei = localStorage.getItem("cell0");
        var Yuusen = localStorage.getItem("cell1");
        var Komoku = localStorage.getItem("cell2");
        var Buhin = localStorage.getItem("cell3");
        var YouTeijikan = localStorage.getItem("cell4");
        var Tantosha = localStorage.getItem("cell5");
        var KakuninJikou = localStorage.getItem("cell6");

        var scliptYoutei = Youtei.split(" ");
        var pt1 = scliptYoutei[0];
        var pt2 = scliptYoutei[1];
        var pt3 = scliptYoutei[2];
        $("#YuusenSelect option").each(function () {
            if ($(this).text() == Yuusen) {
                $(this).attr('selected', 'selected');
            }
        });
        $('#inputKomoku').val(Komoku);
        $("#BuhinSelect option").each(function () {
            if ($(this).text() == Buhin) {
                $(this).attr('selected', 'selected');
            }
        });

        $('#inputYouteiJikan').val(YouTeijikan);

        $("#tantoushaSelect option").each(function () {
            if ($(this).text() == Tantosha) {
                $(this).attr('selected', 'selected');
            }
        });
        $('#inputkakuninjikou').val(KakuninJikou);

        $("#kinmuSelect option").each(function () {
            if ($(this).text() == pt3) {
                $(this).attr('selected', 'selected');
            }
        });
        //毎月実施
        if (Yuusen == "毎月実施") {
            // document.getElementById("tabDirectly").checked = true;
            ConductedWeekly();

            date1Input = $('input#date2');
            date1Input.prop('disabled', false);


        } else {
            Other();
            if (pt2 == "(日)") {
                document.getElementById("tabSunday").checked = true;
            }
            if (pt2 == "(月)") {
                document.getElementById("tabMonday").checked = true;
            }
            if (pt2 == "(火)") {
                document.getElementById("tabTusday").checked = true;
            }
            if (pt2 == "(水)") {
                document.getElementById("tabWednesday").checked = true;
            }
            if (pt2 == "(木)") {
                document.getElementById("tabThursday").checked = true;
            }
            if (pt2 == "(金)") {
                document.getElementById("tabFriday").checked = true;
            }
            if (pt2 == "(土)") {
                document.getElementById("tabSaturday").checked = true;
            }
        }

    }
    // 登録画面移動ため
    if (key == 0) {

        date1Input = $('input#date2');
        date1Input.prop('disabled', false);

        $("#buttonEdit").css('display', 'none');
        $("#buttonDelete").css('display', 'none');
        $("#buttonRegistration").css('display', 'block');
        $("#btnRegistration").css("display", "table-row");
        $('#child1').text("保全計画登録");

    }
    //check input 日付
    function setInputFilter(textbox, inputFilter) {
        ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function (event) {
            textbox.addEventListener(event, function () {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        });
    }

    // Install input filters.
    setInputFilter(document.getElementById("date2"), function (value) {
        return /^-?\d*$/.test(value);
    });
});
//  check input 日付 1-31日
function inputdate2() {
    var checkinput = document.getElementById("date2").value;
    if (checkinput > 31) {
        $('#date2').val("31");
    }
    if (checkinput < 1) {
        $('#date2').val("1");
    }

}