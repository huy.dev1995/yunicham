/*//////データの取得(初期処理)///////////////////////////////////////////////*/

var dataInspectionResultInput = []
function getData() {

    var dataset_inspectionresultinput = []
    var lineInspectionResultInput = JSON.parse(InspectionResultInput);
    dataset_inspectionresultinput.push(lineInspectionResultInput);
    for (var i = 0; i < lineInspectionResultInput.length; i++) {
        Object.values(dataset_inspectionresultinput[0][i]);
        dataInspectionResultInput.push(Object.values(dataset_inspectionresultinput[0][i]));
    }


}
/*///////////////////////////////////////////////////////////////////////////*/

/*//////メインテーブル作成(初期処理)/////////////////////////////////////////*/

function makeMainTable() {

    var tBody = document.getElementById("myTbody");
    var index = 0;
    for (var count = 0; count < dataInspectionResultInput.length; count++) {
        index++;
        var row = tBody.insertRow(count);

        var c1 = row.insertCell(0);
        var c2 = row.insertCell(1);
        var c3 = row.insertCell(2);
        var c4 = row.insertCell(3);

        c1.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none; ";
        c2.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none;";
        c3.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none;";
        c4.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none;";

        c1.innerHTML = dataInspectionResultInput[count][0];
        c2.innerHTML = dataInspectionResultInput[count][1];
        c3.innerHTML = dataInspectionResultInput[count][2];

        var selectbox = "<div class='area'>" +
            "<input type='radio' name='tab_name" + index + "' frameborder='0' id='tabOK" + index + "' value='1'>" +
            "<label class='tab_class' for='tabOK" + index + "'> OK </label>" +
            "<input type='radio' name='tab_name" + index + "'  id='tabReplacementRequired" + index + "' value='2'>" +
            "<label class='tab_class' for='tabReplacementRequired" + index + "'> 要交換</label>" +
            "<input type='radio' name='tab_name" + index + "'  id='tabRepairRequired" + index + "' value='3'>" +
            "<label class='tab_class' for='tabRepairRequired" + index + "'> 要修理</label>" +
            "<input type='radio' name='tab_name" + index + "'  id='tabInspectionRequired" + index + "' value='4'>" +
            "<label class='tab_class' for='tabInspectionRequired" + index + "'> 要点検 </label>" +
            "<input type='radio' name='tab_name" + index + "'  id='tabRefuelingRequired" + index + "' value='5'>" +
            "<label class='tab_class' for='tabRefuelingRequired" + index + "'> 要給油</label>" +
            "<input type='radio' name='tab_name" + index + "' id='tabNotImplemented" + index + "' value='6' checked>" +
            "<label class='tab_class' for='tabNotImplemented" + index + "'> 未実施 </label>" +
            " </div>"
        c4.innerHTML = dataInspectionResultInput[count][3];
        var c5 = row.insertCell(4);
        c5.style.cssText = " border-color:lightgray; border-top-style:none;padding:5px 0 5px 25px;";
        c5.innerHTML = selectbox;

    }

}

/*///////////////////////////////////////////////////////////////////////////*/

/*//////詳細クリック/////////////////////////////////////////////////////////*/
var InspectionPoint = "";
var Item = "";
var Method = "";
var EvaluationCriteria = "";
var Result = "";
var key = localStorage.getItem("key1");
$(document).ready(function () {
    getData();
    makeMainTable();
    // get dat from InspectionResultList.html
    if (key == "1") {
        //get value from loalstrorage set at label
        var plans = localStorage.getItem("indexCell0");
        $('#idPlans').text(plans);
        var inspectionType = localStorage.getItem("indexCell2");
        $('#idInspectionType').text(inspectionType);
        $('#child1').text("点検結果一覧");
        $('#href1').attr('href', 'InspectionResultList.html');
        $('#child2').css("display","none");
        $('#hiddenA').css("display","none");

    }
    if (key == "0") {
        //get value from loalstrorage set at label
        // var plans = localStorage.getItem("indexCell0");
        $('#idPlans').text("2020/12/19");
        var inspectionType0 = localStorage.getItem("ScheduleContents");
        $('#idInspectionType').text(inspectionType0);
        $('#child1').text("設備情報一覧");
        $('#href1').attr('href', 'LineMaintenanceList.html');
        $('#child2').text("設備情報");
        $('#href2').attr('href', 'LineMaintenanceInformation.html');
    }

});
function btnCancel() {

    if (key == "1") {
        window.location.href = "InspectionResultList.html";
    }
    else {
        window.location.href = "LineMaintenanceInformation.html";
    }

}
function btnHold() {
    if (key == "1") {
        window.location.href = "InspectionResultList.html";
    }
    else {
        window.location.href = "LineMaintenanceInformation.html";
    }
}