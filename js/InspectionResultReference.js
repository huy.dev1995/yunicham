/*//////データの取得(初期処理)///////////////////////////////////////////////*/

var dataInspectionResultInput = []
var dataInspectionResultReference = []
function getData() {

    var dataset_inspectionresultinput = []
    var lineInspectionResultInput = JSON.parse(InspectionResultInput);
    dataset_inspectionresultinput.push(lineInspectionResultInput);
    //
    var dataset_inspectionresultreference = []
    var lineInspectionResultReference = JSON.parse(InspectionResultReference);
    dataset_inspectionresultreference.push(lineInspectionResultReference);
    //
    for (var i = 0; i < lineInspectionResultInput.length; i++) {
        Object.values(dataset_inspectionresultinput[0][i]);
        dataInspectionResultInput.push(Object.values(dataset_inspectionresultinput[0][i]));
    }
    //
    for (var i = 0; i < lineInspectionResultReference.length; i++) {
        Object.values(dataset_inspectionresultreference[0][i]);
        dataInspectionResultReference.push(Object.values(dataset_inspectionresultreference[0][i]));
    }

}
/*///////////////////////////////////////////////////////////////////////////*/

/*//////メインテーブル作成(初期処理)/////////////////////////////////////////*/

function makeMainTable1() {

    var tBody = document.getElementById("myTbody1");

    for (var count = 0; count < dataInspectionResultInput.length; count++) {
        var row = tBody.insertRow(count);

        var c1 = row.insertCell(0);
        var c2 = row.insertCell(1);
        var c3 = row.insertCell(2);
        var c4 = row.insertCell(3);

        c1.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none; height: 50px;";
        c2.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none;";
        c3.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none;";
        c4.style.cssText = "border-color:lightgray; padding-left: 15px; border-top-style:none;";

        c1.innerHTML = dataInspectionResultInput[count][0];
        c2.innerHTML = dataInspectionResultInput[count][1];
        c3.innerHTML = dataInspectionResultInput[count][2];
        c4.innerHTML = dataInspectionResultInput[count][3];
        var c5 = row.insertCell(4);
        c5.style.cssText = " border-color:lightgray; padding-left: 15px; border-top-style:none;";
        c5.innerHTML = dataInspectionResultInput[count][5];

        if (c5.innerHTML == "未実施") {
            c5.style.cssText = " border-color:lightgray;padding-left: 15px; border-top-style:none; color: blue;";
        }
        if ((c5.innerHTML == "要交換") || (c5.innerHTML == "要修理") || (c5.innerHTML == "要点検") || (c5.innerHTML == "要給油")) {
            c5.style.cssText = " border-color:lightgray;padding-left: 15px; border-top-style:none; color: red;";
        }
    }

}
function makeMainTable3() {

    var tBody = document.getElementById("myTbody3");

    for (var count = 0; count < dataInspectionResultReference.length; count++) {
        var row = tBody.insertRow(count);

        var c13 = row.insertCell(0);
        var c23 = row.insertCell(1);
        var c33 = row.insertCell(2);
        var c43 = row.insertCell(3);

        c13.style.cssText = "width:150px; border-color:lightgray; padding-left: 15px; border-top-style:none; height: 50px;";
        c23.style.cssText = "width:200px; border-color:lightgray; padding-left: 15px; border-top-style:none;";
        c33.style.cssText = "width:100px; border-color:lightgray; padding-left: 15px; border-top-style:none;";
        c43.style.cssText = "width:870px; border-color:lightgray; padding-left: 15px; border-top-style:none;";

        c13.innerHTML = dataInspectionResultReference[count][0];
        c23.innerHTML = dataInspectionResultReference[count][1];
        c33.innerHTML = dataInspectionResultReference[count][2];
        c43.innerHTML = dataInspectionResultReference[count][3];

    }

}
//button cancel
var key = localStorage.getItem("keyCancel");

function btnCancel() {

    if (key == "0") {

        window.location.href = "ConfirmationList.html";
    }
    if (key == "1") {

        window.location.href = "InspectionResultList.html";
    }
}
//button 確認
function btnVerification() {
    if (key == "0") {

        window.location.href = "ConfirmationList.html";
    }
    if (key == "1") {

        window.location.href = "InspectionResultList.html";
    }
}
/*///////////////////////////////////////////////////////////////////////////*/

/*//////詳細クリック/////////////////////////////////////////////////////////*/
var InspectionPoint = "";
var Item = "";
var Method = "";
var EvaluationCriteria = "";
var Result = "";

$(document).ready(function () {
    getData();
    makeMainTable1();
    makeMainTable3();
    $('#myTbody1 tr').attr('class', 'item');
    $('#myTbody3 tr').attr('class', 'item');
    //data from ConfirmationList.html
    if (key == "0") {
        $('#divconmentto').css("display", "block");
        $('#btnVerification').css("visibility", "visible");
        // $('#childList1').text("確認対象一覧");
        // $('#childA').attr('href', 'ConfirmationList.html');
        $('#childList2').text("確認対象一覧");
        $('#childB').attr('href', 'ConfirmationList.html');
        // get value from localstroage 
        var plans1 = localStorage.getItem("jisshibiCL");
        $('#idPlans').text(plans1);
        var idImplementation1 = localStorage.getItem("jisshibiCL");
        $('#idImplementation').text(idImplementation1);
        var inspectionType1 = localStorage.getItem("tenkenCL");
        $('#idInspectionType').text(inspectionType1);
        var indexSL1 = localStorage.getItem("SLCL")
        var indexTL1 = localStorage.getItem("TLCL")

        var html = "<tr class='tableHeaderBackground'>" +
            "<th class='w_100' style ='text-align: end;'>SL</th > " +
            "<th class='w_100'style ='text-align: end;'>TL</th>  " +
            "</tr >" +
            "<tr>" +
            "<td style='padding-right: 15px; height: 50px; text-align: end;'>" + indexSL1 + "</td>" +
            "<td style='padding-right: 15px; text-align: end;'>" + indexTL1 + "</td>" +
            "</tr>";
        $('#myTable2').find('#myTbody2').append(html);
    }
    // data from InspectionResultList
    if (key == "1") {
        $('#divconmentto').css("display", "none")
        // $('#childList1').text("点検結果一覧");
        // $('#childA').attr('href', 'InspectionResultList.html');
        $('#childList2').text("点検結果一覧");
        $('#childB').attr('href', 'InspectionResultList.html');
        //get value from loalstrorage set at label
        var plans = localStorage.getItem("indexCell0");
        $('#idPlans').text(plans);
        var implementation = localStorage.getItem("indexCell1");
        $('#idImplementation').text(implementation);
        var inspectionType = localStorage.getItem("indexCell2");
        $('#idInspectionType').text(inspectionType);

        var indexSL = localStorage.getItem("indexCell3")
        var indexTL = localStorage.getItem("indexCell4")

        var html = "<tr class='tableHeaderBackground'>" +
            "<th class='w_100' style ='text-align: end;'>SL</th > " +
            "<th class='w_100' style ='text-align: end;'>TL</th>  " +
            "</tr >" +
            "<tr>" +
            "<td style='padding-right: 15px; height: 50px; text-align: end;'>" + indexSL + "</td>" +
            "<td style='padding-right: 15px; text-align: end;'>" + indexTL + "</td>" +
            "</tr>";
        $('#myTable2').find('#myTbody2').append(html);
    }
});
