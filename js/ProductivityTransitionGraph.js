var divSelect;

var timeVariable = {
    time1: "１値",
    time2: "２値",
    time3: "３値",
}

var getYear = "";

function drawGraph(date, data, data2, draw, select1) {

    if (select1 == "直当たりPs数") {
        divSelect = "#lineGraph1";
    } else if (select1 == "可動率") {
        divSelect = "#lineGraph2";
    } else if (select1 == "Loss率") {
        divSelect = "#lineGraph3";
    }

    var rect = d3.select(divSelect).node().getBoundingClientRect(),
        width = rect.width / 3 - 50,
        height = rect.height;

    // set the dimensions and margins of the graph
    var margin = { top: 10, right: 130, bottom: 100, left: 100 },
        width = width * 3 - margin.left,
        height = height - margin.top - margin.bottom;

    var deviceNumber = $("#select1").val();

    if (deviceNumber == null) {
        deviceNumber = "0";
    } else {
        deviceNumber = deviceNumber;
    }

    //Panel title  
    d3.select(divSelect)
        .append("div")
        .style("margin", "auto")
        .append("h2")
        .attr('class', 'text-center mt-3')
        .append('p')
        .attr('class', 'tex-center')
        .text(select1 + '(' + deviceNumber + '号機' + ')')

    // append the svg object to the body of the page
    var svg = d3.select(divSelect)
        .append("svg")
        .attr("id", "graph-chart")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom - 60)
        .append("g")
        .attr("transform",
            "translate(" + (margin.left / 2) + "," + margin.top + ")");

    var scaleX = d3.scalePoint()
        .domain(date)
        .range([0, width + margin.left]);

    var scaleY = d3.scaleLinear()
        .domain([Math.ceil(d3.max(data)), Math.round(d3.min(data))]).nice()
        .range([0, height - margin.bottom]);

    var scaleXLine = d3.scalePoint()
        .domain(data)
        .range([scaleX.step() / 3 - margin.top - 5, scaleX.step() * 30])

    var scaleXLine1 = d3.scalePoint()
        .domain(data2)
        .range([scaleX.step() / 3 - margin.top - 5, scaleX.step() * 30])


    // Add scales to axis

    var x_axis = d3.axisBottom()
        .scale(scaleX);

    var y_axis = d3.axisLeft()
        .scale(scaleY).tickSize(-width - margin.left - (margin.left / 2));

    //color
    var colors = d3.scaleOrdinal(["#0569ff", "#ff0505"])

    //Append group and insert axis
    svg.append("g")
        .attr('class', 'x-axe')
        .call(x_axis)
        .style("transform", 'translate(0px,' + (height - margin.bottom) + 'px)')
        .style("font-size", "10px")
        .call(g => g.select('.domain').remove())
        .selectAll(".tick text")
        .style("text-anchor", "middle")
        .attr("x", scaleX.step() / 2)
    // .call(wrap, scaleX.step());

    svg.append("g")
        .attr('class', 'y-axe')
        .call(y_axis)
        .call(g => g.select('.domain').remove());

    //draw a line 
    var lineFunction = d3.line()
        .x(function (d) { return scaleXLine(d); })
        .y(function (d) { return scaleY(d); })
        .curve(d3.curveLinear);

    var lineFunction1 = d3.line()
        .x(function (d) { return scaleXLine1(d); })
        .y(function (d) { return scaleY(d); })
        .curve(d3.curveLinear);


    if (draw == true) {
        //defining the lines
        var line = svg.append('g').attr("class", "line").append("path");
        var line1 = svg.append('g').attr("class", "line1").append("path");
        var circle = svg.append('g').attr("class", "circle");
        var circle2 = svg.append('g').attr("class", "circle1");

        //plotting lines
        line
            .attr("d", lineFunction(data))
            .attr("stroke", "#4F81BD")
            .attr("stroke-width", 2)
            .attr("fill", "none")
            .append("circle")

        line1
            .attr("d", lineFunction1(data2))
            .attr("stroke", "#ff0505")
            .attr("stroke-width", 2)
            .attr("fill", "none")
            .append("circle")


        circle.selectAll(".circle")
            .data(data).enter()
            .append("circle")
            .attr("id", "circle")
            .attr("class", "circle")
            .attr("cx", (d) => {
                return scaleXLine(d);
            })
            .attr("cy", (d) => {
                return scaleY(d);
            })
            .attr("fill", "#4F81BD")
            .attr("r", 3);

        circle2.selectAll(".circle")
            .data(data2).enter()
            .append("circle")
            .attr("id", "circle")
            .attr("class", "circle")
            .attr("cx", (d) => {
                return scaleXLine1(d);
            })
            .attr("cy", (d) => {
                return scaleY(d);
            })
            .attr("fill", "#ff0505")
            .attr("r", 3);
    }

    let dataNest = ["目標値", "実績"];

    var legendSpace = (width + (margin.left / 2) + margin.top) / dataNest.length;

    let userGuide = svg.append('g').attr('class', 'group-user')

    userGuide.selectAll('.text-user')
        .data(dataNest).enter()
        .append('text')
        .attr('class', 'text-user')
        .text((d, i) => { return d })
        .attr("x", (d, i) => {
            return (legendSpace / dataNest.length) + i * legendSpace + 3 * margin.top + (margin.left / 2) + 10;
        })
        .attr("y", height - margin.top)

    userGuide.selectAll('.color-user')
        .data(dataNest).enter()
        .append('rect')
        .attr('class', 'color-user')
        .attr('stroke-width', '2')
        .attr("x", (d, i) => {
            return (legendSpace / dataNest.length) + i * legendSpace + 3 * margin.top;
        })
        .attr("y", height - 2 * margin.top + 2)
        .attr("width", '50')
        .attr("height", "5px")
        .attr('fill', (d, i) => {
            return colors(i)
        });

    userGuide.append('text')
        .attr('class', 'text-user')
        .text(getYear)
        .attr("x", (d, i) => {
            return ((legendSpace / dataNest.length) + i * legendSpace + 2 * margin.top) * 2;
        })
        .attr("y", height - margin.top)
        .style("font-size", "xx-large")

    $("#togglePopup").click(function () {
        $("#myModal_choukiteishi").attr("class", "modal");
        $("#modal_content_memo").show();

    });

    $("#togglePopup1").click(function () {
        $("#myModal_choukiteishi").attr("class", "");
        $("#modal_content_memo").hide();
    });

    // function wrap(text, width) {
    //     text.each(function () {
    //         var text = d3.select(this),
    //             words = text.text().split(/\s+/).reverse(),
    //             word,
    //             line = [],
    //             lineNumber = 0,
    //             lineHeight = 1.1, // ems
    //             y = text.attr("y"),
    //             dy = text.attr("dy") / 2,
    //             tspan = text.text(null).append("tspan").attr("x", width / 2).attr("y", y).attr("dy", dy + "em");
    //         while (word = words.pop()) {
    //             line.push(word);
    //             tspan.text(line.join(" "));
    //             if (tspan.node().getComputedTextLength() > width) {
    //                 line.pop();
    //                 tspan.text(line.join(" "));
    //                 line = [word];
    //                 tspan = text.append("tspan").attr("x", width / 2).attr("y", y / 3).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
    //             }
    //         }
    //     });
    // }
}

var allNumber = {
    number1: "98.4",
    number2: "2.4"
}

// var date = ["00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00",
//     "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00",
//     "22:00", "23:00", "24:00"];

var date = [];

var temp;

var data1 = [
    0,
    0.11954993825101878,
    0.5096318590668782,
    0.7521014654296014,
    1.985647436250864,
    1.7849543047026206,
    0.0032008709457178597,
    2.193654216963554,
    6.98615795219458,
    7.061800659959485,
    5.781405583582215,
    0.2177566121578267,
    0.9490521243788113,
    2.1394410132295967,
    2.58649288988463,
    4.722326402197536,
    13.371234254074139,
    15.644622727702224,
    13.388198018550518,
    9.43018700922563,
    5.7092846227005944,
    8.763886405536258,
    8.158147186885135,
    4.567110374241677,
    11.758627131689323,
    12.567877,
    13.567877,
    7.567877,
    5.567877,
    8.567877,
];
var data2 = [
    0,
    0.44382090655997275,
    1.351036016039361,
    0.7265420712904793,
    0.39579941383083916,
    1.258926030419839,
    5.8060939421928595,
    0.12264858646871413,
    6.667045737126248,
    8.201343534419847,
    2.4001979892229053,
    1.7221305174623291,
    0.7349697655925613,
    0.7744158515109376,
    13.357917002053133,
    6.497523639578535,
    12.999998728570013,
    3.6945763837161185,
    9.612967245522857,
    5.568163766255545,
    5.246261363759537,
    7.563095318856336,
    14.512015318183966,
    8.964273641231186,
    19.519214933943296
];
var data3 = [
    0,
    0.09586079201757203,
    0.6130597918853078,
    1.883318053017105,
    0.5831421830175554,
    3.0906749420124657,
    5.375923181510496,
    0.24565436375770222,
    2.504219761211967,
    1.2826489665166168,
    2.57435585252223,
    4.687376900543496,
    5.31918291858204,
    4.393133378618449,
    7.622920923104461,
    6.6255116621890675,
    4.958815970676856,
    0.21326328282487572,
    1.737140889765183,
    12.391432173501398,
    11.46601718393704,
    16.650701511074292,
    0.43370103946505,
    21.632449827092536,
    19.474251446784095,
];

$("#number1").text(allNumber.number1);
$("#number2").text(allNumber.number2);

var checkTab = false;

function settingButton() {

    if (checkTab == false) {
        alert("直当たりPs数、可動率、Loss率を一つタブ選んでください！！！");
        return;
    }

    $("#datepicker1").datepicker({
        buttonImage: "",        // カレンダーアイコン画像
        buttonText: "", // ツールチップ表示文言
        buttonImageOnly: true,           // 画像として表示
        showOn: "both"                   // カレンダー呼び出し元の定義
    });

    // 日本語化
    $.datepicker.regional['ja'] = {
        closeText: '閉じる',
        prevText: '<前',
        nextText: '次>',
        currentText: '今日',
        monthNames: ['1月', '2月', '3月', '4月', '5月', '6月',
            '7月', '8月', '9月', '10月', '11月', '12月'],
        monthNamesShort: ['1月', '2月', '3月', '4月', '5月', '6月',
            '7月', '8月', '9月', '10月', '11月', '12月'],
        dayNames: ['日曜日', '月曜日', '火曜日', '水曜日', '木曜日', '金曜日', '土曜日'],
        dayNamesShort: ['日', '月', '火', '水', '木', '金', '土'],
        dayNamesMin: ['日', '月', '火', '水', '木', '金', '土'],
        weekHeader: '週',
        dateFormat: 'yy/mm/dd',
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: true,
        yearSuffix: '年'
    };
    $.datepicker.setDefaults($.datepicker.regional['ja']);

    $(".icon-date-1").click(function (e) {
        e.preventDefault();
        $("#datepicker1").focus();
    });

    $("#datepicker2").datepicker({
        buttonImage: "",        // カレンダーアイコン画像
        buttonText: "", // ツールチップ表示文言
        buttonImageOnly: true,           // 画像として表示
        showOn: "both"                   // カレンダー呼び出し元の定義
    });

    // 日本語化
    $.datepicker.regional['ja'] = {
        closeText: '閉じる',
        prevText: '<前',
        nextText: '次>',
        currentText: '今日',
        monthNames: ['1月', '2月', '3月', '4月', '5月', '6月',
            '7月', '8月', '9月', '10月', '11月', '12月'],
        monthNamesShort: ['1月', '2月', '3月', '4月', '5月', '6月',
            '7月', '8月', '9月', '10月', '11月', '12月'],
        dayNames: ['日曜日', '月曜日', '火曜日', '水曜日', '木曜日', '金曜日', '土曜日'],
        dayNamesShort: ['日', '月', '火', '水', '木', '金', '土'],
        dayNamesMin: ['日', '月', '火', '水', '木', '金', '土'],
        weekHeader: '週',
        dateFormat: 'yy/mm/dd',
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: true,
        yearSuffix: '年'
    };
    $.datepicker.setDefaults($.datepicker.regional['ja']);

    $(".icon-date-2").click(function (e) {
        e.preventDefault();
        $("#datepicker2").focus();
    });

    $("#myModal_choukiteishi").attr("class", "modal");
    $("#modal_content_memo").show();

    var myDate = new Date();
    var firstDay = new Date(myDate.getFullYear(), myDate.getMonth(), 1);
    var lastDay = new Date(myDate.getFullYear(), myDate.getMonth() + 1, 0);

    $('#datepicker1').datepicker('setDate', firstDay);
    $('#datepicker2').datepicker('setDate', lastDay);

    $("#togglePopup1").click(function () {
        $("#myModal_choukiteishi").attr("class", "");
        $("#modal_content_memo").hide();
    });

}

var drawOne1 = true;
var drawOne2 = true;
var drawOne3 = true;

$("#togglePopup2").click(function () {

    console.log(12344);

    $("#number1").text(allNumber.number1);
    $("#number2").text(allNumber.number2);
    var dates = [];
    var dateStart = $("#datepicker1").val();
    var dateEnd = $("#datepicker2").val();
    // var graphDisplayTime = $('#select2').val();
    var deviceNumber = $("#select1").val();

    var myDateStart = new Date(dateStart);
    var myDateEnd = new Date(dateEnd);

    var startDate = new Date(myDateStart.getFullYear(), myDateStart.getMonth(), myDateStart.getDate() - 1);
    var endDate = new Date(myDateEnd.getFullYear(), myDateEnd.getMonth(), myDateEnd.getDate());

    while (startDate < endDate) {
        startDate.setDate(startDate.getDate() + 1);
        const options = { month: 'long', day: 'numeric' };
        dates.push(new Date(startDate).toLocaleDateString('ja-JP', options));
    }

    getYear = myDateStart.getFullYear();


    if (myDateStart.getMonth() !== myDateEnd.getMonth()) {
        alert("一月だけ入力が出来");
        return;
    }
    if (startDate == "Invalid Date" || endDate == "Invalid Date") {
        alert("日付を選んでください！！！");
        return;
    }

    if (deviceNumber == "0") {
        alert("設備を設定してください！！！");
        return;
    }

    var newDate = [];

    for (let index = 0; index < dates.length; index++) {
        const element = dates[index].concat("  " + date[index]);
        newDate.push(element);
    }

    var number3 = parseFloat($("#select3").val());
    var number4 = parseFloat($("#select4").val());

    var sumNumber1 = parseFloat(allNumber.number1) + number3;
    var sumNumber2 = parseFloat(allNumber.number2) + number4;

    console.log(sumNumber1);
    console.log(sumNumber2);

    $("#myModal_choukiteishi").attr("class", "");
    $("#modal_content_memo").hide();

    if (temp == "直当たりPs数") {
        $("#lineGraph1").empty();
        // var data1 = [
        //     0.601681335640518, 0.8080722995827196, 0.2552626233133728,
        //     0.5317394769033714, 0.5358891786465578, 0.05942035380246735,
        //     0.04718643895840935, 0.8058405879016064, 0.5150273151362872,
        //     0.8991182650206513, 0.09655946367347146, 0.9465920984641867,
        //     0.2494146624559619, 0.08474395807546942, 0.7834440334258439,
        //     0.17057477989266934, 0.3696568894750156, 0.3623666957995959,
        //     0.903913008520389, 0.0878902291249457, 0.05906104949788382,
        //     0.07479227067412353, 0.30639628429522725, 0.9293211329163098,
        //     0.1482450396200874, 0.19862794634125858, 0.5907450318650147,
        //     0.4611233267608661, 0.8901685489497178, 0.14393778947017233,
        //     0.4599610208450682, 0.058152362693591586, 0.6835681003822165,
        //     0.9063383676563719, 0.47846939998134985, 0.2571191581299985,
        //     0.8545957661274588, 0.0869094790754783, 0.06727372005438137,
        //     0.15166129257010663, 0.8969464090000914, 0.9584824098610203,
        //     0.0032793358400318162, 0.8605756954188652, 0.49934227918277885,
        //     0.6355019813241167, 0.09075746423178743, 0.6063612916942065,
        //     0.7551042201509648, 0.07245913565656781, 0.019345264506300808,
        //     0.2540611435930249, 0.1694404401818672, 0.6601148557804832,
        //     0.1655987100563714, 0.11755247253733114, 0.587641553074612,
        //     0.14519222701223966, 0.8582458984795194, 0.750013619017029,
        //     0.9991538396541453, 0.4580623258587122, 0.2257413506767092,
        //     0.5604992825346515, 0.2468381559609989, 0.6257502224340552,
        //     0.8816289299643569, 0.9308409317326605, 0.48171835609555935,
        //     0.3191817722688246, 0.6722096833145417, 0.06714545775325309,
        //     0.6051655000953817
        // ];

        // var data2 = [
        //     0.023847948286151555, 0.6549095161634055, 0.21769411397756921,
        //     0.39694404115423265, 0.3186250942336655, 0.18384648632490852,
        //     0.5620255402193564, 0.3243488017335665, 0.4634593326141583,
        //     0.8955621425672999, 0.41780292665432417, 0.5215433883517422,
        //     0.09638559438861849, 0.8753148900496766, 0.31538136676808026,
        //     0.5025743469606818, 0.6646223637230542, 0.9674125747483959,
        //     0.49430562696596736, 0.7366727840642049, 0.8670460134355635,
        //     0.7793727698242889, 0.7436213065806867, 0.9190273967621172,
        //     0.24684982474934736, 0.5699255790425701, 0.8437536316182841,
        //     0.9576751872892817, 0.09795548963578904, 0.5018102434978275,
        //     0.6838312402449154, 0.08522835125156769, 0.5727752635706134,
        //     0.1824826792718348, 0.33931087460044873, 0.8668329294355595,
        //     0.8731350101012623, 0.25891235477513685, 0.6568628800554754,
        //     0.9146206380189592, 0.9824362665175252, 0.7933639114444884,
        //     0.20682478156860418, 0.7372246102043132, 0.37354395906810933,
        //     0.022542107325445704, 0.9734199807715105, 0.13614283417225748,
        //     0.2326456161218342, 0.20161702241199841, 0.18682100239676802,
        //     0.4847234936177425, 0.26232982470073773, 0.25290065980673804,
        //     0.8129230530715619, 0.8169155425940298, 0.9015190489356701,
        //     0.5905411992084577, 0.9809079817363744, 0.7548220963459411,
        //     0.07755960988677102, 0.031325561406317926, 0.2196177411366469,
        //     0.6457200746260467, 0.574516135157082, 0.9126036710945997,
        //     0.4499626203976619, 0.043075033270941754, 0.5507339186932452,
        //     0.8761783129146596, 0.051404640822863, 0.5442576884268848,
        //     0.029498518418833886
        // ]

        var dataRadom = [];
        var dataRadom2 = [];
        for (let index = 0; index < dates.length; index++) {
            for (let index1 = 0; index1 <= 2; index1++) {
                var radomDate = Math.random() * index;
                var radomDate2 = Math.random() * (index / 2);
                dataRadom.push(radomDate);
                dataRadom2.push(radomDate2);

            }

        }
        // for (let index = 0; index < 72; index++) {
        //     var radomDate = Math.random() * index;
        //     var radomDate2 = Math.random() * (index / 2);
        //     dataRadom.push(radomDate);
        //     dataRadom2.push(radomDate2);
        // }

        drawGraph(dates, dataRadom, dataRadom2, true, temp);
        drawOne1 = false;
        getYear = "";
    } else if (temp == "可動率") {
        $("#lineGraph2").empty();
        // var data2 = [
        //     0,
        //     0.44382090655997275,
        //     1.351036016039361,
        //     0.7265420712904793,
        //     0.39579941383083916,
        //     1.258926030419839,
        //     5.8060939421928595,
        //     0.12264858646871413,
        //     6.667045737126248,
        //     8.201343534419847,
        //     2.4001979892229053,
        //     1.7221305174623291,
        //     0.7349697655925613,
        //     0.7744158515109376,
        //     13.357917002053133,
        //     6.497523639578535,
        //     12.999998728570013,
        //     3.6945763837161185,
        //     9.612967245522857,
        //     5.568163766255545,
        //     5.246261363759537,
        //     7.563095318856336,
        //     14.512015318183966,
        //     8.964273641231186,
        //     19.519214933943296
        // ];
        var dataRadom = [];
        var dataRadom2 = [];
        for (let index = 0; index < dates.length; index++) {
            for (let index1 = 0; index1 <= 2; index1++) {
                var radomDate = Math.random() * index;
                var radomDate2 = Math.random() * (index / 2);
                dataRadom.push(radomDate);
                dataRadom2.push(radomDate2);

            }

        }
        if ($("#select3").val()) {
            var maxNum = Math.max.apply(null, data2);
            data2 = $.grep(data2, function (n) {
                return n != maxNum;
            });
            data2.push(sumNumber1);
        }

        drawGraph(dates, dataRadom, dataRadom2, true, temp);
        // drawGraph(dates, data2, true, temp);
        drawOne2 = false;
        getYear = "";

    } else if (temp == "Loss率") {
        $("#lineGraph3").empty();
        // var data3 = [
        //     0,
        //     0.09586079201757203,
        //     0.6130597918853078,
        //     1.883318053017105,
        //     0.5831421830175554,
        //     3.0906749420124657,
        //     5.375923181510496,
        //     0.24565436375770222,
        //     2.504219761211967,
        //     1.2826489665166168,
        //     2.57435585252223,
        //     4.687376900543496,
        //     5.31918291858204,
        //     4.393133378618449,
        //     7.622920923104461,
        //     6.6255116621890675,
        //     4.958815970676856,
        //     0.21326328282487572,
        //     1.737140889765183,
        //     12.391432173501398,
        //     11.46601718393704,
        //     16.650701511074292,
        //     0.43370103946505,
        //     21.632449827092536,
        //     19.474251446784095,
        // ];
        var dataRadom = [];
        var dataRadom2 = [];
        for (let index = 0; index < dates.length; index++) {
            for (let index1 = 0; index1 <= 2; index1++) {
                var radomDate = Math.random() * index;
                var radomDate2 = Math.random() * (index / 2);
                dataRadom.push(radomDate);
                dataRadom2.push(radomDate2);

            }

        }

        if ($("#select4").val() != "0") {
            var maxNum = Math.max.apply(null, data3);
            data3 = $.grep(data3, function (n) {
                return n != maxNum;
            });
            data3.push(sumNumber2);
        }

        drawGraph(dates, dataRadom, dataRadom2, true, temp);
        drawOne3 = false;
        getYear = "";
    }

});

function changeRad1() {
    checkTab = true;
    $("#lineGraph2").hide();
    $("#lineGraph3").hide();
    $("#lineGraph1").show();
    $("#select1").val("0");
    if (drawOne1) {
        $("#lineGraph1").empty();
        drawGraph(date, data1, data1, false, "直当たりPs数");
        getYear = "";
    }
    temp = "直当たりPs数";

}

function changeRad2() {
    checkTab = true;
    $("#lineGraph1").hide();
    $("#lineGraph3").hide();
    $("#lineGraph2").show();
    $("#select1").val("0");
    if (drawOne2) {
        $("#lineGraph2").empty();
        drawGraph(date, data2, data2, false, "可動率");
        getYear = "";
    }
    temp = "可動率";

}

function changeRad3() {
    checkTab = true;
    $("#lineGraph1").hide();
    $("#lineGraph2").hide();
    $("#lineGraph3").show();
    $("#select1").val("0");
    if (drawOne3) {
        $("#lineGraph3").empty();
        drawGraph(date, data3, data3, false, "Loss率");
        getYear = "";
    }
    temp = "Loss率";
}

function outputCSV() {
    alert("CSVファイルを出力しました。！！！");
}

function refeshButton() {
    alert("リロードしました。！！！");
}