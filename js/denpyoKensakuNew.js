String.prototype.splitCSV = function(sep) {
  for (var foo = this.split(sep = sep || ","), x = foo.length - 1, tl; x >= 0; x--) {
    if (foo[x].replace(/"\s+$/, '"').charAt(foo[x].length - 1) == '"') {
      if ((tl = foo[x].replace(/^\s+"/, '"')).length > 1 && tl.charAt(0) == '"') {
        foo[x] = foo[x].replace(/^\s*"|"\s*$/g, '').replace(/""/g, '"');
      } else if (x) {
        foo.splice(x - 1, 2, [foo[x - 1], foo[x]].join(sep));
      } else foo = foo.shift().split(sep).concat(foo);
    } else foo[x].replace(/""/g, '"');
  } return foo;
};
var DenpyoKensaku = {

    kakuteiProperty : "kakutei",
    setsumei : "",
    viewUrl : null,
    initUrl : null,
    footerHeight : 50 + 14,
    $shosaiKensakuFlag : null,
    detailUrl : null,
    keywordDefaultMsg : [],
    init: function() {
        if ($.browser.msie && $.browser.version.match(/^7/)) {
            var listWidth = $("#listTable").outerWidth(true);
            var searchConditionWidth = $("table.d_kensaku_table:first").outerWidth(true);
            if (listWidth > searchConditionWidth) {
                $("#denlist").width(listWidth);
            } else {
                $("#denlist").width(searchConditionWidth);
            }
        }
    },
    search : function(buttonNode) {
        this._uncheckKakutei();
        var $pageNum = $("#denpyoKensakuPageNum");
        $pageNum.val("0");
        var $form = $(buttonNode).closest("form");
        DenpyoKensaku.clearDefaultMsg();
        $form.submit();
    },

    _uncheckKakutei : function() {
        var $kakuteiList = this._getKakuteiList();
        $kakuteiList.prop("checked", false);
    },

    _resetValue : function() {
        $("div.denpyoKensakuArea").find(":input").each(function() {
            $component = $(this);
            switch (this.type) {
                case 'select-one':
                    for(var i = 0; i < $component.prop("options").length; i++) {
                        var option = $component.prop("options")[i];
                        if (option.defaultSelected) {
                            $component.val(option.value);
                        }
                    }
                    break;
                case 'checkbox':
                case 'radio':
                    if ($component.prop("defaultChecked") || $component.data("defaultChecked")) {
                        $component.prop("checked", true);
                    } else {
                        $component.prop("checked", false);
                    }
                    break;
                case 'text':
                    $component.triggerHandler("focus.cache");
                    $component.val($component.prop("defaultValue"));
                    $component.triggerHandler("blur.blurEvent");
                    break;
                case 'hidden':
                    break;
            }

        });
        if (this.$shosaiKensakuFlag) {
            var $shosaiKensakuFlag = this.$shosaiKensakuFlag;
            var defaultValue = $shosaiKensakuFlag.data("defaultValue");
            var bk = $shosaiKensakuFlag.val();
            $shosaiKensakuFlag.val(defaultValue);
            if (defaultValue != bk) {
                this.foldCondition($("#denpyoKensakuShosaiLink"));
            }
        }
    },

    _getKakuteiList : function() {
        if (this.kakuteiProperty.match(/\(/)) {
            return $("input[name^='" + this.kakuteiProperty + "']");
        } else {
            return $("input[name='" + this.kakuteiProperty + "']");
        }
    },

    checkKakutei : function() {
        var $kakutei = $("input[name^='kakutei'][type=checkbox]");
        if (0 < $kakutei.not(":checked").length) {
            $kakutei.prop("checked", true);
        } else {
            $kakutei.prop("checked", false);
        }
    },

    sort : function(propertyName) {
        this._resetValue();
        this._uncheckKakutei();
        var $orderByProperty = $("#denpyoKensakuOrderByProperty");
        var changeDirectionFlag = $orderByProperty.val() == propertyName;
        $orderByProperty.val(propertyName);
        var $orderByDirection = $("#denpyoKensakuOrderByDirection");
        var orderByDirection;
        if (!changeDirectionFlag) {
            orderByDirection = "desc";
        } else if ($orderByDirection.val() == "desc") {
            orderByDirection = "asc";
        } else {
            orderByDirection = "desc";
        }
        $orderByDirection.val(orderByDirection);
        var $isBarcode = $("input[type=hidden][name='isBarcodeMode']");
        var $isKariShihaKbn = $("input[type=hidden][name='kariShihaKbn']");
        if (($isKariShihaKbn.length !== 0) || ($isBarcode.length !== 0 && $isBarcode.val() == "false")) {
            $("input[name=allCheckKbn][type=hidden]").val(3);
        }
        $("#denpyoKensakuPageNum").val(0);
        $("form").submit();
    },

    pageFeed : function(pageNum) {
        this._resetValue();
        this._uncheckKakutei();
        var $pageNum = $("#denpyoKensakuPageNum");
        $pageNum.val(pageNum);
        $("form").submit();
    },

    limitChange : function(selectNode) {
        var $form = $(selectNode).closest("form");
        var limitNum = $(selectNode).val();
        this._resetValue();
        this._uncheckKakutei();
        var $limit = $("#denpyoKensakuLimit");
        $limit.val(limitNum);
        $("#denpyoKensakuShoriKbn").val("1");
        $form.submit();
    },

    changeTab : function(anchorNode, tmpFlg) {
        this._resetValue();
        this._uncheckKakutei();
        $("input[name=tmpFlg]").val(tmpFlg);
        $("#denpyoKensakuPageNum").val(0);
        $("form").submit();
    },

    switchTab : function(anchorNode, method) {
        var $form = $(anchorNode).closest("form");
        var url = $form.attr("action").replace(/[^/]+\/?$/, method);
        location.href = url;
    },

    addDisabledAll : function(tmpFlg) {
        var $denpyoKensakuArea = $("div.denpyoKensakuArea");
        if (tmpFlg) {
            f.addDisabled($denpyoKensakuArea.find(":input").not(".imgButton"));
            var $kensakuButton = $("input.kensaku");
            f.addDisabled($kensakuButton);
            $kensakuButton.attr("title", "一時保存データは検索できません");
            var $datePick = $("div.datePick");
            $datePick.addClass("dp-disabled");
            f.addDisabled($datePick.next());
        } else {
            f.removeDisabled($denpyoKensakuArea.find(":input").not(".imgButton"));
            var $kensakuButton = $("input.kensaku");
            f.removeDisabled($kensakuButton);
            $kensakuButton.removeAttr("title");
            var $datePick = $("div.datePick");
            $datePick.removeClass("dp-disabled");
            f.removeDisabled($datePick.next());
        }
    },

    checkAll : function(buttonNode) {
        var $outputDenpyo = $(buttonNode).parent("div").parent("td");
        var $checkBoxList = $outputDenpyo.find("input[type=checkbox]").not(":disabled");
        for (var i = 0; i < $checkBoxList.length; i++) {
            var $checkBox = $checkBoxList.eq(i);
            $checkBox.prop("checked", true);
        }
    },

    uncheckAll : function(buttonNode) {
        var $outputDenpyo = $(buttonNode).parent("div").parent("td");
        var $checkBoxList = $outputDenpyo.find("input[type=checkbox]").not(":disabled");
        for (var i = 0; i < $checkBoxList.length; i++) {
            var $checkBox = $checkBoxList.eq(i);
            var defaultChecked = $checkBox.prop("defaultChecked");
            $checkBox.prop("checked", false);

            $checkBox.data("defaultChecked", defaultChecked);
        }
    },

    submitMain : function(buttonNode, method) {
        var $form = $(buttonNode).closest("form");
        if (method) {
            var action = $form.attr("action");
            $form.attr("action", action.replace(/[^/]+\/?$/, method));
        }
        $form.submit();
    },

    submitDownload : function(buttonNode) {
        var $form = $(buttonNode).closest("form");
        var $listContents = $("div.content");
        var $successContents = $("#successMsg");
        var url = $form.attr("action");
        var $componentList = $("[name]", $form);
        var param = f.getRequestParam($componentList);
        var async = false;
        var xmlHttpRequest = $.ajax({
            type: "POST",
            url: url.replace(/[^/]+\/?$/, "downloadDataSearch"),
            data: param,
            cache: false,
            async: async,
            success: function(html){
                var responseText = html;
                responseText = f.trim(responseText);
                if (!f.checkResponseText(responseText)) {
                    return;
                }
                var data = eval("("+responseText+")");
                $listContents.attr("style","display:none");
                $successContents.attr("style","display:block");
                $form.attr("action", url.replace(/[^/]+\/?$/, "download"));
                $form.submit();
            }
        });
    },

    bindOneShonin : function() {
        var $shoninButton = $("input.multiShonin,button.multiShonin");
        $shoninButton.one("click", function() {
            DenpyoKensaku.shoninExecute(this);
        });
    },

    bindOneBack : function() {
        var $backButton = $("button.multiBack");
        $backButton.one("click", function() {
            DenpyoKensaku.backExecute(this);
        });
    },

    shoninExecute : function(buttonNode) {
        var $shonin = $("input[name^='shonin'][type=checkbox]");
        if (DenpyoKensaku.alertCheckbox($shonin)) {
            DenpyoKensaku.bindOneShonin();
            return;
        }

        var selfApprovingShainDenpyoArray = [];
        var selfApprovingInputShainDenpyoArray = [];
        var data = this.getSelfApprovingDenpyo();
        if (data) {
            if (data.error) {
                return;
            }
            selfApprovingShainDenpyoArray = data.selfApprovingShainDenpyoArray;
            selfApprovingInputShainDenpyoArray = data.selfApprovingInputShainDenpyoArray;
        }

        if (selfApprovingShainDenpyoArray.length > 0 || selfApprovingInputShainDenpyoArray.length > 0) {
            var message = this.createSelfApprovingMessage(selfApprovingShainDenpyoArray, selfApprovingInputShainDenpyoArray);
            var $confirmWindow = f.showConfirmWindow({
                message:message,
                messageTextFlag:false,
                onCancel:function() {
                    DenpyoKensaku.bindOneShonin();
                },
                onKakutei:function() {
                    DenpyoKensaku.selfApprovalAlertFlag = false;
                    DenpyoKensaku.shoninExecute(buttonNode);
                }
            });
        } else {
            var $form = $(buttonNode).closest("form");
            $form.find("input[name=shoninShoriKbn]").val(1);
            var method = "execute";
            this.submitMain(buttonNode, method);
        }
    },

    backExecute : function(buttonNode) {
        var $shonin = $("input[name^='shonin'][type=checkbox]");
        if (DenpyoKensaku.alertCheckbox($shonin)) {
            DenpyoKensaku.bindOneBack();
            return;
        }

        f.showConfirmWindow({
            message: Locale["denpyoKensakuNew_backExecuteConfirmMsg"],
            onCancel:function($confirmWindow) {
                DenpyoKensaku.bindOneBack();
            },
            onKakutei:function($confirmWindow) {
                var $form = $(buttonNode).closest("form");
                $form.find("input[name='shoninShoriKbn']").val(2);
                var method = "execute";
                DenpyoKensaku.submitMain(buttonNode, method);
            }
        });
    },

    submitCancel : function(buttonNode) {
        $(buttonNode).removeAttr("onclick");
        $("input[name=shoriKbn]").val(1);
        var method = "search";
        this.submitMain(buttonNode, method);
    },

    submitBack : function(buttonNode, method) {
        if (typeof method == "undefined") {
            method = "initializeView";
        }
        $("input[name=shoriKbn]").val(0);
        this.submitMain(buttonNode, method);
    },

    initMove : function() {
        this._resetValue();
        this._uncheckKakutei();
        var $form = $("form:first");
        var $componentList = $("input[name]", $form);
        var param = "?" + f.getRequestParam($componentList);
        var url = this.initUrl + param;
        f.openWindow(url, window, null, {width:1024,height:768});
    },

    detailMove : function() {
        var args = arguments;
        this._resetValue();
        this._uncheckKakutei();
        for (var i = 0; i < args.length; i++) {
            $("#denpyoKensakuArg" + i).val(args[i]);
        }
        var $form = $("form:first");
        var $componentList = $("input[name]", $form);
        var param = "?" + f.getRequestParam($componentList);
        var url = this.viewUrl + param;
        f.openWindow(url, window, null, {width:1024,height:768});
    },

    moveMain : function() {
        var $form = $("form");
        $form.submit();
    },

    moveNext : function(buttonNode, url) {
        $(buttonNode).removeAttr("onclick");
        f.makeLoader();
        location.href = url;
    },

    foldShosaiKensaku : function(buttonNode, targetClass) {
        var $button = $(buttonNode);
        var $tr = $button.closest("tr");
        var isExtend = 0 < $button.filter(".extend").length;
        $button.hide();
        if (isExtend) {
            var $shosaiKensakuFlag = $("input[name='" + targetClass + "ShosaiKensakuFlag']");
            var shosaiKensakuFlagBk = $shosaiKensakuFlag.val();
            $shosaiKensakuFlag.val(1);
            $button.before($button.siblings(".fold").show());
            var $targetTr = $tr.nextAll("tr." + targetClass);
            $targetTr.show();
            f.removeDisabled($("input", $targetTr));
            if (shosaiKensakuFlagBk != 1) {
                f.focusFirstElement($("input", $targetTr));
            }
        } else {
            var $shosaiKensakuFlag = $("input[name='" + targetClass + "ShosaiKensakuFlag']");
            var shosaiKensakuFlagBk = $shosaiKensakuFlag.val();
            $shosaiKensakuFlag.val(0);
            $button.before($button.siblings(".extend").show());
            var $targetTr = $tr.nextAll("tr." + targetClass);
            $targetTr.hide();
            f.addDisabled($("input", $targetTr));
            if (shosaiKensakuFlagBk != 0) {
                f.focusFirstElement($("input", $tr));
            }
        }

        var $frameset = window.parent.$("frameset");
        if (0 < $frameset.length) {
            if (isExtend) {
                $frameset.data("defaultRows", $frameset.attr("rows"));
                var $content = $("div.content");
                $frameset.attr("rows", ($content.offset().top + $content.height() + 15) + ",*");
            } else {
                var defaultRows = $frameset.data("defaultRows");
                if (defaultRows) {
                    $frameset.attr("rows", defaultRows);
                }
            }
        }
    },

    initFoldCondition : function(isShosaiMode) {
        var $shosaiKensakuFlag = $("input[name=shosaiKensakuFlag]");
        $shosaiKensakuFlag.data("defaultValue", $shosaiKensakuFlag.val());
        this.$shosaiKensakuFlag = $shosaiKensakuFlag;
        if (isShosaiMode) {
            this.foldCondition($("#denpyoKensakuShosaiLink"));
        }
    },

    foldCondition : function(anchorNode) {
        var $denpyoKensakuArea = $(anchorNode).closest("div.denpyoKensakuArea");
        var $shosaiKensakuFlag = this.$shosaiKensakuFlag;
        $denpyoKensakuArea.toggleClass("shosaiMode");
        if ($denpyoKensakuArea.hasClass("shosaiMode")) {
            $shosaiKensakuFlag.val(1);
            f.focusFirstElement(f.getFirstElement($("table.shosaiKensakuArea")));
        } else {
            $shosaiKensakuFlag.val(0);
        }
    },

    fileDataDownloadUrl : null,

    downloadFileData : function(spanNode, tmpFlg, fileNo) {
        if (typeof tmpFlg == "undefined") {
            tmpFlg = $("input[name=fileDataTmpFlg1]").val();
        }
        if (typeof fileNo == "undefined") {
            fileNo = $("input[name=eFileNo1]").val();
        }
        var url = this.fileDataDownloadUrl;
        url = url.replace("%s", tmpFlg);
        url = url.replace("%d", encodeURIComponent(fileNo));
        f.openWindow(url, window, null, {width:1,height:1});
    },

    setsumeiTooltip : function() {
        var $tooltipTarget = $("span.setsumeiTooltip");
        $tooltipTarget.removeData("tooltipContent");
        var preTpl = "<div class=\"setsumeiTooltipPanel\" style=\"background-color: #ffffff; border: 1px solid #999999;\"><div style=\"width: 460px; height: 16px;background-color:rgb(56, 120, 190);\"></div><div style=\"margin-left:10px;width:440px;padding-top: 5px; padding-right: 5px; padding-bottom: 5px; padding-left: 5px;\"><p>";
        var postTpl = "</p></div></div>";
        var setsumei = this.setsumei;
        var tpl = preTpl + setsumei + postTpl;
        $tooltipTarget.bind("mouseenter", function(ev) {
            var $tooltipTarget = $(this);
            var $tooltip = $(tpl);

            var top = ev.pageY;
            var left = ev.pageX;
            var $body = $("body");
            $body.append($tooltip);
            $tooltip.css({
                "position" : "absolute",
                "top": (top + 10),
                "left": left
            });

            var bodyWidth = $body.width();
            bodyWidth = bodyWidth - 50;
            var tooltipLeft = left + 20;
            var tooltipWidth = $tooltip.width();
            var rightEdgeFlag = false;
            if (bodyWidth <= ( tooltipLeft + tooltipWidth)) {
                $tooltip.css("left", bodyWidth - tooltipWidth - 10);
                rightEdgeFlag = true;
            } else {
                $tooltip.css("left", tooltipLeft);
            }

            var bodyHeight = $body.height();
            bodyHeight = bodyHeight - 15;
            var tooltipTop = top + 10;
            var tooltipHeight = $tooltip.height();
            if (bodyHeight <= (tooltipTop + tooltipHeight)) {
                if (rightEdgeFlag) {
                    tooltipTop = top - tooltipHeight - 15;
                } else {
                    tooltipTop = bodyHeight - tooltipHeight;
                }
                $tooltip.css("top", tooltipTop);
            }

            $tooltipTarget.data("tooltipPanel", $tooltip);
        }).bind("mouseleave", function(ev){
            var $tooltipTarget = $(this);
            var $tooltip = $tooltipTarget.data("tooltipPanel");
            if ($tooltip) {
                $tooltip.remove();
            }
        });
    },

    bindCheckbox : function() {
        var $parentCheckbox = $("input.headerAllCheck");
        var $childCheckbox = this._getKakuteiList();

        var $childbin = $childCheckbox.bind("click.bindcheckbox", function() {
            var checkLength = $childCheckbox.filter("input:checked").length;
            if (checkLength > 0 && $childCheckbox.not(":disabled").length === checkLength) {
                $parentCheckbox.prop("checked", true);
            } else {
                $parentCheckbox.prop("checked", false);
            }
        });
        if ($childbin.length !== 0) {
            $childbin.eq(0).triggerHandler("click.bindcheckbox");
        }

        $parentCheckbox.bind("click.bindcheckbox", function() {
            if ($(this).is(":checked")) {
                $childCheckbox.not(":disabled").prop("checked", true);
            } else {
                $childCheckbox.prop("checked", false);
            }
        });
    },

    alertCheckbox : function($target) {
        if ($target.not(":checked").length === $target.length) {
            f.showConfirmWindow({
                message: Locale["denpyoKensakuNew_alertCheckboxMsg"],
                cancelLabel: Locale["denpyoKensakuNew_alertCheckboxCancelLabel"],
                onCancel:function($confirmWindow) {
                }
            }).find("input.confirmKakutei").hide();
            return true;
        }
        return false;
    },

    windowClose : function() {
        var win = window.parent || window;
        win.close();
    },

    warningTooltip : function() {
        var $divContent = $("div.content");
        var $tooltipTargetList = $("span.icon_alert");
        var tpl = "<div class=\"d_warning_tooltip\"></div>";
        var header = "<tr><td class=\"message\">";
        $tooltipTargetList.each(function() {
            var $tooltipTarget = $(this);
            $tooltipTarget.removeData("tooltipContent");
            var title = $tooltipTarget.attr('title');
            $tooltipTarget.removeAttr('title');
            var messageArray = title.split('\u001f');
            var content = "<table cellspacing=\"0\" cellpadding=\"0\">";
            for (var i = 0; i < messageArray.length; i++) {
                var messageTable = "";
                var matches = messageArray[i].match(/^([^,]+),([\s\S]+)/);
                var meisaiGyo = matches[1];
                if (meisaiGyo != "-9") {
                    var meisaiNo = meisaiGyo > -1 ? "<td style=\"white-space: nowrap;vertical-align: top;\">"+ Locale["denpyoKensakuNew_warningTooltipMeisaiNo"] + (parseInt(meisaiGyo) + 1) + ":</td>" : "";
                    var message = "<td>" + f.escapeTooltip(matches[2]) + "</td>";
                    messageTable += "<table cellspacing=\"0\" cellpadding=\"0\"><tr>" + meisaiNo + message + "</tr></table>";
                } else {
                    var arrMessage = matches[2].splitCSV();
                    messageTable += "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">";
                    messageTable += "<tr><td>" + f.escapeTooltip(arrMessage[0]) + "</td></tr>";
                    if (arrMessage[4] == "1") {
                        messageTable += "<tr><td>" + f.escapeTooltip(arrMessage[1]) + "</td></tr>";
                        messageTable += "<tr><td style=\"padding-top: 3px\"><table cellspacing=\"0\" cellpadding=\"0\">";
                        messageTable += "<tr><td>" + f.escapeTooltip(arrMessage[2]) + "</td><td style=\"padding-left: 20px;\" class=\"text-align-right\">" + f.addComma(arrMessage[5], 0) + "</td></tr>";
                        messageTable += "<tr><td>" + f.escapeTooltip(arrMessage[3]) + "</td><td style=\"padding-left: 20px;\" class=\"text-align-right\">" + f.addComma(arrMessage[6], 0) + "</td></tr>";
                        messageTable += "<tr><td>" + Locale["denpyoKensakuNew_shinsei_kingaku"] + "</td><td style=\"padding-left: 20px;\" class=\"text-align-right\">" + f.addComma(arrMessage[7], 0) + "</td></tr>";
                        messageTable += "</table></td></tr>";
                    } else {
                        messageTable += "<tr><td>" + f.escapeTooltip(arrMessage[1]) + " " + f.escapeTooltip(arrMessage[2]) + "</td></tr>";
                    }
                    messageTable += "</table>";
                }
                content = content + header + messageTable + "</td></tr>";
                if (i < messageArray.length - 1) {
                    content = content + "<tr><td><hr style=\"background: #999;height: 1px;border: 0 solid;\"></td></tr>";
                }
            }
            var tooltipContent = content + "</table>";
            if (tooltipContent.length <= 0) {
                return;
            }
            $tooltipTarget.data("tooltipContent", tooltipContent);
        });
        $tooltipTargetList.bind("mouseenter", function(ev) {
            var $tooltipTarget = $(this);
            var tooltipContent = $tooltipTarget.data("tooltipContent");
            if (tooltipContent.length <= 0) {
                return;
            }
            var $tooltip = $(tpl);
            $tooltip.append(tooltipContent);
            $divContent.addClass("d_tooltipContent").append($tooltip);

            var maxMessageWidth = 500;
            var messageWidth = $tooltip.find("td.message").outerWidth(true);
            if (messageWidth > maxMessageWidth) {
                $tooltip.find("td.message").css("width", maxMessageWidth + "px");
            }

            var top = ev.pageY;
            var tooltipTop = top + 5;
            var bodyHeight = $("body").height();
            var tooltipHeight = $tooltip.outerHeight(true);
            if (bodyHeight <= (tooltipTop + tooltipHeight)) {
                tooltipTop = top - tooltipHeight - 5;
            }
            var left = ev.pageX;
            var bodyWidth = $divContent.width();
            var tooltipLeft = left + 5;
            var tooltipWidth = $tooltip.outerWidth(true);
            if (bodyWidth <= (tooltipLeft + tooltipWidth)) {
                tooltipLeft = left - tooltipWidth - 5;
            }
            tooltipTop = tooltipTop < 0 ? 0 : tooltipTop;
            tooltipLeft = tooltipLeft < 0 ? 0 : tooltipLeft;
            $tooltip.css({
                "top": tooltipTop,
                "left": tooltipLeft
            });

            $tooltipTarget.data("tooltipPanel", $tooltip);
        }).bind("mouseleave", function(ev) {
            $divContent.removeClass("d_tooltipContent");
            var $tooltipTarget = $(this);
            var $tooltip = $tooltipTarget.data("tooltipPanel");
            if ($tooltip) {
                $tooltip.remove();
            }
        });
    },

    setDefaultMsg : function($searchButton){
        for (key in DenpyoKensaku.keywordDefaultMsg) {
            var msg = DenpyoKensaku.keywordDefaultMsg[key];
            $("input[name='" + key + "']").bind("focus.initMsg",msg,function(ev){
                $(this).removeClass("d_init_comment");
                if ($(this).val() == ev.data) {
                    $(this).val('');
                }
            }).bind("blur.initMsg",msg,function(ev){
                if (jQuery.trim($(this).val()) == "") {
                    $(this).val(ev.data);
                    $(this).addClass("d_init_comment");
                } else {
                    var $keyword = $(this);
                    $keyword.removeClass("d_init_comment");

                    var keyword = $keyword.val();
                    if (!keyword) {
                        return;
                    }
                    keyword = keyword.replace(/[\s　]+/g, " ");
                    $keyword.val(keyword);
                }

            }).bind("keypress", function(ev) {
                if (ev.keyCode == 13) {
                    $searchButton.triggerHandler("click");
                }
            }).triggerHandler("blur.initMsg");
        }
    },

    clearDefaultMsg : function(){
        for (key in DenpyoKensaku.keywordDefaultMsg) {
            $("input[name='" + key + "']").triggerHandler("focus.initMsg");
        }
    },

    showDefaultMsg : function(){
        for (key in DenpyoKensaku.keywordDefaultMsg) {
            $("input[name='" + key + "']").triggerHandler("blur.initMsg");
        }
    },

    getSelfApprovingDenpyo : function() {
        var data;
        if (this.selfApprovalAlertFlag) {
            var $form = $("form");
            var url = $form.attr("action");
            var $componentList = $("[name]", $form);
            var param = f.getRequestParam($componentList);
            var xmlHttpRequest = $.ajax({
                type: "POST",
                url: url.replace(/[^/]+\/?$/, "selfApprovingAlertCheck"),
                data: param,
                cache: false,
                async: false,
                success: function(html) {
                    var responseText = html;
                    responseText = f.trim(responseText);
                    if (!f.checkResponseText(responseText)) {
                        data = {"error": true};
                        return data;
                    }
                    data = eval("(" + responseText + ")");
                }
            });
        }
        return data;
    },

    createSelfApprovingMessage : function(selfApprovingShainDenpyoArray, selfApprovingInputShainDenpyoArray) {
        return this._createSelfApprovingMessageInner(selfApprovingShainDenpyoArray, Locale["denpyo_selfApprovingShainMsg"])
             + this._createSelfApprovingMessageInner(selfApprovingInputShainDenpyoArray, Locale["denpyo_selfApprovingInputShainMsg"])
             + "<div class=\"d_marginBottom10\">"+ Locale["denpyo_ignoreMsgShonin"] + "</div>";
    },

    _createSelfApprovingMessageInner : function(denpyoArray, alertMessage) {
        if (denpyoArray.length === 0) {
            return "";
        }
        var denpyoMessage = "";
        $.each(denpyoArray, function(i, denpyo) {
            denpyoMessage += "<li>・";
            if (denpyo.length === 2) {
                denpyoMessage += f.escapeHTML(denpyo[0]) + " " + Locale["denpyo_denpyoNo"] + f.escapeHTML(denpyo[1]);
            } else {
                denpyoMessage += Locale["denpyo_denpyoNo"] + f.escapeHTML(denpyo[0]);
            }
            denpyoMessage += "</li>";
        });
        return "<div class=\"alertArea d_alert_Area_Popup d_self_approval_alert_Area_Popup \" >"
             + "<div>"
             + "<span class=\"icon_alert\"></span><span>" + alertMessage + "</span>"
             + "</div>"
             + "</div>"
             + "<ul class=\"d_marginBottom10\">" + denpyoMessage + "</ul>";
    }
};

var DenpyoOutput = {

    multiDenpyoPrint : function(buttonNode) {
        var $form = $(buttonNode).closest("form");
        var preAction = $form.attr("action");

        if (preAction == null) {
            return;
        }

        var $print = $("input[name=eMultiDenpyoNo][type=checkbox]");
        if (DenpyoKensaku.alertCheckbox($print)) {
            return;
        }

        $form.attr("action", preAction.replace(/[^/]+\/?$/, "multiOutput"));

        var cookieName = $("#outputCookieName").val();
        var cookiePath = preAction.replace(/[^/]+\/?$/, '').replace(/[^/]+\/?$/, '');
        var $preview = $(buttonNode);
        f.outputAndCompletionWatcher($preview, cookieName, cookiePath, function() {
            f.openPostWindow($form, window, null, {childFlag:false,width:1024,height:768,preAction:preAction});
        }, 3000);

    }

};

var ShiharaiKakuteiKensaku = {

    hurimotoList : {},
    kakuteiInfoPartList : {},
    keyPartList : [],
    shiharaiKakuteiKbn : 0,
    selectCount : 0,
    totalCount : 0,
    kensakuCount : 0,
    anotherPageCount : 0,
    execBeforeUnload:true,

    initForBarcodeMode : function() {
        BarcodeCommon.setBarcodeTitle();
        ShiharaiKakuteiKensaku.changeKakutei();

        ShiharaiKakuteiKensaku.execBeforeUnload = true;
        $("input[name=barcodeNo]").one("keypress", function(ev) {
            ShiharaiKakuteiKensaku.executeClick(ev);
        });

        $("form:first").bind("submit", function() {
            ShiharaiKakuteiKensaku.execBeforeUnload = false;
        });

        document.onkeydown = function(e) {
            if (document.activeElement.type == "text") {
                return;
            }

            if(e == null) {
                e = event;
            }
            var srcTarget = e.target;
            if (srcTarget == undefined) {
                srcTarget = e.srcElement;
            }
            if(srcTarget == undefined || "barcodeNo" != srcTarget.name) {
                var keycode = e.keyCode;
                if(e.shiftKey === false && ((keycode >= 48 && keycode <= 57) || (keycode >= 96 && keycode <= 105))) {
                    $("input[name=barcodeNo]").focus();
                }
            }
        };

        $("input[name=barcodeNo]").keyup(function(e) {
            var barcodeValue = $("input[name=barcodeNo]").val();
            if(barcodeValue.length === 12 && barcodeValue.match(/[０-９]+/) && !barcodeValue.match(/[^0-9０-９]+/)) {
                barcodeValue = barcodeValue.replace(/[０-９]/g, function(s) {
                    return String.fromCharCode(s.charCodeAt(0) - 0xFEE0);
                });
                $("input[name=barcodeNo]").val(barcodeValue);
                ShiharaiKakuteiKensaku.executeCheckBarcode();
            }
        });

        window.onbeforeunload = function(e) {
            var ev = e || window.event;
            if (ShiharaiKakuteiKensaku.execBeforeUnload && ShiharaiKakuteiKensaku.selectCount > 0) {
                var msg = "チェックした伝票が残っています。";
                if (ev) {
                    ev.returnValue = msg;
                }
                return msg;
            }
        };
    },

    changeHurimotoCd : function(selectNode) {
        var hurimotoCd = $(selectNode).val();
        var hurimoto = this.hurimotoList[hurimotoCd];
        if (hurimoto) {
            var year = "";
            var month = "";
            var day = "";
            var matches = hurimoto.defaultShihaDate.match(/^(\d{4})(\d{2})(\d{2})$/);
            if (matches) {
                year = matches[1];
                month = f.removeZero(matches[2]);
                day = f.removeZero(matches[3]);
            }
            $("input[name=shihaDateYear]").val(year);
            $("input[name=shihaDateMonth]").val(month);
            $("input[name=shihaDateDay]").val(day);
        }
    },

    changeKakutei : function() {
        var $checkedList = $("input[name^='kakutei('][type=checkbox]:checked");
        ShiharaiKakuteiKensaku.selectCount = parseInt(ShiharaiKakuteiKensaku.anotherPageCount, 10) + $checkedList.length;

        var isAllCheck = (ShiharaiKakuteiKensaku.totalCount == ShiharaiKakuteiKensaku.selectCount);
        ShiharaiKakuteiKensaku._setSelectCount(isAllCheck);
    },

    changeKakuteiBarcode : function(selectNode) {
        var $selectNode = $(selectNode);
        if (!$selectNode.is(":checked") && !$selectNode.is(":disabled")) {
            var $targetRow = $selectNode.closest("tr");
            $targetRow.find("img.barcodeReadImg").hide();
            $targetRow.find("input[name^='barcodeReadTime(']").val("");
        }
        ShiharaiKakuteiKensaku.changeKakutei();
    },

    kakuteiAllCheck : function() {
        var $kakutei = $("input[name^='kakutei('][type=checkbox]").not(":disabled");
        var $allCheckKbn = $("input[name=allCheckKbn][type=hidden]");
        if (ShiharaiKakuteiKensaku.selectCount < ShiharaiKakuteiKensaku.totalCount) {
            ShiharaiKakuteiKensaku.selectCount = ShiharaiKakuteiKensaku.totalCount;
            ShiharaiKakuteiKensaku.anotherPageCount = parseInt(ShiharaiKakuteiKensaku.totalCount, 10) - $kakutei.length;
            ShiharaiKakuteiKensaku._setSelectCount(true);
            $kakutei.prop("checked", true);
            $allCheckKbn.val(1);
        } else {
            ShiharaiKakuteiKensaku.selectCount = 0;
            ShiharaiKakuteiKensaku.anotherPageCount = 0;
            ShiharaiKakuteiKensaku._setSelectCount(false);
            $kakutei.prop("checked", false);
            $allCheckKbn.val(2);
            $("img.barcodeReadImg").hide();
            $("input[name^='barcodeReadTime(']").val("");
        }
    },

    submitKakutei : function(buttonNode) {
        if (ShiharaiKakuteiKensaku.selectCount < 1) {
            f.showConfirmWindow({
                message: "データを選択してください。",
                cancelLabel: "閉じる"
            }).find("input.confirmKakutei").hide();
            return false;
        }

        $("input[type=text][name='barcodeNo']").val("");

        var $form = $("form:first");
        $form.attr("target", "hiddenFrame");
        var url = $form.attr("action");
        $form.attr("action", url.replace(/[^/]+\/?$/, "showKakuteiInfo"));
        $form.submit();
        $form.attr("target", window.name);
        $form.attr("action", url);
        ShiharaiKakuteiKensaku.execBeforeUnload = true;
    },

    executeCheckBarcode : function() {
        var $form = $("form:first");
        var url = $form.attr("action");
        var $isBarcode = $("input[type=hidden][name='isBarcodeMode']");
        if ($isBarcode.length !== 0 && $isBarcode.val() != "false") {
            $form.attr("action", url.replace(/[^/]+\/?$/, "checkBarcode"));
        } else {
            $form.attr("action", url.replace(/[^/]+\/?$/, "search"));
        }
        $form.submit();
    },

    showDialog : function() {
        var $childFrame = $("#hiddenFrame").contents();
        var errors = $childFrame.find("div.errors").html();
        if (f.trim(errors) != "") {
            $("div.errors").html(errors);
            $childFrame.find("div.errors").html("");
        } else {
            var msg = "";
            if ($childFrame.find("input[name='minusSeisanGakuFlag']").val() == "true") {
                var minusLabel = "";
                if (ShiharaiKakuteiKensaku.shiharaiKakuteiKbn == 0) {
                    minusLabel = "精算額";
                } else {
                    minusLabel = $childFrame.find("input[name='totalLabel']").val();
                }
                msg = "<table cellspacing=\"0\" cellpadding=\"0\" class=\"kakuteiInfo\">"
                    + "<tr><td colspan=\"2\" class=\"info\">"
                    + "以下の内容で支払確定します。<br />"
                    + "<div class=\"d_helpPanel_caution\">"
                    + "マイナスの" + minusLabel + "が含まれています。<br />このまま支払確定してよろしいですか？"
                    + "</div></td></tr>"
                    + "<tr><td class=\"label dialogLabel\">振込元</td><td class=\"value\">" + f.escapeTooltip($childFrame.find("input[name='hurimotoName']").val()) + "</td></tr>"
                    + "<tr><td class=\"label dialogLabel\">支払日</td><td class=\"value\">" + $childFrame.find("input[name='shihaDate']").val() + "</td></tr>";
                if (typeof $childFrame.find("input[name='totalKingakuLabel']").val() != "undefined") {
                    msg += "<tr><td class=\"label dialogLabel\">" + $childFrame.find("input[name='totalKingakuLabel']").val() + "の合計</td><td class=\"value\">" + $childFrame.find("input[name='totalKingaku']").val() + "円</td></tr>";
                }
                if (typeof $childFrame.find("input[name='sousaiKingakuLabel']").val() != "undefined") {
                    msg += "<tr><td class=\"label dialogLabel\">" + $childFrame.find("input[name='sousaiKingakuLabel']").val() + "</td><td class=\"value\">" + $childFrame.find("input[name='sousaiKingaku']").val() + "円</td></tr>";
                }
                msg += "<tr><td class=\"label dialogLabel\">" + $childFrame.find("input[name='totalLabel']").val() + "の合計</td><td class=\"value\">" + $childFrame.find("input[name='total']").val() + "円</td></tr>";

                msg += "<tr><td colspan=\"2\" class=\"seisanGakuHeader\">"
                    + "マイナスの" + minusLabel
                    + "</td></tr>"
                    + "</table>"
                    + "<div class=\"seisanGaku\">"
                    + "<table cellspacing=\"0\" cellpadding=\"0\" class=\"kakuteiInfo\">";
                for (var i = 0; i < ShiharaiKakuteiKensaku.keyPartList.length; i++) {
                    var key = ShiharaiKakuteiKensaku.keyPartList[i];
                    var option = ShiharaiKakuteiKensaku.kakuteiInfoPartList[key];
                    msg += "<tr><td class=\"label\">" + f.escapeHTML(option["name"]) + "</td><td class=\"value\">" + f.addComma(option["gaku"]) + "円</td></tr>";
                }
                msg += "</table>"
                    + "</div>";

                var kakuteiTypeLabel = "";
                if (ShiharaiKakuteiKensaku.shiharaiKakuteiKbn == 0) {
                    kakuteiTypeLabel = "精算";
                } else if (ShiharaiKakuteiKensaku.shiharaiKakuteiKbn == 1){
                    kakuteiTypeLabel = "支払";
                } else if (ShiharaiKakuteiKensaku.shiharaiKakuteiKbn == 2){
                    kakuteiTypeLabel = "仮払金";
                }
                msg += "<div class=\"seisanGakuFooter\">※"
                    + "他の" + kakuteiTypeLabel + "と相殺したい場合は、キャンセルして"
                    + "<br /><span style=\"padding-left:13px;\">マイナスの支払先を支払確定対象から外してください。</span></div>";
            } else {
                msg = "<table cellspacing=\"0\" cellpadding=\"0\" class=\"kakuteiInfo\">"
                    + "<tr><td colspan=\"2\" class=\"info\">"
                    + "以下の内容で支払確定します。<br />よろしいですか？<br /></td></tr>"
                    + "<tr><td class=\"label dialogLabel\">振込元</td><td class=\"value\">" + f.escapeTooltip($childFrame.find("input[name='hurimotoName']").val()) + "</td></tr>"
                    + "<tr><td class=\"label dialogLabel\">支払日</td><td class=\"value\">" + $childFrame.find("input[name='shihaDate']").val() + "</td></tr>";
                if (typeof $childFrame.find("input[name='totalKingakuLabel']").val() != "undefined") {
                    msg += "<tr><td class=\"label dialogLabel\">" + $childFrame.find("input[name='totalKingakuLabel']").val() + "の合計</td><td class=\"value\">" + $childFrame.find("input[name='totalKingaku']").val() + "円</td></tr>";
                }
                if (typeof $childFrame.find("input[name='sousaiKingakuLabel']").val() != "undefined") {
                    msg += "<tr><td class=\"label dialogLabel\">" + $childFrame.find("input[name='sousaiKingakuLabel']").val() + "</td><td class=\"value\">" + $childFrame.find("input[name='sousaiKingaku']").val() + "円</td></tr>";
                }
                msg += "<tr><td class=\"label dialogLabel\">" + $childFrame.find("input[name='totalLabel']").val() + "の合計</td><td class=\"value\">" + $childFrame.find("input[name='total']").val() + "円</td></tr>"
                    + "</table>";
            }
            f.showConfirmWindow({
                message:msg,
                messageTextFlag:false,
                onCancel:function($confirmWindow) {
                },
                onKakutei:function($confirmWindow) {
                    var kakuteiButton = $("#kakuteiButton");
                    $(kakuteiButton).removeAttr("onclick");
                    var method = "execute";
                    DenpyoKensaku.submitMain(kakuteiButton, method);
                }
            });
        }
    },

    _setSelectCount : function(isAllCheck) {
        var expandedHeight;
        var contractedHeight;
        var isAlert = $(".alertArea").css('display') == "block" ? true : false;
        if (window.location.href && window.location.href.match(/.*(sapShihaShiharaiKakuteiKensaku).*/)) {
            expandedHeight = "486px";
            contractedHeight = "450px";
        } else if (window.location.href && window.location.href.match(/.*(sapKaribShiharaiKakuteiKensaku).*/) && isAlert) {
            expandedHeight = "500px";
            contractedHeight = "463px";
        } else if (window.location.href && window.location.href.match(/.*(sapShainShiharaiKakuteiKensaku).*/) && isAlert) {
            expandedHeight = "500px";
            contractedHeight = "470px";
        } else {
            expandedHeight = "547px";
            contractedHeight = "507px";
        }
        var $kensakuScrollArea = $("div.kensakuScrollArea");

        var $selectCountDiv = $("div.selectCount");
        if (ShiharaiKakuteiKensaku.selectCount < 1) {
            $selectCountDiv.hide();
            $kensakuScrollArea.css("maxHeight", expandedHeight);
            ShiharaiKakuteiKensaku._changeButton(isAllCheck);
        } else {
            $selectCountDiv.show();
            $kensakuScrollArea.css("maxHeight", contractedHeight);
            ShiharaiKakuteiKensaku._showMessage($selectCountDiv);
            ShiharaiKakuteiKensaku._changeButton(isAllCheck);
        }
    },

    _showMessage : function($selectCountDiv) {
        var msg = ShiharaiKakuteiKensaku.kensakuCount + "件中&nbsp;<span class=\"count\">" + ShiharaiKakuteiKensaku.selectCount + "</span>件&nbsp;が選択されています。";
        $selectCountDiv.empty();
        $selectCountDiv.html(msg);
        ShiharaiKakuteiKensaku.clickFlag = false;
    },

    _changeButton : function(isAllCheck) {
        var $allCheckButton = $("#allCheckButton");
        if (isAllCheck) {
            $allCheckButton.removeClass("btn_allcheck_m2");
            $allCheckButton.addClass("btn_alluncheck_m2");
        } else {
            $allCheckButton.removeClass("btn_alluncheck_m2");
            $allCheckButton.addClass("btn_allcheck_m2");
        }
    },

    executeClick : function(ev) {
        if (ev.keyCode == 13) {
            ShiharaiKakuteiKensaku.executeCheckBarcode();
        } else {
            $("input[name=barcodeNo]").one("keypress", function(ev) {
                ShiharaiKakuteiKensaku.executeClick(ev);
            });
        }
    },

    confirmBack : function(selectNode) {
        if (ShiharaiKakuteiKensaku.selectCount > 0) {
            f.showConfirmWindow({
                message: "チェックした伝票が残っています。\nよろしいですか？",
                onKakutei: function() {
                    ShiharaiKakuteiKensaku.execBeforeUnload = false;
                    DenpyoKensaku.submitBack(selectNode, 'backView');
                }
            });
        } else {
            DenpyoKensaku.submitBack(selectNode, 'backView');
        }
    }
};

var ShihaShiharaiKensaku = {

    huriGinFormUrl : null,
    huriGinWin : null,
    buttonNode : null,

    changeTesuryoKbn : function(selectNode) {
        var $td = $(selectNode).closest("td");
        $("input.changeFlag", $td).val(1);
    },

    openHuriGinForm : function(buttonNode) {
        this.huriGinWin = f.openWindow(this.huriGinFormUrl, window, null, {width:640,height:680});
        this.buttonNode = buttonNode;
    },

    afterOpenHuriGinForm : function() {
        f.makeMask(true);
        c.preventDefaultFlag = true;
        this.huriGinDataFromOpener();
    },

    huriGinDataFromOpener : function() {
        var $componentList = this.huriGinWin.$("input[name],select[name]");
        var $tr = $(this.buttonNode).closest("tr");
        for (var i = 0; i < $componentList.length; i++) {
            var $component = $componentList.eq(i);
            var name = $component.attr("name");
            var value = $("input[name^='" + name + "']", $tr).val();
            if ($component.attr("type") == "radio" || $component.attr("type") == "checkbox") {
                if ($component.val() == value) {
                    $component.prop("checked", true);
                }
            } else {
                $component.val(value);
                if (0 < $component.filter(".cache").length) {
                    this.huriGinWin.c.cache[name] = value;
                }
            }
            if (name == "huriGinCd") {
                var $shitenCd = this.huriGinWin.$("input[name=huriShitenCd]");
                if (value) {
                    f.removeDisabled($shitenCd);
                    f.removeDisabled($("input.kensaku", $shitenCd.closest("tr")));
                } else {
                    f.addDisabled($shitenCd);
                    f.addDisabled($("input.kensaku", $shitenCd.closest("tr")));
                }
            } else if (name == "huriGinName") {
                this.huriGinWin.$("span.huriGinName").text(value);
            } else if (name == "huriShitenName") {
                this.huriGinWin.$("span.huriShitenName").text(value);
            }
        }
    },

    huriGinDataToOpener : function() {
        var $componentList = this.huriGinWin.$("input[name],select[name]");
        var $tr = $(this.buttonNode).closest("tr");
        var changeFlag = false;
        for (var i = 0; i < $componentList.length; i++) {
            var $component = $componentList.eq(i);
            var name = $component.attr("name");
            var value = $component.val();
            var $target = $("input[name^='" + name + "']", $tr);
            if ($component.attr("type") == "radio" || $component.attr("type") == "checkbox") {
                if (!$component.prop("checked")) {
                    continue;
                }
            }
            if (!changeFlag && value != $target.val()) {
                changeFlag = true;
            }
            $target.val(value);

            if (name == "huriGinCd" || name == "huriGinKana" || name == "huriShitenCd" || name == "huriShitenKana" || name == "huriKoza" || name == "huriMegiKana") {
                $("span." + name, $tr).text(value);
            } else if (name == "huriShu") {
                var label = f.getSelectedOptions($component, true).text();
                var $span = $("span." + name, $tr).text(label);
                var huriKoza = this.huriGinWin.$("input[name=huriKoza]").val();
                if (huriKoza) {
                    $span.show();
                } else {
                    $span.hide();
                }
            }
        }
        if (changeFlag) {
            var $td = $(this.buttonNode).closest("td");
            $td.addClass("highlight");
            $("input.changeFlag", $td).val(1);
        }
    },

    submitHuriGinFormKakutei : function(buttonNode) {
        if (this.checkHuriGinData()) {
            this.huriGinDataToOpener();
            this.closeHuriGinForm();
        } else {
            this.huriGinWin.f.focusFirstElement();
        }
    },

    checkHuriGinData : function() {
        var $form = this.huriGinWin.$("form");
        this.huriGinWin.$("div.errors").html("");
        var $componentList = this.huriGinWin.$("[name]", $form);
        var $huriMegiKana = $componentList.filter("[name=huriMegiKana]");
        var huriMegiKana = this.huriGinWin.f.convertToZengin($huriMegiKana.val());
        $huriMegiKana.val(huriMegiKana);
        var param = f.getRequestParam($componentList);
        var url = $form.attr("action");
        var xmlHttpRequest = $.ajax({
            type: "POST",
            data: param,
            url: url,
            cache: false,
            async: false
        });
        var html = xmlHttpRequest.responseText;
        if (f.trim(html) == "") {
            return true;
        } else {
            this.huriGinWin.$("div.errors").html(html);
            f.focusFirstElement();
            return false;
        }
    },

    closeHuriGinForm : function() {
        $(this.huriGinWin).unload();
        this.huriGinWin.close();
        this.huriGinWin = null;
        this.buttonNode = null;
    },

    afterCloseHuriGinForm : function() {
        f.removeMask();
        c.preventDefaultFlag = false;
    }

};

var ShiwakeKensaku = {

    outputFlagRow : 0,

    checkOutputFlag : function(checkboxNode) {
        var $createShiwakeDateDiv = $("div.createShiwakeDate");
        if ($(checkboxNode).prop("checked")) {
            $createShiwakeDateDiv.show();
        } else {
            $createShiwakeDateDiv.hide();
        }
    },

    checkDenpyo : function(nameAttr) {
        if (typeof nameAttr == "undefined") {
            nameAttr = "denpyo";
        }
        var $denpyo = $("input[name='" + nameAttr + "'][type=checkbox]");
        if (0 < $denpyo.not(":checked").length) {
            $denpyo.each(function() {
                $(this).prop("checked", true);
            });
        } else {
            $denpyo.each(function() {
                $(this).prop("checked", false);
            });
        }
    },

    modifyShiwakeDate : function(buttonNode) {
        DenpyoKensaku.submitMain(buttonNode, "modifyShiwakeDate");
    },

    kanjoFormUrl : null,
    kanjoWin : null,
    kanjoButtonNode : null,

    openKanjoForm : function(buttonNode) {
        if (f.hasOpenChildWindow()) {
            return;
        }
        this.kanjoWin = f.openWindow(this.kanjoFormUrl, window, null, {width:640,height:680});
        this.kanjoButtonNode = buttonNode;
    },

    afterOpenKanjoForm : function() {
        f.makeMask(true);
        ((window.parent && window != window.parent) ? window.parent : window).c.preventDefaultFlag = true;
        this.kanjoDataFromOpener();
    },

    kanjoDataFromOpener : function() {
        this._dataFromOpener(this.kanjoWin, this.kanjoButtonNode);
    },

    kanjoDataToOpener : function() {
        if (this._dataToOpener(this.kanjoWin, this.kanjoButtonNode)) {
            var $td = $(this.kanjoButtonNode).closest("td");
            var $hkamokuTd = $td.next("td");
            $("span.hkamokuName", $hkamokuTd).html("&nbsp;");
            $("input.hkamokuCd", $hkamokuTd).val("");
            $("input.hkamokuName", $hkamokuTd).val("");
            $("input.renkei", $hkamokuTd).val("");
            $("input.subRenkei1", $hkamokuTd).val("");
            $("input.subRenkei2", $hkamokuTd).val("");
            $("input.subRenkei3", $hkamokuTd).val("");
            $("input.subRenkei4", $hkamokuTd).val("");
        }
    },

    submitKanjo : function(buttonNode) {
        if (this.checkKanjoData()) {
            ShiwakeKensaku.kanjoDataToOpener();
            var kanjoButtonNode = this.kanjoButtonNode;
            ShiwakeKensaku.closeKanjoForm();
            var $td = $(kanjoButtonNode).closest("td");
            f.removeDisabled($("input.edit", $td.next("td")));
        } else {
            this.kanjoWin.f.focusFirstElement();
        }
    },

    checkKanjoData : function() {
        return this._checkData(this.kanjoWin);
    },

    closeKanjoForm : function() {
        $(this.kanjoWin).unload();
        this.kanjoWin.close();
        this.kanjoWin = null;
        this.kanjoButtonNode = null;
    },

    hkamokuFormUrl : null,
    hkamokuWin : null,
    hkamokuButtonNode : null,

    openHkamokuForm : function(buttonNode) {
        if (f.hasOpenChildWindow()) {
            return;
        }
        this.hkamokuWin = f.openWindow(this.hkamokuFormUrl, window, null, {width:640,height:680});
        this.hkamokuButtonNode = buttonNode;
    },

    afterOpenHkamokuForm : function() {
        f.makeMask(true);
        ((window.parent && window != window.parent) ? window.parent : window).c.preventDefaultFlag = true;
        this.hkamokuDataFromOpener();
    },

    hkamokuDataFromOpener : function() {
        this._dataFromOpener(this.hkamokuWin, this.hkamokuButtonNode);
        var $td = $(this.hkamokuButtonNode).closest("td");
        var $kanjoCd = $("input.kanjoCd", $td.prev("td"));
        this.hkamokuWin.$("input[name=kanjoCd]").val($kanjoCd.val());
    },

    hkamokuDataToOpener : function() {
        this._dataToOpener(this.hkamokuWin, this.hkamokuButtonNode);
    },

    submitHkamoku : function(buttonNode) {
        if (this.checkHkamokuData()) {
            ShiwakeKensaku.hkamokuDataToOpener();
            ShiwakeKensaku.closeHkamokuForm();
        } else {
            this.hkamokuWin.f.focusFirstElement();
        }
    },

    checkHkamokuData : function() {
        return this._checkData(this.hkamokuWin);
    },

    closeHkamokuForm : function() {
        $(this.hkamokuWin).unload();
        this.hkamokuWin.close();
        this.hkamokuWin = null;
        this.hkamokuButtonNode = null;
    },

    bumonFormUrl : null,
    bumonWin : null,
    bumonButtonNode : null,

    openBumonForm : function(buttonNode) {
        if (f.hasOpenChildWindow()) {
            return;
        }
        this.bumonWin = f.openWindow(this.bumonFormUrl, window, null, {width:640,height:680});
        this.bumonButtonNode = buttonNode;
    },

    afterOpenBumonForm : function() {
        f.makeMask(true);
        ((window.parent && window != window.parent) ? window.parent : window).c.preventDefaultFlag = true;
        this.bumonDataFromOpener();
    },

    bumonDataFromOpener : function() {
        this._dataFromOpener(this.bumonWin, this.bumonButtonNode);
    },

    bumonDataToOpener : function() {
        this._dataToOpener(this.bumonWin, this.bumonButtonNode);
    },

    submitBumon : function(buttonNode) {
        if (this.checkBumonData()) {
            ShiwakeKensaku.bumonDataToOpener();
            ShiwakeKensaku.closeBumonForm();
        } else {
            this.bumonWin.f.focusFirstElement();
        }
    },

    checkBumonData : function() {
        return this._checkData(this.bumonWin);
    },

    closeBumonForm : function() {
        $(this.bumonWin).unload();
        this.bumonWin.close();
        this.bumonWin = null;
        this.bumonButtonNode = null;
    },

    projectFormUrl : null,
    projectWin : null,
    projectButtonNode : null,

    openProjectForm : function(buttonNode) {
        if (f.hasOpenChildWindow()) {
            return;
        }
        this.projectWin = f.openWindow(this.projectFormUrl, window, null, {width:640,height:680});
        this.projectButtonNode = buttonNode;
    },

    afterOpenProjectForm : function() {
        f.makeMask(true);
        ((window.parent && window != window.parent) ? window.parent : window).c.preventDefaultFlag = true;
        this.projectDataFromOpener();
    },

    projectDataFromOpener : function() {
        this._dataFromOpener(this.projectWin, this.projectButtonNode);
    },

    projectDataToOpener : function() {
        this._dataToOpener(this.projectWin, this.projectButtonNode);
    },

    submitProject : function(buttonNode) {
        if (this.checkProjectData()) {
            ShiwakeKensaku.projectDataToOpener();
            ShiwakeKensaku.closeProjectForm();
        } else {
            this.projectWin.f.focusFirstElement();
        }
    },

    checkProjectData : function() {
        return this._checkData(this.projectWin);
    },

    closeProjectForm : function() {
        $(this.projectWin).unload();
        this.projectWin.close();
        this.projectWin = null;
        this.projectButtonNode = null;
    },

    zeikbnFormUrl : null,
    zeikbnWin : null,
    zeikbnButtonNode : null,

    openZeikbnForm : function(buttonNode) {
        if (f.hasOpenChildWindow()) {
            return;
        }
        var $td = $(buttonNode).closest("td");
        var $zeigaku = $("input.zeigaku", $td);
        var kingakuName = $zeigaku.attr("name").replace("Zeigaku", "Kingaku");
        var $prevTr = $td.parent("tr").prev("tr");
        var $kingaku = $("input[name='" + kingakuName + "']", $prevTr);
        var url = this.zeikbnFormUrl;
        url = url.replace("%d", $kingaku.val());

        this.zeikbnWin = f.openWindow(url, window, null, {width:640,height:680});
        this.zeikbnButtonNode = buttonNode;
    },

    afterOpenZeikbnForm : function() {
        f.makeMask(true);
        ((window.parent && window != window.parent) ? window.parent : window).c.preventDefaultFlag = true;
        this.zeikbnDataFromOpener();
    },

    zeikbnDataFromOpener : function() {
        this._dataFromOpener(this.zeikbnWin, this.zeikbnButtonNode);
    },

    zeikbnDataToOpener : function() {
        this._dataToOpener(this.zeikbnWin, this.zeikbnButtonNode);
        var $td = $(this.zeikbnButtonNode).closest("td");
        var $zeigaku = $("input.zeigaku", $td);
        var $thisTr = $td.parent("tr");
        var zeigaku = f.addComma($zeigaku.val());
        $("span." + $zeigaku.attr("name"), $thisTr).text(zeigaku);
    },

    submitZeikbn : function(buttonNode) {
        if (this.checkZeikbnData()) {
            ShiwakeKensaku.zeikbnDataToOpener();
            ShiwakeKensaku.closeZeikbnForm();
        } else {
            this.zeikbnWin.f.focusFirstElement();
        }
    },

    checkZeikbnData : function() {
        return this._checkData(this.zeikbnWin);
    },

    closeZeikbnForm : function() {
        $(this.zeikbnWin).unload();
        this.zeikbnWin.close();
        this.zeikbnWin = null;
        this.zeikbnButtonNode = null;
    },

    tekiyoFormUrl : null,
    tekiyoWin : null,
    tekiyoButtonNode : null,

    openTekiyoForm : function(buttonNode) {
        if (f.hasOpenChildWindow()) {
            return;
        }
        this.tekiyoWin = f.openWindow(this.tekiyoFormUrl, window, null, {width:640,height:680});
        this.tekiyoButtonNode = buttonNode;
    },

    afterOpenTekiyoForm : function() {
        f.makeMask(true);
        ((window.parent && window != window.parent) ? window.parent : window).c.preventDefaultFlag = true;
        this.tekiyoDataFromOpener();
    },

    tekiyoDataFromOpener : function() {
        this._dataFromOpener(this.tekiyoWin, this.tekiyoButtonNode);
    },

    tekiyoDataToOpener : function() {
        this._dataToOpener(this.tekiyoWin, this.tekiyoButtonNode);
    },

    submitTekiyo : function(buttonNode) {
        if (this.checkTekiyoData()) {
            ShiwakeKensaku.tekiyoDataToOpener();
            ShiwakeKensaku.closeTekiyoForm();
        } else {
            this.tekiyoWin.f.focusFirstElement();
        }
    },

    checkTekiyoData : function() {
        return this._checkData(this.tekiyoWin);
    },

    closeTekiyoForm : function() {
        $(this.tekiyoWin).unload();
        this.tekiyoWin.close();
        this.tekiyoWin = null;
        this.tekiyoButtonNode = null;
    },

    freetextFormUrl : null,
    freetextWin : null,
    freetextButtonNode : null,

    openFreetextForm : function(buttonNode, no) {
        if (f.hasOpenChildWindow()) {
            return;
        }
        var url = this.freetextFormUrl.replace("%d", no);
        this.freetextWin = f.openWindow(url, window, null, {width:640,height:680});
        this.freetextButtonNode = buttonNode;
    },

    afterOpenFreetextForm : function() {
        f.makeMask(true);
        ((window.parent && window != window.parent) ? window.parent : window).c.preventDefaultFlag = true;
        this.freetextDataFromOpener();
    },

    freetextDataFromOpener : function() {
        this._dataFromOpener(this.freetextWin, this.freetextButtonNode);
    },

    freetextDataToOpener : function() {
        this._dataToOpener(this.freetextWin, this.freetextButtonNode);
    },

    submitFreetext : function(buttonNode) {
        if (this.checkFreetextData()) {
            ShiwakeKensaku.freetextDataToOpener();
            ShiwakeKensaku.closeFreetextForm();
        } else {
            this.freetextWin.f.focusFirstElement();
        }
    },

    checkFreetextData : function() {
        return this._checkData(this.freetextWin);
    },

    closeFreetextForm : function() {
        $(this.freetextWin).unload();
        this.freetextWin.close();
        this.freetextWin = null;
        this.freetextButtonNode = null;
    },

    shiwakeDateFormUrl : null,
    shiwakeDateWin : null,
    shiwakeDateButtonNode : null,

    openShiwakeDateForm : function(buttonNode) {
        if (f.hasOpenChildWindow()) {
            return;
        }
        this.shiwakeDateWin = f.openWindow(this.shiwakeDateFormUrl, window, null, {width:640,height:680});
        this.shiwakeDateButtonNode = buttonNode;
    },

    afterOpenShiwakeDateForm : function() {
        f.makeMask(true);
        ((window.parent && window != window.parent) ? window.parent : window).c.preventDefaultFlag = true;
        this.shiwakeDateDataFromOpener();
    },

    shiwakeDateDataFromOpener : function() {
        this._dataFromOpener(this.shiwakeDateWin, this.shiwakeDateButtonNode);
    },

    shiwakeDateDataToOpener : function() {
        this._dataToOpener(this.shiwakeDateWin, this.shiwakeDateButtonNode);
        var $td = $(this.shiwakeDateButtonNode).closest("td");
        var year = $("input[name^='shiwakeDateYear']", $td).val();
        var month = f.addZero($("input[name^='shiwakeDateMonth']", $td).val(), 2);
        var day = f.addZero($("input[name^='shiwakeDateDay']", $td).val(), 2);
        $("span.shiwakeDate", $td).text(year + "/" + month + "/" + day);
    },

    submitShiwakeDate : function(buttonNode) {
        if (this.checkShiwakeDateData()) {
            ShiwakeKensaku.shiwakeDateDataToOpener();
            ShiwakeKensaku.closeShiwakeDateForm();
        } else {
            this.shiwakeDateWin.f.focusFirstElement();
        }
    },

    checkShiwakeDateData : function() {
        return this._checkData(this.shiwakeDateWin);
    },

    closeShiwakeDateForm : function() {
        $(this.shiwakeDateWin).unload();
        this.shiwakeDateWin.close();
        this.shiwakeDateWin = null;
        this.shiwakeDateButtonNode = null;
    },

    _dataFromOpener : function(win, buttonNode) {
        var $componentList = win.$("input[name]").not("[disabled]");
        var $td = $(buttonNode).closest("td");
        for (var i = 0; i < $componentList.length; i++) {
            var $component = $componentList.eq(i);
            var name = $component.attr("name");
            var value = $("input." + name, $td).val();
            if (typeof value != "undefined") {
                $component.val(value);
            }

        }
    },

    _dataToOpener : function(win, buttonNode) {
        var $componentList = win.$("input[name]").not("[disabled]");
        var $td = $(buttonNode).closest("td");
        var changeFlag = false;
        for (var i = 0; i < $componentList.length; i++) {
            var $component = $componentList.eq(i);
            var name = $component.attr("name");
            var value = $component.val();
            var $target = $("input." + name, $td);
            if ($target.length === 0) {
                continue;
            }
            if (!changeFlag && value != $target.val()) {
                changeFlag = true;
            }
            $target.val(value);
            $("span." + name, $td).text(value);
        }
        if (changeFlag) {
            $td.addClass("highlight");
            $("input.changeFlag", $td).val(1);
        }
        return changeFlag;
    },

    _checkData : function(win) {
        var $form = win.$("div.content form");
        $("div.errors").html("");
        var $componentList = win.$("[name]", $form);
        var param = f.getRequestParam($componentList);
        var url = $form.attr("action");
        var xmlHttpRequest = $.ajax({
            type: "POST",
            data: param,
            url: url,
            cache: false,
            async: false
        });
        var html = xmlHttpRequest.responseText;
        if (f.trim(html) == "") {
            return true;
        } else {
            win.$("div.errors").html(html);
            f.focusFirstElement();
            return false;
        }
    },

    afterCloseForm : function() {
        f.removeMask();
        ((window.parent && window != window.parent) ? window.parent : window).c.preventDefaultFlag = false;
    },

    changeMeisaiDate : function(checkboxNode) {
        var $denpyoKensakuInputArea = $("#denpyoKensakuInputArea");
        var $shimeDate = $("input[name^='shimeDate']", $denpyoKensakuInputArea);
        var $datePick = $("div.datePick", $denpyoKensakuInputArea);
        var $kensakuShimeDate = $datePick.next();
        if ($(checkboxNode).prop("checked")) {
            f.removeDisabled($shimeDate);
            $datePick.removeClass("dp-disabled");
            f.removeDisabled($kensakuShimeDate);
        } else {
            f.addDisabled($shimeDate);
            $datePick.addClass("dp-disabled");
            f.addDisabled($kensakuShimeDate);
        }
    }

};

var ShiwakeDownload = {

    checkKomokuOutputFlag : function(checkboxNode) {
        var $checkbox = $(checkboxNode);
        var $text = window.parent["list"].$("input[name='komokuOutputFlag']");
        if ($checkbox.prop("checked")) {
            $text.val($checkbox.val());
        } else {
            $text.val(0);
        }
    }

};

var BarcodeKensaku = {
    execBeforeUnload:true,

    init : function() {
        BarcodeKensaku.bindCheckboxBarcode($("input.headerAllCheck"),$("input[name^='kakutei(']"),$("#selDenpyoCount"));

        var $kakuteiChkLst = $("input[name^='kakutei(']").not(":disabled");
        $kakuteiChkLst.each(function() {
           var $tr = $(this).closest("tr");
           if($(this).prop("checked") == true){
             $tr.addClass("checked");
            }
        });

        $("input[name=barcodeNo]").one("keypress", function(ev) {
            BarcodeKensaku.executeClick(ev);
        });
        BarcodeKensaku.bindOneShonin();

        $(window).resize(function() {
            $("body").height($(window).height() - 64);
        }).triggerHandler("resize");

        $("form:first").bind("submit", function() {
            BarcodeKensaku.execBeforeUnload = false;
        });
        document.onkeydown = function(e){
            if(e == null){
                e = event;
            }
            var srcTarget = e.target;
            if (srcTarget == undefined){
                srcTarget = e.srcElement;
            }
           if(srcTarget == undefined || "barcodeNo" != srcTarget.name){
               var keycode = e.keyCode;
               if(e.shiftKey === false && ((keycode >= 48 && keycode <= 57) || (keycode >= 96 && keycode <= 105))){
                   $("input[name=barcodeNo]").focus();
               }
           }
        };

        $("input[name=barcodeNo]").keyup(function(e){
            var barcodeValue = $("input[name=barcodeNo]").val();
            if(barcodeValue.length === 12 && barcodeValue.match(/[０-９]+/) && !barcodeValue.match(/[^0-9０-９]+/)) {
                barcodeValue = barcodeValue.replace(/[０-９]/g, function(s) {
                    return String.fromCharCode(s.charCodeAt(0) - 0xFEE0);
                });
                $("input[name=barcodeNo]").val(barcodeValue);
                $('form').submit();
            }
        });

        window.onbeforeunload = function(e) {
            var ev = e || window.event;
            if (BarcodeKensaku.execBeforeUnload) {
                if($("input[name^='kakutei(']:checked").length >= 1){
                    var msg = Locale["denpyoKensakuNew_windowOnbeforeunloadMsg"];
                    if (ev) {
                        ev.returnValue = msg;
                    }
                    return msg;
                }
            }
        };
    },

    bindOneShonin : function() {
        $("button.kakutei").unbind("click").one("click",function(e){
            BarcodeKensaku.executeKakutei(e);
        });
    },

    confirmClose : function(){
        if ($("input[name^='kakutei(']:checked").length >= 1) {
            var message = Locale["denpyoKensakuNew_confirmCloseMsg"];
            f.showConfirmWindow({
                message: message,
                onKakutei: function() {
                    BarcodeKensaku.execBeforeUnload = false;
                    DenpyoKensaku.windowClose();
                }
            });
        } else {
            DenpyoKensaku.windowClose();
        }
    },

    executeKakutei : function(e) {
        var $shonin = $("input[name^='kakutei('][type=checkbox]");
        if (BarcodeKensaku.alertCheckboxBarcode($shonin)) {
            BarcodeKensaku.bindOneShonin();
            return;
        }

        var buttonName = $(e.target).closest("button").attr("name");
        var isShonin = buttonName === "shonin";

        var selfApprovingShainDenpyoArray = [];
        var selfApprovingInputShainDenpyoArray = [];
        if (isShonin) {
            var data = DenpyoKensaku.getSelfApprovingDenpyo();
            if (data) {
                if (data.error) {
                    BarcodeKensaku.execBeforeUnload = false;
                    return;
                }
                selfApprovingShainDenpyoArray = data.selfApprovingShainDenpyoArray;
                selfApprovingInputShainDenpyoArray = data.selfApprovingInputShainDenpyoArray;
            }
        }

        var message = "";
        var shoninShoriKbn = 1;
        if (isShonin && selfApprovingShainDenpyoArray.length > 0 || selfApprovingInputShainDenpyoArray.length > 0) {
            message = DenpyoKensaku.createSelfApprovingMessage(selfApprovingShainDenpyoArray, selfApprovingInputShainDenpyoArray);
        } else {
            var totalCount = $shonin.filter("input:checked").length;
            if (isShonin) {
                message = "<div>" + Locale["denpyoKensakuNew_executeKakuteiConfirmApprove"].replace("{0}", totalCount) + "</div>";
            } else if (buttonName === "sashimodoshi") {
                message = "<div>" + Locale["denpyoKensakuNew_executeKakuteiConfirmRemand"].replace("{0}", totalCount) + "</div>";
                shoninShoriKbn = 2;
            } else {
                return;
            }
        }
        var $confirmWindow = f.showConfirmWindow({
            message:message,
            messageTextFlag:false,
            onCancel:function() {
                BarcodeKensaku.bindOneShonin();
            },
            onKakutei:function() {
                $("#shoninShoriKbn").val(shoninShoriKbn);
                var $form = $("form");
                var action = $form.attr("action");
                $form.attr("action", action.replace(/[^/]+\/?$/, "execute"));
                $form.submit();
            }
        });

    },

    executeClick : function(ev) {
        if (ev.keyCode == 13) {
            var $pageNum = $("#denpyoKensakuPageNum");
            $pageNum.val("0");
            $('form').submit();
        } else {
            $("input[name=barcodeNo]").one("keypress", function(ev) {
                BarcodeKensaku.executeClick(ev);
            });
        }
    },

    alertCheckboxBarcode : function($target) {
        if ($target.not(":checked").length === $target.length) {
            f.showConfirmWindow({
                message: Locale["denpyoKensakuNew_alertCheckboxMsg"],
                cancelLabel: Locale["denpyoKensakuNew_alertCheckboxCancelLabel"],
                onCancel:function($confirmWindow) {
                }
            }).find("input.confirmKakutei").hide();
            return true;
        }
        return false;
    },

    bindCheckboxBarcode : function($parentCheckbox,$childCheckbox,$countTag) {

        var $childbin = $childCheckbox.bind("click.bindCheckboxBarcode",function(){
            var checkLength = $childCheckbox.filter("input:checked").length;
            if(checkLength > 0 && $childCheckbox.not(":disabled").length === checkLength){
                $parentCheckbox.prop("checked",true);
            }else{
                $parentCheckbox.prop("checked",false);
            }
            $countTag.text($childCheckbox.filter("input:checked").length);
        });
        if ($childbin.length !== 0) {
            $childbin.eq(0).triggerHandler("click.bindCheckboxBarcode");
        }

        $parentCheckbox.bind("click.bindCheckboxBarcode",function(){
            if($(this).is(":checked")){
                $childCheckbox.not(":disabled").prop("checked",true);
            }else{
                $childCheckbox.prop("checked",false);
            }
            $countTag.text($childCheckbox.filter("input:checked").length);
        });
    },

    executeCheckBarcode : function() {
        var $form = $("form:first");
        var $pageNum = $("#denpyoKensakuPageNum");
        $pageNum.val("0");
        $form.submit();
    }

};

var TokusokuKensaku = {

    sendMailMsg : null,
    seisanArray : ["2","4","6","9"],
    shinseiArray : ["1","3","5","8","98"],
    shuchoArray : ["1","2","3","4"],
    shiharaiArray : ["7"],

    init : function() {
        var denpyoShu = $("select[name='denpyoShu']").val();
        this.changeDenpyoShu($("select[name='denpyoShu']"));
        this.changeKaribaraiKbn($("input[name='karibaraiKbn']:checked").get());
        TokusokuKensaku.bindCheckbox($("input.headerAllCheck"), $("input[name^='kakutei']"));
    },

    sendMail : function(buttonNode) {
        var $kakutei = $("input[name^='kakutei'][type=checkbox]");
        if (DenpyoKensaku.alertCheckbox($kakutei)) {
            return;
        }
        DenpyoKensaku._resetValue();
        var $confirmWindow = f.showConfirmWindow({
            message:this.sendMailMsg,
            messageTextFlag:false,
            onCancel:function() {
                return;
            },
            onKakutei:function() {
                var $form = $(buttonNode).closest("form");
                var action = $form.attr("action");
                $form.attr("action", action.replace(/[^/]+\/?$/, "executeSendMail"));
                $form.submit();
            }
        });
    },

    bindCheckbox : function($parentCheckbox,$childCheckbox) {

        var $sendMailButton = $("#sendMailButton");

        var $childbin = $childCheckbox.bind("click.bindcheckbox",function(ev) {
            var checkLength = $childCheckbox.filter("input:checked").length;
            if (checkLength > 0 && $childCheckbox.not(":disabled").length === checkLength) {
                $parentCheckbox.prop("checked",true);
            } else {
                $parentCheckbox.prop("checked",false);
            }
            if (checkLength === 0) {
                f.addDisabled($sendMailButton);
            } else {
                if ($sendMailButton.prop("disabled")) {
                    f.removeDisabled($sendMailButton);
                }
            }
        });
        if ($childbin.length !== 0) {
            $childbin.eq(0).triggerHandler("click.bindcheckbox");
        }

        $parentCheckbox.bind("click.bindcheckbox",function(ev) {
            if ($(this).is(":checked")) {
                $childCheckbox.not(":disabled").prop("checked",true);

                var checkLength = $childCheckbox.filter("input:checked").length;
                if (checkLength > 0) {
                    f.removeDisabled($sendMailButton);
                }
            } else {
                $childCheckbox.prop("checked",false);
                f.addDisabled($sendMailButton);
            }
        });
    },

    changeDenpyoShu : function(selectNode) {
        var denpyoShu = $(selectNode).val();
        if (jQuery.inArray(denpyoShu,this.shinseiArray) != -1) {
            $(".shinsei-display").css("display","");
            this.changeKaribaraiKbn($("input[name='karibaraiKbn']:checked").get());
        } else {
            $(".shinsei-display").css("display","none");
        }

        if (jQuery.inArray(denpyoShu,this.shuchoArray) != -1) {
            $(".shucho-display").css("display","");
        } else {
            $(".shucho-display").css("display","none");
        }

        if (jQuery.inArray(denpyoShu,this.shiharaiArray) != -1) {
            $(".shiharai-display").css("display","");
        } else {
            $(".shiharai-display").css("display","none");
        }

        if (jQuery.inArray(denpyoShu,this.seisanArray) != -1
                || jQuery.inArray(denpyoShu,this.shiharaiArray) != -1) {
            $(".seisan-shiharai-display").css("display","");
        } else {
            $(".seisan-shiharai-display").css("display","none");
        }
    },

    changeKaribaraiKbn : function(selectNode) {
        var karibraiKbn = $(selectNode).val();

        if ($(selectNode).is(':visible') && karibraiKbn == 1) {
            $(".shinsei-input-disp").css("display","");
        } else {
            $(".shinsei-input-disp").css("display","none");
        }
    },

    moveMain: function() {
        var $form = $("form:first");
        this._uncheckKakutei();
        var $pageNum = $("#denpyoKensakuPageNum");
        $form.submit();
    }
};

var KaribHenkinKakuteiKensaku = {

    selectCount : 0,
    totalCount : 0,

    changeKakutei : function() {
        var $checkedList = $("input[name^='kakutei'][type=checkbox]:checked");
        KaribHenkinKakuteiKensaku.selectCount = $checkedList.length;
        var isAllCheck = (KaribHenkinKakuteiKensaku.totalCount == KaribHenkinKakuteiKensaku.selectCount);
        KaribHenkinKakuteiKensaku._changeParentCheckbox(isAllCheck);
    },

    kakuteiAllCheck : function() {
        var $kakutei = $("input[name^='kakutei'][type=checkbox]");
        if (KaribHenkinKakuteiKensaku.selectCount < KaribHenkinKakuteiKensaku.totalCount) {
            KaribHenkinKakuteiKensaku.selectCount = KaribHenkinKakuteiKensaku.totalCount;
            KaribHenkinKakuteiKensaku._changeParentCheckbox(true);
            $kakutei.prop("checked", true);
        } else {
            KaribHenkinKakuteiKensaku.selectCount = 0;
            KaribHenkinKakuteiKensaku._changeParentCheckbox(false);
            $kakutei.prop("checked", false);
        }
    },

    submitKakutei : function(buttonNode) {
        if (KaribHenkinKakuteiKensaku.selectCount < 1) {
            f.showConfirmWindow({
                message: "データを選択してください。",
                cancelLabel: "閉じる"
            }).find("input.confirmKakutei").hide();
            return false;
        }

        var $form = $("form:first");
        $form.attr("target", "hiddenFrame");
        var url = $form.attr("action");
        $form.attr("action", url.replace(/[^/]+\/?$/, "showKakuteiInfo"));
        $form.submit();
        $form.attr("target", window.name);
        $form.attr("action", url);
    },

    showDialog : function() {
        var $childFrame = $("#hiddenFrame").contents();
        var errors = $childFrame.find("div.errors").html();
        if (f.trim(errors) != "") {
            $("div.errors").html(errors);
            $childFrame.find("div.errors").html("");
        } else {
            var msg = "<table cellspacing=\"0\" cellpadding=\"0\" class=\"kakuteiInfo\">"
                    + "<tr><td colspan=\"2\" class=\"info\">"
                    + "以下の内容で返金確定します。<br />よろしいですか？<br /></td></tr>"
                    + "<tr><td class=\"label dialogLabel\">返金先</td><td class=\"value\">" + f.escapeTooltip($childFrame.find("input[name='hurimotoName']").val()) + "</td></tr>"
                    + "<tr><td class=\"label dialogLabel\">返金日</td><td class=\"value\">" + $childFrame.find("input[name='henkinDate']").val() + "</td></tr>"
                    + "<tr><td class=\"label dialogLabel\">返金額の合計</td><td class=\"value\">" + $childFrame.find("input[name='totalHenkinGaku']").val() + "円</td></tr>"
                    + "</table>";

            f.showConfirmWindow({
                message:msg,
                messageTextFlag:false,
                onCancel:function($confirmWindow) {
                },
                onKakutei:function($confirmWindow) {
                    var kakuteiButton = $("#kakuteiButton");
                    $(kakuteiButton).removeAttr("onclick");
                    var method = "execute";
                    DenpyoKensaku.submitMain(kakuteiButton, method);
                }
            });
        }
    },

    _changeParentCheckbox : function(isAllCheck) {
        var $parentCheckbox = $("input.headerAllCheck");
        $parentCheckbox.prop("checked", isAllCheck);
    },

    outputCsv : function() {
        var $form = $("form:first");
        DenpyoKensaku._resetValue();
        var action = $form.attr("action");
        var cookieName = $("#outputCookieName").val();
        var cookiePath = action.replace(/[^/]+\/?$/, '').replace(/[^/]+\/?$/, '');
        var $csvout = $("button.csvout");
        f.outputAndCompletionWatcher($csvout, cookieName, cookiePath, function() {
            $form.attr("action", action.replace(/[^/]+\/?$/, "outputCsv"));
            $form.trigger("submit", false);
            DenpyoKensaku.showDefaultMsg();
            $form.attr("action", action.replace(/[^/]+\/?$/, "search"));
        });
    }
};



c.keyKbn = 2;


f.getOpenerWindow = function(openerWindow) {
    if (openerWindow.opener) {
        return openerWindow.opener;
    } else {
        return openerWindow;
    }
};

$(function() {
    var $searchButton = $("button.search");
    $searchButton.one("click", function() {
        DenpyoKensaku.search(this);
    });
    var $successMsg = $("div.successMsg");
    if (0 < $successMsg.length) {
        setTimeout(function() {
            DenpyoKensaku.moveMain();
        }, 1000);
    } else {
        $("input[name='searchKeyword']").bind("blur", function(ev) {
            var $keyword = $(this);
            var keyword = $keyword.val();
            if (!keyword) {
                return;
            }
            keyword = keyword.replace(/[\s　]+/g, " ");
            $keyword.val(keyword);
        }).bind("keypress", function(ev) {
            if (ev.keyCode == 13) {
                $searchButton.triggerHandler("click");
            }
        });
        if (DenpyoKensaku.kakuteiProperty) {
            DenpyoKensaku.bindCheckbox();
        }
        DenpyoKensaku.bindOneShonin();
        DenpyoKensaku.bindOneBack();
        DenpyoKensaku.setDefaultMsg($searchButton);
    }

    $("textarea[name^='shoninComment']").bind("focus", function(ev) {
        $(this).parent("div.shoninCommentInputArea").css({
            "position": "relative",
            "top": 0,
            "left": 0
        });
        var fullwidth = $(window).width() - $(this).offset().left - 25;
        if (fullwidth < 0) {
            fullwidth = $(this).width();
        } else if (250 < fullwidth) {
            fullwidth = 250;
        }
        $(this).css({
            "height": "6em",
            "width": fullwidth
        }).removeClass("expandable").addClass("expanded");
    }).bind("blur", function(ev) {
        $(this).parent("div.shoninCommentInputArea").css({
            "position": "",
            "top": "",
            "left": ""
        });
        $(this).css({
            "height": "",
            "width": ""
        }).removeClass("expanded").addClass("expandable");
    });

    var tpl = "<div class=\"tooltipPanel\"></div>";
    $("div.shoninCommentList").each(function() {
        var tooltipContent = f.escapeTooltip($(this).next().val());
        if (!tooltipContent) {
            return;
        }
        $(this).data("tooltipContent", tooltipContent).bind("mouseenter", function(ev) {
            var $tooltip = $(tpl);
            var $content = $("<div class=\"tooltipPlainContent\"></div>");
            $content.html(tooltipContent);
            $tooltip.append($content);
            var top = ev.pageY;
            var left = ev.pageX;
            $("body").append($tooltip);
            $tooltip.css({
                "position" : "absolute",
                "top": (top + 10),
                "left": 0
            });
            var wrapperWidth = $("body").width();
            var tooltipLeft = left + 10;
            var tooltipWidth = $tooltip.width();
            if (wrapperWidth <= (tooltipLeft + tooltipWidth)) {
                tooltipLeft = wrapperWidth - tooltipWidth;
            }
            $tooltip.css("left", tooltipLeft);
            $(this).data("tooltipPanel", $tooltip);
        }).bind("mouseleave", function(ev){
            var $tooltip = $(this).data("tooltipPanel");
            if ($tooltip) {
                $tooltip.remove();
            }
        });
    });

    var $form = $("form:first");
    if (0 < $form.length) {
         $form.bind("submit", function(ev, loadingFlag) {
            if (typeof loadingFlag == "undefined") {
                loadingFlag = true;
            }
            var $input = $(this).find(":input");
            var $inputDisabled = $input.filter("[disabled]");
            f.removeDisabled($inputDisabled);
            var $kingakuList = $input.filter(".kingaku");
            for (var i = 0; i < $kingakuList.length; i++) {
                var $kingaku = $kingakuList.eq(i);
                var value = f.removeComma($kingaku.val());
                $kingaku.val(value);
            }
            if (!loadingFlag) {
                setTimeout(function() {
                    f.addDisabled($inputDisabled);
                    for (var i = 0; i < $kingakuList.length; i++) {
                        var $kingaku = $kingakuList.eq(i);
                        $kingaku.triggerHandler("blur.kingaku");
                    }
                }, 0);
            }
        });
    }

    if(DenpyoKensaku.focusFlag != 1) {
        f.focusFirstElement();
    }

    $(window).resize(function() {
        $("body").height($(window).height() - DenpyoKensaku.footerHeight);
    }).triggerHandler("resize");
});
