	function memoCanvasDefault2() {

		//定義・definiations
		const canvas2 = document.getElementById('paint_canvas_2_Alt1');
		const context2 = canvas2.getContext('2d');

		//Specifications
		context2.strokeStyle = "black";			//最初時ブラック化・initial brush color black;
		context2.lineWidth = 1;					//initial brush width;

		let drawing = false;
		let pathsry = [];
		let pathsryRedo = [];

		let points = [];

		var mouse = {x: 0, y: 0};
		var previous = {x: 0, y: 0};

		let status = [];

		//Mouse Down Event
		// mousedown
		// touchstart
		canvas2.addEventListener('mousedown', function(e){

			var currentCanvasID = e.currentTarget.id;
			// console.log(currentCanvasID);


			drawing = true;

			previous = {x:mouse.x, y:mouse.y};

			// ************************
			mouse = setMouseCoordinates(canvas2, e);
			// ************************


			points = [];
			points.push({x:mouse.x, y:mouse.y});


		});


		//Mouse Move Event
		// mousemove
		// touchmove
		canvas2.addEventListener('mousemove', function(e){

			if (drawing) {

				previous = {x:mouse.x, y:mouse.y};


				// ************************
				mouse = setMouseCoordinates(canvas2, e);
				// ************************

				// saving the points in the points array
				points.push({x:mouse.x, y:mouse.y})

				//start Drawing
				context2.beginPath();
				context2.moveTo(previous.x, previous.y);

				context2.lineTo(mouse.x, mouse.y);
				context2.stroke();
			}

		}, false);


		//Mouse Up Event
		// mouseup
		// touchend
		canvas2.addEventListener('mouseup', function(e){
			drawing = false;

			// Adding the path to the array or the paths
			pathsry.push(points);
			pathsryRedo.push(points);

			// // var text = $("#memo" + memoButton).text();
			// if (memoButton == "1") {

			// 	pathsry1.push(points);
			// 	pathsryRedo1.push(points);
			// 	// status1.push(data1);

			// } else if (memoButton == "2") {

			// 	pathsry2.push(points);
			// 	pathsryRedo2.push(points);
			// 	// status2.push(data1);

			// } else if (memoButton == "3") {

			// 	pathsry3.push(points);
			// 	pathsryRedo3.push(points);
			// 	// status3.push(data1);

			// } else if (memoButton == "4") {

			// 	pathsry4.push(points);
			// 	pathsryRedo4.push(points);
			// 	// status4.push(data1);

			// } else if (memoButton == "5") {

			// 	pathsry5.push(points);
			// 	pathsryRedo5.push(points);
			// 	// status5.push(data1);

			// } else if (memoButton == "6") {

			// 	pathsry6.push(points);
			// 	pathsryRedo6.push(points);
			// 	// status6.push(data1);

			// } else if (memoButton == "7") {

			// 	pathsry7.push(points);
			// 	pathsryRedo7.push(points);
			// 	// status7.push(data1);

			// } else if (memoButton == "8") {

			// 	pathsry8.push(points);
			// 	pathsryRedo8.push(points);
			// 	// status8.push(data1);

			// } else if (memoButton == "9") {

			// 	pathsry9.push(points);
			// 	pathsryRedo9.push(points);
			// 	// status9.push(data1);

			// } else if (memoButton == "10"){

			// 	pathsry10.push(points);
			// 	pathsryRedo10.push(points);
			// 	// status10.push(data1);

			// }

			var statusStrokeStyle = context2.strokeStyle;
			var statusLineWidth = context2.lineWidth;
			checkStroke(statusStrokeStyle, statusLineWidth);

		}, false);

		//**************************************************
		//*                                                *
		//*                                                *
		//**************************************************

		function checkStroke(statusStrokeStyle, statusLineWidth){

			var data1 = [];
			data1.push(statusStrokeStyle);
			data1.push(statusLineWidth);
			status.push(data1);

			// if (memoButton == "1") {

			// 	status1.push(data1);

			// } else if (memoButton == "2") {

			// 	status2.push(data1);

			// } else if (memoButton == "3") {

			// 	status3.push(data1);

			// } else if (memoButton == "4") {

			// 	status4.push(data1);

			// } else if (memoButton == "5") {

			// 	status5.push(data1);

			// } else if (memoButton == "6") {

			// 	status6.push(data1);

			// } else if (memoButton == "7") {

			// 	status7.push(data1);

			// } else if (memoButton == "8") {

			// 	status8.push(data1);

			// } else if (memoButton == "9") {

			// 	status9.push(data1);

			// } else if (memoButton == "10"){

			// 	status10.push(data1);

			// }

		}
		//**************************************************
		//*                                                *
		//*                                                *
		//**************************************************	
		//*******************************unDo****************************************
		//**************************************************
		//*                                                *
		//*                                                *
		//**************************************************	
		undo.addEventListener("click",Undo);
		function Undo(){

			var text = $("#memo" + memoButton).text();

			// if (memoButton == "1" && text == "メモ編集") {

			// 	pathsry = pathsry1;

			// } else if (memoButton == "2" && text == "メモ編集") {

			// 	pathsry = pathsry2;

			// } else if (memoButton == "3" && text == "メモ編集") {

			// 	pathsry = pathsry3;

			// } else if (memoButton == "4" && text == "メモ編集") {

			// 	pathsry = pathsry4;

			// } else if (memoButton == "5" && text == "メモ編集") {

			// 	pathsry = pathsry5;

			// } else if (memoButton == "6" && text == "メモ編集") {

			// 	pathsry = pathsry6;

			// } else if (memoButton == "7" && text == "メモ編集") {

			// 	pathsry = pathsry7;

			// } else if (memoButton == "8" && text == "メモ編集") {

			// 	pathsry = pathsry8;

			// } else if (memoButton == "9" && text == "メモ編集") {

			// 	pathsry = pathsry9;

			// } else if (memoButton == "10" && text == "メモ編集"){

			// 	pathsry = pathsry10;

			// }
			// remove the last path from the paths array
			pathsry.splice(-1,1);

			// draw all the paths in the paths array
			drawPaths();
		}
		//**************************************************
		//*                                                *
		//*                                                *
		//**************************************************		

		//**************************************************
		//*                                                *
		//*                                                *
		//**************************************************
		function drawPaths(){
			// delete everything
			context2.clearRect(0, 0, canvas2.width, canvas2.height);
			// draw all the paths in the paths array
			// alert(status);
			var h = 0;
			pathsry.forEach(path=>{
				context2.strokeStyle = status[h][0];
				context2.lineWidth = status[h][1];
				context2.beginPath();

				context2.moveTo(path[0].x, path[0].y);

				for(let i = 1; i < path.length; i++){

					context2.lineTo(path[i].x,path[i].y);
				};

				context2.stroke();
				h++;
				// debugger;

			});
		}

		//*******************************unDo****************************************
		//---------------------------------------------------------------------------
		//*******************************reDo****************************************
		//**************************************************
		//*                                                *
		//*                                                *
		//**************************************************	
		redo.addEventListener("click",Redo);
		function Redo(){
			drawPathsRedo();
		}

		function drawPathsRedo(){
			// delete everything
			context2.clearRect(0, 0, canvas2.width, canvas2.height);
			// draw all the paths in the paths array
			// alert(status);
			var h = 0;
			pathsryRedo.forEach(path=>{
				context2.strokeStyle = status[h][0];
				context2.lineWidth = status[h][1];
				context2.beginPath();

				context2.moveTo(path[0].x, path[0].y);

				for(let i = 1; i < path.length; i++){
					context2.lineTo(path[i].x,path[i].y); 
				};

				context2.stroke();
				h++;
				// debugger;

			});

		}
		//**************************************************
		//*                                                *
		//*                                                *
		//**************************************************
		//*******************************reDo****************************************
		//---------------------------------------------------------------------------
		//***************************************************************************
		// Handle Mouse Coordinates
		function setMouseCoordinates(canvas2, e) {
			var boundings = canvas2.getBoundingClientRect();

			//*********************************************************************************************************
			return {
				x : Math.round(event.clientX - boundings.left), y : Math.round(event.clientY - boundings.top)
				// x : Math.round(event.changedTouches[0].clientX - boundings.left), y : Math.round(event.changedTouches[0].clientY - boundings.top)
			}
			//*********************************************************************************************************

		}
		//***************************************************************************
		//---------------------------------------------------------------------------
		//***************************************************************************
		//クリア
		var clearButton = document.getElementById('clear2');

		clearButton.addEventListener('click', function(){
			context2.clearRect(0, 0, canvas2.width, canvas2.height);

			// if (memoButton == "1") {

			// 	pathsry1 = [], pathsryRedo1 = [], status1 = [];

			// } else if (memoButton == "2") {

			// 	pathsry2 = [], pathsryRedo2 = [], status2 = [];

			// } else if (memoButton == "3") {

			// 	pathsry3 = [], pathsryRedo3 = [], status3 = [];

			// } else if (memoButton == "4") {

			// 	pathsry4 = [], pathsryRedo4 = [], status4 = [];

			// } else if (memoButton == "5") {

			// 	pathsry5 = [], pathsryRedo5 = [], status5 = [];

			// } else if (memoButton == "6") {

			// 	pathsry6 = [], pathsryRedo6 = [], status6 = [];

			// } else if (memoButton == "7") {

			// 	pathsry7 = [], pathsryRedo7 = [], status7 = [];

			// } else if (memoButton == "8") {

			// 	pathsry8 = [], pathsryRedo8 = [], status8 = [];

			// } else if (memoButton == "9") {

			// 	pathsry9 = [], pathsryRedo9 = [], status9 = [];

			// } else if (memoButton == "10"){

			// 	pathsry10 = [], pathsryRedo10 = [], status10 = [];

			// }

		});

		//ペンカラ・Pen color black
		var blackPenButton = document.getElementById('blackPen2');
		blackPenButton.addEventListener('click', function(event){

			// alert(currentID2);
			const canvas2 = document.getElementById(currentID2);
			const context2 = canvas2.getContext('2d');

			context.strokeStyle = 'black';
			context.globalCompositeOperation = 'source-over';
		});

		//ペンカラ・Pen color red
		var redPenButton = document.getElementById('redPen2');
		redPenButton.addEventListener('click', function(event){

			// alert(currentID2);
			const canvas2 = document.getElementById(currentID2);
			const context2 = canvas2.getContext('2d');

			context2.strokeStyle = 'red';
			context2.globalCompositeOperation = 'source-over';
		});

		//ペンカラ・Pen color blue
		var bluePenButton = document.getElementById('bluePen2');
		bluePenButton.addEventListener('click', function(event){

			// alert(currentID2);
			const canvas2 = document.getElementById(currentID2);
			const context2 = canvas2.getContext('2d');

			context2.strokeStyle = 'blue';
			context2.globalCompositeOperation = 'source-over';
		});
	

		//ペンサイズ・dot size big
		var bigDotButton = document.getElementById('bigDot2');
		bigDotButton.addEventListener('click', function(event){

			const canvas2 = document.getElementById(currentID2);
			const context2 = canvas2.getContext('2d');

			context2.lineWidth = 5;
			context2.globalCompositeOperation = 'source-over';
		});

		//ペンサイズ・dot size big middle
		var middleDotButton = document.getElementById('middleDot2');
		middleDotButton.addEventListener('click', function(event){

			const canvas2 = document.getElementById(currentID2);
			const context2 = canvas2.getContext('2d');

			context2.lineWidth = 3;
			context2.globalCompositeOperation = 'source-over';
		});

		//ペンサイズ・dot size small
		var smallDotButton = document.getElementById('smallDot2');
		smallDotButton.addEventListener('click', function(event){

			const canvas2 = document.getElementById(currentID2);
			const context2 = canvas2.getContext('2d');

			context2.lineWidth = 1;
			context2.globalCompositeOperation = 'source-over';
		});

		//ペンサイズ・dot default
		var pencilButton = document.getElementById('pencil2');
		pencilButton.addEventListener('click', function(event){

			const canvas2 = document.getElementById(currentID2);
			const context2 = canvas2.getContext('2d');

			context2.lineWidth = 1;
			context2.globalCompositeOperation = 'source-over';
		});

		//消しゴム・Earser Big Size
		var eraserBigButton = document.getElementById('eraserBig2');
		eraserBigButton.addEventListener('click', function(event){

			const canvas2 = document.getElementById(currentID2);
			const context2 = canvas2.getContext('2d');

			context2.lineWidth = 7;
			context2.strokeStyle = 'white';
			// context.globalCompositeOperation = 'destination-out';
		});

		//消しゴム・Earser Middle Size
		var eraserMiddleButton = document.getElementById('eraserMiddle2');
		eraserMiddleButton.addEventListener('click', function(event){

			const canvas2 = document.getElementById(currentID2);
			const context2 = canvas2.getContext('2d');

			context2.lineWidth = 5;
			context2.strokeStyle = 'white';
			// context.globalCompositeOperation = 'destination-out';
		});

		//消しゴム・Earser Small Size
		var eraserSmallButton = document.getElementById('eraserSmall2');
		eraserSmallButton.addEventListener('click', function(event){

			const canvas2 = document.getElementById(currentID2);
			const context2 = canvas2.getContext('2d');

			context2.lineWidth = 3;
			context2.strokeStyle = 'white';
			// context.globalCompositeOperation = 'destination-out';
		});

		//消しゴム・Earser Default
		var eraserButton = document.getElementById('eraser2');
		eraserButton.addEventListener('click', function(event){

			const canvas2 = document.getElementById(currentID2);
			const context2 = canvas2.getContext('2d');

			context2.lineWidth = 3;
			context2.strokeStyle = 'white';
			// context.globalCompositeOperation = 'destination-out';
		});


		//***********************************addImagesToCanvas************************************		
		document.getElementById("fileUpload2").addEventListener("change", upLoad, false);
		function upLoad(e) {
			// alert(1);
			var img = new Image();
			var f = document.getElementById("fileUpload2").files[0];
			var url = window.URL || window.webkitURL;
			var src = url.createObjectURL(f);

			img.src = src;

			// if (memoButton == "1") {

			// 	f1 = f;

			// } else if (memoButton == "2") {

			// 	f2 = f;

			// } else if (memoButton == "3") {

			// 	f3 = f;

			// } else if (memoButton == "4") {

			// 	f4 = f;

			// } else if (memoButton == "5") {

			// 	f5 = f;

			// } else if (memoButton == "6") {

			// 	f6 = f;

			// } else if (memoButton == "7") {

			// 	f7 = f;

			// } else if (memoButton == "8") {

			// 	f8 = f;

			// } else if (memoButton == "9") {

			// 	f9 = f;

			// } else if (memoButton == "10"){

			// 	f10 = f;
			// }

			const canvas2 = document.getElementById(currentID2);
			const context2 = context2.getContext('2d');
			
			img.onload = function() {
				context2.drawImage(img, 0, 0);
				url.revokeObjectURL(src);
			}
		}
		//***********************************addImagesToCanvas2************************************	
	//--------------------------------------------------------------------------------------------
	}